import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject

import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile

import internal.GlobalVariable as GlobalVariable

import com.kms.katalon.core.annotation.BeforeTestCase
import com.kms.katalon.core.annotation.BeforeTestSuite
import com.kms.katalon.core.annotation.AfterTestCase
import com.kms.katalon.core.annotation.AfterTestSuite
import com.kms.katalon.core.context.TestCaseContext
import com.kms.katalon.core.context.TestSuiteContext

import java.io.File;
import java.io.FileWriter
import java.io.IOException;

class ErrorHandling {
	/**
	 * Executes after every test case ends.
	 * @param testCaseContext related information of the executed test case.
	 */
	@BeforeTestCase
	def sampleBeforeTestCase(TestCaseContext testCaseContext) {
	    println "testCaseContext.getTextCaseId()=${testCaseContext.getTestCaseId()}"
	    GlobalVariable.currentTestCaseId = testCaseContext.getTestCaseId()
	}
	
//	@AfterTestCase
//	def sampleAfterTestCase(TestCaseContext testCaseContext) {
//		Mobile.comment(GlobalVariable.reportStatus)
//		if (GlobalVariable.reportStatus != "success") {
//		Scanner sc = new Scanner(new File("customer.csv"))
//		String strNumber
//		while(sc.hasNext()) {
//			String filePerLine = sc.nextLine()
//			String[] data = filePerLine.split(",")
//			strNumber = data[0].trim ();
//		}
//		int number = 1
//		FileWriter writer = new FileWriter("customer.csv",true);
//		if (strNumber != null) {
//			number = Integer.parseInt(strNumber)
//			number = number + 1
//		}
//		writer.write(number + "," +"null" +","+"null"+","+"null"+","+"null"+","+"null"+","+"null"+","+"NOT OK"+","+GlobalVariable.currentTestCaseId);
//		writer.write("\n")
//		sc.close()
//		writer.close();
//		
//		while(Mobile.verifyElementNotVisible(findTestObject('Object Repository/Dashboard/Dashboard Main/Saldo Rekening Utama'), 3, FailureHandling.OPTIONAL) == true) {
//			Mobile.tap(findTestObject('Object Repository/Dashboard/Dashboard Main/Icon Back'), 0)
//			
//			if (Mobile.verifyElementVisible(findTestObject('Object Repository/common/android.widget.TextView - PIN'), 3, FailureHandling.OPTIONAL) == true) {
//				Mobile.tap(findTestObject('Object Repository/common/android.widget.ImageView back pin'), 3)
//			}
//			if (Mobile.verifyElementVisible(findTestObject('Object Repository/common/android.widget.TextView - Konfirmasi'), 3, FailureHandling.OPTIONAL) == true) {
//				Mobile.tap(findTestObject('Object Repository/common/android.widget.ImageButton back confirmasi'), 3)
//				if(Mobile.verifyElementVisible(findTestObject('Object Repository/common/android.widget.TextView - Pembatalan Transaksi'), 3, FailureHandling.OPTIONAL) == true) {
//					Mobile.tap(findTestObject('Object Repository/common/android.widget.Button - Ya'), 0)
//				}
//			}
//		}
//		}
//		GlobalVariable.reportStatus = "gagal"
//	}
}