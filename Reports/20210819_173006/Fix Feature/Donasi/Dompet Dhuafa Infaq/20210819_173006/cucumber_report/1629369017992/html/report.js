$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("C:/Users/LENOVO/Documents/Brimo/bitbucket/brimo-katalon/Include/features/Donasi/Donasi.feature");
formatter.feature({
  "name": "Title of your feature",
  "description": "  I want to use this template for my feature file",
  "keyword": "Feature",
  "tags": [
    {
      "name": "@tag"
    }
  ]
});
formatter.scenarioOutline({
  "name": "Title of your scenario outline",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@Dompet_Dhuafa_Infaq"
    }
  ]
});
formatter.step({
  "name": "I open brimo application with username and password with \u003ccases\u003e",
  "keyword": "Given "
});
formatter.step({
  "name": "Open menu lainnya",
  "keyword": "When "
});
formatter.step({
  "name": "Open menu donasi",
  "keyword": "When "
});
formatter.step({
  "name": "Open pilih produk donasi",
  "keyword": "When "
});
formatter.step({
  "name": "Select produk donasi",
  "keyword": "When "
});
formatter.step({
  "name": "Open pilih jenis donasi",
  "keyword": "When "
});
formatter.step({
  "name": "Select jenis donasi",
  "keyword": "When "
});
formatter.step({
  "name": "Open select amount",
  "keyword": "When "
});
formatter.step({
  "name": "Select amount",
  "keyword": "When "
});
formatter.step({
  "name": "Confirmation my paying",
  "keyword": "When "
});
formatter.step({
  "name": "I input correct PIN",
  "keyword": "Then "
});
formatter.step({
  "name": "Transaction success \u003cfeature\u003e",
  "keyword": "Then "
});
formatter.step({
  "name": "Close application",
  "keyword": "Then "
});
formatter.examples({
  "name": "",
  "description": "",
  "keyword": "Examples",
  "rows": [
    {
      "cells": [
        "cases",
        "feature"
      ]
    },
    {
      "cells": [
        "2",
        "Donasi"
      ]
    }
  ]
});
formatter.scenario({
  "name": "Title of your scenario outline",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@tag"
    },
    {
      "name": "@Dompet_Dhuafa_Infaq"
    }
  ]
});
formatter.step({
  "name": "I open brimo application with username and password with 2",
  "keyword": "Given "
});
formatter.match({
  "location": "common.openBrimoAndDataFromExcel(int)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Open menu lainnya",
  "keyword": "When "
});
formatter.match({
  "location": "common.openMenuLainnya()"
});
