@tag
Feature: Title of your feature
  I want to use this template for my feature file

   @AsuransiBRIns
  Scenario Outline: Pembayaran Asuransi Prudential Premi Pertama
    #Given I open brimo application with username and password with <cases>
    And Checking flow <cases>
    When Open Asuransi Page and choose asuransi BRIns
    When Choose the type of insurance
    When Click payment on confirmation page
    Then I input correct PIN
    Then Transaction success <feature>
    #When Transaksi Berhasil user klik OK
    #Then Tutup aplikasi
    Examples:
     |cases       |
     |1					|
     
     
     @AsuransiPrudentialPremiPertama
  Scenario Outline: Pembayaran Asuransi Prudential Premi Pertama
    #Given I open brimo application with username and password with <cases>
    And Checking flow <cases>
    When Open Asuransi Page and choose asuransi BRIns
    When Choose the type of insurance
    When Click payment on confirmation page
    Then I input correct PIN
    Then Transaction success <feature>
    #When Transaksi Berhasil user klik OK
    #Then Tutup aplikasi
    Examples:
     |cases       |
     |2					  |
     
     
     @AsuransiPrudentialPremiLanjutan
  Scenario Outline: Pembayaran Asuransi Prudential Premi Pertama
    #Given I open brimo application with username and password with <cases>
    And Checking flow <cases>
    When Open Asuransi Page and choose asuransi BRIns
    When Choose the type of insurance
    When Click payment on confirmation page
    Then I input correct PIN
    Then Transaction success <feature>
    #When Transaksi Berhasil user klik OK
    #Then Tutup aplikasi
    Examples:
     |cases       |
     |3					  |
     
     
     @AsuransiPrudentialTopup
  Scenario Outline: Pembayaran Asuransi Prudential Premi Pertama
    #Given I open brimo application with username and password with <cases>
    And Checking flow <cases>
    When Open Asuransi Page and choose asuransi BRIns
    When Choose the type of insurance
    When Click payment on confirmation page
    Then I input correct PIN
    Then Transaction success <feature>
    #When Transaksi Berhasil user klik OK
    #Then Tutup aplikasi
    Examples:
     |cases       |
     |4				   	|
     
     
     
    @AsuransiPrudentialBiayaCetakUlang
  Scenario Outline: Pembayaran Asuransi Prudential Premi Pertama
    #Given I open brimo application with username and password with <cases>
    And Checking flow <cases>
    When Open Asuransi Page and choose asuransi BRIns
    When Choose the type of insurance
    When Click payment on confirmation page
    Then I input correct PIN
    Then Transaction success <feature>
    #When Transaksi Berhasil user klik OK
    #Then Tutup aplikasi
    Examples:
     |cases       |
     |5					|
     
     
     @AsuransiPrudentialBiayaPerubahan
  Scenario Outline: Pembayaran Asuransi Prudential Premi Pertama
    #Given I open brimo application with username and password with <cases>
    And Checking flow <cases>
    When Open Asuransi Page and choose asuransi BRIns
    When Choose the type of insurance
    When Click payment on confirmation page
    Then I input correct PIN
    Then Transaction success <feature>
    #When Transaksi Berhasil user klik OK
    #Then Tutup aplikasi
    Examples:
     |cases       |
     |6					  |
     
     
     @AsuransiPrudentialBiayaCetakKartu
  Scenario Outline: Pembayaran Asuransi Prudential Premi Pertama
    #Given I open brimo application with username and password with <cases>
    And Checking flow <cases>
    When Open Asuransi Page and choose asuransi BRIns
    When Choose the type of insurance
    When Click payment on confirmation page
    Then I input correct PIN
    Then Transaction success <feature>
    #When Transaksi Berhasil user klik OK
    #Then Tutup aplikasi
    Examples:
     |cases       |
     |7 					|