#Author: your.email@your.domain.com
#Keywords Summary :
#Feature: List of scenarios.
#Scenario: Business rule through list of steps with arguments.
#Given: Some precondition step
#When: Some key actions
#Then: To observe outcomes or validation
#And,But: To enumerate more Given,When,Then steps
#Scenario Outline: List of steps for data-driven as an Examples and <placeholder>
#Examples: Container for s table
#Background: List of steps run before each of the scenarios
#""" (Doc Strings)
#| (Data Tables)
#@ (Tags/Labels):To group Scenarios
#<> (placeholder)
#""
## (Comments)
#Sample Feature Definition Template
@tag
Feature: Title of your feature
  I want to purchase pulsa using brimo app

 @Purchase_Pulsa_M3
  Scenario Outline: User Purchase pulsa M3
     Given I open brimo application with username and password with <cases>
     #And Checking flow <cases>
     When Open Dashboard page
     When User Input Manual Number
     #When User Verify Operator Number
     When User Choose Amount
     When User Choose Rekening for payment
     When User confrm payment detail
     Then I input correct PIN
     #Then The screen displays payment receipt
     #When user confirms the contents of the payment receipt
     Then Transaction success <feature>
     When Close Application

     Examples: 
      | cases | feature | 
      | 1 | Pulsa |

 @Purchase_Pulsa_Telkomsel
  Scenario Outline: User Purchase pulsa Telkomsel
     Given I open brimo application with username and password with <cases>
     #And Checking flow <cases>
     When Open Dashboard page
     When User Input Manual Number
     #When User Verify Operator Number
     When User Choose Amount
     When User Choose Rekening for payment
     When User confrm payment detail
     Then I input correct PIN
     #Then The screen displays payment receipt
     Then Transaction success <feature>
     When Close Application

     Examples: 
      | cases | feature | 
      | 2 | Pulsa |
      
@Purchase_Pulsa_3
  Scenario Outline: User Purchase pulsa 3
     Given I open brimo application with username and password with <cases>
     #And Checking flow <cases>
     When Open Dashboard page
     When User Input Manual Number
     #When User Verify Operator Number
     When User Choose Amount
     When User Choose Rekening for payment
     When User confrm payment detail
     Then I input correct PIN
     #Then The screen displays payment receipt
     #When user confirms the contents of the payment receipt
     Then Transaction success <feature>
     When Close Application

     Examples: 
      | cases | feature | 
      | 3 | Pulsa |

@Purchase_Pulsa_XL
  Scenario Outline: User Purchase pulsa XL
     Given I open brimo application with username and password with <cases>
     #And Checking flow <cases>
     When Open Dashboard page
     When User Input Manual Number
     #When User Verify Operator Number
     When User Choose Amount
     When User Choose Rekening for payment
     When User confrm payment detail
     Then I input correct PIN
     #Then The screen displays payment receipt
     #When user confirms the contents of the payment receipt
     Then Transaction success <feature>
     When Close Application

     Examples: 
      | cases | feature | 
      | 4 | Pulsa |
      
@Purchase_Pulsa_Smartfren
  Scenario Outline: User Purchase pulsa Smartfren
     Given I open brimo application with username and password with <cases>
     #And Checking flow <cases>
     When Open Dashboard page
     When User Input Manual Number
     #When User Verify Operator Number
     When User Choose Amount
     When User Choose Rekening for payment
     When User confrm payment detail
     Then I input correct PIN
     #Then The screen displays payment receipt
     #When user confirms the contents of the payment receipt
     Then Transaction success <feature>
     When Close Application

     Examples: 
      | cases | feature | 
      | 5 | Pulsa |

@Purchase_Pulsa_Axis
  Scenario Outline: User Purchase pulsa Smartfren
     Given I open brimo application with username and password with <cases>
     #And Checking flow <cases>
     When Open Dashboard page
     When User Input Manual Number
     #When User Verify Operator Number
     When User Choose Amount
     When User Choose Rekening for payment
     When User confrm payment detail
     Then I input correct PIN
     #Then The screen displays payment receipt
     #When user confirms the contents of the payment receipt
     Then Transaction success <feature>
     When Close Application

     Examples: 
      | cases | feature | 
      | 6 | Pulsa |
