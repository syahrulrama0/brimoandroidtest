
@tag
Feature: Feature Dompet Digital (OVO)
  Fitur ini untuk halaman Dompet Digital (OVO)
  @tag23
  Scenario Outline: User tidak menyimpan nomor tujuan
    Given I open brimo application with username <username> and password <password>
    When Pada halaman Home klik icon Dompet Digital
    When Klik Top Up Baru
    When User akan pilih Dompet Digital <dompetDigital> dan <gopayType>
    When User input Nomor Handphone tujuan <phoneNumber>
    When User klik Lanjutkan
    And User input Nominal <nominal> untuk Top Up
    #When User melihat detail Sumber Dana <sumberDana>
    #Then User klik Top Up pada halaman Konfirmasi
    #When Input <condition> PIN <number>
    Then I input correct PIN <pinNumber>
    #Then Transaction success
    When Close application

    #Then Tutup aplikasi
    Examples: 
      | username      | password | dompetDigital | gopayType | phoneNumber  | nominal | sumberDana    | condition | pinNumber |
      | Fegaeka20031994 | M4ret1994 | OVO       		|null			| 087883697742 |   21111 | rekeningUtama | correct   | 281094 |
      #| syahrulrama0 | 31121997Rama | LinkAja       |null			| 089657642899 |   10000 | rekeningUtama | correct   | 123457 |
      #| herlindarosa00 | Tehoca00 | GoPay         | customer  | 089657642899 |   21000 | rekeningUtama | correct   | 123457 |
       #| syahrulrama0  | 31121997Rama | ShopeePay     |null			| 089657642899 |   21000 | rekeningUtama | correct   | 200031 |
     
