#Author: herlindarosa77@gmail.com
#Keywords Summary :
#Feature: List of scenarios.
#Scenario: Business rule through list of steps with arguments.
#Given: Some precondition step
#When: Some key actions
#Then: To observe outcomes or validation
#And,But: To enumerate more Given,When,Then steps
#Scenario Outline: List of steps for data-driven as an Examples and <placeholder>
#Examples: Container for s table
#Background: List of steps run before each of the scenarios
#""" (Doc Strings)
#| (Data Tables)
#@ (Tags/Labels):To group Scenarios
#<> (placeholder)
#""
## (Comments)
#Sample Feature Definition Template
@tag
Feature: Feature Dompet Digital (LinkAja)
  Fitur ini untuk halaman Dompet Digital (LinkAja)

  @tag23
  Scenario Outline: User tidak menyimpan nomor tujuan
    Given I open brimo application with username <username> and password <password>
    When Pada halaman Home klik icon Dompet Digital
    When Klik Top Up Baru
    When User akan pilih Dompet Digital <dompetDigital> dan <gopayType>
    When User input Nomor Handphone tujuan <phoneNumber>
    When User klik Lanjutkan
    And User input Nominal <nominal> untuk Top Up
    #When User melihat detail Sumber Dana <sumberDana>
    #Then User klik Top Up pada halaman Konfirmasi
    Then I input correct PIN <pinNumber>
    #Then Transaction success
    Then Close application


    #Then User klik OK pada halaman Transaksi Berhasil
    #Then Tutup aplikasi
    Examples: 
      | username       | password | dompetDigital | gopayType | phoneNumber  | nominal | sumberDana    | condition | pinNumber |
      | Fegaeka20031994 | M4ret1994 | LinkAja       | null      | 085559547821 |   10111 | rekeningUtama | correct   | 281094 |
