#Author: herlindarosa77@gmail.com
#Keywords Summary :
#Feature: List of scenarios.
#Scenario: Business rule through list of steps with arguments.
#Given: Some precondition step
#When: Some key actions
#Then: To observe outcomes or validation
#And,But: To enumerate more Given,When,Then steps
#Scenario Outline: List of steps for data-driven as an Examples and <placeholder>
#Examples: Container for s table
#Background: List of steps run before each of the scenarios
#""" (Doc Strings)
#| (Data Tables)
#@ (Tags/Labels):To group Scenarios
#<> (placeholder)
#""
## (Comments)
#Sample Feature Definition Template
@tag
Feature: Fitur Dompet Digital (OVO)
  Fitur ini untuk halaman Dompet Digital (OVO)

  #TC101
  @tag1
  Scenario Outline: User dapat mengakses Dompet Digital dengan cara tidak Login
    Given Buka halaman Fast Menu
    When Pilih menu Dompet Digital

    Examples: 
      | username   | password   |
      | brimosv003 | Jakarta123 |

  #TC102
  @tag2
  Scenario Outline: User dapat melihat Saldo Rekening Utama
    Given User Login masukan <username> dan <password>
    When Pada halaman Home klik icon mata untuk melihat Saldo Rekening Utama

    Examples: 
      | username   | password   |
      | brimosv003 | Jakarta123 |

  #TC103
  @tag3
  Scenario Outline: User dapat mengakses Dompet Digital dengan cara Login terlebih dahulu
    Given User Login masukan <username> dan <password>
    When Pada halaman Home klik icon Dompet Digital
    When Diarahkan ke halaman Dompet Digital

  #Examples:
  #| username   | password 	|
  #| brimosv003 | Jakarta123	|
  #
  #TC201 - TC207 dan TC412
  @tag4
  Scenario Outline: User dapat mencari Daftar Dompet Digital
    Given User Login masukan <username> dan <password>
    When Buka halaman Dompet Digital dengan <kontak> , <history> dan <topUpVia>
    When Buka halaman Nominal dengan <amount> , <status> dan <alias>
    When Buka halaman Konfirmasi
    When Masukan <condition> PIN <number>
    Then Transaksi Berhasil

    Examples: 
      | username   | password   | topUpVia     | amount | status  | condition | number | alias |
      | brimosv003 | Jakarta123 | searchkontak |  21000 | success | correct   | 191919 | null  |

  #topUpVia : searchkontak
  #condition : correct
  #TC411 top up by transaksi terakhir
  @tag5
  Scenario Outline: User dapat Top Up OVO berdasarkan data transaksi Top Up Terakhir yang pernah di lakukan
    Given User Login masukan <username> dan <password>
    When Buka halaman Dompet Digital dengan <kontak> , <history> dan <topUpVia>
    When Buka halaman Nominal dengan <amount> , <status> dan <alias>
    When Buka halaman Konfirmasi
    When Masukan <condition> PIN <number>
    Then Transaksi Berhasil

    Examples: 
      | username   | password   | contact | topUpVia | amount | status  | alias | condition | number | transaction |
      | brimosv003 | Jakarta123 | yes     | history  |  25000 | success | null  | correct   | 191919 | no          |

  #rosa topUpVia : Search Kontak, Top Up Terakhir, Daftar Dompet Digital,
  #topUpVia : searchkontak, topupbaru, history dan kontak
  #condition : correct dan incorrect
  #TC304, TC305, TC315, TC318 dan TC413, TC414 TOP UP BARU
  @tag6
  Scenario Outline: Top Up Via Top Up Baru
    Given User Login masukan <username> dan <password>
    When Pada halaman Home klik icon Dompet Digital
    When User klik Top Up Baru
    When Buka halaman Top Up Baru dan input Nomor Tujuan <phoneNumber>
    When Buka halaman Nominal dengan <amount> , <status> dan <alias>
    When Buka halaman Konfirmasi
    When Masukan <condition> PIN <number>
    Then Transaksi Berhasil

    Examples: 
      | username   | password   | amount | status  | alias | condition | number | phoneNumber |
      | brimosv003 | Jakarta123 |  20000 | success | null  | correct   | 191919 | 08008008001 |

  #TC301 TOP UP BARU - User tidak memilih Dompet Digital dan Nomor Handphone
  @tag7
  Scenario Outline: User tidak memilih Dompet Digital dan Nomor Handphone
    Given User Login masukan <username> dan <password>
    When Pada halaman Home klik icon Dompet Digital
    When User klik Top Up Baru
    When User tidak memilih Dompet Digital dan Nomor Handphone

    Examples: 
      | username   | password   |
      | brimosv003 | Jakarta123 |

  #TC302 TOP UP BARU - User tidak memilih Dompet Digital tetapi user input Nomor Handphone
  @tag8
  Scenario Outline: User tidak memilih Dompet Digital tetapi user input Nomor Handphone
    Given User Login masukan <username> dan <password>
    When Pada halaman Home klik icon Dompet Digital
    When User klik Top Up Baru
    When User tidak memilih Dompet Digital tetapi user input Nomor Handphone <phoneNumber>

    Examples: 
      | username   | password   | phoneNumber |
      | brimosv003 | Jakarta123 | 08888888888 |

  #TC303 TOP UP BARU - User memilih Dompet Digital, lalu user pilih OVO dan user tidak input Nomor Handphone
  @tag9
  Scenario Outline: User memilih Dompet Digital, lalu user pilih OVO dan user tidak input Nomor Handphone
    Given User Login masukan <username> dan <password>
    When Pada halaman Home klik icon Dompet Digital
    When User klik Top Up Baru
    When User pilih OVO dan user tidak input Nomor Handphone

    Examples: 
      | username   | password   |
      | brimosv003 | Jakarta123 |

  #TC308 TOP UP BARU - User input Nomor Handphone di bawah 13 angka
  @tag10
  Scenario Outline: User input Nomor Handphone di bawah 13 angka
    Given User Login masukan <username> dan <password>
    When Pada halaman Home klik icon Dompet Digital
    When User klik Top Up Baru
    When Buka halaman Top Up Baru dan input Nomor Tujuan <phoneNumber>

    Examples: 
      | username   | password   | phoneNumber |
      | brimosv003 | Jakarta123 |     0812186 |

  #TC309 TOP UP BARU - User input Nomor Handphone lebih dari 13 angka
  @tag11
  Scenario Outline: User input Nomor Handphone di bawah 13 angka
    Given User Login masukan <username> dan <password>
    When Pada halaman Home klik icon Dompet Digital
    When User klik Top Up Baru
    When Buka halaman Top Up Baru dan input Nomor Tujuan <phoneNumber>

    Examples: 
      | username   | password   | phoneNumber      |
      | brimosv003 | Jakarta123 | 0812345678910111 |

  #TC310 TOP UP BARU - User input Nomor Handphone yang tidak valid
  @tag12
  Scenario Outline: User input Nomor Handphone yang tidak valid
    Given User Login masukan <username> dan <password>
    When Pada halaman Home klik icon Dompet Digital
    When User klik Top Up Baru
    When Buka halaman Top Up Baru dan input Nomor Tujuan <phoneNumber>

    Examples: 
      | username   | password   | phoneNumber   |
      | brimosv003 | Jakarta123 | 0690000000000 |

  #TC311 TOP UP BARU - User salah input nomor handphone
  @tag13
  Scenario Outline: User salah input nomor handphone
    Given User Login masukan <username> dan <password>
    When Pada halaman Home klik icon Dompet Digital
    When User klik Top Up Baru
    When Buka halaman Top Up Baru dan input Nomor Tujuan <phoneNumber>

    Examples: 
      | username   | password   | phoneNumber  |
      | brimosv003 | Jakarta123 | 081823456789 |

  #TC316 TOP UP BARU - User input nominal top up OVO di bawah 20.000
  @tag14
  Scenario Outline: User input nominal top up OVO di bawah 20.000
    Given User Login masukan <username> dan <password>
    When Pada halaman Home klik icon Dompet Digital
    When User klik Top Up Baru
    When Buka halaman Top Up Baru dan input Nomor Tujuan <phoneNumber>
    When Input <nominal> untuk Top Up

    Examples: 
      | username   | password   | phoneNumber | nominal |
      | brimosv003 | Jakarta123 | 08008008001 |   12000 |

  #TC317 TOP UP BARU - User input nominal untuk top up melebihi jumlah saldo
  @tag15
  Scenario Outline: User input nominal untuk top up melebihi jumlah saldo
    Given User Login masukan <username> dan <password>
    When Pada halaman Home klik icon Dompet Digital
    When User klik Top Up Baru
    When Buka halaman Top Up Baru dan input Nomor Tujuan <phoneNumber>
    When Input <nominal> untuk Top Up

    Examples: 
      | username   | password   | phoneNumber | nominal      |
      | brimosv003 | Jakarta123 | 08008008001 | 120000000000 |

  #TC319 TOP UP BARU - User tidak input nominal top up
  @tag16
  Scenario Outline: User tidak input nominal top up
    Given User Login masukan <username> dan <password>
    When Pada halaman Home klik icon Dompet Digital
    When User klik Top Up Baru
    When Buka halaman Top Up Baru dan input Nomor Tujuan <phoneNumber>
    When Tidak Input <nominal> untuk Top Up

    Examples: 
      | username   | password   | phoneNumber |
      | brimosv003 | Jakarta123 | 08008008001 |

  #TC208 TOP UP BARU - User dapat melihat action Hapus dari daftar, Edit, Jadikan Favorit
  @tag17
  Scenario Outline: User dapat melihat action Hapus dari daftar, Edit, Jadikan Favorit
    Given User Login masukan <username> dan <password>
    When Pada halaman Home klik icon Dompet Digital
    When User klik icon three dots

    Examples: 
      | username   | password   |
      | brimosv003 | Jakarta123 |

  #TC209 TOP UP BARU - User dapat Hapus Daftar Dompet Digital
  @tag18
  Scenario Outline: User dapat melihat action Hapus dari daftar, Edit, Jadikan Favorit
    Given User Login masukan <username> dan <password>
    When Pada halaman Home klik icon Dompet Digital
    When User klik icon three dots
    When Pilih Hapus dari Daftar

    Examples: 
      | username   | password   |
      | brimosv003 | Jakarta123 |

  #TC210 TOP UP BARU - User dapat Edit dan Update Daftar Dompet Digital
  @tag19
  Scenario Outline: User dapat Edit dan Update Daftar Dompet Digital
    Given User Login masukan <username> dan <password>
    When Pada halaman Home klik icon Dompet Digital
    When User klik icon three dots
    When Pilih Edit <simpanSebagai> lalu Simpan

    Examples: 
      | username   | password   | simpanSebagai   |
      | brimosv003 | Jakarta123 | Rosa Ocha Linda |

  #TC211 TOP UP BARU - User dapat menjadikan Favorit di Daftar Dompet Digital
  @tag20
  Scenario Outline: User dapat menjadikan Favorit di Daftar Dompet Digital
    Given User Login masukan <username> dan <password>
    When Pada halaman Home klik icon Dompet Digital
    When User klik icon three dots
    When Pilih Jadikan Favorit

    Examples: 
      | username   | password   |
      | brimosv003 | Jakarta123 |

  #TC212 TOP UP BARU - User dapat Hapus Favorit dari Daftar Dompet Digital
  @tag21
  Scenario Outline: User dapat Hapus Favorit dari Daftar Dompet Digital
    Given User Login masukan <username> dan <password>
    When Pada halaman Home klik icon Dompet Digital
    When User klik icon three dots
    When Pilih Hapus dari Favorit

    Examples: 
      | username   | password   |
      | brimosv003 | Jakarta123 |

  #TC306 TOP UP BARU - User dapat input Nomor Handphone otomatis dari Contact
  @tag22
  Scenario Outline: User dapat input Nomor Handphone otomatis dari Contact
    Given User Login masukan <username> dan <password>
    When Pada halaman Home klik icon Dompet Digital
    When User klik Top Up Baru
    When Pilih Dompet Digital OVO
    When User klik icon Contact pilih salah satu Contact akun OVO yang akan di top up <inputContact>
    When Buka halaman Nominal dengan <amount> , <status> dan <alias>
    When Buka halaman Konfirmasi
    When Masukan <condition> PIN <number>
    Then Transaksi Berhasil

    Examples: 
      | username   | password   | amount | status  | alias | condition | number | phoneNumber | inputContact   |
      | brimosv003 | Jakarta123 |  20000 | success | null  | correct   | 191919 | 08008008001 | OVO - HEROSA 1 |

  #TC307 TOP UP BARU - Jika sudah input Nomor Handphone otomatis dari Contact User dapat edit
  @tag23
  Scenario Outline: Jika sudah input Nomor Handphone otomatis dari Contact User dapat edit
    Given User Login masukan <username> dan <password>
    When Pada halaman Home klik icon Dompet Digital
    When User klik Top Up Baru
    When Pilih Dompet Digital OVO
    When User klik icon Contact pilih salah satu Contact akun OVO yang akan di top up, <inputContact>
    When User dapat edit <editContact> Nomor Handphone
    When Buka halaman Nominal dengan <amount> , <status> dan <alias>
    When Buka halaman Konfirmasi
    When Masukan <condition> PIN <number>
    Then Transaksi Berhasil

    Examples: 
      | username   | password   | amount | status  | alias | condition | number | phoneNumber | inputContact   | editContact |
      | brimosv003 | Jakarta123 |  20000 | success | null  | correct   | 191919 | 08008008001 | OVO - HEROSA 1 | 08008008002 |

  #TC502
  @tag24
  Scenario Outline: User melakukan proses Top Up OVO terlalu lama
    Given User Login masukan <username> dan <password>
    When Pada halaman Home klik icon Dompet Digital
    When User klik Top Up Baru
    When Buka halaman Top Up Baru dan input Nomor Tujuan <phoneNumber>
    When Buka halaman Nominal dengan <amount> , <status> dan <alias>
    When User tidak melalukan aksi selama 5 menit
    When Buka halaman Konfirmasi
    When Masukan <condition> PIN <number>
    Then Transaksi Berhasil

    Examples: 
      | username   | password   | amount | status  | alias | condition | number | phoneNumber |
      | brimosv003 | Jakarta123 |  20000 | success | null  | correct   | 191919 | 08008008001 |

  #TC501
  @tag25
  Scenario Outline: User salah input PIN
    Given User Login masukan <username> dan <password>
    When Pada halaman Home klik icon Dompet Digital
    When User klik Top Up Baru
    When Buka halaman Top Up Baru dan input Nomor Tujuan <phoneNumber>
    When Buka halaman Nominal dengan simpan Nomor Tujuan <amount> , <status> dan <alias>
    When Buka halaman Konfirmasi
    When Masukan <condition> PIN <number>
    Then Transaksi Berhasil

    Examples: 
      | username   | password   | amount | status  | alias | condition | number | phoneNumber |
      | brimosv003 | Jakarta123 |  20000 | success | ocha  | correct   | 191919 | 08008008001 |

  #TC314
  #@tag26
  #Scenario Outline: User dapat menyimpan nomor tujuan tapi tidak input nama untuk di Simpan Sebagai
  #Given User Login masukan <username> dan <password>
  #When Pada halaman Home klik icon Dompet Digital
  #When User klik Top Up Baru
  #When Buka halaman Top Up Baru dan input Nomor Tujuan <phoneNumber>
  #When Buka halaman Nominal dengan simpan Nomor Tujuan <amount> dan <alias>
  #When Buka halaman Konfirmasi
  #When Masukan <condition> PIN <number>
  #Then Transaksi Berhasil
  #
  #Examples:
  #| username   | password   | amount | status  | alias | condition | number | phoneNumber |
  #| brimosv003 | Jakarta123 |  20000 | success | ocha  | correct   | 191919 | 08008008001 |
  
  
  #Sumber dana
  #TC401
  @tag27
  Scenario Outline: User dapat menyimpan nomor tujuan tapi tidak input nama untuk di Simpan Sebagai
    Given User Login masukan <username> dan <password>
    When Pada halaman Home
		When User klik button icon Dompet Digital
    When Pada halaman Home klik icon Dompet Digital
    When User klik Top Up Baru
    When Buka halaman Top Up Baru dan input Nomor Tujuan <phoneNumber>
    When Buka halaman Nominal dengan <amount> , <status> dan <alias>

    Examples: 
      | username   | password   | amount | status  | alias | condition | number | phoneNumber |
      | brimosv003 | Jakarta123 |  20000 | success | ocha  | correct   | 191919 | 08008008001 |
