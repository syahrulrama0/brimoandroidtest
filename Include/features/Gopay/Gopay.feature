
@tag
Feature: Title of your feature
  I want to use this template for my feature file

  @tag23
  Scenario Outline: User tidak menyimpan nomor tujuan
    Given I open brimo application with username <username> and password <password>
    When Pada halaman Home klik icon Dompet Digital
    When Klik Top Up Baru
    When User akan pilih Dompet Digital <dompetDigital> dan <gopayType>
    When User input Nomor Handphone tujuan <phoneNumber>
    When User klik Lanjutkan
    And User input Nominal <nominal> untuk Top Up
    #When User melihat detail Sumber Dana <sumberDana>
    Then I input correct PIN <pinNumber>
    #Then Transaction success
    Then Tutup aplikasi

    #Then User klik OK pada halaman Transaksi Berhasil
    #Then Tutup aplikasi
    Examples: 
      | username       | password | dompetDigital | gopayType | phoneNumber  | nominal | sumberDana    | condition | pinNumber |
      | Fegaeka20031994 | M4ret1994 | GoPay         | customer  | 081322277343 |   24111 | rekeningUtama | correct   | 281094 |
