@tag
Feature: Bayar Tiket KAI
  User dapat membayar Tiket KAI

  #TC101 User dapat Bayar Tiker KAI Sumber Dana Rekening Utama
  @tag1
  Scenario Outline: User dapat Bayar Tiker KAI
    Given I open brimo application with username and password with <cases>
    And Checking flow <cases>
    When Pada halaman Home klik Lainnya
    When Klik KAI di Fitur Lainnya
    When Input Nomor KAI
    When User pilih kondisi pembayaran KAI
    When User di arahkan ke halaman Konfirmasi klik Bayar
    Then I input correct PIN
    When Transaction success <feature>
    Then Tutup aplikasi

    Examples: 
      | cases | feature |
      | 1 | KAI |
