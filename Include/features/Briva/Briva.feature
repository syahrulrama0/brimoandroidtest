#Author: your.email@your.domain.com
#Keywords Summary :
#Feature: List of scenarios.
#Scenario: Business rule through list of steps with arguments.
#Given: Some precondition step
#When: Some key actions
#Then: To observe outcomes or validation
#And,But: To enumerate more Given,When,Then steps
#Scenario Outline: List of steps for data-driven as an Examples and <placeholder>
#Examples: Container for s table
#Background: List of steps run before each of the scenarios
#""" (Doc Strings)
#| (Data Tables)
#@ (Tags/Labels):To group Scenarios
#<> (placeholder)
#""
## (Comments)
#Sample Feature Definition Template
@tag
Feature: Title of your feature
  I want to use this template for my feature file

  #@tag1
  #Scenario Outline: Title of your scenario outline
    #Given I open an application with username <username> and password <password>
    #When I Open BRIVA Menu
    #When I Open Menu Pembayaran Baru
    #Then I Input Destination Number on Pembayaran Baru Menu <brivaNumber>
    #When I Open Nominal Briva Page with amount <amount> and save the name with <saveName>
    #When I Open Sumber Dana List on Briva
    #And Select sumber dana positive
    #When I Open Konfirmasi Page
    #Then Close application
#
    #Examples: 
      #| username  | password | brivaNumber | amount | saveName |
      #| syahrulrama0 | 31121997Rama | 1120109800798861 | 2 | null |
      
  @Success_Pay_Briva_Tagihan_Via_Pembayaran_Baru
  Scenario Outline: Pay BRIVA Via Pembayaran Baru
    Given I open brimo application with username and password with <cases>
    #And Checking flow <cases>
    When I Open BRIVA Menu
    When I Open Menu Pembayaran Baru
    Then I Input Destination Number on Pembayaran Baru Menu
    When I Open Tagihan Page and want to save the contact
    #And Select sumber dana positive
    When I Open Konfirmasi Tagihan Page
    Then I input correct PIN
    Then Transaction success <feature>
    When Close application

    Examples: 
      | cases | feature |
    	| 1 | Briva |