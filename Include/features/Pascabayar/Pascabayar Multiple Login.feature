@tag
Feature: Feature Pascabayar
  Fitur ini untuk membayar pascabayar kartu Halo

  #TC101
  @tag1
  Scenario Outline: User melakukan Pembayaran Baru    
    Given I open brimo application with username and password with <cases>
    And Checking flow <cases>
    When Pada halaman Home klik Lainnya
    When Pada Fitur Lainnya klik Pascabayar
    When User klik Pembayaran Baru
    When Input Nomor Handphone
    When Pada halaman Pascabayar Klik Lanjutkan
    When User pilih condition
    When Pada halaman Konfirmasi klik Bayar
    Then I input correct PIN
    Then Transaction success <feature>
    Then Close application

    Examples: 
      | cases | feature |
      |         1 | Pascabayar |