#Author: your.email@your.domain.com
#Keywords Summary :
#Feature: List of scenarios.
#Scenario: Business rule through list of steps with arguments.
#Given: Some precondition step
#When: Some key actions
#Then: To observe outcomes or validation
#And,But: To enumerate more Given,When,Then steps
#Scenario Outline: List of steps for data-driven as an Examples and <placeholder>
#Examples: Container for s table
#Background: List of steps run before each of the scenarios
#""" (Doc Strings)
#| (Data Tables)
#@ (Tags/Labels):To group Scenarios
#<> (placeholder)
#""
## (Comments)
#Sample Feature Definition Template
@tag
Feature: Feature Pascabayar
  Fitur ini untuk membayar pascabayar kartu Halo

  #TC101
  @Pascabayar_Halo
  Scenario Outline: User melakukan Pembayaran Baru    
    Given I open brimo application with username and password with <cases>
    #And Checking flow <cases>
    When Pada halaman Home klik Lainnya
    When Pada Fitur Lainnya klik Pascabayar
    When User klik Pembayaran Baru
    When Input Nomor Handphone
    When Pada halaman Pascabayar Klik Lanjutkan
    When User pilih condition
    When Pada halaman Konfirmasi klik Bayar
    Then I input correct PIN
    Then Transaction success <feature>
    Then Close application

    Examples: 
      | cases | feature |
      |         1 | Pascabayar |
      
	@Pascabayar_XL
  Scenario Outline: User melakukan Pembayaran Baru    
    Given I open brimo application with username and password with <cases>
    #And Checking flow <cases>
    When Pada halaman Home klik Lainnya
    When Pada Fitur Lainnya klik Pascabayar
    When User klik Pembayaran Baru
    When Input Nomor Handphone
    When Pada halaman Pascabayar Klik Lanjutkan
    When User pilih condition
    When Pada halaman Konfirmasi klik Bayar
    Then I input correct PIN
    Then Transaction success <feature>
    Then Close application

    Examples: 
      | cases | feature |
      |         2 | Pascabayar |
      
	@Pascabayar_Indosat
  Scenario Outline: User melakukan Pembayaran Baru    
    Given I open brimo application with username and password with <cases>
    #And Checking flow <cases>
    When Pada halaman Home klik Lainnya
    When Pada Fitur Lainnya klik Pascabayar
    When User klik Pembayaran Baru
    When Input Nomor Handphone
    When Pada halaman Pascabayar Klik Lanjutkan
    When User pilih condition
    When Pada halaman Konfirmasi klik Bayar
    Then I input correct PIN
    Then Transaction success <feature>
    Then Close application

    Examples: 
      | cases | feature |
      |         3 | Pascabayar |
      
      
 