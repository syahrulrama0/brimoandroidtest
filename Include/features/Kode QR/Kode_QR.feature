#Author: your.email@your.domain.com
#Keywords Summary :
#Feature: List of scenarios.
#Scenario: Business rule through list of steps with arguments.
#Given: Some precondition step
#When: Some key actions
#Then: To observe outcomes or validation
#And,But: To enumerate more Given,When,Then steps
#Scenario Outline: List of steps for data-driven as an Examples and <placeholder>
#Examples: Container for s table
#Background: List of steps run before each of the scenarios
#""" (Doc Strings)
#| (Data Tables)
#@ (Tags/Labels):To group Scenarios
#<> (placeholder)
#""
## (Comments)
#Sample Feature Definition Template
@QR_CPM
Feature: Title of your feature
  I want to use this template for my feature file

  @tag_Fast_Menu_Kode_QR
  Scenario Outline: Title of your scenario outline
    Given Pilih Fast Menu Kode QR
    When Pilih Sumber Dana_QR <sumberdana>
    Then Pilih button Lanjutkan QR
    #And Input PIN_QR <rowNumber>
    And Verifikasi Kode QR
    And Pilih button OK QR

    Examples: 
      | cases | username      | password     |
      |         2 | Mazdimdimaz12 | Mahardhika12 |

  #| username   | password   | status  | condition | pin | bank     | rekening | sumberdana     |
  #| brimosv008 | Jakarta123 | success | correct   | 118899 | bank bri | @kings   | Saving         |
  #|Mazdimdimaz12 | Mahardhika12| success | correct   | 191919 | bank bri | @kings   | Tabungan_Utama |
  
  @tag_Menu_Kode_QR
  Scenario Outline: Title of your scenario outline
    Given I open brimo application with username and password with <cases>
    When Pilih Menu Kode QR
    When Pilih Sumber Dana_QR <sumberdana>
    Then Pilih button Lanjutkan QR
    And Input PIN_QR
    And Verifikasi Kode QR
    And Pilih button OK QR

    Examples: 
      Examples: 
      | rowNumber | username      | password     |
      |         1 | Mazdimdimaz12 | Mahardhika12 |
      
      #| username      | password     | status  | condition | pin    | rekening | sumberdana |
      #| brimosv008 | Jakarta123 | success | correct 	| 191919 | @kings   | Saving         |
      #| Mazdimdimaz12 | Mahardhika12 | success | correct   | 118899 | @kings   | Saving     |

  @tag_Menu_Kode_QR_PIN_SALAH_BENAR
  Scenario Outline: Menginputkan PIN SALAH lalu kembali menginputkan PIN BENAR
    Given Login BRIMO <username> <password>
    #When Unconnect Internet
    #When Click button Baiklah
    When Pilih Menu Kode QR
    When Pilih Sumber Dana_QR <sumberdana>
    Then Pilih button Lanjutkan QR
    And Input PIN_QR <rowNumber>
    When Pilih Sumber Dana_QR <sumberdana>
    Then Pilih button Lanjutkan QR
    And Input PIN_QR <rowNumber>
    And Verifikasi Kode QR
    And Pilih button OK QR

    Examples: 
      | username   | password   | status  | condition | condition2 | pin    | rekening | sumberdana     |
      #| brimosv008 | Jakarta123 | success | incorrect | correct    | 191919 | @kings   | Saving         |
      | brimosv008 | Jakarta123 | success | incorrect | correct    | 191919 | @kings   | Tabungan_Utama |

  @tag_Menu_Kode_QR_Minta_Kode_Baru
  Scenario Outline: Title of your scenario outline
    Given Login BRIMO <username> <password>
    When Pilih Menu Kode QR
    When Pilih Sumber Dana_QR <sumberdana>
    Then Pilih button Lanjutkan QR
    And Input PIN_QR <condition> <ForgetPin> <pin>
    And Verifikasi Kode QR
    And Minta Kode QR Baru

    Examples: 
      | username   | password   | status  | condition | pin    | rekening | sumberdana     |
      #| brimosv008 | Jakarta123 | success | correct 	| 118899 | @kings 	| Saving				 |
      | brimosv008 | Jakarta123 | success | correct   | 191919 | @kings   | Tabungan_Utama |

  @tag_Menu_Kode_QR_Kadaluarsa
  Scenario Outline: Title of your scenario outline
    Given Login BRIMO <username> <password>
    When Pilih Menu Kode QR
    When Pilih Sumber Dana_QR <sumberdana>
    Then Pilih button Lanjutkan QR
    And Input PIN_QR <condition> <ForgetPin> <pin>
    And Verifikasi Kode QR
    And Kode QR Kadaluarsa

    Examples: 
      | username   | password   | status  | condition | pin    | rekening | sumberdana     |
      #| brimosv008 | Jakarta123 | success | correct 	| 191919 | @kings 	|Saving					 |
      | brimosv008 | Jakarta123 | success | correct   | 118899 | @kings   | Tabungan_Utama |
