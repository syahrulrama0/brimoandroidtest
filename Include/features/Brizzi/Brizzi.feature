#Author: your.email@your.domain.com
#Keywords Summary :
#Feature: List of scenarios.
#Scenario: Business rule through list of steps with arguments.
#Given: Some precondition step
#When: Some key actions
#Then: To observe outcomes or validation
#And,But: To enumerate more Given,When,Then steps
#Scenario Outline: List of steps for data-driven as an Examples and <placeholder>
#Examples: Container for s table
#Background: List of steps run before each of the scenarios
#""" (Doc Strings)
#| (Data Tables)
#@ (Tags/Labels):To group Scenarios
#<> (placeholder)
#""
## (Comments)
#Sample Feature Definition Template
@tag
Feature: Title of your feature
  I want to use this template for my feature file

  @Top_Up_Brizzi_By_Manual_Number
  Scenario Outline: Title of your scenario outline
    Given I open brimo application with username and password with <cases>
    #And Checking flow <cases>
    When Open Brizzi Menu
    When Pay using account number and amount
    #And I open sumber dana list
    #And Select sumber dana positive
    And I open konfirmasi page
    When Open Brizzi confirmation page
    Then I input correct PIN
    #Then Transaction success
    #Then Transaction Brizzi success
    Then Transaction success <feature>
    Then Close application

    Examples: 
      | cases | feature |
      | 1 | Brizzi |
      
  #@Top_Up_Brizzi_By_History
  #Scenario Outline: Title of your scenario outline
    #Given I open an application with username <username> and password <password>
    #When Open Brizzi Menu
    #When Pay using history account and amount <amount>
    #When Choose found resource case positve where source found is <sourceFound>
    #When Open BRIZZI sumber dana page
    #When Open Brizzi confirmation page
    #When Input correct PIN number <pinNumber>
  #	Then Transaction Brizzi success
  #	Then Close application
#
    #Examples: 
      #| username  | password | amount | sourceFound  | pinNumber |
      #| brimosv007 | Jakarta123 | 20 | saving | 191919 |
      #| brimosv007 | Jakarta123 | 20 | giro | 191919 |
      #
  #@Input_Wrong_PIN
  #Scenario Outline: Title of your scenario outline
    #Given I open an application with username <username> and password <password>
    #When Open Brizzi Menu
    #When Pay using history account and amount <amount>
    #When Choose found resource case positve where source found is <sourceFound>
    #When Open BRIZZI sumber dana page
    #When Open Brizzi confirmation page
  #	Then Input incorrect PIN number
  #	Then Close application
#
    #Examples: 
      #| username  | password | amount | sourceFound  |
      #| brimosv007 | Jakarta123 | 20 | saving |
      #
 #		@Forget_PIN
  #	Scenario Outline: Title of your scenario outline
    #Given I open an application with username <username> and password <password>
    #When Open Brizzi Menu
    #When Pay using history account and amount <amount>
    #When Choose found resource case positve where source found is <sourceFound>
    #When Open BRIZZI sumber dana page
    #When Open Brizzi confirmation page
  #	Then Forget PIN Number
  #	Then Close application
#
    #Examples: 
      #| username  | password | amount | sourceFound  |
      #| brimosv007 | Jakarta123 | 20 | saving |
    #
    #@Sumber_Dana_Negative_Case  
    #Scenario Outline: Title of your scenario outline
    #Given I open an application with username <username> and password <password>
    #When Open Brizzi Menu
    #When Pay using history account and amount <amount>
    #When Choose found resource case negative where source found is <sourceFound>
  #	Then Close application 
#
    #Examples: 
      #| username  | password | amount | sourceFound |
      #| brimosv007 | Jakarta123 | 20 | dormant |
      #| brimosv007 | Jakarta123 | 20 | freeze |
      #| brimosv007 | Jakarta123 | 20 | closed |
      #
    #@Sumber_Dana_With_Not_Enough_Amount  
    #Scenario Outline: Title of your scenario outline
    #Given I open an application with username <username> and password <password>
    #When Open Brizzi Menu
    #When Pay using history account and amount <amount>
    #When Choose found resource case not enough amount at brizzi where source found is <sourceFound>
  #	Then Close application 
#
    #Examples: 
      #| username  | password | amount | sourceFound |
      #| brimosv007 | Jakarta123 | 20 | saving |
      #| brimosv007 | Jakarta123 | 20 | giro |
      #
    #@Brizzi_Scan_Menu
    #Scenario Outline: Title of your scenario outline
    #Given I open an application with username <username> and password <password>
    #When Open Brizzi Menu
    #When Open Brizzi scan menu
  #	Then Close application 
#
    #Examples: 
      #| username  | password | 
      #| brimosv007 | Jakarta123 |