#Author: your.email@your.domain.com
#Keywords Summary :
#Feature: List of scenarios.
#Scenario: Business rule through list of steps with arguments.
#Given: Some precondition step
#When: Some key actions
#Then: To observe outcomes or validation
#And,But: To enumerate more Given,When,Then steps
#Scenario Outline: List of steps for data-driven as an Examples and <placeholder>
#Examples: Container for s table
#Background: List of steps run before each of the scenarios
#""" (Doc Strings)
#| (Data Tables)
#@ (Tags/Labels):To group Scenarios
#<> (placeholder)
#""
## (Comments)
#Sample Feature Definition Template
@tag
Feature: Title of your feature
  I want to use this template for my feature file

  @Top_Up_Brizzi_By_Manual_Number
  Scenario Outline: Title of your scenario outline
    Given I open brimo application with username and password with <cases>
    And Checking flow <cases>
    When Open Brizzi Menu
    When Pay using account number and amount
    #And I open sumber dana list
    #And Select sumber dana positive
    And I open konfirmasi page
    When Open Brizzi confirmation page
    Then I input correct PIN
    #Then Transaction success
    #Then Transaction Brizzi success
    Then Transaction success <feature>
    Then Close application

    Examples: 
      | cases | feature |
      | 1 | Brizzi |
      