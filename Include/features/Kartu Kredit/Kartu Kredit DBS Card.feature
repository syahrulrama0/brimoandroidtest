#Author: your.email@your.domain.com
#Keywords Summary :
#Feature: List of scenarios.
#Scenario: Business rule through list of steps with arguments.
#Given: Some precondition step
#When: Some key actions
#Then: To observe outcomes or validation
#And,But: To enumerate more Given,When,Then steps
#Scenario Outline: List of steps for data-driven as an Examples and <placeholder>
#Examples: Container for s table
#Background: List of steps run before each of the scenarios
#""" (Doc Strings)
#| (Data Tables)
#@ (Tags/Labels):To group Scenarios
#<> (placeholder)
#""
## (Comments)
#Sample Feature Definition Template
@tag
Feature: Title of your feature
  I want to payment Credit Card with Brimo

  @DBS_Credit_Card_Payment_with_Pembayaran_Baru 
  Scenario Outline: User Payment Credit Card DBS with method Pembayaran Baru
     Given I open brimo application with username and password with <cases>
     #And Checking flow <cases>
     When Open Dashboard page
     When Open Credit Card Menu
     When User Click method Pembayaran Baru
     When User Select Bank
     When User Input Destination Credit Number
     When User Input Nominal Payment
     When User Select Condition
     When Confrm payment detail
     When I input correct PIN
     #Then Screen displays payment receipt
     #When Confirms the contents of the payment receipt
     Then  Transaction success <feature>
     Then Close Application

    Examples: 
    	| cases | feature     |
    	| 3     | Kartu Kredit|
    	
  #@DBS_Credit_Card_Payment_with_Saldo_Giro_TidakCukup 
  #Scenario Outline: User Payment Credit Card DBS with Saldo Giro Tidak Cukup
     #Given Login with existing account where <username> and <password>
     #When Open Dashboard page
     #When Open Credit Card Menu
     #When User Click method Pembayaran Baru
     #When User Select Bank <creditCard>
     #When User Input Destination Credit Number <creditNumber>
     #When User Input Nominal Payment <nominalPayment>
     #When User Select Sumber Dana <jenisSumberDana>
     #Then Close Application
     #
 #		Examples: 
      #| username        | password    	| creditCard      | creditNumber      | nominalPayment |jenisSumberDana           | 
      #| Fegaeka20031994 | M4ret1994     | DBS_Card	    	| 4374500000606250  | 100000         |Saldo_Tidak_Cukup    | 
      