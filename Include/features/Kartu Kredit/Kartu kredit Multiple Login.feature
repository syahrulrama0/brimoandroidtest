@tag
Feature: Title of your feature
  I want to payment Credit Card with Brimo

  @BRI_Credit_Card_Payment_with_Pembayaran_Baru 
  Scenario Outline: User Payment Credit Card Bank BRI with method Pembayaran Baru
     Given I open brimo application with username and password with <cases>
     And Checking flow <cases>
     When Open Dashboard page
     When Open Credit Card Menu
     When User Click method Pembayaran Baru
     When User Select Bank
     When User Input Destination Credit Number
     When User Input Nominal Payment
     When User Select Condition
     When Confrm payment detail
     When I input correct PIN
     Then Screen displays payment receipt
     #When Confirms the contents of the payment receipt
     Then  Transaction success <feature>
     Then Close Application

    Examples: 
    	| cases | feature      |
    	| 1     | Kartu Kredit |
    	
    	
    	  @Citibank_Credit_Card_Payment_with_Pembayaran_Baru 
  Scenario Outline: User Payment Credit Card Citibank with method Pembayaran Baru
     Given I open brimo application with username and password with <cases>
     And Checking flow <cases>
     When Open Dashboard page
     When Open Credit Card Menu
     When User Click method Pembayaran Baru
     When User Select Bank
     When User Input Destination Credit Number
     When User Input Nominal Payment
     When User Select Condition
     When Confrm payment detail
     When I input correct PIN
     Then Screen displays payment receipt
     #When Confirms the contents of the payment receipt
     Then  Transaction success <feature>
     Then Close Application

    Examples: 
    	| cases | feature     |
    	| 2     | Kartu Kredit|
    	
    	
    	@DBS_Credit_Card_Payment_with_Pembayaran_Baru 
  Scenario Outline: User Payment Credit Card DBS with method Pembayaran Baru
     Given I open brimo application with username and password with <cases>
     And Checking flow <cases>
     When Open Dashboard page
     When Open Credit Card Menu
     When User Click method Pembayaran Baru
     When User Select Bank
     When User Input Destination Credit Number
     When User Input Nominal Payment
     When User Select Condition
     When Confrm payment detail
     When I input correct PIN
     Then Screen displays payment receipt
     #When Confirms the contents of the payment receipt
     Then  Transaction success <feature>
     Then Close Application

    Examples: 
    	| cases | feature     |
    	| 3     | Kartu Kredit|
    	
    	
    	
    	 @HSBC_Credit_Card_Payment_with_Pembayaran_Baru 
  Scenario Outline: User Payment Credit Card HSBC with method Pembayaran Baru
     Given I open brimo application with username and password with <cases>
     And Checking flow <cases>
     When Open Dashboard page
     When Open Credit Card Menu
     When User Click method Pembayaran Baru
     When User Select Bank
     When User Input Destination Credit Number
     When User Input Nominal Payment
     When User Select Condition
     When Confrm payment detail
     When I input correct PIN
     Then Screen displays payment receipt
     #When Confirms the contents of the payment receipt
     Then  Transaction success <feature>
     Then Close Application

    Examples: 
    	| cases | feature      |
    	| 4     | Kartu Kredit |
    	
    	
    	@Standard_Chartedred_Credit_Card_Payment_with_Pembayaran_Baru 
  Scenario Outline: User Payment Credit Card Standard Chartered with method Pembayaran Baru
     Given I open brimo application with username and password with <cases>
     And Checking flow <cases>
     When Open Dashboard page
     When Open Credit Card Menu
     When User Click method Pembayaran Baru
     When User Select Bank
     When User Input Destination Credit Number
     When User Input Nominal Payment
     When User Select Condition
     When Confrm payment detail
     When I input correct PIN
     Then Screen displays payment receipt
     #When Confirms the contents of the payment receipt
     Then  Transaction success <feature>
     Then Close Application

    Examples: 
    	| cases | feature      |
    	| 5     | Kartu Kredit |