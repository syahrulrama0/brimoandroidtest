#Author: your.email@your.domain.com
#Keywords Summary :
#Feature: List of scenarios.
#Scenario: Business rule through list of steps with arguments.
#Given: Some precondition step
#When: Some key actions
#Then: To observe outcomes or validation
#And,But: To enumerate more Given,When,Then steps
#Scenario Outline: List of steps for data-driven as an Examples and <placeholder>
#Examples: Container for s table
#Background: List of steps run before each of the scenarios
#""" (Doc Strings)
#| (Data Tables)
#@ (Tags/Labels):To group Scenarios
#<> (placeholder)
#""
## (Comments)
#Sample Feature Definition Template
@tag
Feature: Title of your feature
  I want to use this template for my feature file

  @tag1
  Scenario Outline: Title of your scenario outline
    Given I open brimo application with username and password with <cases>
    When Successfully go to dashboard i go to mutasi menu
    #When Bank account <sumberdana>
    When The date on rowNumber
    Then I tap Cari button to check it 
    When Close app    
    Examples: 
      |cases| 
      |1|
     
      #@tag2
  #Scenario Outline: Title of your scenario outline
    #Given open application using <username> and <password>
    #When Successfully go to dashboard i go to mutasi menu
    #When Bank account <sumberdana>
    #When The date <date>
    #Then I tap Cari button to check it    
    #Examples: 
      #|username      |password    |sumberdana |date   |
      #|syahrulrama0|31121997Rama|rekeningUtama|Kemarin|
      
      #@tag3
  #Scenario Outline: Title of your scenario outline
    #Given open application using <username> and <password>
    #When Successfully go to dashboard i go to mutasi menu
    #When Bank account <sumberdana>
    #When The date <date>
    #Then I tap Cari button to check it    
    #Examples: 
      #|username      |password    |sumberdana |date      |
      #|syahrulrama0|31121997Rama|rekeningUtama|Minggu Ini|
      
      #@tag4
  #Scenario Outline: Title of your scenario outline
    #Given open application using <username> and <password>
    #When Successfully go to dashboard i go to mutasi menu
    #When Bank account <sumberdana>
    #When The date <date>
    #Then I tap Cari button to check it    
    #Examples: 
      #|username      |password    |sumberdana |date     |
      #|syahrulrama0|31121997Rama|rekeningUtama|Bulan Ini|