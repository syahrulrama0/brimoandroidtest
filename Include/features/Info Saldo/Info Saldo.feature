#Author: your.email@your.domain.com
#Keywords Summary :
#Feature: List of scenarios.
#Scenario: Business rule through list of steps with arguments.
#Given: Some precondition step
#When: Some key actions
#Then: To observe outcomes or validation
#And,But: To enumerate more Given,When,Then steps
#Scenario Outline: List of steps for data-driven as an Examples and <placeholder>
#Examples: Container for s table
#Background: List of steps run before each of the scenarios
#""" (Doc Strings)
#| (Data Tables)
#@ (Tags/Labels):To group Scenarios
#<> (placeholder)
#""
## (Comments)
#Sample Feature Definition Template
@tag
Feature: Title of your feature
  I want to use this template for my feature file

  #@tag1
  #Scenario Outline: Title of your scenario outline
    #Given I open an application with username <username> and password <password>
    #When I open info saldo menu
    #Then Verify saldo
    #Then Close application
#
    #Examples: 
      #| username  | password |
      #| syahrulrama0 | 31121997Rama |
      
  @tag2
  Scenario Outline: Title of your scenario outline
    Given I open brimo application with username and password with <cases>
    When I open info saldo menu
    Then I open tabungan valas
    Then Back dashboard info saldo
    Then Close application

    Examples: 
      | cases |
      | 1 |