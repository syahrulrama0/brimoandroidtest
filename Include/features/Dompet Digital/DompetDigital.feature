
@tag
Feature: Title of your feature
  I want to use this template for my feature file

  @Dompet_Digital_Success
  Scenario Outline: Running Dompet Digital Feature Possitive Case
    Given I open brimo application with username and password with <cases>
    When I open dompet digital dana page
    When I open top up baru menu
    And I select dompet digital and input phone number
    And I input amount and save name on nominal page
    #And I see the sumber dana list
    #And Select sumber dana positive
    Then Show konfirmasi dana page
    Then I input correct PIN
    Then Transaction success <feature>
    Then Close application

    Examples: 
      | cases | feature |
      | 1 | Dompet Digital |
      #| 2 | Dompet Digital |
      #| 3 | Dompet Digital |
      #| 4 | Dompet Digital |
      #| 5 | Dompet Digital |
      #| 6 | Dompet Digital |
      
@Dompet_Digital_Dana_Success
  Scenario Outline: Running Dompet Digital Feature Possitive Case
    Given I open brimo application with username and password with <cases>
    #And Checking flow <cases>
    When I open dompet digital dana page
    When I open top up baru menu
    And I select dompet digital and input phone number
    And I input amount and save name on nominal page
    #And I see the sumber dana list
    #And Select sumber dana positive
    Then Show konfirmasi dana page
    Then I input correct PIN
    Then Transaction success <feature>
    Then Close application

    Examples: 
      | cases | feature |
      | 1 | Dompet Digital |

@Dompet_Digital_Gopay_Success
  Scenario Outline: Running Dompet Digital Feature Possitive Case
    Given I open brimo application with username and password with <cases>
    #And Checking flow <cases>
    When I open dompet digital dana page
    When I open top up baru menu
    And I select dompet digital and input phone number
    And I input amount and save name on nominal page
    #And I see the sumber dana list
    #And Select sumber dana positive
    Then Show konfirmasi dana page
    Then I input correct PIN
    Then Transaction success <feature>
    Then Close application

    Examples: 
      | cases | feature |
      | 2 | Dompet Digital |
      
@Dompet_Digital_ShopeePay_Success
  Scenario Outline: Running Dompet Digital Feature Possitive Case
    Given I open brimo application with username and password with <cases>
    #And Checking flow <cases>
    When I open dompet digital dana page
    When I open top up baru menu
    And I select dompet digital and input phone number
    And I input amount and save name on nominal page
    #And I see the sumber dana list
    #And Select sumber dana positive
    Then Show konfirmasi dana page
    Then I input correct PIN
    Then Transaction success <feature>
    Then Close application

    Examples: 
      | cases | feature |
      | 4 | Dompet Digital |
      
@Dompet_Digital_LinkAja_Success
  Scenario Outline: Running Dompet Digital Feature Possitive Case
    Given I open brimo application with username and password with <cases>
    #And Checking flow <cases>
    When I open dompet digital dana page
    When I open top up baru menu
    And I select dompet digital and input phone number
    And I input amount and save name on nominal page
    #And I see the sumber dana list
    #And Select sumber dana positive
    Then Show konfirmasi dana page
    Then I input correct PIN
    Then Transaction success <feature>
    Then Close application

    Examples: 
      | cases | feature |
      | 5 | Dompet Digital |
      
@Dompet_Digital_Ovo_Success
  Scenario Outline: Running Dompet Digital Feature Possitive Case
    Given I open brimo application with username and password with <cases>
    #And Checking flow <cases>
    When I open dompet digital dana page
    When I open top up baru menu
    And I select dompet digital and input phone number
    And I input amount and save name on nominal page
    #And I see the sumber dana list
    #And Select sumber dana positive
    Then Show konfirmasi dana page
    Then I input correct PIN
    Then Transaction success <feature>
    Then Close application

    Examples: 
      | cases | feature |
      | 6 | Dompet Digital |
      