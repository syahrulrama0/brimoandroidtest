#Author: your.email@your.domain.com
#Keywords Summary :
#Feature: List of scenarios.
#Scenario: Business rule through list of steps with arguments.
#Given: Some precondition step
#When: Some key actions
#Then: To observe outcomes or validation
#And,But: To enumerate more Given,When,Then steps
#Scenario Outline: List of steps for data-driven as an Examples and <placeholder>
#Examples: Container for s table
#Background: List of steps run before each of the scenarios
#""" (Doc Strings)
#| (Data Tables)
#@ (Tags/Labels):To group Scenarios
#<> (placeholder)
#""
## (Comments)
#Sample Feature Definition Template
@tag
Feature: Title of your feature
  I want to use this template for my feature file

  @Dompet_Digital_Dana_Success
  Scenario Outline: Running Dompet Digital Feature Possitive Case
    #Given I open brimo application with username and password with <cases>
    When I open dompet digital dana page
    When I open top up baru menu
    And I select dompet digital and input phone number
    And I input amount and save name on nominal page
    #And I see the sumber dana list
    #And Select sumber dana positive
    Then Show konfirmasi dana page
    Then I input correct PIN
    Then Transaction success <feature>
    #Then Close application

    Examples: 
      | cases | feature |
      | 1 | Dompet Digital |
