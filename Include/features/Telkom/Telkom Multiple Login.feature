#Author: your.email@your.domain.com
#Keywords Summary :
#Feature: List of scenarios.
#Scenario: Business rule through list of steps with arguments.
#Given: Some precondition step
#When: Some key actions
#Then: To observe outcomes or validation
#And,But: To enumerate more Given,When,Then steps
#Scenario Outline: List of steps for data-driven as an Examples and <placeholder>
#Examples: Container for s table
#Background: List of steps run before each of the scenarios
#""" (Doc Strings)
#| (Data Tables)
#@ (Tags/Labels):To group Scenarios
#<> (placeholder)
#""
## (Comments)
#Sample Feature Definition Template
@tag_telkom
Feature: Title of your feature
  I want to use this template for my feature file

  @Telkom_Pembayaran_Baru
  Scenario Outline: Title of your scenario outline
    Given I open brimo application with username and password with <cases>
    And Checking flow <cases>
    When Click menu telkom
    Then Click button Pembayaran Baru
    And Input Nomor Telepon
    And Click button Telkom Lanjutkan
    #When Pilih Telkom Sumber Dana <sumberdana>
    #And Tap Lihat Detail Tagihan
    #And  Tap Simpan untuk selanjutnya
    #And  Input <nama> Daftar Simpan
    And Click button bayar telkom
    Then Konfirmasi detail bayar
    And Click button bayar telkom2
    Then I input correct PIN
    #And Halaman Transaksi Telkom
    Then Transaction success <feature>
    Then Close application

    Examples: 
    | cases | feature |
    | 1     | Telkom |
