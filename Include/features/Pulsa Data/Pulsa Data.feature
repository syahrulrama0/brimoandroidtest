#Author: your.email@your.domain.com
#Keywords Summary :
#Feature: List of scenarios.
#Scenario: Business rule through list of steps with arguments.
#Given: Some precondition step
#When: Some key actions
#Then: To observe outcomes or validation
#And,But: To enumerate more Given,When,Then steps
#Scenario Outline: List of steps for data-driven as an Examples and <placeholder>
#Examples: Container for s table
#Background: List of steps run before each of the scenarios
#""" (Doc Strings)
#| (Data Tables)
#@ (Tags/Labels):To group Scenarios
#<> (placeholder)
#""
## (Comments)
#Sample Feature Definition Template
@tag
Feature: Title of your feature
  I want to use this template for my feature file

  @Purchase_Data_Telkomsel
  Scenario Outline: Purchase Data Ballance With Manual Phone Number
    Given I open brimo application with username and password with <cases>
    When Open pulsa data page
    When Purchase data by manual phone number
    When Open sumber dana page
    And Select sumber dana positive
    Then I Confirmation My package
    When Open confirmation page pulsa data
  	Then I input correct PIN
    Then Transaction success <feature>
    When Close Application

    Examples: 
      |cases| feature |
     	| 1 | Pulsa Data |
     	
   @Purchase_Data_Indosat
  Scenario Outline: Purchase Data Ballance With Manual Phone Number
    Given I open brimo application with username and password with <cases>
    When Open pulsa data page
    When Purchase data by manual phone number
    When Open sumber dana page
    And Select sumber dana positive
    Then I Confirmation My package
    When Open confirmation page pulsa data
  	Then I input correct PIN
    Then Transaction success <feature>
    When Close Application

    Examples: 
      |cases| feature |
     	| 2 | Pulsa Data |

