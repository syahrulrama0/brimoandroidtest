#Author: your.email@your.domain.com
#Keywords Summary :
#Feature: List of scenarios.
#Scenario: Business rule through list of steps with arguments.
#Given: Some precondition step
#When: Some key actions
#Then: To observe outcomes or validation
#And,But: To enumerate more Given,When,Then steps
#Scenario Outline: List of steps for data-driven as an Examples and <placeholder>
#Examples: Container for s table
#Background: List of steps run before each of the scenarios
#""" (Doc Strings)
#| (Data Tables)
#@ (Tags/Labels):To group Scenarios
#<> (placeholder)
#""
## (Comments)
#Sample Feature Definition Template
@tag
Feature: Title of your feature
  I want to use this template for my feature file
#
  @Purchase_Data_Manual
  Scenario Outline: Purchase Data Ballance With Manual Phone Number
    Given I open an application with username <username> and password <password>
    When Open pulsa data page
    When Purchase data by manual phone number <phoneNumber>
    When Open sumber dana page
    And Select sumber dana positive
    Then I Confirmation My package
    When Open confirmation page pulsa data
  	Then I input correct PIN <pinNumber>
     #Then Transaction success
     When Close Application

    Examples: 
      | username  | password | phoneNumber | pinNumber|
      | uciha1ndra | testing123 | 082223456512  |191919|
      
