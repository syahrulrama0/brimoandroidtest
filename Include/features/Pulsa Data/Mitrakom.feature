#Author: your.email@your.domain.com
#Keywords Summary :
#Feature: List of scenarios.
#Scenario: Business rule through list of steps with arguments.
#Given: Some precondition step
#When: Some key actions
#Then: To observe outcomes or validation
#And,But: To enumerate more Given,When,Then steps
#Scenario Outline: List of steps for data-driven as an Examples and <placeholder>
#Examples: Container for s table
#Background: List of steps run before each of the scenarios
#""" (Doc Strings)
#| (Data Tables)
#@ (Tags/Labels):To group Scenarios
#<> (placeholder)
#""
## (Comments)
#Sample Feature Definition Template
@tag
Feature: Title of your feature
  I want to use this template for my feature file
#
  #@Purchase_Data_Manual
  #Scenario Outline: Purchase Data Ballance With Manual Phone Number
    #Given I open an application with username <username> and password <password>
    #When Open pulsa data page
    #When Purchase data by manual phone number <phoneNumber>
    #When Open sumber dana page
    #And Select sumber dana positive
    #Then I Confirmation My package
    #When Open confirmation page pulsa data
  #	Then Close application
#
    #Examples: 
      #| username  | password | phoneNumber |
      #| syahrulrama0 | 31121997Rama | 08561234567  |
      
  @Purchase_Data_Icon_Contact
  Scenario Outline: Purchase Data Ballance With Icon Contact
  	Given I open an application with username <username> and password <password>
    When Open pulsa data page
    When Purchase data by icon contact <provider>
    When Open sumber dana page
    And Select sumber dana positive
    Then I Confirmation My package
    When Open confirmation page pulsa data
  	Then Close application
    
    Examples: 
      | username  | password | phoneNumber | provider |
      | syahrulrama0 | 31121997Rama | 08561234567  | mitrakom |
      
   #@Purchase_Data_By_History
   #Scenario Outline: Purchase Data Ballance With History
   #	Given Login with existings account where <username> and <password>
   #	When Open Dashboard page
    #When Open pulsa data page
    #When Purchase data by history
    #When Open sumber dana page
    #When Choose found resource case positve where source found is <sourceFound>
    #When Open confirmation page pulsa data
    #When Input correct PIN number <pinNumber>
  #	Then Transaction Paket Data is Successfull
  #	Then Close application
    #
    #Examples: 
      #| username  | password | phoneNumber | pinNumber | sourceFound |
      #| brimosv007 | Jakarta123 | 08561234567  | 191919 | saving |
      #
      #
   #@Input_Incorrect_PIN
   #Scenario Outline: Input Incorrect PIN Until Account Get Blocked
   #	Given Login with existings account where <username> and <password>
   #	When Open Dashboard page
    #When Open pulsa data page
    #When Purchase data by history
    #When Open sumber dana page
    #When Choose found resource case positve where source found is <sourceFound>
    #When Open confirmation page pulsa data
    #Then Input incorrect PIN number
  #	Then Close application 
    #
    #Examples: 
      #| username  | password | phoneNumber | pinNumber | sourceFound |
      #| brimosv007 | Jakarta123 | 08561234567  | 191919 | saving |
   #
   #@Lupa_PIN
   #Scenario Outline: User Choose Lupa PIN While In Screen Input PIN
    #Given Login with existings account where <username> and <password>
    #When Open Dashboard page
    #When Open pulsa data page
    #When Purchase data by history
    #When Open sumber dana page
    #When Choose found resource case positve where source found is <sourceFound>
    #When Open confirmation page pulsa data
    #Then Forget PIN Number
  #	Then Close application 
    #
    #Examples: 
      #| username  | password | phoneNumber | pinNumber | sourceFound |
      #| brimosv007 | Jakarta123 | 08561234567  | 191919 | saving |
      #
    #@Pilih_Sumber_Dana_Negative
   #Scenario Outline: User Choose Lupa PIN While In Screen Input PIN
    #Given Login with existings account where <username> and <password>
    #When Open Dashboard page
    #When Open pulsa data page
    #When Purchase data by history
    #When Open sumber dana page
    #When Choose found resource case negative where source found is <sourceFound>
  #	Then Close application 
    #
    #Examples: 
      #| username  | password | sourceFound |
      #| brimosv007 | Jakarta123 | dormant |
      #| brimosv007 | Jakarta123 | freeze |
      #| brimosv007 | Jakarta123 | closed |
      #
   #	@Pilih_Sumber_Dana_Not_Enough_Amount
   #	Scenario Outline: User Choose Lupa PIN While In Screen Input PIN
    #Given Login with existings account where <username> and <password>
    #When Open Dashboard page
    #When Open pulsa data page
    #When Purchase data by history
    #When Open sumber dana page
    #When Choose found resource case negative where source found is <sourceFound>
  #	Then Close application 
    #
    #Examples: 
      #| username  | password | sourceFound |
      #| brimosv007 | Jakarta123 | giro |
      #| brimosv007 | Jakarta123 | saving |
      #
    #@Offline_Function_Pulsa_Data
    #Scenario Outline: User Choose Lupa PIN While In Screen Input PIN
    #Given Login with existings account where <username> and <password>
    #When Open Dashboard page
    #When Open pulsa data page
    #When Turn ON Offline mode on pulsa data
  #	Then Close application 
    #
    #Examples: 
      #| username  | password |
      #| brimosv007 | Jakarta123 | 