#Author: your.email@your.domain.com
#Keywords Summary :
#Feature: List of scenarios.
#Scenario: Business rule through list of steps with arguments.
#Given: Some precondition step
#When: Some key actions
#Then: To observe outcomes or validation
#And,But: To enumerate more Given,When,Then steps
#Scenario Outline: List of steps for data-driven as an Examples and <placeholder>
#Examples: Container for s table
#Background: List of steps run before each of the scenarios
#""" (Doc Strings)
#| (Data Tables)
#@ (Tags/Labels):To group Scenarios
#<> (placeholder)
#""
## (Comments)
#Sample Feature Definition Template
@tag
Feature: Title of your feature
  I want to Top Up DPLK with Brimo

  @DPLK_Top_Up_with_Rekening_Utama 
  Scenario Outline: User Top Up DPLK with method Rekening Utama
     Given I open brimo application with username and password with <cases>
     #And Checking flow <cases>
     When Open Dashboard page
     When Open DPLK Menu
     When User Input DPLK Account Number
     When User Input Nominal Top Up
     When User Select Condition for Top Up
     When Confrm Top Up detail
     When I input correct PIN
     #Then Screen displays payment receipt with Transaksi Berhasil
     #When Confirms the contents of the Top Up Receipt
     Then Transaction success <feature>
     Then Close Application

    Examples: 
      | cases | feature |
      | 1 | DPLK |
      