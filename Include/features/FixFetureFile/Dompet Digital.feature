#Author: your.email@your.domain.com
#Keywords Summary :
#Feature: List of scenarios.
#Scenario: Business rule through list of steps with arguments.
#Given: Some precondition step
#When: Some key actions
#Then: To observe outcomes or validation
#And,But: To enumerate more Given,When,Then steps
#Scenario Outline: List of steps for data-driven as an Examples and <placeholder>
#Examples: Container for s table
#Background: List of steps run before each of the scenarios
#""" (Doc Strings)
#| (Data Tables)
#@ (Tags/Labels):To group Scenarios
#<> (placeholder)
#""
## (Comments)
#Sample Feature Definition Template
@tag
Feature: Title of your feature
  I want to use this template for my feature file

  @tag1
  Scenario Outline: Top Up Via History or Phone Book
    Given Login with existing account where <username> and <password>
    When Open Dashboard page
    When Open Dompet Digital Page with <kontak> , <history> and <topUpVia>
    When Open Nominal Page wih <amount> , <status> and <alias>
    #When Batalkan Transaksi <transaction>
    When Open Konfirmasi Page
    #When Back from input PIN section
    When Input <condition> PIN <number>
    #When Icon Back at transaksi berhasil
    Then Transaction Is Succesfull 
    #Then Open Lupa PIN Page
    #diatas perlu di comment apabila menggunakan incorrect
        
    Examples: 
      | username  | password | kontak | history | topUpVia | amount | status | alias | condition | number | transaction |
      | brimosv007 | Jakarta123 | yes | yes | history | 50000 | success | null | correct | 191919 | no |
      
      #topUpVia : searchkontak, topupbaru, history dan kontak
      #condition : correct (PIN BENAR) , incorrect (PIN SALAH), forgetPIN (LUPA PIN)
      #contact & history : yes artinya mempunyai 
      #status untuk : input nominal
      #alias : nama kontak
      #viaContact : via phone book atau manual
      #wallet : gopay, dana, shopeepay, ovo, linkaja
      
   #@tag1
   #Scenario Outline: Top Up Via New Account
    #Given Login with existing account where <username> and <password>
    #When Open Dashboard page
    #When Open Dompet Digital Page with <kontak> , <history> and <topUpVia>
    #When Open Top Up Baru Page with <wallet> , <phoneNumber> and <viaContact>
    #When Open Nominal Page wih <amount> , <status> and <alias>
    #When Open Konfirmasi Page
    #When Input <condition> PIN <number>
    #Then Transaction Is Succesfull
        #
    #Examples: 
      #| username  | password | contact | history | topUpVia | amount | status | alias | condition | number | wallet | phoneNumber | viaContact |
      #| brimosv007 | Jakarta123 | yes | yes | topupbaru | 50000 | success | null | correct | 191919 | gopay | 082210112982 | no |