
@tag
Feature: Title of your feature
  I want to use this template for my feature file

  @tag1
  Scenario Outline: Title of your scenario outline
    Given I Start Application
    When I open dompet digital via fast menu
    Then I verify method top up
    And I input amount in dompet digital fast menu nominal page on <rowNumber>
    Then Show fast menu dompet digital konfirmasi
    Then I input correct PIN
    Then Transaction success <feature>
    Then Close application

    Examples: 
      | rowNumber  | feature |
      | 2 | Fast menu (Dompet Digital) |