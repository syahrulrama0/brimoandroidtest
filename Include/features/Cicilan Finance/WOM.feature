#Author: your.email@your.domain.com
#Keywords Summary :
#Feature: List of scenarios.
#Scenario: Business rule through list of steps with arguments.
#Given: Some precondition step
#When: Some key actions
#Then: To observe outcomes or validation
#And,But: To enumerate more Given,When,Then steps
#Scenario Outline: List of steps for data-driven as an Examples and <placeholder>
#Examples: Container for s table
#Background: List of steps run before each of the scenarios
#""" (Doc Strings)
#| (Data Tables)
#@ (Tags/Labels):To group Scenarios
#<> (placeholder)
#""
## (Comments)
#Sample Feature Definition Template
@tag
Feature: Feature Cicilan
  Fitur ini untuk Cicilan Finance WOM

  #TC101 User dapat Bayar Cicilan WOM
  @tag1
  Scenario Outline: User dapat Bayar Cicilan WOM
    Given User Login masukan <username> dan <password>
    When Pada halaman Home klik Lainnya
    When Klik Cicilan di Fitur Lainnya
    When Klik Pembayaran Baru
    When Pilih Jenis Cicilan <jenisCicilan>
    When Input Nomor Pelanggan <noPelanggan>
    When Klik Lanjutkan
    #When User pilih Sumber Dana <sumberDana>
    #When User di arahkan ke halaman Konfirmasi klik Bayar
    #When Input <condition> PIN <number>
    #When Transaksi Berhasil user klik OK
    Then Tutup aplikasi
    Examples: 
      | username       | password | status  | noPelanggan      | sumberDana    | condition | number | jenisCicilan |
      #| brimosv003 | Jakarta123 | success | 1210123450001 | rekeningUtama | correct   | 191919 | WOM |
      | syahrulrama0 | 31121997Rama | success | 1646120200300893 | rekeningUtama | correct   | 191919 | WOM          |
