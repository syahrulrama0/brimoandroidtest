#Author: your.email@your.domain.com
#Keywords Summary :
#Feature: List of scenarios.
#Scenario: Business rule through list of steps with arguments.
#Given: Some precondition step
#When: Some key actions
#Then: To observe outcomes or validation
#And,But: To enumerate more Given,When,Then steps
#Scenario Outline: List of steps for data-driven as an Examples and <placeholder>
#Examples: Container for s table
#Background: List of steps run before each of the scenarios
#""" (Doc Strings)
#| (Data Tables)
#@ (Tags/Labels):To group Scenarios
#<> (placeholder)
#""
## (Comments)
#Sample Feature Definition Template
@tag
Feature: Title of your feature
  I want to use this template for my feature file

  @cicilan_oto
  Scenario Outline: Title of your scenario outline
    Given I open brimo application with username and password with <cases>
    And Checking flow <cases>
    When Pada halaman Home klik Lainnya
    When Klik Cicilan di Fitur Lainnya
    When Klik Pembayaran Baru
    When Pilih Jenis Cicilan
    When Input Nomor Pelanggan
    When Klik Lanjutkan
#		When User pilih Sumber Dana <sumberDana>
    When Confirmation cicilan
    Then I input correct PIN
    Then Transaction success <feature>
    #When Transaksi Berhasil user klik OK
    Then Tutup aplikasi

    Examples: 
      | cases | feature |
      | 2 		| Cicilan |
      
      


  @cicilan_wom
  Scenario Outline: Title of your scenario outline
    Given I open brimo application with username and password with <cases>
    And Checking flow <cases>
    When Pada halaman Home klik Lainnya
    When Klik Cicilan di Fitur Lainnya
    When Klik Pembayaran Baru
    When Pilih Jenis Cicilan
    When Input Nomor Pelanggan
    When Klik Lanjutkan
#		When User pilih Sumber Dana <sumberDana>
    When Confirmation cicilan
    Then I input correct PIN
    Then Transaction success <feature>
    #When Transaksi Berhasil user klik OK
    Then Tutup aplikasi

    Examples: 
      | cases | feature |
      | 3 | Cicilan |
      
      

  @cicilan_fif
  Scenario Outline: Title of your scenario outline
    Given I open brimo application with username and password with <cases>
    And Checking flow <cases>
    When Pada halaman Home klik Lainnya
    When Klik Cicilan di Fitur Lainnya
    When Klik Pembayaran Baru
    When Pilih Jenis Cicilan
    When Input Nomor Pelanggan
    When Klik Lanjutkan
#		When User pilih Sumber Dana <sumberDana>
    When Confirmation cicilan
    Then I input correct PIN
    Then Transaction success <feature>
    #When Transaksi Berhasil user klik OK
    Then Tutup aplikasi

    Examples: 
      | cases | feature |
      | 1 | Cicilan |
      
      
