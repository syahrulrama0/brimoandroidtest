@tag_Transfer_tambah_daftar_baru
Feature: Title of your feature
  I want to use this template for my feature file

  @Transfer_tambah_daftar_baru
  Scenario Outline: Title of your scenario outline
    Given I open brimo application with username and password with <cases>
    #And Checking flow <cases>
    When Click menu transfer
    Then Click button Tambah Daftar Baru
    And Tap bank tujuan
    And Input Nomor Rekening
    And Click button Lanjutkan
    When Input Nominal Transfer
    #When Pilih Transfer Sumber Dana <sumberdana>
    And Click button Transfer
    Then Konfirmasi detail transaksi
    And Click button Transfer2
    Then I input correct PIN
    Then Transaction success <feature>
    #And Halaman Transaksi
    Then Close Application
    Examples: 
      | cases | feature  |
      |     1 | Transfer |

  @Transfer_tambah_daftar_baru_bank_lain_bca
  Scenario Outline: Title of your scenario outline
    Given I open brimo application with username and password with <cases>
    #And Checking flow <cases>
    When Click menu transfer
    Then Click button Tambah Daftar Baru
    And Tap bank tujuan
    And Input Nomor Rekening
    And Click button Lanjutkan
    When Input Nominal Transfer
    #Then Pilih Sumber Dana <sumberdana>
    And Click button Transfer
    Then Konfirmasi detail transaksi
    And Click button Transfer2

    Then I input correct PIN
    #And Click button Cek Status
    Then Transaction success <feature>
    #And Halaman Transaksi
    Then Close Application
    Examples: 
      | cases | feature  |
      |     2 | Transfer |

  @Transfer_tambah_daftar_baru_bank_lain_bni
  Scenario Outline: Title of your scenario outline
    Given I open brimo application with username and password with <cases>
    #And Checking flow <cases>
    When Click menu transfer
    Then Click button Tambah Daftar Baru
    And Tap bank tujuan
    And Input Nomor Rekening
    And Click button Lanjutkan
    When Input Nominal Transfer
    #Then Pilih Sumber Dana <sumberdana>
    And Click button Transfer
    Then Konfirmasi detail transaksi
    And Click button Transfer2

    Then I input correct PIN
    #And Click button Cek Status
    Then Transaction success <feature>
    Then Close Application
    Examples: 
      | cases | feature  |
      |     3 | Transfer |

  @Transfer_tambah_daftar_baru_bank_lain_mandiri
  Scenario Outline: Title of your scenario outline
    Given I open brimo application with username and password with <cases>
    #And Checking flow <cases>
    When Click menu transfer
    Then Click button Tambah Daftar Baru
    And Tap bank tujuan
    And Input Nomor Rekening
    And Click button Lanjutkan
    When Input Nominal Transfer
    #Then Pilih Sumber Dana <sumberdana>
    And Click button Transfer
    Then Konfirmasi detail transaksi
    And Click button Transfer2

    Then I input correct PIN
    #And Click button Cek Status
    Then Transaction success <feature>
    Then Close Application
    Examples: 
      | cases | feature  |
      |     4 | Transfer |

  @Transfer_tambah_daftar_baru_bank_lain_citibank
  Scenario Outline: Title of your scenario outline
    Given I open brimo application with username and password with <cases>
    #And Checking flow <cases>
    When Click menu transfer
    Then Click button Tambah Daftar Baru
    And Tap bank tujuan
    And Input Nomor Rekening
    And Click button Lanjutkan
    When Input Nominal Transfer
    #Then Pilih Sumber Dana <sumberdana>
    And Click button Transfer
    Then Konfirmasi detail transaksi
    And Click button Transfer2

    Then I input correct PIN
    #And Click button Cek Status
    Then Transaction success <feature>
    Then Close Application
    Examples: 
      | cases | feature  |
      |     5 | Transfer |
