@tag
Feature: Title of your feature
  I want to use this template for my feature file

  @ListrikPLNToken
  Scenario Outline: Pembayaran Tagihan Listrik PLN dengan saldo cukup
    Given I open brimo application with username and password with <cases>
    And Checking flow <cases>
    When Successfully go to dashboard i go to listrik menu
    Then I tap Tambah Daftar Baru and choose Produk Listrik and input ID Pelanggan
    When Get nominal at row number
    When Confirmation listrik Token
    Then I input correct PIN
    Then Transaction success <feature>
    Then App Closed 
    Examples: 
      |cases| feature |
      |1| Listrik |

      
  @ListrikPLNTagihan
  Scenario Outline: Pembayaran Tagihan Listrik PLN saldo cukup
    Given I open brimo application with username and password with <cases>
    And Checking flow <cases>
    When Successfully go to dashboard i go to listrik menu
    Then I tap Tambah Daftar Baru and choose Produk Listrik and input ID Pelanggan
    When Do Payment 
		Then I input correct PIN
		Then Transaction success <feature>
		Then App Closed 
    Examples: 
      |cases|feature|
      |2|Listrik Tagihan| 