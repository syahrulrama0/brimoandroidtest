#Author: your.email@your.domain.com
#Keywords Summary :
#Feature: List of scenarios.
#Scenario: Business rule through list of steps with arguments.
#Given: Some precondition step
#When: Some key actions
#Then: To observe outcomes or validation
#And,But: To enumerate more Given,When,Then steps
#Scenario Outline: List of steps for data-driven as an Examples and <placeholder>
#Examples: Container for s table
#Background: List of steps run before each of the scenarios
#""" (Doc Strings)
#| (Data Tables)
#@ (Tags/Labels):To group Scenarios
#<> (placeholder)
#""
## (Comments)
#Sample Feature Definition Template
@tag
Feature: Title of your feature
  I want to use this template for my feature file

  @ListrikPLNToken
  Scenario Outline: Pembayaran Tagihan Listrik PLN dengan saldo cukup
    Given I open brimo application with username and password with <cases>
    #And Checking flow <cases>
    When Successfully go to dashboard i go to listrik menu
    Then I tap Tambah Daftar Baru and choose Produk Listrik and input ID Pelanggan
    When Get nominal at row number
    When Confirmation listrik Token
    Then I input correct PIN
    Then Transaction success <feature>
    Then App Closed 
    Examples: 
      |cases| feature |
      |1|Listrik|
      #Then I try input the right PIN <pin>
      #Then I finish the transaction
      
  @ListrikPLNTagihan
  Scenario Outline: Pembayaran Tagihan Listrik PLN saldo cukup
    Given I open brimo application with username and password with <cases>
    #And Checking flow <cases>
    When Successfully go to dashboard i go to listrik menu
    Then I tap Tambah Daftar Baru and choose Produk Listrik and input ID Pelanggan
    When Do Payment
		Then I input correct PIN
		Then Transaction success <feature>
		Then App Closed 
    Examples: 
      |cases|feature|
      |2|Listrik| 
      
  @ListrikPLNNonTagihan
  Scenario Outline: Pembayaran Tagihan Listrik PLN saldo cukup
    Given I open brimo application with username and password with <cases>
    #And Checking flow <cases>
    When Successfully go to dashboard i go to listrik menu
    Then I tap Tambah Daftar Baru and choose Produk Listrik and input ID Pelanggan
    When Do Payment 
		Then I input correct PIN
		Then Transaction success <feature>
		Then App Closed 
    Examples: 
      |cases|feature|
      |3|Listrik|