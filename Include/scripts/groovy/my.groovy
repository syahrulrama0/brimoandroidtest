//import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
//import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
//import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
//import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
//
//import com.kms.katalon.core.annotation.Keyword
//import com.kms.katalon.core.checkpoint.Checkpoint
//import com.kms.katalon.core.checkpoint.CheckpointFactory
//import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
//import com.kms.katalon.core.model.FailureHandling
//import com.kms.katalon.core.testcase.TestCase
//import com.kms.katalon.core.testcase.TestCaseFactory
//import com.kms.katalon.core.testdata.TestData
//import com.kms.katalon.core.testdata.TestDataFactory
//import com.kms.katalon.core.testobject.ObjectRepository
//import com.kms.katalon.core.testobject.TestObject
//import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
//import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
//import com.sun.net.httpserver.Authenticator.Failure
//
//import internal.GlobalVariable
//
//import org.openqa.selenium.WebElement
//import org.openqa.selenium.WebDriver
//import org.openqa.selenium.By
//
//import com.kms.katalon.core.mobile.keyword.internal.MobileAbstractKeyword
//import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
//import com.kms.katalon.core.webui.driver.DriverFactory
//
//import com.kms.katalon.core.testobject.RequestObject
//import com.kms.katalon.core.testobject.ResponseObject
//import com.kms.katalon.core.testobject.ConditionType
//import com.kms.katalon.core.testobject.TestObjectProperty
//
//import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
//import com.kms.katalon.core.util.KeywordUtil
//
//import com.kms.katalon.core.webui.exception.WebElementNotFoundException
//
//import cucumber.api.java.en.And
//import cucumber.api.java.en.Given
//import cucumber.api.java.en.Then
//import cucumber.api.java.en.When
//
//
//
//class my {
//	/**
//	 * The step definitions below match with Katalon sample Gherkin steps
//	 */
//
//	//TC101
//	@Given("Buka halaman Fast Menu")
//	def bukaFastMenu() {
//		Mobile.callTestCase(findTestCase('Test Cases/Action/Fast Menu/fastMenu'), [:], FailureHandling.STOP_ON_FAILURE)
//	}
//
//	@When("Pilih menu Dompet Digital")
//	def pilihDompetDigital() {
//		Mobile.callTestCase(findTestCase('Test Cases/Action/Fast Menu/pilihDompetDigital'), [:], FailureHandling.STOP_ON_FAILURE)
//	}
//
//	@Given("User Login masukan (.*) dan (.*)")
//	def userLogin(String username, String password) {
//		Mobile.callTestCase(findTestCase('Test Cases/Action/Login/pageLogin'), [('username') : username, ('password') : password], FailureHandling.STOP_ON_FAILURE)
//	}
//
//	@When("Pada halaman Home klik icon Dompet Digital")
//	def klikDompetDigital() {
//		Mobile.callTestCase(findTestCase('Test Cases/Action/Login/pageDashboard'), [:], FailureHandling.STOP_ON_FAILURE)
//	}
//
//	@When("Diarahkan ke halaman Dompet Digital")
//	def I_verify_the_status_in_step(String status) {
//		Mobile.callTestCase(findTestCase('Test Cases/Action/Login/pageDompetDigital'), [:], FailureHandling.STOP_ON_FAILURE)
//	}
//
//	//TC102
//	@When("Pada halaman Home klik icon mata untuk melihat Saldo Rekening Utama")
//	def melihatSaldoRekening() {
//		Mobile.callTestCase(findTestCase('Test Cases/Action/Dashboard/melihatRekeningSaldo'), [:], FailureHandling.STOP_ON_FAILURE)
//	}
//
//	//TCTC201 - TC207
//	@When("Buka halaman Dompet Digital dengan (.*) , (.*) dan (.*)")
//	def halamanDompetDigital(String contact, String history, String topUpVia) {
//		Mobile.callTestCase(findTestCase('Test Cases/Action/Top Up/bukaDompetDigitalKondisi'), [('contact') : contact, ('history') : history, ('topUpVia') : topUpVia], FailureHandling.STOP_ON_FAILURE)
//	}
//
//	@When("Buka halaman Nominal dengan (.*) , (.*) dan (.*)")
//	def halamanNominal(String amount, String status, String alias ) {
//		Mobile.callTestCase(findTestCase('Test Cases/Action/Top Up/halamanNominal'), [('amount') : amount, ('status') : status, ('alias') : alias], FailureHandling.STOP_ON_FAILURE)
//	}
//
//	@When("Buka halaman Konfirmasi")
//	def halamanKonfirmasi() {
//		//		Mobile.callTestCase('Test Cases/Action/Top Up/halamanKonfirmasi', [:], FailureHandling.STOP_ON_FAILURE)
//		Mobile.callTestCase(findTestCase('Test Cases/Action/Top Up/halamanKonfirmasi'), [:], FailureHandling.STOP_ON_FAILURE )
//	}
//
//	@When("Masukan (.*) PIN (.*)")
//	def inputConditionalPIN(String condition, String number) {
//		Mobile.callTestCase(findTestCase('Test Cases/Action/Top Up/halamanPIN'), [('condition') : condition, ('number') : number], FailureHandling.STOP_ON_FAILURE)
//	}
//
//	@When("Transaksi Berhasil")
//	def transaksiBerhasil() {
//		Mobile.callTestCase(findTestCase('Test Cases/Action/Top Up/halamanTransaksiBerhasil'), [:], FailureHandling.STOP_ON_FAILURE)
//	}
//
//	//TC411
//	@When("Buka halaman Top Up Baru dan input Nomor Tujuan (.*)")
//	def openTopUpBaru(String phoneNumber) {
//		Mobile.callTestCase(findTestCase('Test Cases/Action/Top Up/halamanTopUpBaru'), [('phoneNumber') : phoneNumber], FailureHandling.STOP_ON_FAILURE)
//	}
//
//	@When("User klik Top Up Baru")
//	def userKlikTopUpBaru() {
//		Mobile.callTestCase(findTestCase('Test Cases/Action/Top Up/halamanTopUpBaruKlik'), [:] , FailureHandling.STOP_ON_FAILURE)
//	}
//
//	@When("User tidak memilih Dompet Digital dan Nomor Handphone")
//	def userTidakMemilihDompetDigital() {
//		Mobile.callTestCase(findTestCase('Test Cases/Action/Top Up/tidakInputData'), [:] , FailureHandling.STOP_ON_FAILURE)
//	}
//
//	@When("User tidak memilih Dompet Digital tetapi user input Nomor Handphone (.*)")
//	def userTidakInputDompetDigital(String phoneNumber) {
//		Mobile.callTestCase(findTestCase('Test Cases/Action/Top Up/tidakInputDompetDigital'), [('phoneNumber'): phoneNumber] , FailureHandling.STOP_ON_FAILURE)
//	}
//
//	@When("User pilih OVO dan user tidak input Nomor Handphone")
//	def userTidakInputNomorHandphone() {
//		Mobile.callTestCase(findTestCase('Test Cases/Action/Top Up/tidakInputNomorHandphone'), [:] , FailureHandling.STOP_ON_FAILURE)
//	}
//
//	@When("Input (.*) untuk Top Up")
//	def inputNominal(String nominal) {
//		Mobile.callTestCase(findTestCase('Test Cases/Action/Top Up/halamanNominalBasic'), [('nominal') : nominal] , FailureHandling.STOP_ON_FAILURE)
//	}
//
//	//TC319
//	@When("Tidak Input (.*) untuk Top Up")
//	def tidakInputNominal(String nominal) {
//		//todo
//	}
//
//	//TC208
//	@When("User klik icon three dots")
//	def userKlikIconThreeDots() {
//		Mobile.callTestCase(findTestCase('Test Cases/Action/Icon Three Dots/IconThreeDots'), [:], FailureHandling.STOP_ON_FAILURE)
//	}
//
//	//TC209
//	@When("Pilih Hapus dari Daftar")
//	def hapusDariDaftar() {
//		Mobile.callTestCase(findTestCase('Test Cases/Action/Icon Three Dots/hapusDaftar'), [:], FailureHandling.STOP_ON_FAILURE)
//	}
//
//	//TC210
//	@When("Pilih Edit (.*) lalu Simpan")
//	def	editDaftar(String simpanSebagai) {
//		Mobile.callTestCase(findTestCase('Test Cases/Action/Icon Three Dots/EditUpdateDaftar'), [('simpanSebagai') : simpanSebagai], FailureHandling.STOP_ON_FAILURE)
//	}
//
//	//TC211
//	@When("Pilih Jadikan Favorit")
//	def jadikanFavorit() {
//		Mobile.callTestCase(findTestCase('Test Cases/Action/Icon Three Dots/jadikanFavorit'), [:], FailureHandling.STOP_ON_FAILURE)
//	}
//
//	//TC212
//	@When("Pilih Hapus dari Favorit")
//	def hapusDariFavorit() {
//		Mobile.callTestCase(findTestCase('Test Cases/Action/Icon Three Dots/hapusDaftarFavorit'), [:], FailureHandling.STOP_ON_FAILURE)
//	}
//
//	//TC306
//	@When ("Pilih Dompet Digital OVO")
//	def pilihOVO(){
//		Mobile.callTestCase(findTestCase('Test Cases/Action/Top Up/pilihOVO'), [:], FailureHandling.STOP_ON_FAILURE)
//	}
//
//	@When ("User klik icon Contact pilih salah satu Contact akun OVO yang akan di top up (.*)")
//	def userKlikIcon(String inputContact) {
//		Mobile.callTestCase(findTestCase('Test Cases/Action/Top Up/pilihContact'), [('inputContact') : inputContact], FailureHandling.STOP_ON_FAILURE)
//	}
//
//	//TC307
//	@When ("User klik icon Contact pilih salah satu Contact akun OVO yang akan di top up, (.*)")
//	def userKlikIconContact(String inputContact) {
//		Mobile.callTestCase(findTestCase('Test Cases/Action/Top Up/pilihContact2'), [('inputContact') : inputContact], FailureHandling.STOP_ON_FAILURE)
//	}
//
//
//	@When ("User dapat edit (.*) Nomor Handphone")
//	def editContact(String editContact) {
//		Mobile.callTestCase(findTestCase('Test Cases/Action/Top Up/editContact'), [('editContact') : editContact], FailureHandling.STOP_ON_FAILURE)
//	}
//
//	//TC502
//	@When ("User tidak melalukan aksi selama 5 menit")
//	def sessionHabis() {
//		Mobile.callTestCase(findTestCase('Test Cases/Action/Top Up/sessionHabis'), [:], FailureHandling.STOP_ON_FAILURE)
//	}
//
//	//312
//	@When ("Buka halaman Nominal dengan simpan Nomor Tujuan (.*) , (.*) dan (.*)")
//	def simpanNomorTujuan(String amount, String status, String alias) {
//		Mobile.callTestCase(findTestCase('Test Cases/Action/Top Up/simpanNomorTujuan'), [('amount') : amount, ('status') : status, ('alias') : alias], FailureHandling.STOP_ON_FAILURE)
//	}
//}