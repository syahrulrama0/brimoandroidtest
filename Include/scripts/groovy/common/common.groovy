package common
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase

import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When



class common {
	/**
	 * The step definitions below match with Katalon sample Gherkin steps
	 */
	@Given("I open an application with username (.*) and password (.*)")
	def iOpenAnApplication(String username, String password) {
		Mobile.callTestCase(findTestCase('Test Cases/Common/Start Existing Application'),[('username'):username, ('password'):password],FailureHandling.STOP_ON_FAILURE)
	}

	@Given("I open an application with excel data at (\\d+)")
	def iOpenApplicationAndDataViaExcel(int rowNumber) {
		Mobile.callTestCase(findTestCase('Test Cases/Common/New Start Existing Application'),[('rowNumber'):rowNumber],FailureHandling.STOP_ON_FAILURE)
	}

	@And("Select sumber dana positive")
	def selectSumberDanaPositive() {
		Mobile.callTestCase(findTestCase('Test Cases/Common/Sumber Dana Positive'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@When("Open menu lainnya")
	def openMenuLainnya() {
		Mobile.callTestCase(findTestCase('Test Cases/Common/Open Menu Lainnya'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("Close application")
	def closeApplication() {
		Mobile.closeApplication()
	}

	@Given("I Start Application")
	def startApplication() {
		Mobile.callTestCase(findTestCase('Test Cases/Common/Only Start Application'),[:],FailureHandling.STOP_ON_FAILURE)
	}

	@Given("I open brimo application with username (.*) and password (.*)")
	def openBrimoApplication(String username, String password) {
		Mobile.callTestCase(findTestCase('Test Cases/Common/Start Existing Application'),[('username'):username,('password'):password],FailureHandling.STOP_ON_FAILURE)
	}

	@Then("I input correct PIN")
	def inputCorrectPIN() {
		Mobile.callTestCase(findTestCase('Test Cases/Common/PIN/PIN Success'),[:],FailureHandling.STOP_ON_FAILURE)
	}

	@When("Transaction success (.*)")
	def transactionsuccess(String feature) {
		Mobile.callTestCase(findTestCase('Test Cases/Common/Transaksi Berhasil'), [('feature'):feature], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("I input data correct PIN from excel")
	def inputCorrectPINFromExcel() {
		Mobile.callTestCase(findTestCase('Test Cases/Common/PIN/PIN Success from excel'),[:],FailureHandling.STOP_ON_FAILURE)
	}

	@When("Back to confirm payment detail")
	def arrowBackPIN() {
		Mobile.callTestCase(findTestCase('Test Cases/Common/Arrow Back PIN'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@When("Back to input amount")
	def arrowBackToAmount() {
		Mobile.callTestCase(findTestCase('Test Cases/Common/Arrow Back Confirm'), [:], FailureHandling.STOP_ON_FAILURE)
	}
	@When("Back to dashboard pulsa")
	def arrowBackToDashboardPulsa() {
		Mobile.callTestCase(findTestCase('Test Cases/Common/Arrow Back Dashboard Pulsa'), [:], FailureHandling.STOP_ON_FAILURE)
	}
	@Given("I open brimo application with username and password with (\\d+)")
	def openBrimoAndDataFromExcel(int cases) {
		Mobile.callTestCase(findTestCase('Test Cases/Common/Open Brimo'),[('cases'):cases], FailureHandling.STOP_ON_FAILURE)
	}
	@Then("Kill application")
	def killApplication() {
		Mobile.callTestCase(findTestCase('Test Cases/Common/Kill Application'),[:],FailureHandling.STOP_ON_FAILURE)
	}
	@When("I Open brizzi via fast menu")
	def openBrizziViaFastMenu() {
		Mobile.callTestCase(findTestCase('Test Cases/Fast Menu/Brizzi/Open Brizzi Via Fast Menu'), [:], FailureHandling.STOP_ON_FAILURE)
	}
	@And("Checking flow (\\d+)")
	def checkingFlow(int cases) {
		Mobile.callTestCase(findTestCase('Test Cases/Common/Checking Flow'), [('cases'):cases], FailureHandling.STOP_ON_FAILURE)
	}
	@When("I swipe notification menu")
	def openNotification() {
		Mobile.callTestCase(findTestCase('Test Cases/Common/Swipe Notification'), [:], FailureHandling.STOP_ON_FAILURE)
	}
	@When("I turn off my connection")
	def turnOffMyConnection() {
		Mobile.callTestCase(findTestCase('Test Cases/Common/Offline Mode'),[:],FailureHandling.STOP_ON_FAILURE)
	}
	@When("I turn on my connection")
	def turnOnMyConnection() {
		Mobile.callTestCase(findTestCase('Test Cases/Common/Online Mode'),[:],FailureHandling.STOP_ON_FAILURE)
	}
	@When("I give delay 10 min")
	def sessionTimeout() {
		Mobile.callTestCase(findTestCase('Test Cases/Common/Timeout Session'),[:],FailureHandling.STOP_ON_FAILURE)
	}
	@Then("I verify fast menu")
	def verifyFastMenu() {
		Mobile.callTestCase(findTestCase('Test Cases/Common/Verify Fast Menu'),[:],FailureHandling.STOP_ON_FAILURE)
	}
	@When("I input wrong PIN")
	def wrongPIN() {
		Mobile.callTestCase(findTestCase('Test Cases/Common/PIN/Wrong PIN'),[:],FailureHandling.STOP_ON_FAILURE)
	}
	@Then("I verify blocked account")
	def verifyBlockedAccount() {
		Mobile.callTestCase(findTestCase('Test Cases/Common/PIN/Verify Blocked Account'),[:],FailureHandling.STOP_ON_FAILURE)
	}
	@Then("I give dalay 7 min")
	def transactionFailed() {
		Mobile.callTestCase(findTestCase('Test Cases/Common/Failed Transacation Delay'),[:],FailureHandling.STOP_ON_FAILURE)
	}
	@Then("I verify not enought balance")
	def verifyCurrentBalance() {
		Mobile.callTestCase(findTestCase('Test Cases/Common/Verify Saldo Tidak Cukup'),[:],FailureHandling.STOP_ON_FAILURE)
	}
}