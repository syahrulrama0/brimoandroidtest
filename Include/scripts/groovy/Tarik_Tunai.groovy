import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When



class Tarik_Tunai {
	/**
	 * The step definitions below match with Katalon sample Gherkin steps
	 */
	@Given("Pilih Menu Tarik Tunai")
	def Pilih_Menu_Tarik_Tunai() {

		Mobile.tap(findTestObject('Object Repository/Tarik Tunai Object/Icon Menu Lainnya'), 0)
		Mobile.tap(findTestObject('Object Repository/Tarik Tunai Object/Icon - Menu Tarik Tunai'), 0)
		Mobile.delay(5)
	}

	@When("Pilih Tarik Tunai Sumber Dana (.*)")
	def Pilih_Tarik_Tunai_Sumber_Dana(String sumberdana) {

		Mobile.callTestCase(findTestCase('Test Cases/Transfer/Halaman Sumber Dana'), [('sumberdana'):sumberdana], FailureHandling.STOP_ON_FAILURE)
	}
	
	@When("Tarik Tunai Sumber Dana Normal (\\d+)")
	def Tarik_Tunai_Sumber_Dana_Normal(int rowNumber)
	{
		Mobile.callTestCase(findTestCase('Test Cases/Tarik Tunai/Tarik Tunai Sumber Dana Normal'), [('rowNumber'):rowNumber], FailureHandling.STOP_ON_FAILURE)
		
	}
	
	@Then("Pilih Tarik Tunai Nominal")
	def Pilih_Tarik_Tunai_Nominal() {

		Mobile.callTestCase(findTestCase('Test Cases/Tarik Tunai/Tarik Tunai Nominal'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("Pilih Tarik Tunai button Lanjutkan")
	def Pilih_Tarik_Tunai_button_Lanjutkan() {

		Mobile.tap(findTestObject('Object Repository/Tarik Tunai Object/Button - Lanjutkan Tarik Tunai'), 0)
	}

	@And("Pilih Tarik Tunai Konfirmasi")
	def Pilih_Tarik_Tunai_Konfirmasi() {
		Mobile.callTestCase(findTestCase('Test Cases/Tarik Tunai/Tarik Tunai Konfirmasi'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@And("Input Tarik Tunai PIN (.*) (.*) (.*)")
	def Pilih_Tarik_Tunai_button_Lanjutkan(String condition, String pinPassword, String number) {

		Mobile.callTestCase(findTestCase('Test Cases/Transfer/Halaman PIN'), [('condition'):condition,('pinPassword'):pinPassword,('number'):number], FailureHandling.STOP_ON_FAILURE)
	}
	
	@And("Input Normal PIN Tarik Tunai (\\d+)")
	def Input_Normal_PIN_Tarik_Tunai(int rowNumber)
	{
		Mobile.callTestCase(findTestCase('Test Cases/Tarik Tunai/Tarik Tunai PIN'), [('rowNumber'):rowNumber], FailureHandling.STOP_ON_FAILURE)
		
	}

	@And("Verifikasi Tarik Tunai")
	def Verifikasi_Tarik_Tunai() {
		Mobile.callTestCase(findTestCase('Test Cases/Tarik Tunai/Tarik Tunai Verifikasi'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@And("Pilih button OK")
	def Pilih_button_OK() {
		Mobile.tap(findTestObject('Object Repository/Tarik Tunai Object/Tarik Tunai - Button - OK'), 0)
	}

	@And("Batalkan Penarikan Tarik Tunai")
	def Batalkan_Penarikan_Tarik_Tunai() {
		Mobile.tap(findTestObject('Object Repository/Tarik Tunai Object/Tarik Tunai - Batalkan Penarikan'), 0)
		Mobile.tap(findTestObject('Object Repository/Tarik Tunai Object/Pembatalan Transaksi Button - Ya'), 0)
	}

	@And("Token Tarik Tunai Kadaluarsa")
	def Token_Tarik_Tunai_Kadaluarsa() {
		Mobile.delay(300)
		Mobile.verifyElementVisible(findTestObject('Object Repository/Tarik Tunai Object/Tarik Tunai - Button - SALIN'), 0)
		Mobile.verifyElementVisible(findTestObject('Object Repository/Tarik Tunai Object/Batas waktu Penarikan - 00 Menit 00 Detik'), 0)
	}
}