package fastmenu
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When



class pulsadata {

	@When("I open pulsa data via fast menu")
	def iOpenPulsaDataViaFastMenu() {
		Mobile.callTestCase(findTestCase('Test Cases/Fast Menu/Pulsa Data/Open Pulsa Data Visa Fast Menu'),[:],FailureHandling.STOP_ON_FAILURE)
	}
//
	@When("User Input Manual Phone Number (.*) via fast menu")
	def userInputManualPhoneNumberToBuyPulsa(String phoneNumber) {
		Mobile.callTestCase(findTestCase('Test Cases/Fast Menu/Pulsa Data/Method Input Manual Number'),[('phoneNumber'):phoneNumber],FailureHandling.STOP_ON_FAILURE)
	}
//
	@When("User Verify Operator Number that used")
	def verifyPhoneNumberThatUserInput() {
		Mobile.callTestCase(findTestCase('Test Cases/Fast Menu/Pulsa Data/Verifikasi Operator'),[:],FailureHandling.STOP_ON_FAILURE)
	}

	@When("User Choose Amount to top up (\\d+)")
	def userChooseAmountAtPulsaDataMenu(int amount) {
		Mobile.callTestCase(findTestCase('Test Cases/Fast Menu/Pulsa Data/Nominal Pembelian Pulsa'),[('amount'):amount],FailureHandling.STOP_ON_FAILURE)
	}
}