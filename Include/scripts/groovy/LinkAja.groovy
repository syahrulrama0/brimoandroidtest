import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When



class LinkAja {
	/**
	 * The step definitions below match with Katalon sample Gherkin steps
	 */
	@Given("Buka halaman Fast Menu")
	def bukaFastMenu() {
		Mobile.callTestCase(findTestCase('Test Cases/Actionv2/Dashboard/Fast Menu/Open Fast Menu'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@When("Pilih menu Dompet Digital")
	def bukaDompetDigital() {
		Mobile.callTestCase(findTestCase('Test Cases/Actionv2/Dashboard/Fast Menu/Open Dompet Digital'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@Given("User Login masukan (.*) dan (.*)")
	def userLogin(String username, String password) {
		Mobile.callTestCase(findTestCase('Test Cases/Actionv2/Dashboard/Login/Login'), [('username') : username, ('password') : password], FailureHandling.STOP_ON_FAILURE)
	}

	@When("Pada halaman Home klik icon mata untuk melihat Saldo Rekening Utama")
	def klikIconMata() {
		Mobile.callTestCase(findTestCase('Test Cases/Actionv2/Dashboard/Lihat Saldo'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@When("Pada halaman Home klik icon Dompet Digital")
	def klikDompetDigital() {
		Mobile.callTestCase(findTestCase('Test Cases/Actionv2/Dashboard/Dompet Digital Menu'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@And("Pilih icon Three Dots")
	def klikIconThreeDots() {
		Mobile.callTestCase(findTestCase('Test Cases/Actionv2/Wallet Form/Icon Three Dots'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("Pilih Hapus dari Daftar")
	def hapusDariDaftar() {
		Mobile.callTestCase(findTestCase('Test Cases/Actionv2/Wallet Form/Delete Data Daftar Dompet Digital'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@When("Pilih Edit (.*) lalu Simpan")
	def	editDaftar(String simpanSebagai) {
		Mobile.callTestCase(findTestCase('Test Cases/Actionv2/Wallet Form/Edit Update Data Daftar Dompet Digital'), [('simpanSebagai') : simpanSebagai], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("Pilih Jadikan Favorit")
	def jadikanFavorit() {
		Mobile.callTestCase(findTestCase('Test Cases/Actionv2/Wallet Form/Add Favorit'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("Pilih Hapus dari Favorit")
	def hapusFavorit() {
		Mobile.callTestCase(findTestCase('Test Cases/Actionv2/Wallet Form/Delete Favorit'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@When("Klik Top Up Baru")
	def klikTopUpBaru() {
		Mobile.callTestCase(findTestCase('Test Cases/Actionv2/Wallet New Form/Top Up Baru'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@When("User input Nomor Handphone tujuan (.*)")
	def inputNomorHandphone(String phoneNumber) {
		Mobile.callTestCase(findTestCase('Test Cases/Actionv2/Wallet New Form/Input Nomor Handphone'), [('phoneNumber') : phoneNumber], FailureHandling.STOP_ON_FAILURE)
	}

	@When("User pilih Dompet Digital (.*)")
	def pilihDompetDigital(String dompetDigital) {
		Mobile.callTestCase(findTestCase('Test Cases/Actionv2/Wallet New Form/Select Dompet Digital'), [('dompetDigital') : dompetDigital], FailureHandling.STOP_ON_FAILURE)
	}

	@When("User akan pilih Dompet Digital (.*) dan (.*)")
	def pilihDompetDigitaldanTipe(String dompetDigital, String gopayType) {
		Mobile.callTestCase(findTestCase('Test Cases/Actionv2/Wallet New Form/Select Dompet Digital'), [('dompetDigital') : dompetDigital, ('gopayType') : gopayType], FailureHandling.STOP_ON_FAILURE)
	}

	@When("User dapat edit (.*) Nomor Handphone")
	def editContact(String editContact) {
		Mobile.callTestCase(findTestCase('Test Cases/Actionv2/Wallet New Form/Edit Contact'), [('editContact') : editContact], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("User klik Lanjutkan")
	def klikLanjutkan() {
		Mobile.callTestCase(findTestCase('Test Cases/Actionv2/Wallet New Form/Click Lanjutkan'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@And("User input Nominal (.*) untuk Top Up")
	def inputNominal(String nominal) {
		Mobile.callTestCase(findTestCase('Test Cases/Actionv2/Wallet Nominal Form/Input Nominal'), [('nominal') : nominal ], FailureHandling.STOP_ON_FAILURE)
	}

	@When("User melihat detail Sumber Dana (.*)")
	def lihatSumberDana(String sumberDana) {
		Mobile.callTestCase(findTestCase('Test Cases/Actionv2/Wallet Nominal Form/View Sumber Dana'), [('sumberDana') : sumberDana ], FailureHandling.STOP_ON_FAILURE)
	}

	@And("User klik Top Up pada halaman Nominal")
	def klikTopUp() {
		Mobile.callTestCase(findTestCase('Test Cases/Actionv2/Wallet Nominal Form/Click Top Up'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@And("User klik Top Up pada halaman Konfirmasi")
	def klikTopUpKonfirmasi() {
		Mobile.callTestCase(findTestCase('Test Cases/Actionv2/Wallet Confirm/Click Top Up'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("User klik OK pada halaman Transaksi Berhasil")
	def clickOK() {
		Mobile.callTestCase(findTestCase('Test Cases/Actionv2/Transaction/Click OK'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@When("User Cheklist Simpan untuk selanjutnya")
	def checklistSimpan() {
		Mobile.callTestCase(findTestCase('Test Cases/Actionv2/Wallet Nominal Form/Cheklist Simpan Nama'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@When("User input Nama (.*) untuk di Simpan Sebagai")
	def inputNama(String name) {
		Mobile.callTestCase(findTestCase('Test Cases/Actionv2/Wallet Nominal Form/Input Name'), [('name') : name ], FailureHandling.STOP_ON_FAILURE)
	}

	@And("User klik button Kembali")
	def klikButtonKembali() {
		Mobile.callTestCase(findTestCase('Test Cases/Actionv2/Wallet Confirm/Click Back Button'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@When("Klik Data Top Up Terakhir")
	def klikDataTopUpTerakhir() {
		Mobile.callTestCase(findTestCase('Test Cases/Actionv2/Wallet Form/Select Data Top Up Terakhir'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@When("Klik Data Daftar Dompet Digital")
	def klikDataDaftar() {
		Mobile.callTestCase(findTestCase('Test Cases/Actionv2/Wallet Form/Select Data Daftar Dompet Digital'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("Tutup aplikasi")
	def tutupAplikasi() {
		Mobile.delay(3)
		Mobile.closeApplication()
	}

	@When("User verifikasi halaman Transaksi Berhasil")
	def verifikasiTransaksiBerhasil() {
		Mobile.callTestCase(findTestCase('Test Cases/Actionv2/Transaction/verify'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@When("Menekan icon berbagi")
	def iconBerbagi() {
		Mobile.callTestCase(findTestCase('Test Cases/Actionv2/Transaction/Share'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@And("User klik icon back button lalu pilih Ya")
	def clickBackButtonYa() {
		Mobile.callTestCase(findTestCase('Test Cases/Actionv2/Wallet Confirm/Click Back Button and click Ya'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@And("User klik icon back button lalu pilih Tidak")
	def clickBackButtonTidak() {
		Mobile.callTestCase(findTestCase('Test Cases/Actionv2/Wallet Confirm/Click Back Button click Tidak'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@When("Koneksi Internet tidak terhubung")
	def offlineMode() {
		Mobile.callTestCase(findTestCase('Test Cases/Actionv2/Dashboard/Offline Mode'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@When("Klik button Baiklah")
	def klikBaiklah() {
		Mobile.callTestCase(findTestCase('Test Cases/Actionv2/Dashboard/Button Baiklah'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@When("Koneksi Internet terhubung")
	def onlineMode() {
		Mobile.callTestCase(findTestCase('Test Cases/Actionv2/Dashboard/Online Mode'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@When("User tidak melakukan aksi selama 5 menit")
	def sessionHabis() {
		Mobile.callTestCase(findTestCase('Test Cases/Actionv2/Dashboard/End Session'), [:], FailureHandling.STOP_ON_FAILURE)
	}


	@When("Input some (.*) PIN (.*)")
	def inputSomeConditionalPIN(String condition, String number) {
		Mobile.callTestCase(findTestCase('Test Cases/Actionv2/PIN/Validate PIN'), [('condition') : condition, ('number') : number], FailureHandling.STOP_ON_FAILURE)
	}

	@When("Klik Lupa PIN")
	def klikLupaPIN() {
		Mobile.callTestCase(findTestCase('Test Cases/Actionv2/PIN/Lupa PIN'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@When ("User pilih salah satu nomor di Contact untuk top up (.*)")
	def klikIconContact(String inputContact) {
		Mobile.callTestCase(findTestCase('Test Cases/Actionv2/Wallet New Form/Select Contact'), [('inputContact') : inputContact], FailureHandling.STOP_ON_FAILURE)
	}
}