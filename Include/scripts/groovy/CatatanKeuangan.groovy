import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When



class CatatanKeuangan {
	/**
	 * The step definitions below match with Katalon sample Gherkin steps
	 */
//	@Given("Start application with (.*) and (.*)")
//	def startapp(String username, String password) {
//		Mobile.callTestCase(findTestCase('Test Cases/Catatan Keuangan/Login'), [('username') : username, ('password') : password], FailureHandling.STOP_ON_FAILURE)
//	}

//	@When("Successfully go to dashboard i go to catatan keuangan menu")
//	def catatankeuangan() {
//		Mobile.callTestCase(findTestCase('Test Cases/Catatan Keuangan/Catatan Keuangan Menu'), [:], FailureHandling.STOP_ON_FAILURE)
//	}

//	@When("I try make a new notes for pengeluaran with input (.*) and choose (.*)")
//	def pengeluaranwithinput(String nominal, String kategori) {
//		Mobile.callTestCase(findTestCase('Test Cases/Catatan Keuangan/Masukkan jumlah dan pilih kategori'), [('nominal') : nominal, ('kategori') : kategori], FailureHandling.STOP_ON_FAILURE)
//	}

//	@When("I try make a new notes for pemasukan with input (.*) and choose (.*)")
//	def pemasukanwithinput(String nominal, String kategori) {
//		Mobile.callTestCase(findTestCase('Test Cases/Catatan Keuangan/Catatan Baru Pemasukan'), [('nominal') : nominal, ('kategori') : kategori], FailureHandling.STOP_ON_FAILURE)
//	}

//	@When("I Input (.*) pengeluaran and type of (.*)")
//	def lanjutanpengeluaran(String date, String pembayaran) {
//		Mobile.callTestCase(findTestCase('Test Cases/Catatan Keuangan/Masukkan tanggal dan pembayaran'), [('date') : date, ('pembayaran') : pembayaran], FailureHandling.STOP_ON_FAILURE)
//	}

//	@When("Go to pemasukan Page")
//	def pemasukanpage() {
//		Mobile.tap(findTestObject('Object Repository/CatatanKeuangan/slidepemasukan'), 0)
//	}
//
//	@When("I already input some information, i give a (.*) too")
//	def note(String note) {
//		Mobile.callTestCase(findTestCase('Test Cases/Catatan Keuangan/Note'), [('note') : note], FailureHandling.STOP_ON_FAILURE)
//	}
//
//	@Then("I choose (.*)")
//	def choosedate(String date) {
//		Mobile.callTestCase(findTestCase('Test Cases/Catatan Keuangan/Pilih tanggal pemasukan'), [('date') : date], FailureHandling.STOP_ON_FAILURE)
//	}

//	@When("I input a (.*) too")
//	def inputnotes(String note) {
//		Mobile.callTestCase(findTestCase('Test Cases/Catatan Keuangan/Note untuk pemasukan'), [('note') : note], FailureHandling.STOP_ON_FAILURE)
//	}

//	@Then("All information will be saved")
//	def informationsaved() {
//		Mobile.tap(findTestObject('Object Repository/CatatanKeuangan/buttonsimpanpemasukan'), 0)
//	}

//	@When("Open laporan Page")
//	def laporanpage() {
//		Mobile.callTestCase(findTestCase('Test Cases/Catatan Keuangan/Catatan Laporan'), [ : ], FailureHandling.STOP_ON_FAILURE)
//	}

//	@When("Choose time (.*)")
//	def choosetime(String waktu) {
//		Mobile.callTestCase(findTestCase('Test Cases/Catatan Keuangan/Pilih Waktu Laporan'), [('waktu') : waktu ], FailureHandling.STOP_ON_FAILURE)
//	}
}