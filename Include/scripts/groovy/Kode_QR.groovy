import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When



class Kode_QR {
	/**
	 * The step definitions below match with Katalon sample Gherkin steps
	 */
	@When("Pilih Fast Menu Kode QR")
	def Pilih_Fast_Menu_Kode_QR() {

		Mobile.startExistingApplication('id.co.bri.brimo', FailureHandling.STOP_ON_FAILURE)
		Mobile.tap(findTestObject("Object Repository/Fast Menu - Kode QR CPM/Fast Menu - Kode QR"), 0)
	}

	@When("Pilih Sumber Dana_QR (.*)")
	def Pilih_Sumber_Dana_QR(String sumberdana) {
		Mobile.callTestCase(findTestCase('Test Cases/Menu Transfer/Base Code Menu Transfer/Halaman Sumber Dana'), [('sumberdana'):sumberdana], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("Pilih button Lanjutkan QR")
	def Pilih_button_Lanjutkan_QR() {
		Mobile.tap(findTestObject("Object Repository/Fast Menu - Kode QR CPM/Button - Lanjutkan Kode QR"), 0)
	}

	@And("Input PIN_QR")
	def Input_PIN_QR() {
		
		Mobile.callTestCase(findTestCase('Test Cases/Kode QR/Menu Kode QR - PIN NORMAL'), [:], FailureHandling.STOP_ON_FAILURE)
	
	}

	@And("Verifikasi Kode QR")
	def Verifikasi_Kode_QR() {
		Mobile.verifyElementVisible(findTestObject('Object Repository/Kode QR CPM/Verify QRIS - Icon BRIMO'), 0)
		Mobile.verifyElementVisible(findTestObject('Object Repository/Kode QR CPM/Verify- Kode berlaku selama'), 0)
		Mobile.verifyElementVisible(findTestObject('Object Repository/Kode QR CPM/Kode QR Button - OK'), 0)
	}

	@And("Minta Kode QR Baru")
	def Minta_Kode_QR_Baru() {
		Mobile.tap(findTestObject("Object Repository/Kode QR CPM/Button - Minta Kode Baru"), 0)
	}

	@And("Kode QR Kadaluarsa")
	def Kode_QR_Kadaluarsa() {
		Mobile.delay(300)
		Mobile.verifyElementExist(findTestObject("Object Repository/Kode QR CPM/Verify - Kode QR sudah tidak tersedia"), 0)
		Mobile.verifyElementExist(findTestObject("Object Repository/Kode QR CPM/Verify - Kode QR Transaksi Belum Berhasil"), 0)
	}

	@And("Pilih button OK QR")
	def Pilih_button_OK_QR() {
		Mobile.tap(findTestObject('Object Repository/Kode QR CPM/Kode QR Button - OK'), 0)
	}

	@Given("Pilih Menu Kode QR")
	def Pilih_Menu_Kode_QR() {


		Mobile.tap(findTestObject("Object Repository/Kode QR CPM/Menu - Kode QR"), 0)
	}
}