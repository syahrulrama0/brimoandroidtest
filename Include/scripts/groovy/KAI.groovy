import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When



class KAI {
	/**
	 * The step definitions below match with Katalon sample Gherkin steps
	 */

	@When("Pada halaman Home klik Lainnya")
	def	padaHalamanHomeKlikLainnya() {
		Mobile.callTestCase(findTestCase('Test Cases/Actionv2/Dashboard/Click Lainnya'), [:], FailureHandling.STOP_ON_FAILURE)
	}
	@When("Klik KAI di Fitur Lainnya")
	def	klikKAI() {
		Mobile.callTestCase(findTestCase('Test Cases/Actionv2/Dashboard/Click KAI'), [:], FailureHandling.STOP_ON_FAILURE)
	}
	@When("Input Nomor KAI")
	def	inputNomorKAI() {
		Mobile.callTestCase(findTestCase('Test Cases/Actionv2/KAI/Form/Input Nomor KAI'), [:], FailureHandling.STOP_ON_FAILURE)
	}

//	@When("User Lihat Detail Tagihan dan pilih Sumber Dana (.*)")
//	def LihatDetaildanlihatSumberDana(String sumberDana) {
//		Mobile.callTestCase(findTestCase('Test Cases/Actionv2/KAI/Form/Sumber Dana'), [('sumberDana') : sumberDana ], FailureHandling.STOP_ON_FAILURE)
//	}
	
	@When("User pilih kondisi pembayaran KAI")
	def userPilihKondisiKAI() {
		Mobile.callTestCase(findTestCase('Test Cases/Actionv2/KAI/Form/Sumber Dana'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@When("User Lihat Detail Tagihan dan pilih Sumber Dana (.*)")
	def LihatDetaildanlihatSumberDana(String sumberDana) {
		Mobile.callTestCase(findTestCase('Test Cases/Actionv2/KAI/Form/Sumber Dana'), [('sumberDana') : sumberDana ], FailureHandling.STOP_ON_FAILURE)
	}

	@When("User di arahkan ke halaman Konfirmasi klik Bayar")
	def	klikBayarDiHalamanKonfirmasi() {
		Mobile.callTestCase(findTestCase('Test Cases/Actionv2/KAI/Form/Confirmation Bayar'), [:], FailureHandling.STOP_ON_FAILURE)
	}
	@When("Transaksi Berhasil user klik OK")
	def	saatTransaksiBerhasilKlikOK() {
		Mobile.callTestCase(findTestCase('Test Cases/Actionv2/KAI/Form/Transaction Succes'), [:], FailureHandling.STOP_ON_FAILURE)
	}
}