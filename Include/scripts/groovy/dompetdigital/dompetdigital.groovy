package dompetdigital
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When



class dompetdigital {

	@When("I open dompet digital dana page")
	def openDompetDigitalDana() {
		Mobile.callTestCase(findTestCase('Test Cases/Dompet Digital/Open Dompet Digital Menu'),[:],FailureHandling.STOP_ON_FAILURE)
	}

	@When("I open top up baru menu")
	def openTopUpBaruMenu() {
		Mobile.callTestCase(findTestCase('Test Cases/Dompet Digital/Open Top Up Baru Menu'),[:],FailureHandling.STOP_ON_FAILURE)
	}

	@And("I select dompet digital and input phone number")
	def selectDanaAndInputPhoneNumber() {
		Mobile.callTestCase(findTestCase('Test Cases/Dompet Digital/Select Dompet Digital and Input Phone Number'),[:],FailureHandling.STOP_ON_FAILURE)
	}

	@And("I input amount and save name on nominal page")
	def inputDanaNominalPage() {
		Mobile.callTestCase(findTestCase('Test Cases/Dompet Digital/Input Nominal Page'),[:],FailureHandling.STOP_ON_FAILURE)
	}

	@And("I see the sumber dana list")
	def sumberDanaList() {
		Mobile.callTestCase(findTestCase('Test Cases/Dompet Digital/Open Sumber Dana List'),[:],FailureHandling.STOP_ON_FAILURE)
	}

	@Then("Show konfirmasi dana page")
	def konfirmasiDanaPage() {
		Mobile.callTestCase(findTestCase('Test Cases/Dompet Digital/Go To Konfirmasi Page'),[:],FailureHandling.STOP_ON_FAILURE)
	}
	
	@Given("Login with (.*) and (.*)")
	def login(String username, String password) {
		Mobile.callTestCase(findTestCase('Test Cases/Dompet Digital/Abnormal/Login Blokir'), [('username') : username, ('password') : password], FailureHandling.STOP_ON_FAILURE)
	}
	
	@When("Choose linkaja and input (.*) number")
	def choose_linkaja(String phone) {
		Mobile.callTestCase(findTestCase('Test Cases/Dompet Digital/Abnormal/Inputnumber'), [('phone') : phone], FailureHandling.STOP_ON_FAILURE)
	}
	
	@When("Confirmation lupa pin")
	def lupapinconf() {
		Mobile.tap(findTestObject('Object Repository/Dompet Digitals/Abnormal/android.widget.Button - Top Up'), 0)
	}
	
	@When("Forget PIN (.*)")
	def forget_pin(String password) {
		Mobile.callTestCase(findTestCase('Test Cases/Dompet Digital/Abnormal/Resetpin'), [('password') : password], FailureHandling.STOP_ON_FAILURE)
	}
	
	@When("Confirmation Page")
	def confirmation_page() {
		Mobile.tap(findTestObject('Object Repository/Dompet Digitals/Abnormal/topupakhir'), 0)
		Mobile.delay(300)
	}
	
	@When("Turn ON offline mode (.*)")
	def offlinemode(String phone) {
		Mobile.callTestCase(findTestCase('Test Cases/Dompet Digital/Abnormal/offlinecase'), [('phone') : phone], FailureHandling.STOP_ON_FAILURE)
	}
	
	@When("Session habis")
	def session_habis() {
		Mobile.callTestCase(findTestCase('Test Cases/Dompet Digital/Abnormal/timeout'), [:], FailureHandling.STOP_ON_FAILURE)
	}
	
	@When("Try to input PIN")
	def trytoinputpin() {
		Mobile.callTestCase(findTestCase('Test Cases/Dompet Digital/Abnormal/salahpin'), [:], FailureHandling.STOP_ON_FAILURE)
	}
	
	@Then("Transaction success dompet digital")
	def transactionDompetDigital() {
		Mobile.callTestCase(findTestCase('Test Cases/Dompet Digital/Transaksi Berhasil'), [:], FailureHandling.STOP_ON_FAILURE)
	}
}