package pulsadata
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When

class data {
	@When("Open icon kontak")
	def openIconKontak() {
		Mobile.tap(findTestObject('Object Repository/Dashboard/Pulsa Data'),0)
		Mobile.tap(findTestObject('Object Repository/icon kontak'), 0)
	}

	@When("Open Pulsa/Data page using (.*) method and phone Number is (.*) with amount (.*) and user (.*)")
	def openPulsaDataPage(String method, String phoneNumber, String amount, String user) {
		Mobile.callTestCase(findTestCase('Test Cases/PulsaData/PulsaData'), [('method') : method, ('phoneNumber') : phoneNumber, ('amount') : amount, ('user') : user], FailureHandling.STOP_ON_FAILURE)
	}

	@When("Open pulsa data page")
	def pulsaDataPage() {
		Mobile.callTestCase(findTestCase('Test Cases/PulsaData/Open Pulsa Data'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@When("Purchase data by manual phone number")
	def purcahsePacketDataByManualPhoneNumber() {
		Mobile.callTestCase(findTestCase('Test Cases/PulsaData/Purchace Data Manual'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@When("Open sumber dana page")
	def openSumberDanaPage() {
		Mobile.callTestCase(findTestCase('Test Cases/PulsaData/Sumber Dana'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@When("Open confirmation page pulsa data")
	def openKonfirmasiPage() {
		Mobile.callTestCase(findTestCase('Test Cases/PulsaData/Konfirmasi'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@When("Purchase data by icon contact (.*)")
	def purchaseDataByIconContact(String provider) {
		Mobile.callTestCase(findTestCase('Test Cases/PulsaData/Purchase Data Icon Kontak'), [('provider'):provider], FailureHandling.STOP_ON_FAILURE)
	}

	@When("Purchase data by history")
	def purchaseDataByHistory() {
		Mobile.callTestCase(findTestCase('Test Cases/PulsaData/Purchase Data by History'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@When("Turn ON Offline mode on pulsa data")
	def offlineMode() {
		Mobile.callTestCase(findTestCase('Test Cases/PulsaData/Offline Function'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("Transaction Paket Data is Successfull")
	def transactionPulsaDataSuccess() {
		Mobile.callTestCase(findTestCase('Test Cases/PulsaData/Transaksi Berhasil'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("I Confirmation My package")
	def confirmationMyPackage() {
		Mobile.callTestCase(findTestCase('Test Cases/PulsaData/Confirmation My Package'),[:],FailureHandling.STOP_ON_FAILURE)
	}
}