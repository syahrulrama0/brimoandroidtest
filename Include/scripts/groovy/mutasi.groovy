import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When



class mutasi {
	/**
	 * The step definitions below match with Katalon sample Gherkin steps
	 */
	@Given("open application using (.*) and (.*)")
	def openapp(String username, String password) {
		Mobile.callTestCase(findTestCase('Test Cases/Listrik/Login'), [('username') : username, ('password') : password], FailureHandling.STOP_ON_FAILURE)
	}

	@When("Successfully go to dashboard i go to mutasi menu")
	def mutasimenu() {
		Mobile.callTestCase(findTestCase('Test Cases/Mutasi/Menu Mutasi'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@When("Bank account (.*)")
	def bankacc(String sumberdana) {
		Mobile.callTestCase(findTestCase('Test Cases/Mutasi/mutasirek'), [('sumberdana') : sumberdana], FailureHandling.STOP_ON_FAILURE)
	}

	@When("The date on rowNumber")
	def thedate() {
		Mobile.callTestCase(findTestCase('Test Cases/Mutasi/Date'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@When("Choose date manually (.*)")
	def choosemanually(String date) {
		Mobile.callTestCase(findTestCase('Test Cases/Mutasi/Choose Date Manually'), [('date') : date], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("I tap Cari button to check it")
	def cari() {
		Mobile.callTestCase(findTestCase('Test Cases/Mutasi/Cari'),[:], FailureHandling.STOP_ON_FAILURE)
	}

	//	@When("Done i verify")
	//	def verify() {
	//		Mobile.callTestCase(findTestCase('Test Cases/Mutasi/Verify'), [:], FailureHandling.STOP_ON_FAILURE)
	//	}
}