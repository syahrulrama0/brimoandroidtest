import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When



class AsuransiPrudential {
	/**
	 * The step definitions below match with Katalon sample Gherkin steps
	 */
	@Given("Login where (.*) and (.*)")
	def login (String username , String password) {
		Mobile.callTestCase(findTestCase('Test Cases/Listrik/Login'), [('username') : username, ('password') : password], FailureHandling.STOP_ON_FAILURE)
	}

	@When("Open Asuransi Page")
	def opendasuransi() {
		Mobile.callTestCase(findTestCase('Test Cases/Asuransi Prudential/Open Dashboard'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@When("New payment with (.*) and (.*)")
	def newpayment(String tipe , String polis) {
		Mobile.callTestCase(findTestCase('Test Cases/Asuransi Prudential/New Payment'), [('tipe') : tipe , ('polis') : polis], FailureHandling.STOP_ON_FAILURE)
	}

	@When("Continue payment (.*) and (.*)")
	def continuepayment(String tipe , String polis) {
		Mobile.callTestCase(findTestCase('Test Cases/Asuransi Prudential/Continue Payment'), [('tipe') : tipe , ('polis') : polis], FailureHandling.STOP_ON_FAILURE)
	}

	@When("Input a payment for (.*) dan (.*)")
	def inputpayment(String nominal , String sumberdana) {
		Mobile.callTestCase(findTestCase('Test Cases/Asuransi Prudential/Input Nominal payment'), [('nominal') : nominal , ('sumberdana') : sumberdana], FailureHandling.STOP_ON_FAILURE)
	}

	@When("If Less balance (.*) dan (.*)")
	def lessbalance(String nominal , String kondisi) {
		Mobile.callTestCase(findTestCase('Test Cases/Asuransi Prudential/Less Balance'), [('nominal') : nominal , ('kondisi') : kondisi], FailureHandling.STOP_ON_FAILURE)
	}

	@When("Nominal (.*)")
	def nominal(String nominal) {
		Mobile.callTestCase(findTestCase('Test Cases/Asuransi Prudential/New Payment Nominal'), [('nominal') : nominal], FailureHandling.STOP_ON_FAILURE)
	}

	@When("Try to input Sumber Dana (.*)")
	def inputpaymentnominal(String sumberdana) {
		Mobile.callTestCase(findTestCase('Test Cases/Asuransi Prudential/Sumber Dana'), [('sumberdana') : sumberdana], FailureHandling.STOP_ON_FAILURE)
	}
	
	@When("For premi lanjutan (.*)")
	def forpaymentlanjutan(String nominal) {
		Mobile.callTestCase(findTestCase('Test Cases/Asuransi Prudential/Nominal Payment Premi Lanjutan'), [('nominal') : nominal], FailureHandling.STOP_ON_FAILURE)
	}

	@When("Open confirmation page")
	def openconfirmation() {
		Mobile.callTestCase(findTestCase('Test Cases/Asuransi Prudential/Confirmation Page'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@When("Input PIN Prudential(.*)")
	def inputPIN(String PIN) {
		Mobile.callTestCase(findTestCase('Test Cases/Listrik/Pin'), [("PIN") : PIN ], FailureHandling.STOP_ON_FAILURE)
	}

	@When("Payment page Biaya")
	def biayacetakkartu() {
		Mobile.tap(findTestObject('Object Repository/Asuransi Prudential/ButtonBayarKonfirmasi'), 0)
	}

	@When("Save no polis to daftar simpan (.*) and input (.*)")
	def savenama(String nama, String nominal) {
		Mobile.callTestCase(findTestCase('Test Cases/Asuransi Prudential/Save No Polis'), [('nama') : nama, ('nominal') : nominal], FailureHandling.STOP_ON_FAILURE)
	}

	@When("Save no polis to daftar simpan (.*)")
	def save(String nama) {
		Mobile.callTestCase(findTestCase('Test Cases/Asuransi Prudential/Save No Polis Tanpa Nominal'), [('nama') : nama], FailureHandling.STOP_ON_FAILURE)
	}

	@When("Daftar Asuransi Page")
	def daftar() {
		Mobile.callTestCase(findTestCase('Test Cases/Asuransi Prudential/Edit Daftar Simpan'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@When("Tap three dots and edit nama (.*)")
	def threedotsedit(String ubahnama) {
		Mobile.callTestCase(findTestCase('Test Cases/Asuransi Prudential/Edit Name'), [('ubahnama') : ubahnama], FailureHandling.STOP_ON_FAILURE)
	}

	@When("Search the name (.*)")
	def searchname(String cari) {
		Mobile.callTestCase(findTestCase('Test Cases/Asuransi Prudential/Search Name'), [('cari') : cari], FailureHandling.STOP_ON_FAILURE)
	}

	@When("Tap three dots and favorite")
	def threedothapus() {
		Mobile.callTestCase(findTestCase('Test Cases/Asuransi Prudential/Favorite Polis'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@When("Tap three dots and unfavorite")
	def threedotunfavorite() {
		Mobile.callTestCase(findTestCase('Test Cases/Asuransi Prudential/Unfavorite Polis'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@When("Tap three dots and delete")
	def threedotdelete() {
		Mobile.callTestCase(findTestCase('Test Cases/Asuransi Prudential/Delete Polis'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@When("Cari dari daftar simpan (.*)")
	def caridaftarsimpan(String caridaftar) {
		Mobile.callTestCase(findTestCase('Test Cases/Asuransi Prudential/Find Name'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@When("Turn off connection")
	def turnonoffline() {
		Mobile.callTestCase(findTestCase('Test Cases/forscript/Offlinecase'), null)
	}

	@When("Try to input abnormal (.*)")
	def abnormalamount() {
		Mobile.callTestCase(findTestCase('Test Cases/Asuransi Prudential/Abnormal Amount'), null)
	}

	@When("Forgot PIN and input password (.*)")
	def ForgetPIN(String password) {
		Mobile.callTestCase(findTestCase('Test Cases/Asuransi Prudential/Forget PIN'), [('password') : password], FailureHandling.STOP_ON_FAILURE)
	}

	@When("Try to input (.*) and (.*)")
	def trytoinputPIN(String PIN, String status) {
		Mobile.callTestCase(findTestCase('Test Cases/Asuransi Prudential/WrongPIN'), [('PIN') : PIN, ('status') : status ], FailureHandling.STOP_ON_FAILURE)
	}
	
	@When("Click new payment asurance")
	def clickNewPayment() {
		Mobile.callTestCase(findTestCase('Test Cases/Asuransi Prudential/New Payment'),0)
	}
}