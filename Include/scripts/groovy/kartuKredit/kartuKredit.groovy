package kartuKredit
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When



class kartuKredit {
	@When("Open Credit Card Menu")
	def Open_Credit_Card_Menu() {
		Mobile.callTestCase(findTestCase('Test Cases/Kartu Kredit/0 Base Code/Menu Kartu Kredit'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@When("User Click method Pembayaran Baru")
	def User_Click_method_Pembayaran_Baru() {
		Mobile.callTestCase(findTestCase('Test Cases/Kartu Kredit/0 Base Code/Pembayaran Baru'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@When("User Select Bank")
	def User_Select_Bank() {
		Mobile.callTestCase(findTestCase('Test Cases/Kartu Kredit/0 Base Code/Pembayaran Baru - Select Bank'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@When("User Input Destination Credit Number")
	def User_Input_Destination_Credit_Number() {
		Mobile.callTestCase(findTestCase('Test Cases/Kartu Kredit/0 Base Code/Pembayaran Baru - Input Credit Number'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@When("User Input Nominal Payment")
	def User_Input_Nominal_Payment() {
		Mobile.callTestCase(findTestCase('Test Cases/Kartu Kredit/0 Base Code/Input Nominal Pembayaran'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@When("User Input bill amount (.*)")
	def User_Input_bill_amount(String nominalPayment) {
		Mobile.callTestCase(findTestCase('Test Cases/Kartu Kredit/0 Base Code/Form Tagihan Himbara'), [('nominalPayment'):nominalPayment], FailureHandling.STOP_ON_FAILURE)
	}

	@When("Select Sumber Dana for Himbara Credit Payment (.*)")
	def User_Select_Sumber_Dana_for_Himbara_Credit_Payment(String sumberDana) {
		Mobile.callTestCase(findTestCase('Test Cases/Kartu Kredit/0 Base Code/Pilih Sumber Dana Himbara'), [('sumberDana'):sumberDana], FailureHandling.STOP_ON_FAILURE)
	}

	@When("Confrm payment detail")
	def Confrm_payment_detail() {
		Mobile.callTestCase(findTestCase('Test Cases/Kartu Kredit/0 Base Code/Konfirmasi Pembayaran'), [:], FailureHandling.STOP_ON_FAILURE)
	}
	@Then("Screen displays payment receipt")
	def Screen_displays_payment_receipt() {
		Mobile.callTestCase(findTestCase('Test Cases/Kartu Kredit/0 Base Code/Transaksi Sukses'), [:], FailureHandling.STOP_ON_FAILURE)
	}
	@When("Input PIN (.*)")
	def ConditionalPIN(String number) {
		Mobile.callTestCase(findTestCase('Test Cases/Kartu Kredit/0 Base Code/PIN'), [('number') : number], FailureHandling.STOP_ON_FAILURE)
	}
	@When("Confirms the contents of the payment receipt")
	def Confirms_the_contents_of_the_payment_receipt() {
		Mobile.callTestCase(findTestCase('Test Cases/Kartu Kredit/0 Base Code/Konten Struk Pembayaran'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@When("User Select Condition")
	def User_Select_Condition() {
		Mobile.callTestCase(findTestCase('Test Cases/Kartu Kredit/0 Base Code/Payment Condition'), [:], FailureHandling.STOP_ON_FAILURE)
	}
}