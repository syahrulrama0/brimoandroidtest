import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When



class pulsa {
	/**
	 * The step definitions below match with Katalon sample Gherkin steps
	 */

//	@Given("Login with existing account where (.*) and (.*)")
//	def loginexisting(String username, String password) {
//		Mobile.callTestCase(findTestCase('Test Cases/Top Up Shopee Pay/Login'), [('username') : username, ('password') : password], FailureHandling.STOP_ON_FAILURE)
//	}

//	@When("Open Dashboard page")
//	def pressdompetdigitalicon(){
//		Mobile.callTestCase(findTestCase('Test Cases/Pulsa/Laman Awal'), [:], FailureHandling.STOP_ON_FAILURE)
//	}

	//	@When("User choose phone number by (.*), (.*), (\\d+) and (.*)")
	//	def user_choose_phone_number(String method, String phoneNumber, int amount, String user) {
	//		Mobile.callTestCase(findTestCase('Test Cases/Pulsa/Pulsa'), [ ('method'):method, ('phoneNumber'):phoneNumber, ('amount'):amount, ('user'):user], FailureHandling.STOP_ON_FAILURE)
	//	}
	@When("User Input phone number with method getContact (.*)")
	def User_Input_phone_number_with_method_getContact(String contactName) {
		Mobile.callTestCase(findTestCase('Test Cases/Pulsa/Method getContact'), [('contactName'):contactName], FailureHandling.STOP_ON_FAILURE)
	}

	@When("User Input Manual Number")
	def User_Input_Manual_Number() {
		Mobile.callTestCase(findTestCase('Test Cases/Pulsa/Method Input Manual Number'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@When("User Fills in the Phone Number field with Purchase History")
	def User_Fills_in_the_Phone_Number_Field_with_Purchase_History() {
		Mobile.callTestCase(findTestCase('Test Cases/Pulsa/Method by History'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@When("User Verify Operator Number")
	def User_Verify_Operator_Number() {
		Mobile.callTestCase(findTestCase('Test Cases/Pulsa/Verifikasi Operator'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@When("User Choose Amount")
	def User_Choose_Amount() {
		Mobile.callTestCase(findTestCase('Test Cases/Pulsa/Nominal Pembelian Pulsa'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@When("Turn on offline mode if user choose amount (\\d+)")
	def User_Choose_Amount_with_Offline_Mode(int amount) {
		Mobile.callTestCase(findTestCase('Test Cases/Pulsa/Offline Mode'), [('amount'):amount], FailureHandling.STOP_ON_FAILURE)
	}

	@When("User Choose Rekening for payment")
	def User_choose_rekening_number_for_payment() {
		Mobile.callTestCase(findTestCase('Test Cases/Pulsa/Pilih Rekening'), [:],FailureHandling.STOP_ON_FAILURE)
	}

	@When("User confrm payment detail")
	def User_confrm_payment_detail() {
		Mobile.callTestCase(findTestCase('Test Cases/Pulsa/Konfirmasi Pembelian'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@When("User Input PIN (.*)")
	def inputConditionalPIN(String number) {
		Mobile.callTestCase(findTestCase('Test Cases/Pulsa/PIN'), [('number') : number], FailureHandling.STOP_ON_FAILURE)
	}

	@When("User Input Incorrect PIN")
	def User_Input_Incorrect_PIN() {
		Mobile.callTestCase(findTestCase('Test Cases/Pulsa/Incorrect PIN'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@When("User Click forgot PIN")
	def User_Click_forgot_PIN() {
		Mobile.callTestCase(findTestCase('Test Cases/Pulsa/Forgot PIN'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("The screen displays payment receipt")
	def The_screen_displays_payment_receipt() {
		Mobile.callTestCase(findTestCase('Test Cases/Pulsa/Sukses Payment'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@When("user confirms the contents of the payment receipt")
	def user_confirms_the_contents_of_the_payment_receipt() {
		Mobile.callTestCase(findTestCase('Test Cases/Pulsa/Content Struk Pembayaran'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@When("User Input Invalid Phone Number (.*)")
	def User_Input_Invalid_Phone_Number(String phoneNumber) {
		Mobile.callTestCase(findTestCase('Test Cases/Pulsa/Invalid Phone Number (Negative Case)'), [('phoneNumber'):phoneNumber], FailureHandling.STOP_ON_FAILURE)
	}
}