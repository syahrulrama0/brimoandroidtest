import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When



class topupshopeepay {
	/**
	 * The step definitions below match with Katalon sample Gherkin steps
	 */
	@Given("I open an application with username Shopee (.*) and password (.*)")
	def iOpenAnApplicationShoopee(String username, String password) {
		Mobile.callTestCase(findTestCase('Test Cases/Common/Start Existing Application'),[('username'):username,('password'):password],FailureHandling.STOP_ON_FAILURE)
	}

	@When("Open dompet digital page")
	def pressdompetdigitalicon(){
		Mobile.callTestCase(findTestCase('Test Cases/Top Up Shopee Pay/Dompet Digital'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@When("Fill the blank space for (.*) and (.*)")
	def filltheblank(String wallet, String phone){
		Mobile.callTestCase(findTestCase('Test Cases/Top Up Shopee Pay/Wallet and Phone'), [('wallet') : wallet, ('phone') : phone], FailureHandling.STOP_ON_FAILURE)
	}

	@When("Input nominal payment (.*)")
	def inputnominalpayment(String nominal){
		Mobile.callTestCase(findTestCase('Test Cases/Top Up Shopee Pay/nominal'), [('nominal') : nominal], FailureHandling.STOP_ON_FAILURE)
	}

	@When("Go to the confirmation page")
	def confirmationpage() {
		Mobile.tap(findTestObject('Object Repository/ShopeePayTopUp/topupkonfirmasi'), 0)
	}

	@When("Input Pin (.*)")
	def inputpin(String PIN){
		Mobile.callTestCase(findTestCase('Test Cases/Top Up Shopee Pay/PIN'), [('PIN'): PIN ], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("The Transaction Success")
	def transactionsuccess(){
		Mobile.tap(findTestObject('Object Repository/ShopeePayTopUp/OKbutton'), 0)
	}
}