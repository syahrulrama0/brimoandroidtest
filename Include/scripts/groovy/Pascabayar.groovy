import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When



class Pascabayar {
	/**
	 * The step definitions below match with Katalon sample Gherkin steps
	 */
	@When("Pada Fitur Lainnya klik Pascabayar")
	def	klikPascabayar() {
		Mobile.callTestCase(findTestCase('Test Cases/Actionv2/Dashboard/Click Pascabayar'), [:], FailureHandling.STOP_ON_FAILURE)
	}
	@When("User klik Pembayaran Baru")
	def	klikPembayaranBaru() {
		Mobile.callTestCase(findTestCase('Test Cases/Actionv2/Pascabayar/New Form/Click Pembayaran Baru'), [:], FailureHandling.STOP_ON_FAILURE)
	}
	
	@When("Input Nomor Handphone")
	def	inputNomorPascabayar() {
		Mobile.callTestCase(findTestCase('Test Cases/Actionv2/Pascabayar/New Form/Input Nomor Handphone'), [:], FailureHandling.STOP_ON_FAILURE)
	}
	
	@When("Pada halaman Pascabayar Klik Lanjutkan")
	def	padaHalamanPascabayarKlikLanjutkan() {
		Mobile.callTestCase(findTestCase('Test Cases/Actionv2/Pascabayar/New Form/Click Lanjutkan'), [:], FailureHandling.STOP_ON_FAILURE)
	}
	@When("User pilih condition")
	def userPilihKondisi() {
		Mobile.callTestCase(findTestCase('Test Cases/Actionv2/Pascabayar/Tagihan/Sumber Dana'), [:], FailureHandling.STOP_ON_FAILURE)
	}
	@When("Pada halaman Konfirmasi klik Bayar")
	def	padaHalamanKonfirmasiKlikBayar() {
		Mobile.callTestCase(findTestCase('Test Cases/Actionv2/Pascabayar/Confirmation/Click Bayar'), [:], FailureHandling.STOP_ON_FAILURE)
	}
	@When("Transaksi Berhasil klik OK")
	def	transaksiBerhasil() {
		Mobile.callTestCase(findTestCase('Test Cases/Actionv2/Pascabayar/Transaction/Transaction Success'), [:], FailureHandling.STOP_ON_FAILURE)
	}
}