import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When



class TV {
	/**
	 * The step definitions below match with Katalon sample Gherkin steps
	 */
	@Given("Start the application (.*) and (.*)")
	def theapp(String username, String password) {
		Mobile.callTestCase(findTestCase('Test Cases/Listrik/Login'), [('username') : username, ('password') : password], FailureHandling.STOP_ON_FAILURE)
	}

	@When("Open tv menu from daftar menu lain")
	def tvmenu() {
		Mobile.callTestCase(findTestCase('Test Cases/TV/TV Menu'), [ : ], FailureHandling.STOP_ON_FAILURE)
	}

	@When("I make a new payment for tv from produk and account number")
	def tvproduct() {
		Mobile.callTestCase(findTestCase('Test Cases/TV/Produk TV'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@When("Next to the confirmation page and (.*) rek")
	def confirmpage(String sumberdana) {
		Mobile.callTestCase(findTestCase('Test Cases/TV/Choose Rek'), [('sumberdana') : sumberdana ], FailureHandling.STOP_ON_FAILURE)
	}

	@When("Input the right PIN (.*)")
	def rightPIN(String pin) {
		Mobile.callTestCase(findTestCase('Test Cases/Listrik/Pin'), [('pin') : pin], FailureHandling.STOP_ON_FAILURE)
	}

	//	@Then("Success doing payment")
	//	def successpayment() {
	//		Mobile.tap(findTestObject(''), 0)
	//	}
}