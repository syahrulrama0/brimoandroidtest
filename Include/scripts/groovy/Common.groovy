import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When



class Common {
	//Run Existing Application
	
	@Given("Login with existing account where (.*) and (.*)")
	def commonloginWithExixstingApplication(String username, String password) {
		Mobile.callTestCase(findTestCase('Test Cases/Common/Start Existing Application'), [('username') : username, ('password') : password], FailureHandling.STOP_ON_FAILURE)
	}

	@Given("Login on other phone (.*) and (.*)")
	def firstTimeLoginApplication(String username, String password) {
		Mobile.callTestCase(findTestCase('Test Cases/Login Method/First Time Login'), [('username') : username, ('password') : password], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("Close Application")
	def closeApplication() {
		Mobile.closeApplication()
	}

	@When("Open Dashboard page")
	def openDashboardPage() {
		Mobile.callTestCase(findTestCase('Test Cases/Common/Dashboard'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@When("Open Dompet Digital Page with (.*) , (.*) and (.*)")
	def dompetDigitalPage(String contact, String history, String topUpVia) {
		Mobile.callTestCase(findTestCase('Dompet Digital/DompetDigital'), [('contact') : contact, ('history') : history, ('topUpVia') : topUpVia], FailureHandling.STOP_ON_FAILURE)
	}

	@When("Open Top Up Baru Page with (.*) , (.*) and (.*)")
	def openTopUpBaruPage(String wallet, String phoneNumber, String viaContact) {
		Mobile.callTestCase(findTestCase('Dompet Digital/Top Up Baru'), [('wallet') : wallet, ('phoneNumber') : phoneNumber, ('viaContact') : viaContact], FailureHandling.STOP_ON_FAILURE)
	}

	@When("Open Nominal Page wih (.*) , (.*) and (.*)")
	def openNominalPage(String amount, String status, String alias) {
		Mobile.callTestCase(findTestCase('Dompet Digital/Nominal'), [('amount') : amount, ('status') : status, ('alias') : alias], FailureHandling.STOP_ON_FAILURE)
	}

	@When("Open Konfirmasi Page")
	def openKonfirmasiPage() {
		Mobile.callTestCase(findTestCase('Dompet Digital/Konfirmasi'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@When("Input (.*) PIN (.*)")
	def inputConditionalPIN(String condition, String number) {
		Mobile.callTestCase(findTestCase('Dompet Digital/PIN'), [('condition') : condition, ('number') : number], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("Open Lupa PIN Page")
	def LupaPIN() {
		Mobile.callTestCase(findTestCase('Test Cases/Dompet Digital/Lupa PIN'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("Transaction Is Succesfull")
	def transaksiBerhasil() {
		Mobile.callTestCase(findTestCase('Dompet Digital/Transaksi Berhasil'), [:], FailureHandling.STOP_ON_FAILURE)
	}
}