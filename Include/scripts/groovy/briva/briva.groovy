package briva
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When



class briva {

	@When("I Open BRIVA Menu")
	def openBrivaMenu() {
		Mobile.callTestCase(findTestCase('Test Cases/Briva/Open Briva Menu'),[:],FailureHandling.STOP_ON_FAILURE)
	}

	@When("I Open Menu Pembayaran Baru")
	def openMenuPembayaranBaru() {
		Mobile.callTestCase(findTestCase('Test Cases/Briva/Open Pembayaran Baru Menu'),[:],FailureHandling.STOP_ON_FAILURE)
	}

	@Then("I Input Destination Number on Pembayaran Baru Menu")
	def inputDestinationNumber() {
		Mobile.callTestCase(findTestCase('Test Cases/Briva/Input Pembayaran Baru Destination Number'),[:],FailureHandling.STOP_ON_FAILURE)
	}

	@When("I Pay Briva BRIVA via History")
	def payBrivaViaHistory() {
		Mobile.callTestCase(findTestCase('Test Cases/Briva/Briva Via History'),[:],FailureHandling.STOP_ON_FAILURE)
	}

	@When("I Open Nominal Briva Page with amount (.*) and save the name with (.*)")
	def openNominalBrivaPage(String amount, String saveName) {
		Mobile.callTestCase(findTestCase('Test Cases/Briva/Nominal Page'),[('amount'):amount,('saveName'):saveName],FailureHandling.STOP_ON_FAILURE)
	}

	@When("I Open Sumber Dana List on Briva")
	def sumberDanaPage() {
		Mobile.callTestCase(findTestCase('Test Cases/Briva/Sumber Dana Page'),[:],FailureHandling.STOP_ON_FAILURE)
	}

	@When("I Open Konfirmasi Page")
	def konfirmasiPage() {
		Mobile.callTestCase(findTestCase('Test Cases/Briva/Konfirmasi'),[:],FailureHandling.STOP_ON_FAILURE)
	}

	@When("I Verify Element on Konfirmasi Page")
	def verifyKonfirmasiPage() {
		Mobile.callTestCase(findTestCase('Test Cases/Briva/Verify Konfirmasi Page'),[:],FailureHandling.STOP_ON_FAILURE)
	}

	@Then("Briva Transaction Is Succesfull")
	def brivaTransactionSuccesfull() {
		Mobile.callTestCase(findTestCase('Test Cases/Briva/Transaction Successfull'),[:],FailureHandling.STOP_ON_FAILURE)
	}

	@When("I Open Tagihan Page and want to save the contact")
	def brivaTagihanPage() {
		Mobile.callTestCase(findTestCase('Test Cases/Briva/Briva Tagihan'),[:],FailureHandling.STOP_ON_FAILURE)
	}

	@When("I Open Konfirmasi Tagihan Page")
	def brivaTagihanKonfirmasiPage() {
		Mobile.callTestCase(findTestCase('Test Cases/Briva/Briva Tagihan Konfirmasi'),[:],FailureHandling.STOP_ON_FAILURE)
	}
}