import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When



class Telkom {
	/**
	 * The step definitions below match with Katalon sample Gherkin steps
	 */
	@Given("Click menu telkom")
	def Click_menu_telkom() {
		Mobile.tap(findTestObject('Object Repository/Tarik Tunai Object/Icon Menu Lainnya'), 0)
		Mobile.tap(findTestObject('Object Repository/Telkom Object/Telkom - Icon Menu Telkom'), 0)
	}

	@When("Click button Pembayaran Baru")
	def Click_button_Pembayaran_Baru() {
		Mobile.tap(findTestObject('Object Repository/Telkom Object/Telkom - Pembayaran Baru'), 0)
	}

	@And("Input Nomor Telepon")
	def inputNomorTelepon() {
		Mobile.callTestCase(findTestCase('Test Cases/Telkom/Input Nomor Telepon'),[:],FailureHandling.STOP_ON_FAILURE)
//		Mobile.setText(findTestObject('Object Repository/Telkom Object/Telkom- Nomor Telepon'), nomor, 0)
	}

	@And("Click button Telkom Lanjutkan")
	def Input_Nomor_Telepon() {
		
		Mobile.tap(findTestObject('Object Repository/Telkom Object/Button - Lanjutkan Enable'), 0)
	}
	@And("Tap Lihat Detail Tagihan")
	def Tap_Lihat_Detail_Tagihan() {
		Mobile.tap(findTestObject('Object Repository/Telkom Object/tap button - Lihat Detail Tagihan'), 0)
	}
	@When("Pilih Telkom Sumber Dana (.*)")
	def Pilih_Telkom_Sumber_Dana(String sumberdana) {
		Mobile.callTestCase(findTestCase('Test Cases/Transfer/Halaman Sumber Dana'), [('sumberdana'):sumberdana], FailureHandling.STOP_ON_FAILURE)
	}
	@And("Click button bayar telkom")
	def Click_button_bayar_telkom() {
		Mobile.tap(findTestObject('Object Repository/Telkom Object/Telkom Button - Bayar'), 0)
	}
	
	@Then("Konfirmasi detail bayar")
	def Konfirmasi_detail_bayar()
	{
		Mobile.callTestCase(findTestCase('Test Cases/Telkom/Telkom Halaman Konfirmasi'),[:], FailureHandling.STOP_ON_FAILURE)

	}
	@And("Click button bayar telkom2")
	def Click_button_bayar_telkom2() {
		Mobile.tap(findTestObject('Object Repository/Telkom Object/Telkom Button - Bayar'), 0)
	}

	@And("Input Telkom PIN (\\d+)")
	def Input_Telkom_PIN(int rowNumber) {

		Mobile.callTestCase(findTestCase('Test Cases/Telkom/Telkom Halaman PIN'), [('rowNumber'):rowNumber], FailureHandling.STOP_ON_FAILURE)
	}
	@And("Halaman Transaksi Telkom")
	def Halaman_Transaksi_Telkom() {
		Mobile.callTestCase(findTestCase('Test Cases/Telkom/Telkom Halaman Transaksi'), [:], FailureHandling.STOP_ON_FAILURE)
	}
	
	
	@When("Pilih Daftar Telkom (.*)")
	def Pilih_Daftar_Telkom(String nama) {
		Mobile.tap(findTestObject('Object Repository/Telkom Object/Telkom - Daftar Telkom (1)'), 0)
		//Mobile.callTestCase(findTestCase('Halaman Pilih Telkom Daftar Transfer'), [('nama'):nama], FailureHandling.STOP_ON_FAILURE)
	}

	@When("Pilih Telkom Pembayaran Terakhir (.*)")
	def Pilih_Telkom_Pembayaran_Terakhir(String nama) {
		Mobile.tap(findTestObject('Object Repository/Telkom Object/Telkom - Pembayaran Terakhir'), 0)
		//Mobile.callTestCase(findTestCase('Halaman Pilih Telkom Daftar Transfer'), [('nama'):nama], FailureHandling.STOP_ON_FAILURE)
	}

	@When("Pencarian Daftar Telkom (.*)")
	def Pencarian_Daftar_Telkom(String list_contact) {
		Mobile.setText(findTestObject('Object Repository/Telkom Object/Telkom - Cari Daftar'), list_contact, 0)
		//Mobile.callTestCase(findTestCase('Halaman Pilih Telkom Daftar Transfer'), [('nama'):nama], FailureHandling.STOP_ON_FAILURE)
	}
}