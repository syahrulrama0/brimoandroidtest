package donasi
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When



class donasi {
	@When("Open menu donasi")
	def openMenuDonasi() {
		Mobile.callTestCase(findTestCase('Test Cases/Donasi/Open Menu Donasi'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@When("Open pilih produk donasi")
	def openPilihProdukDonasi() {
		Mobile.callTestCase(findTestCase('Test Cases/Donasi/Open Pilih Produk Donasi'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@When("Open pilih jenis donasi")
	def openPilihJenisDonasi() {
		Mobile.callTestCase(findTestCase('Test Cases/Donasi/Open Pilih Jenis Donasi'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@When("Open select amount")
	def openSelectAmount() {
		Mobile.callTestCase(findTestCase('Test Cases/Donasi/Open Pilih Nominal'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@When("Select produk donasi")
	def selectProdukDonasi() {
		Mobile.callTestCase(findTestCase('Test Cases/Donasi/Select Produk Donasi'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@When("Select jenis donasi")
	def selectJenisDonasi() {
		Mobile.callTestCase(findTestCase('Test Cases/Donasi/Select Jenis Donasi'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@When("Select amount")
	def selectAmount() {
		Mobile.callTestCase(findTestCase('Test Cases/Donasi/Select Nominal'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@When("Input manual amount")
	def inputManualAmount() {
		Mobile.callTestCase(findTestCase('Test Cases/Donasi/Input Nominal'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("Open konfirmasi donasi menu")
	def openKonfirmasiDonasiMenu() {
		Mobile.callTestCase(findTestCase('Test Cases/Donasi/Konfirmasi'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@When("Open sumber dana donation page")
	def openSumberDanaPage() {
		Mobile.callTestCase(findTestCase('Test Cases/Donasi/Open Sumber Dana'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@When("Confirmation my paying")
	def confitmationMyPaying() {
		Mobile.callTestCase(findTestCase('Test Cases/Donasi/Pay'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("Transaction donasi is sucessfull")
	def trasactionDonasiIsSucessfull() {
		Mobile.callTestCase(findTestCase('Test Cases/Donasi/Transaksi Berhasil'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("Input abnormal nominal")
	def inputAbnormalNominal() {
		Mobile.callTestCase(findTestCase('Test Cases/Donasi/Abnormal Nominal'), [:], FailureHandling.STOP_ON_FAILURE)
	}
}