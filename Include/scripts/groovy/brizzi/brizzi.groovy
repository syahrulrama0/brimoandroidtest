package brizzi
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.sun.net.httpserver.Authenticator.Failure

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When



class brizzi {

	@When("Pay using account number and amount")
	def openBRIZZIMenuManual() {
		Mobile.callTestCase(findTestCase('Test Cases/Brizzi/Brizzi Manual'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@When("Open BRIZZI sumber dana page")
	def openBrizziSumberDanaPage() {
		Mobile.callTestCase(findTestCase('Test Cases/BRIZZI/Sumber Dana'),[:],FailureHandling.STOP_ON_FAILURE)
	}

	@Then("Open Brizzi confirmation page")
	def openBrizziConfirmationPage() {
		Mobile.callTestCase(findTestCase('Test Cases/Brizzi/Konfirmasi'),[:],FailureHandling.STOP_ON_FAILURE)
	}

	@Then("Transaction Brizzi success")
	def transactionBrizziSuccess() {
		Mobile.callTestCase(findTestCase('Test Cases/BRIZZI/Transaksi Berhasil'),[:],FailureHandling.STOP_ON_FAILURE)
	}

	@When("Open Brizzi scan menu")
	def openBrizziScanMenu() {
		Mobile.callTestCase(findTestCase('Test Cases/BRIZZI/Brizzi Scan Menu'),[:],FailureHandling.STOP_ON_FAILURE)
	}

	@And("I open konfirmasi page")
	def notEnoughSourceBrizzi() {
		Mobile.callTestCase(findTestCase('Test Cases/Brizzi/Open Konfirmasi Page'),[:],FailureHandling.STOP_ON_FAILURE)
	}

	@When("Open Brizzi Menu")
	def openBrizziMenu() {
		Mobile.callTestCase(findTestCase('Test Cases/Brizzi/Open Menu Brizzi'),[:],FailureHandling.STOP_ON_FAILURE)
	}

	@And("I open sumber dana list")
	def openListSumberDana() {
		Mobile.callTestCase(findTestCase('Test Cases/Brizzi/Open List Sumber Dana'),[:],FailureHandling.STOP_ON_FAILURE)
	}
}