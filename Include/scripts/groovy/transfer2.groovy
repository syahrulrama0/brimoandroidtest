import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When

//import common.screenshoot
//import common.SumberDana

class transfer2 {
	//def ss = new screenshoot()
	//def sd = new SumberDana()
	/**
	 * The step definitions below match with Katalon sample Gherkin steps
	 */
	@Given("Login Sukses")
	def loginSuccess() {
		Mobile.callTestCase(findTestCase('Test Cases/General/login sukses'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@When("Click menu transfer")
	def Click_menu_transfer() {
		
		Mobile.callTestCase(findTestCase('Test Cases/Transfer/Menu Transfer'),[:], FailureHandling.STOP_ON_FAILURE)
		//ss.takeScreenshotAsCheckpoint()
	}

	@And("Pilih history transfer (.*)")
	def Pilih_history_transfer(bank) {

		Mobile.callTestCase(findTestCase('Test Cases/Halaman Pilih History Terakhir'), [bank:bank], FailureHandling.STOP_ON_FAILURE)
		Mobile.delay(5)
	}

	@Then("Cari Daftar Transfer (.*)")

	def Cari_Daftar_Transfer(String list_contact) {

		//ss.takeScreenshotAsCheckpoint()

		Mobile.setText(findTestObject('Object Repository/Daftar Transfer/Search - Daftar Transfer'), list_contact, 0)

		//Hard_Code
		//Mobile.callTestCase(findTestCase('Test Cases/Halaman Search'), [:], FailureHandling.STOP_ON_FAILURE)

	}

	@And("Edit Daftar Transfer (.*)")
	def  Edit_Daftar_Transfer(String setting) {
		Mobile.callTestCase(findTestCase('Test Cases/Transfer/Halaman Setting Daftar Transfer'), [('setting'):setting], FailureHandling.STOP_ON_FAILURE)
	}


	@And("Pilih Daftar transfer (.*)")
	def Pilih_Daftar_transfer(String bank) {
		Mobile.callTestCase(findTestCase('Test Cases/Transfer/Halaman Pilih Daftar Transfer'), [('bank'):bank], FailureHandling.STOP_ON_FAILURE)
	}

	@And ("Pilihan Cara Transfer (.*) , (.*) and (.*)")
	def Pilihan_Cara_Transfer(String contact, String history, String pilihantransfer) {

		Mobile.callTestCase(findTestCase('Test Cases/Halaman Transfer'), [('contact'):contact, ('history'):history, ('pilihantransfer'):pilihantransfer], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("Click button Tambah Daftar Baru")
	def I_verify_the_status_in_step() {

		//ss.takeScreenshotAsCheckpoint()
		Mobile.tap(findTestObject('Object Repository/Test Transfer/Button Tambah Daftar Baru'), 0)
	}

	@And("Tap bank tujuan")
	def tap_bank_tujuan() {
		Mobile.callTestCase(findTestCase('Test Cases/Transfer/Halaman Bank Tujuan'), [:], FailureHandling.STOP_ON_FAILURE)

		Mobile.delay(5)
	}

	@And("Input Nomor Rekening")
	def Input_Nomor_Rekening() {

		Mobile.callTestCase(findTestCase('Test Cases/Transfer/Halaman Nomor Rekening'), [:], FailureHandling.STOP_ON_FAILURE)

	}



	@And("Click button Lanjutkan")
	def Click_button_Lanjutkan() {


		Mobile.tap(findTestObject('Object Repository/Transfer2/android.widget.Button - Lanjutkan'), 0)
	}

	@And("Click button Lanjutkan2 (.*)")
	def Click_button_Lanjutkan2(String nama) {

		//ss.takeScreenshotAsCheckpoint()

		if (nama.toString() =='rekening non kupedes') {
			Mobile.tap(findTestObject('Object Repository/Transfer2/android.widget.Button - Lanjutkan'), 0)
			Mobile.verifyElementVisible(findTestObject('Object Repository/Transfer2/Transfer 2-1/Verifikasi - Rekening Tujuan tidak di temukan'), 0)
		}
		else if (nama.toString() =='rekening kupedes'){
			Mobile.tap(findTestObject('Object Repository/Transfer2/android.widget.Button - Lanjutkan'), 0)
			Mobile.verifyElementVisible(findTestObject('Object Repository/Transfer2/Transfer 2-1/Verifikasi - Rekening Tujuan tidak di temukan'), 0)
		}
	}
	@And("Tap Simpan untuk selanjutnya")
	def Tap_Simpan_untuk_selanjutnya() {

		//ss.takeScreenshotAsCheckpoint()
		Mobile.tap(findTestObject('Object Repository/Test Transfer/CheckBox Simpan'), 0)
	}

	@And("Input (.*) Daftar Simpan")
	def Input_Daftar_Simpan(String nama) {

		//ss.takeScreenshotAsCheckpoint()
		Mobile.setText(findTestObject('Object Repository/Test Transfer/Input Simpan Sebagai- Nama'), nama, 0)
	}

	@When("Input Nominal Transfer")
	def Input_Nominal_Transfer() {

		Mobile.callTestCase(findTestCase('Test Cases/Transfer/Halaman Nominal Normal'), [:], FailureHandling.STOP_ON_FAILURE)

	}

	@When("Input Nominal Upnormal Transfer (.*)")
	def Input_Nominal_Upnormal_Transfer(String amount) {
		Mobile.callTestCase(findTestCase('Test Cases/Halaman Nominal Upnormal - Switch case'), [('amount'):amount], FailureHandling.STOP_ON_FAILURE)
		//Mobile.callTestCase(findTestCase('Test Cases/Halaman Nominal Upnormal'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@When("Pilih Transfer Sumber Dana (.*)")
	def Pilih_Transfer_Sumber_Dana(String sumberdana) {

		//sd.Sumber_Dana(sumberdana)
		Mobile.callTestCase(findTestCase('Test Cases/Transfer/Halaman Sumber Dana'), [('sumberdana'):sumberdana], FailureHandling.STOP_ON_FAILURE)
	}

	@And("Click button Transfer")
	def Click_button_Transfer() {


		Mobile.tap(findTestObject('Object Repository/Test Transfer/Sumber Dana - Button Transfer'), 0)
		Mobile.delay(3)
	}

	@And("Click button kembali")
	def  Click_button_kembali() {

		//ss.takeScreenshotAsCheckpoint()
		Mobile.tap(findTestObject('Object Repository/Transfer2/Transfer 2-1/android.widget.ImageButton1'), 0)

		Mobile.tap(findTestObject('Object Repository/Transfer2/Transfer 2-1/android.widget.Button - Ya'), 0)

		Mobile.tap(findTestObject('Object Repository/Transfer2/Transfer 2-1/android.widget.ImageButton1'), 0)

		Mobile.tap(findTestObject('Object Repository/Transfer2/Transfer 2-1/android.widget.ImageButton1'), 0)
	}

	@Then("Konfirmasi detail transaksi")
	def Konfirmasi_detail_transaksi()
	{
		Mobile.callTestCase(findTestCase('Test Cases/Transfer/Halaman Konfirmasi'),[:], FailureHandling.STOP_ON_FAILURE)

	}

	@And("Click button Transfer2")
	def Click_button_Transfer2() {
		//ss.takeScreenshotAsCheckpoint()
		Mobile.tap(findTestObject('Object Repository/Transfer/android.widget.Button - Transfer (1)'), 0)
	}

	@And("Input PIN (.*) (.*) (.*)")
	def Input_PIN(String condition, String pinPassword, String number) {

		Mobile.callTestCase(findTestCase('Test Cases/Transfer/Halaman PIN'), [('condition'):condition,('pinPassword'):pinPassword,('number'):number], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("input correct PIN (\\d+)")
	def input_correct_pin(int rowNumber) {

		Mobile.callTestCase(findTestCase('Test Cases/Transfer/Halaman PIN Normal'), [('rowNumber'):rowNumber], FailureHandling.STOP_ON_FAILURE)
	}


	@And("Click button Cek Status")
	def Click_button_Cek_Status() {

		//ss.takeScreenshotAsCheckpoint()
		Mobile.tap(findTestObject('Object Repository/Transfer2/Button - Cek Status Transaksi'), 0)
	}

	@And("Halaman Transaksi")
	def Halaman_Transaksi() {
		Mobile.callTestCase(findTestCase('Test Cases/Transfer/Halaman Transaksi'),[:], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("Share Bukti Transaksi")
	def Share_Bukti_Transaksi() {

		//ss.takeScreenshotAsCheckpoint()

		Mobile.tap(findTestObject('Object Repository/Transfer - Share Sosmed/android.widget.TextView (1)'), 0)

		Mobile.tap(findTestObject('Object Repository/Transfer2/Transfer 2-1/WA'), 0)
	}

	@And("Click button Transfer Upnormal")
	def Click_button_Transfer_Upnormal() {


		Mobile.tap(findTestObject('Object Repository/Test Transfer/Sumber Dana - Button Transfer'), 0)
		Mobile.verifyElementExist(findTestObject('Object Repository/Test Transfer/Verify Transfer - Rekening tujuan transfer tidak boleh sama dengan rekening sumber transaksi'), 0)
	}

	@And("Take a Picture")
	def takeAPicture(){
		//ss.takeScreenshotAsCheckpoint()
	}

	@Given("Click Fast Menu transfer")
	def Click_Fast_Menu_transfer() {
		Mobile.startExistingApplication('id.co.bri.brimo', FailureHandling.STOP_ON_FAILURE)
		Mobile.tap(findTestObject('Object Repository/Test Transfer/Icon Fast Menu Transfer'), 0)
	}

	@Given("Login BRIMO (.*) (.*)")
	//	def iOpenAnApplication(String username, String password) {
	//		Mobile.callTestCase(findTestCase('Test Cases/Common/Start Existing Application'),[('username'):username,('password'):password],FailureHandling.STOP_ON_FAILURE)
	//	}
	def login (String username , String password) {
		Mobile.callTestCase(findTestCase('Test Cases/Listrik/Login'), [('username') : username, ('password') : password], FailureHandling.STOP_ON_FAILURE)
	}

	@And("Verifikasi Saldo Tidak Cukup")
	def Verifikasi_Saldo_Tidak_Cukup()
	{
		Mobile.callTestCase(findTestCase('Test Cases/Transfer/Verifikasi Saldo Tidak Cukup'),[:], FailureHandling.STOP_ON_FAILURE)

	}
}