import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When



class AsuransiBRIns {
	/**
	 * The step definitions below match with Katalon sample Gherkin steps
	 */
	@Given("Open app BRIMO and login with (.*) and (.*)")
	def login (String username , String password) {
		Mobile.callTestCase(findTestCase('Test Cases/Listrik/Login'), [('username') : username, ('password') : password], FailureHandling.STOP_ON_FAILURE)
	}

	@When("Open Asuransi Page and choose asuransi BRIns")
	def opendasuransiBRIns() {
		Mobile.callTestCase(findTestCase('Test Cases/Asuransi Prudential/Open Dashboard'), [:], FailureHandling.STOP_ON_FAILURE)
	}
	
	@When("Choose the type of insurance")
	def chooseTypeInsurance() {
		Mobile.callTestCase(findTestCase('Test Cases/Asuransi BRIns/Choose for jenis insurance'),[:], FailureHandling.STOP_ON_FAILURE)
	}

	@When("Choose for (.*) BRIns insurance and input the (.*) insurance")
	def choosebrins(String jenis, String polis) {
		Mobile.callTestCase(findTestCase('Test Cases/Asuransi BRIns/Choose for jenis insurance'), [('jenis') : jenis, ('polis') : polis], FailureHandling.STOP_ON_FAILURE)
	}

	@When("Choose sumberdana (.*)  for payment")
	def sumberdanapayment(String sumberdana) {
		Mobile.callTestCase(findTestCase('Test Cases/Asuransi BRIns/Sumber dana asuransi'), [('sumberdana') : sumberdana], FailureHandling.STOP_ON_FAILURE)
	}

	@When("Click payment on confirmation page")
	def clickconfirmpage() {
		Mobile.callTestCase(findTestCase('Test Cases/Asuransi BRIns/Payment Confirmation'), [:], FailureHandling.STOP_ON_FAILURE)
	}
	

	@Then("Transaction successfully done")
	def successfullydone() {
		Mobile.tap(findTestObject('Object Repository/Asuransi BRIns/OKpayment'), 0)
	}
	@When("Close app")
	def closeapp() {
		Mobile.closeApplication()
	}
}