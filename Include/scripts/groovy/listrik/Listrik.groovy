package listrik
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When



class Listrik {
	/**
	 * The step definitions below match with Katalon sample Gherkin steps
	 */
	@Given("Login on existing account where (.*) and (.*)")
	def loginOnExixstingApplication(String username, String password) {
		Mobile.callTestCase(findTestCase('Test Cases/Listrik/Login'), [('username') : username, ('password') : password], FailureHandling.STOP_ON_FAILURE)
	}

	@When("Successfully go to dashboard i go to listrik menu")
	def listrikmenu (){
		Mobile.tap(findTestObject('Object Repository/Listrik/listrikiconn'), 0)
	}

	@Then("I tap Tambah Daftar Baru and choose Produk Listrik and input ID Pelanggan")
	def tambahbaru() {
		Mobile.callTestCase(findTestCase('Test Cases/Listrik/produklistrik'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("Tambah Daftar Baru and choose Produk Listrik (.*) and input ID Pelanggan (.*) for token")
	def tambahbarutoken(String produk, String id) {
		Mobile.callTestCase(findTestCase('Test Cases/Listrik/produklistrik'), [('produk') : produk, ('id') : id], FailureHandling.STOP_ON_FAILURE)
	}

	@When("Get nominal at row number")
	def sumberdana() {
		Mobile.callTestCase(findTestCase('Test Cases/Listrik/SumberDana'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@When("I go to confirmation page")
	def confirm() {
		Mobile.callTestCase(findTestCase('Test Cases/Listrik/Confirmation Page'), 0)
	}

	@Then("I try input the right PIN (.*)")
	def trypin(String pin) {
		Mobile.callTestCase(findTestCase('Test Cases/Listrik/Pin'), [('pin') : pin], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("I finish the transaction")
	def finishtransaction() {
		Mobile.tap(findTestObject('Object Repository/Listrik/Token/ButtonOK'), 0)
	}

	@Then("App Closed")
	def close() {
		Mobile.closeApplication()
	}

	@When("Do Payment")
	def picksumberdana() {
		Mobile.callTestCase(findTestCase('Test Cases/Listrik/Sumber Dana Token'), [:], FailureHandling.STOP_ON_FAILURE)
	}
	
	@When("Confirmation listrik Token")
	def confirmationListrikToken() {
		Mobile.callTestCase(findTestCase('Test Cases/Listrik/Confirmation Page'), [:], FailureHandling.STOP_ON_FAILURE)
	}
}