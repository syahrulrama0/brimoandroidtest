import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.util.KeywordUtil as KeywordUtil

//if (Mobile.verifyElementNotExist(findTestObject('Object Repository/Dompet Digitals/Main Dompet Digital/Belum Ada Top Up Terakhir'), 0) == false) {
//	Mobile.tap(findTestObject('Object Repository/Dompet Digitals/Main Dompet Digital/Layout History'),0)
//}
//else if (Mobile.verifyElementNotExist(findTestObject('Object Repository/Dompet Digitals/Main Dompet Digital/Belum Ada daftar'), 0) == false) {
//	Mobile.tap(findTestObject('Object Repository/Dompet Digitals/Main Dompet Digital/Kontak 1'),0)
//}

//if (Mobile.verifyElementNotVisible(findTestObject('Object Repository/Dompet Digitals/Main Dompet Digital/Belum Ada Top Up Terakhir'),0, FailureHandling.OPTIONAL)) {
//	Mobile.tap(findTestObject('Object Repository/Fast Menu/Main Prod/Layout History'),0)
//} else {
//	Mobile.tap(findTestObject('Object Repository/Dompet Digitals/Main Dompet Digital/Kontak 1'),0)
//}

if (Mobile.verifyElementVisible(findTestObject('Object Repository/Fast Menu/Main Prod/Layout History'),0, FailureHandling.OPTIONAL)) {
	Mobile.tap(findTestObject('Object Repository/Fast Menu/Main Prod/Layout History'),0)
}
else if (Mobile.verifyElementVisible(findTestObject('Object Repository/Dompet Digitals/Main Dompet Digital/Kontak 1'),0,FailureHandling.OPTIONAL)) {
	Mobile.tap(findTestObject('Object Repository/Dompet Digitals/Main Dompet Digital/Kontak 1'),0)
}
else {
	Throw new Exception("Tidak ada metode yang bisa digunakan")
}

//def userHistory = Mobile.getText(findTestObject('Object Repository/Fast Menu/Main Prod/Username'),0)
//def userKontak = Mobile.getText(findTestObject('Object Repository/Fast Menu/Main Prod/Belum Ada Daftar'), 0)
//
//if (!userHistory.contains("Tidak")) {
//	Mobile.tap(findTestObject('Object Repository/Fast Menu/Main Prod/Layout History'),0)
//}
//else {
//	if (!userKontak.contains("Belum")) {
//		Mobile.tap(findTestObject('Object Repository/Dompet Digitals/Main Dompet Digital/Kontak 1'),0)
//	}
//	else {
//		Mobile.closeApplication()
//	}
//}