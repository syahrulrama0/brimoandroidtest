import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

//Mobile.tap(findTestObject('Object Repository/TV/lanjutkanpaymenttv'), 0)
Mobile.tap(findTestObject('Object Repository/Asuransi BRIns/panahsumberdana'), 0)
switch(sumberdana) {
	case "rekeningUtama":
	pilihRekening = Mobile.getText(findTestObject("Object Repository/v2/Wallet Nominal Form/Sumber Dana/norek 1 - rekening utama - TextView - 0206 0100 0051 308"), 0)
	Mobile.tap(findTestObject("Object Repository/v2/Wallet Nominal Form/Sumber Dana/norek 1 - rekening utama - TextView - 0206 0100 0051 308"), 0)
	break
	
	case "saldoTidakCukup":
	pilihRekening = Mobile.getText(findTestObject("Object Repository/v2/Wallet Nominal Form/Sumber Dana/Norek 2  - GIRO SALDO TIDAK CUKUP"), 0)
	Mobile.tap(findTestObject("Object Repository/v2/Wallet Nominal Form/Sumber Dana/Norek 2  - GIRO SALDO TIDAK CUKUP"), 0)
	break
	
	case "britamax":
	Mobile.swipe(startX, endY, endX, startY)
	pilihRekening = Mobile.getText(findTestObject("Object Repository/v2/Wallet Nominal Form/Sumber Dana/Norek 3 - Britama x - 0206 2900 0008 507"), 0)
	Mobile.tap(findTestObject("Object Repository/v2/Wallet Nominal Form/Sumber Dana/Norek 3 - Britama x - 0206 2900 0008 507"), 0)
	break
	
	case "freeze":
	Mobile.swipe(startX, endY, endX, startY)
	pilihRekening = Mobile.getText(findTestObject("Object Repository/v2/Wallet Nominal Form/Sumber Dana/Freeze"), 0)
	Mobile.tap(findTestObject("Object Repository/v2/Wallet Nominal Form/Sumber Dana/Freeze"), 0)
	break
	
	case "dormant":
	Mobile.swipe(startX, endY, endX, startY)
	pilihRekening = Mobile.getText(findTestObject("Object Repository/v2/Wallet Nominal Form/Sumber Dana/Norek 4 - Dormant"), 0)
	Mobile.tap(findTestObject("Object Repository/v2/Wallet Nominal Form/Sumber Dana/Norek 4 - Dormant"), 0)
	break
	
	case "closed":
	Mobile.swipe(startX, endY, endX, startY)
	pilihRekening = Mobile.getText(findTestObject("Object Repository/v2/Wallet Nominal Form/Sumber Dana/Closed"), 0)
	Mobile.tap(findTestObject("Object Repository/v2/Wallet Nominal Form/Sumber Dana/Closed"), 0)
	break
}

if (sumberdana == "rekeningUtama" || sumberdana == "britamax" ) {
	//tap button Top Up
	Mobile.tap(findTestObject('Object Repository/v2/KAI/Form/Button - Bayar'), 0)
}
else if (sumberdana == "freeze" || sumberdana == "dormant" || sumberdana == "closed"  || sumberdana == "saldoTidakCukup") {
	Mobile.comment(pilihRekening)
} else {
	throw new Exception ("Jenis Rekening tidak ditemukan")
}

GlobalVariable.getNomorRekening = pilihRekening
Mobile.comment(GlobalVariable.getNomorRekening)
