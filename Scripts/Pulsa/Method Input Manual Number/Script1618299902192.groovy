import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

import com.kms.katalon.keyword.excel.ExcelKeywords

String phoneCode
def realNumber

Mobile.tap(findTestObject('Object Repository/Pulsa Data/PulsaData/Pulsa data menu'), 0)

String excelFile = GlobalVariable.excelFile
sheet = ExcelKeywords.getExcelSheetByName(GlobalVariable.excelFile, "Pulsa")
String phoneNumber = ExcelKeywords.getCellValueByAddress(sheet, "A"+GlobalVariable.giveCases)

Mobile.delay(2)

Mobile.setText(findTestObject('Pulsa Data/add/Nomor Tujuan'), phoneNumber, 0)
def fieldPhone = Mobile.getText(findTestObject('Pulsa Data/add/Field Telepon'), 0)
phoneCode = fieldPhone.substring(0,4)
realNumber = fieldPhone.toString()

CustomKeywords.'common.screenshot.takeScreenshotAsCheckpoint'()

Mobile.comment(phoneCode)
GlobalVariable.getPhoneCode = phoneCode
Mobile.comment(GlobalVariable.getPhoneCode)
GlobalVariable.getNumber = realNumber
Mobile.comment(GlobalVariable.getNumber)

