import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
String phoneCode
def realNumber

Mobile.delay(5)
Mobile.tap(findTestObject('Object Repository/Pulsa Data/PulsaData/Pulsa data menu'), 0)

Mobile.setText(findTestObject('Pulsa Data/add/Nomor Tujuan'), phoneNumber.toString(), 0)
def fieldPhone = Mobile.getText(findTestObject('Pulsa Data/add/Field Telepon'), 0)
phoneCode = fieldPhone.substring(0,4)
realNumber = fieldPhone.toString()

//Kode Nomor Operator Finnet 
//SIMPATI
if (phoneCode == "0812" ||phoneCode == "0822" ||phoneCode == "0821" || phoneCode == "0814" ||phoneCode == "0815" ||phoneCode == "0816" ||phoneCode == "0859" ||phoneCode == "0855" ||phoneCode == "0856" ||phoneCode == "0857" ||phoneCode == "0858" || phoneCode == "0817" ||phoneCode == "0818" ||phoneCode == "0819" ||phoneCode == "0859" ||phoneCode == "0877" ||phoneCode == "0878" || phoneCode == "0895" ||phoneCode == "0896" ||phoneCode == "0897" ||phoneCode == "0898" ||phoneCode == "0899" || phoneCode == "0838" ||phoneCode == "0831" ||phoneCode == "0832" ||phoneCode == "0833" || phoneCode == "0881" ||phoneCode == "0882" ||phoneCode == "0883" ||phoneCode == "0884" ||phoneCode == "0885" ||phoneCode == "0886" ||phoneCode == "0887" ||phoneCode == "0888" ||phoneCode == "0889") {
throw new Exception("sekian dan terimakasih")
}
else {
	def alert = Mobile.getText(findTestObject('Object Repository/Pulsa Data/Validation phone number (negative case)/android.widget.TextView - Nomor Anda Tidak Terdaftar'), 0)
	Mobile.verifyElementText(findTestObject('Object Repository/Pulsa Data/Validation phone number (negative case)/android.widget.TextView - Nomor Anda Tidak Terdaftar'), "Nomor Anda Tidak Terdaftar")
	Mobile.comment(alert)
}

CustomKeywords.'common.screenshot.sc'()