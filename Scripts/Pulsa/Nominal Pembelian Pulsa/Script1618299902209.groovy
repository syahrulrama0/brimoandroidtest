import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import com.kms.katalon.keyword.excel.ExcelKeywords

String excelFile = GlobalVariable.excelFile
sheet = ExcelKeywords.getExcelSheetByName(GlobalVariable.excelFile, "Pulsa")
int amount = ExcelKeywords.getCellValueByAddress(sheet, "B"+GlobalVariable.giveCases)

String pilihNominal
switch(amount) {
	case 15000 :
	Mobile.tap(findTestObject('Object Repository/Pulsa Data/Nominal/android.widget.TextView - Rp15.000'), 0)
	pilihNominal = Mobile.getText(findTestObject('Object Repository/Pulsa Data/Nominal/android.widget.TextView - Rp15.000'), 0)
	break
	case 20000 :
	Mobile.tap(findTestObject('Object Repository/Pulsa Data/Nominal/android.widget.TextView - Rp20.000'), 0)
	pilihNominal = Mobile.getText(findTestObject('Object Repository/Pulsa Data/Nominal/android.widget.TextView - Rp20.000'), 0)
	break
	case 25000 :
	Mobile.tap(findTestObject('Object Repository/Pulsa Data/Nominal/android.widget.TextView - Rp25.000'), 0)
	pilihNominal = Mobile.getText(findTestObject('Object Repository/Pulsa Data/Nominal/android.widget.TextView - Rp25.000'), 0)
	break
	case 30000 :
	Mobile.tap(findTestObject('Object Repository/Pulsa Data/Nominal/android.widget.TextView - Rp30.000'), 0)
	pilihNominal = Mobile.getText(findTestObject('Object Repository/Pulsa Data/Nominal/android.widget.TextView - Rp30.000'), 0)
	break
	case 40000 :
	Mobile.tap(findTestObject('Object Repository/Pulsa Data/Nominal/android.widget.TextView - Rp40.000'), 0)
	pilihNominal = Mobile.getText(findTestObject('Object Repository/Pulsa Data/Nominal/android.widget.TextView - Rp40.000'), 0)
	break
	case 50000 :
	Mobile.tap(findTestObject('Object Repository/Pulsa Data/Nominal/android.widget.TextView - Rp50.000'), 0)
	pilihNominal = Mobile.getText(findTestObject('Object Repository/Pulsa Data/Nominal/android.widget.TextView - Rp50.000'), 0)
	Mobile.comment(pilihNominal)
	break
	case 75000 :
	Mobile.tap(findTestObject('Object Repository/Pulsa Data/Nominal/android.widget.TextView - Rp75.000'), 0)
	pilihNominal = Mobile.getText(findTestObject('Object Repository/Pulsa Data/Nominal/android.widget.TextView - Rp75.000'), 0)
	case 100000 :
	Mobile.tap(findTestObject('Object Repository/Pulsa Data/Nominal/android.widget.TextView - Rp100.000'), 0)
	pilihNominal = Mobile.getText(findTestObject('Object Repository/Pulsa Data/Nominal/android.widget.TextView - Rp100.000'), 0)
	break
	case 150000 :
	Mobile.tap(findTestObject('Object Repository/Pulsa Data/Nominal/android.widget.TextView - Rp150.000'), 0)
	pilihNominal = Mobile.getText(findTestObject('Object Repository/Pulsa Data/Nominal/android.widget.TextView - Rp150.000'), 0)
	break
	case 200000 :
	Mobile.tap(findTestObject('Object Repository/Pulsa Data/Nominal/android.widget.TextView - Rp200.000'), 0)
	pilihNominal = Mobile.getText(findTestObject('Object Repository/Pulsa Data/Nominal/android.widget.TextView - Rp200.000'), 0)
	break
	case 300000 :
	Mobile.tap(findTestObject('Object Repository/Pulsa Data/Nominal/android.widget.TextView - Rp300.000'), 0)
	pilihNominal = Mobile.getText(findTestObject('Object Repository/Pulsa Data/Nominal/android.widget.TextView - Rp300.000'), 0)
	break
	case 500000 :
	Mobile.tap(findTestObject('Object Repository/Pulsa Data/Nominal/android.widget.TextView - Rp500.000'), 0)
	pilihNominal = Mobile.getText(findTestObject('Object Repository/Pulsa Data/Nominal/android.widget.TextView - Rp500.000'), 0)
	break
	case 1000000 :
	Mobile.scrollToText('1.000.000')
	Mobile.tap(findTestObject('Object Repository/Pulsa Data/Nominal/android.widget.TextView - Rp1.000.000'), 0)
	pilihNominal = Mobile.getText(findTestObject('Object Repository/Pulsa Data/Nominal/android.widget.TextView - Rp1.000.000'), 0)
	break
	default:
	break
}
CustomKeywords.'common.screenshot.takeScreenshotAsCheckpoint'()
String totalPembelian = Mobile.getText(findTestObject('Object Repository/Pulsa Data/PulsaData/Total Pembelian'), 0)
if(pilihNominal == totalPembelian) {
	Mobile.comment(pilihNominal)
	Mobile.comment(totalPembelian)
	Mobile.tap(findTestObject('Object Repository/Pulsa Data/PulsaData/Button Beli'), 0)
}else {
	throw new Exception ("nominal tidak sama")
}
GlobalVariable.getNominal = totalPembelian
Mobile.comment(GlobalVariable.getNominal)

