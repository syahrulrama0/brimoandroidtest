import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

Mobile.tap(findTestObject('Object Repository/Pulsa Data/Struk Pembayaran/Lihat Detail Transaksi'), 0)
Mobile.verifyElementExist(findTestObject('Object Repository/Pulsa Data/Struk Pembayaran/Date and Time'), 0)
Mobile.verifyElementExist(findTestObject('Object Repository/Pulsa Data/Struk Pembayaran/Nomor Referensi'), 0)
Mobile.verifyElementExist(findTestObject('Object Repository/Pulsa Data/Struk Pembayaran/Jenis Transaksi'), 0)

import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import internal.GlobalVariable as GlobalVariable
import com.kms.katalon.core.configuration.RunConfiguration as RunConfiguration
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.util.internal.PathUtil as PathUtil

import org.openqa.selenium.Rectangle as Rectangle

def getNumberPhone = GlobalVariable.getNumber
def nomorTujuan = Mobile.getText(findTestObject('Pulsa Data/Struk Pembayaran/Nomor Tujuan'), 0)
if(nomorTujuan == getNumberPhone) {
	Mobile.comment(nomorTujuan)
	Mobile.comment(getNumberPhone)
}else{
	throw new Exception ("Nomor Tujuan Berbeda")
}

Mobile.verifyElementExist(findTestObject('Object Repository/Pulsa Data/Struk Pembayaran/Provider'), 0)
Mobile.verifyElementExist(findTestObject('Object Repository/Pulsa Data/Struk Pembayaran/Jenis Produk'), 0)
Mobile.verifyElementExist(findTestObject('Object Repository/Pulsa Data/Struk Pembayaran/Nomor Serial'), 0)

def nominal = Mobile.getText(findTestObject('Object Repository/Pulsa Data/Struk Pembayaran/Nominal'), 0)
nominal = nominal.replaceAll("\\D+","")
Mobile.comment(nominal)
def biayaAdmin = Mobile.getText(findTestObject('Object Repository/Pulsa Data/Struk Pembayaran/Biaya Admin'), 0)
biayaAdmin = biayaAdmin.replaceAll("\\D+","")
Mobile.comment(biayaAdmin)

nominal = Integer.parseInt(nominal)
biayaAdmin = Integer.parseInt(biayaAdmin)
def verifyTotalHarga = nominal + biayaAdmin

def totalHarga = Mobile.getText(findTestObject('Object Repository/Pulsa Data/Struk Pembayaran/Total Harga'), 0)
if(totalHarga == verifyTotalHarga) {
	Mobile.comment(totalHarga)
	Mobile.comment(verifyTotalHarga)
}

def time = Mobile.getText(findTestObject('Object Repository/Pulsa Data/Struk Pembayaran/Date and Time'), 0)
def refnum = Mobile.getText(findTestObject('Object Repository/Pulsa Data/Struk Pembayaran/Nomor Referensi'), 0)

CustomKeywords.'common.convert.fileWrite'(time, refnum, "Pembelian Pulsa")

CustomKeywords.'common.screenshot.sc'()

Mobile.tap(findTestObject('Object Repository/Pulsa Data/Struk Pembayaran/android.widget.Button - OK'), 0)

