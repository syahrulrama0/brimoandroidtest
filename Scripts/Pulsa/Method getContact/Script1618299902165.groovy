import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

Mobile.delay(5)
Mobile.tap(findTestObject('Object Repository/Pulsa Data/PulsaData/Pulsa data menu'), 0)

String phoneCode
def realNumber

Mobile.tap(findTestObject('Object Repository/Pulsa Data/PulsaData/ikon kontak'), 0)
Mobile.setText(findTestObject('Object Repository/Pulsa Data/getContact/Search Nomor Telepon'), contactName, 0)
	
def contactNumber =  Mobile.getText(findTestObject('Object Repository/Pulsa Data/getContact/Nomor Tujuan JB'), 0)
Mobile.comment(contactNumber)
Mobile.tap(findTestObject('Object Repository/Pulsa Data/getContact/Layout JB'), 0)
	
def destinationNumber = Mobile.getText(findTestObject('Pulsa Data/add/Field Telepon'), 0)
Mobile.comment(destinationNumber)
	
realNumber = destinationNumber
	
if(contactNumber.substring(0,3) == "+62") {
	def nomorTujuan = Mobile.getText(findTestObject('Pulsa Data/add/Field Telepon'), 0)
	StringBuffer sb = new StringBuffer(nomorTujuan)
	sb.delete(0,1)
	String newString = "+62" + sb.toString()
	if (newString == contactNumber) {
			
		phoneCode = nomorTujuan.substring(0,4)

	}else{
		throw new Exception("Nomor Berbeda")
	}
}else{
	if (contactNumber == destinationNumber) {
		
		phoneCode = contactNumber.substring(0,4)
	}else{
		throw new Exception ("nomor Berbeda")
	}
}

CustomKeywords.'common.screenshot.sc'()

Mobile.comment(phoneCode)
GlobalVariable.getPhoneCode = phoneCode
Mobile.comment(GlobalVariable.getPhoneCode)
GlobalVariable.getNumber = realNumber
Mobile.comment(GlobalVariable.getNumber)
