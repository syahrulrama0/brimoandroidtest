import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

Mobile.delay(5)
Mobile.tap(findTestObject('Object Repository/Pulsa Data/PulsaData/Pulsa data menu'), 0)
Mobile.verifyElementVisible(findTestObject('Object Repository/Pulsa Data/PulsaData/History pembelian terakhir'), 0)

String phoneCode
def realNumber
if (method.toString() == 'manual'){
	Mobile.setText(findTestObject('Pulsa Data/add/Nomor Tujuan'), phoneNumber.toString(), 0)
	def fieldPhone = Mobile.getText(findTestObject('Pulsa Data/add/Field Telepon'), 0)
	phoneCode = fieldPhone.substring(0,4)
	realNumber = fieldPhone.toString()
	
    }
	
else if (method.toString() == 'getContact') {
	Mobile.tap(findTestObject('Object Repository/Pulsa Data/PulsaData/ikon kontak'), 0)
	Mobile.setText(findTestObject('Object Repository/Pulsa Data/getContact/Search Nomor Telepon'), user, 0)
	
	def contactNumber =  Mobile.getText(findTestObject('Object Repository/Pulsa Data/getContact/Nomor Tujuan JB'), 0)
	Mobile.comment(contactNumber)
	Mobile.tap(findTestObject('Object Repository/Pulsa Data/getContact/Layout JB'), 0)
	
	def destinationNumber = Mobile.getText(findTestObject('Pulsa Data/add/Field Telepon'), 0)
	Mobile.comment(destinationNumber)
	
	realNumber = destinationNumber
	
	if (contactNumber.substring(0,3) == "+62") {
		def nomorTujuan = Mobile.getText(findTestObject('Pulsa Data/add/Field Telepon'), 0)
		
		StringBuffer sb = new StringBuffer(nomorTujuan)
		
		sb.delete(0,1)

		String newString = "+62" + sb.toString()
		if (newString == contactNumber) {
			
			phoneCode = nomorTujuan.substring(0,4)
		}
		else {
			throw new Exception("Nomor Berbeda")
		}
	} else{
		if (contactNumber == destinationNumber) {
		
			phoneCode = contactNumber.substring(0,4)
		}
		else	{
			throw new Exception ("nomor beda")
		}
	}
	
}

String[] nominal
Mobile.comment(phoneCode)
//SIMPATI
if (phoneCode == "0812" ||phoneCode == "0822" ||phoneCode == "0821") {
//	 Verifikasi nominal ["15.000", "20.000", "25.000", "30.000", "40.000", "50.000", "75.000", "100.000", "200.000", "300.000", "500.000", "1.000.000"]
	 Mobile.verifyElementExist(findTestObject('Object Repository/Pulsa Data/Nominal/android.widget.TextView - Rp15.000'), 0)
	 Mobile.verifyElementExist(findTestObject('Object Repository/Pulsa Data/Nominal/android.widget.TextView - Rp20.000'), 0)
	 Mobile.verifyElementExist(findTestObject('Object Repository/Pulsa Data/Nominal/android.widget.TextView - Rp25.000'), 0)
	 Mobile.verifyElementExist(findTestObject('Object Repository/Pulsa Data/Nominal/android.widget.TextView - Rp30.000'), 0)
	 Mobile.verifyElementExist(findTestObject('Object Repository/Pulsa Data/Nominal/android.widget.TextView - Rp40.000'), 0)
	 Mobile.verifyElementExist(findTestObject('Object Repository/Pulsa Data/Nominal/android.widget.TextView - Rp50.000'), 0)
	 Mobile.verifyElementExist(findTestObject('Object Repository/Pulsa Data/Nominal/android.widget.TextView - Rp75.000'), 0)
	 Mobile.verifyElementExist(findTestObject('Object Repository/Pulsa Data/Nominal/android.widget.TextView - Rp100.000'), 0)
	 Mobile.verifyElementExist(findTestObject('Object Repository/Pulsa Data/Nominal/android.widget.TextView - Rp150.000'), 0)
	 Mobile.verifyElementExist(findTestObject('Object Repository/Pulsa Data/Nominal/android.widget.TextView - Rp200.000'), 0)
	 Mobile.verifyElementExist(findTestObject('Object Repository/Pulsa Data/Nominal/android.widget.TextView - Rp300.000'), 0)
	 Mobile.verifyElementExist(findTestObject('Object Repository/Pulsa Data/Nominal/android.widget.TextView - Rp500.000'), 0)
	 Mobile.scrollToText('1.000.000')
	 Mobile.verifyElementExist(findTestObject('Object Repository/Pulsa Data/Nominal/android.widget.TextView - Rp1.000.000'), 0)
}
//XL
else if (phoneCode == "0817" ||phoneCode == "0818" ||phoneCode == "0819" ||phoneCode == "0859" ||phoneCode == "0877" ||phoneCode == "0878") {
//	Verifikasi nominal ["25.000", "50.000", "100.000", "200.000"]
	Mobile.verifyElementExist(findTestObject('Object Repository/Pulsa Data/Nominal/android.widget.TextView - Rp25.000'), 0)
	Mobile.verifyElementExist(findTestObject('Object Repository/Pulsa Data/Nominal/android.widget.TextView - Rp50.000'), 0)
	Mobile.verifyElementExist(findTestObject('Object Repository/Pulsa Data/Nominal/android.widget.TextView - Rp100.000'), 0)
	Mobile.verifyElementExist(findTestObject('Object Repository/Pulsa Data/Nominal/android.widget.TextView - Rp200.000'), 0)
}
//Indosat
else if (phoneCode == "0814" ||phoneCode == "0815" ||phoneCode == "0816" ||phoneCode == "0859" ||phoneCode == "0855" ||phoneCode == "0856" ||phoneCode == "0857" ||phoneCode == "0858") {
//	Verifikasi nominal ["25.000", "50.000", "100.000", "150.000"]
	Mobile.verifyElementExist(findTestObject('Object Repository/Pulsa Data/Nominal/android.widget.TextView - Rp25.000'), 0)
	Mobile.verifyElementExist(findTestObject('Object Repository/Pulsa Data/Nominal/android.widget.TextView - Rp50.000'), 0)
	Mobile.verifyElementExist(findTestObject('Object Repository/Pulsa Data/Nominal/android.widget.TextView - Rp100.000'), 0)
	Mobile.verifyElementExist(findTestObject('Object Repository/Pulsa Data/Nominal/android.widget.TextView - Rp150.000'), 0)
}
//Mobile.tap(findTestObject('Object Repository/Pulsa Data/Nominal/android.widget.TextView - Rp'+amount.toString()), 0)
//def pilihNominal = Mobile.getText(findTestObject('Object Repository/Pulsa Data/Nominal/android.widget.TextView - Rp'+amount.toString()), 0)
String pilihNominal 
//Pilih Nominal Telkomsel
switch(amount) {
	case 15 : 
	Mobile.tap(findTestObject('Object Repository/Pulsa Data/Nominal/android.widget.TextView - Rp15.000'), 0)
	pilihNominal = Mobile.getText(findTestObject('Object Repository/Pulsa Data/Nominal/android.widget.TextView - Rp15.000'), 0)
	break
	case 20 :
	Mobile.tap(findTestObject('Object Repository/Pulsa Data/Nominal/android.widget.TextView - Rp20.000'), 0)
	pilihNominal = Mobile.getText(findTestObject('Object Repository/Pulsa Data/Nominal/android.widget.TextView - Rp20.000'), 0)
	break
	case 25 :
	Mobile.tap(findTestObject('Object Repository/Pulsa Data/Nominal/android.widget.TextView - Rp25.000'), 0)
	pilihNominal = Mobile.getText(findTestObject('Object Repository/Pulsa Data/Nominal/android.widget.TextView - Rp25.000'), 0)
	break
	case 30 :
	Mobile.tap(findTestObject('Object Repository/Pulsa Data/Nominal/android.widget.TextView - Rp30.000'), 0)
	pilihNominal = Mobile.getText(findTestObject('Object Repository/Pulsa Data/Nominal/android.widget.TextView - Rp30.000'), 0)
	break
	case 40 :
	Mobile.tap(findTestObject('Object Repository/Pulsa Data/Nominal/android.widget.TextView - Rp40.000'), 0)
	pilihNominal = Mobile.getText(findTestObject('Object Repository/Pulsa Data/Nominal/android.widget.TextView - Rp40.000'), 0)
	break
	case 50 :
	Mobile.tap(findTestObject('Object Repository/Pulsa Data/Nominal/android.widget.TextView - Rp50.000'), 0)
	pilihNominal = Mobile.getText(findTestObject('Object Repository/Pulsa Data/Nominal/android.widget.TextView - Rp50.000'), 0)
	Mobile.comment(pilihNominal)
	break
	case 75 :
	Mobile.tap(findTestObject('Object Repository/Pulsa Data/Nominal/android.widget.TextView - Rp75.000'), 0)
	pilihNominal = Mobile.getText(findTestObject('Object Repository/Pulsa Data/Nominal/android.widget.TextView - Rp75.000'), 0)
	case 100 :
	Mobile.tap(findTestObject('Object Repository/Pulsa Data/Nominal/android.widget.TextView - Rp100.000'), 0)
	pilihNominal = Mobile.getText(findTestObject('Object Repository/Pulsa Data/Nominal/android.widget.TextView - Rp100.000'), 0)
	break
	case 150 :
	Mobile.tap(findTestObject('Object Repository/Pulsa Data/Nominal/android.widget.TextView - Rp150.000'), 0)
	pilihNominal = Mobile.getText(findTestObject('Object Repository/Pulsa Data/Nominal/android.widget.TextView - Rp150.000'), 0)
	break
	case 200 :
	Mobile.tap(findTestObject('Object Repository/Pulsa Data/Nominal/android.widget.TextView - Rp200.000'), 0)
	pilihNominal = Mobile.getText(findTestObject('Object Repository/Pulsa Data/Nominal/android.widget.TextView - Rp200.000'), 0)
	break
	case 300 :
	Mobile.tap(findTestObject('Object Repository/Pulsa Data/Nominal/android.widget.TextView - Rp300.000'), 0)
	pilihNominal = Mobile.getText(findTestObject('Object Repository/Pulsa Data/Nominal/android.widget.TextView - Rp300.000'), 0)
	break
	case 500 :
	Mobile.tap(findTestObject('Object Repository/Pulsa Data/Nominal/android.widget.TextView - Rp500.000'), 0)
	pilihNominal = Mobile.getText(findTestObject('Object Repository/Pulsa Data/Nominal/android.widget.TextView - Rp500.000'), 0)
	break
	case 1000 :
	Mobile.tap(findTestObject('Object Repository/Pulsa Data/Nominal/android.widget.TextView - Rp1.000.000'), 0)
	pilihNominal = Mobile.getText(findTestObject('Object Repository/Pulsa Data/Nominal/android.widget.TextView - Rp1.000.000'), 0)
	break
	default:
	break
}

String totalPembelian = Mobile.getText(findTestObject('Object Repository/Pulsa Data/PulsaData/Total Pembelian'), 0)
if(pilihNominal == totalPembelian) {
	Mobile.comment(pilihNominal)
	Mobile.comment(totalPembelian)
	Mobile.tap(findTestObject('Object Repository/Pulsa Data/PulsaData/Button Beli'), 0)
}else {
	throw new Exception ("nominal tidak sama")
}
GlobalVariable.getNumber = realNumber
Mobile.comment(GlobalVariable.getNumber)
GlobalVariable.getNominal = totalPembelian
Mobile.comment(GlobalVariable.getNominal)

