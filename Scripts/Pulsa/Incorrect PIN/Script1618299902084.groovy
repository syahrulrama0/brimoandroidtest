import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

Mobile.verifyElementVisible(findTestObject('PIN/PIN'), 0)

Mobile.verifyElementVisible(findTestObject('PIN/Lupa PIN'), 0)

Mobile.verifyElementVisible(findTestObject('PIN/Masukkan PIN'), 0)

CustomKeywords.'common.screenshot.sc'()

String[] pinArray = ["111111", "111999", "999999"]
for (int i=0; i<=pinArray.length-1; i++) {
	for (int j=1; j<=6; j++) {
		Mobile.tap(findTestObject('Object Repository/PIN/Button '+ pinArray[i].substring(j-1,j)), 0)
	}
	
	if (i == pinArray.length-1) {
		Mobile.verifyElementVisible(findTestObject('Object Repository/PIN/User Blokir'), 0)
	}
	
	else {
		Mobile.tap(findTestObject('Object Repository/Pulsa Data/Konfirmasi/detail/android.widget.Button - Beli'), 0)
	}
}
Mobile.callTestCase(findTestCase('Test Cases/Common/Connection'), [:], FailureHandling.STOP_ON_FAILURE)

CustomKeywords.'database.common.methods.executeUpdate'(('UPDATE tbl_user SET login_retry ="0", status="1" WHERE username = "brimosv005' ) + '"')