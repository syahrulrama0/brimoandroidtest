import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

Mobile.delay(5)
Mobile.tap(findTestObject('Object Repository/Pulsa Data/PulsaData/Pulsa data menu'), 0)

Mobile.delay(7)
Mobile.tap(findTestObject('Object Repository/Pulsa Data/History/android.widget.ImageView'), 0)

String phoneCode
def realNumber

def getPhoneNumber = Mobile.getText(findTestObject('Object Repository/Pulsa Data/History/Phone Number by History'), 0)
Mobile.comment(getPhoneNumber)

def destinationNumber = Mobile.getText(findTestObject('Object Repository/Pulsa Data/History/Verify Phone Number'), 0)
Mobile.comment(destinationNumber)

if(getPhoneNumber == destinationNumber) {
	Mobile.comment(getPhoneNumber)
	Mobile.comment(destinationNumber)
	phoneCode = getPhoneNumber.substring(0,4)
	realNumber = destinationNumber
}else {
	throw new Exception ("nomor Berbeda")
}

CustomKeywords.'common.screenshot.sc'()

Mobile.comment(phoneCode)
GlobalVariable.getPhoneCode = phoneCode
Mobile.comment(GlobalVariable.getPhoneCode)
GlobalVariable.getNumber = realNumber
Mobile.comment(GlobalVariable.getNumber)


