import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

//def getDestinationNumber = GlobalVariable.getNumber
def verifyNomorTujuan = Mobile.getText(findTestObject('Object Repository/Pulsa Data/Metode pembayaran/Verify Nomor'),0)

if(verifyNomorTujuan == GlobalVariable.getNumber) {
	Mobile.comment(GlobalVariable.getNumber)
	Mobile.comment(verifyNomorTujuan)
}else {
	throw new Exception ("nomor tujuan tidak sama")
}

//def getNominalPulsa = GlobalVariable.getNominal
getNominalPulsa = GlobalVariable.getNominal.replaceAll("\\D+","")
def verifyNominalPulsa = Mobile.getText(findTestObject('Object Repository/Pulsa Data/Sumber dana/Nominal pembelian pulsa'), 0)
verifyNominalPulsa = verifyNominalPulsa.replaceAll("\\D+","")

if(verifyNominalPulsa == getNominalPulsa) {
	Mobile.comment(getNominalPulsa)
	Mobile.comment(verifyNominalPulsa)
}else {
	throw new Exception ("jumlah nominal pembelian pulsa tidak sama")
}
CustomKeywords.'common.screenshot.takeScreenshotAsCheckpoint'()
//Mobile.tap(findTestObject('Object Repository/Pulsa Data/Metode pembayaran/Button Beli 2'), 0)