import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.keyword.excel.ExcelKeywords

import internal.GlobalVariable

Mobile.tap(findTestObject('Object Repository/Mutasi/pilihtanggal'), 0)
Mobile.delay(3)

String excelFile = GlobalVariable.excelFile
sheet = ExcelKeywords.getExcelSheetByName(GlobalVariable.excelFile, "Mutasi")
String date = ExcelKeywords.getCellValueByAddress(sheet, "A"+GlobalVariable.giveCases)

switch (date.toString()) {
	case 'Hari Ini':
	Mobile.checkElement(findTestObject('Object Repository/Mutasi/Tanggalharini'), 0)

break
	case 'Kemarin':
	Mobile.checkElement(findTestObject('Object Repository/Mutasi/kemarin'), 0)

break
	case 'Minggu Ini':
	Mobile.checkElement(findTestObject('Object Repository/Mutasi/mingguini'), 0)

break
	case 'Bulan Ini':
	Mobile.checkElement(findTestObject('Object Repository/Mutasi/bulanini'), 0)

break
	case 'Pilih Tanggal Sendiri':
	Mobile.checkElement(findTestObject('Object Repository/Mutasi/pilihtgl'), 0)
    Mobile.delay(3)
	Mobile.tap(findTestObject('Object Repository/Mutasi/pilihtglawal'), 0)
	
break

default:
break
}