import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

Mobile.startExistingApplication('id.co.bri.brimo')

Mobile.tap(findTestObject('Laman Login not First Time Login/Button Login'), 0)

not_run: Mobile.tap(findTestObject('Laman First Time Login/Button Punya Akun'), 0)

Mobile.verifyElementVisible(findTestObject('Laman Login/Image BRImo'), 0)

Mobile.verifyElementVisible(findTestObject('Laman Login/Selamat Datang'), 0)

Mobile.setText(findTestObject('Laman Login/Field Username'), username.toString(), 0)

Mobile.setText(findTestObject('Laman Login/Field Password'), password.toString(), 0)

//Mobile.setText(findTestObject('Laman Login/Field Username'), "brimo007", 0)
//
//Mobile.setText(findTestObject('Laman Login/Field Password'), "Jakarta123", 0)

Mobile.tap(findTestObject('Laman Login/Button Login'), 0)

not_run: Mobile.verifyElementVisible(findTestObject('Laman Dashboard/Brimo Image'), 0)

Mobile.verifyElementVisible(findTestObject('Laman Dashboard/Field Rekening Lain'), 0)

Mobile.verifyElementVisible(findTestObject('Laman Dashboard/Icon Home'), 0)

//ss
Date current = new Date()

String currentDate = current.format('MM_dd_yy')

String currentTime = currentTime.format('hh_mm_ss')

