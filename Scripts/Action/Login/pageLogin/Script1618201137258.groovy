import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

Mobile.startExistingApplication('id.co.bri.brimo')

Mobile.verifyElementVisible(findTestObject('Object Repository/Login/Halo - Icon Brimo'), 0)

Mobile.tap(findTestObject('Object Repository/Login/Halo - Button - Login'), 0)

Mobile.verifyElementVisible(findTestObject('Object Repository/Login/Icon Brimo'), 0)

Mobile.verifyElementVisible(findTestObject('Object Repository/Login/TextView - Selamat Datang'), 0)

Mobile.setText(findTestObject('Object Repository/Login/EditText - Username'), username.toString(), 0)

Mobile.setText(findTestObject('Object Repository/Login/EditText - Password'), password.toString(), 0)

//ss
Date today = new Date()

String todaysDate = today.format('MM_dd_yy')

String nowTime = today.format('hh_mm_ss')

Mobile.takeScreenshot(((('C:/Users/LENOVO/Katalon Studio/111rose/FreshV1AutomationBrimo/Screenshot/' + todaysDate) +
	'-') + nowTime) + '.png', FailureHandling.STOP_ON_FAILURE)

Mobile.tap(findTestObject('Object Repository/Login/Button - Login'), 0)

