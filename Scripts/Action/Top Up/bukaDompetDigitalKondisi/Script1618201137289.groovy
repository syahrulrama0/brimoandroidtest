import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

//WebUI.callTestCase(findTestCase('Common/Dashboard'), [:], FailureHandling.STOP_ON_FAILURE)

//VERIFYYY WOYYYY
Mobile.verifyElementVisible(findTestObject('Object Repository/Dashboard/Icon Brimo'), 0)
Mobile.verifyElementVisible(findTestObject('Object Repository/Dashboard/TextView - DompetDigital'), 0)

//ss
Date today = new Date()

String todaysDate = today.format('MM_dd_yy')

String nowTime = today.format('hh_mm_ss')

Mobile.takeScreenshot(((('C:/Users/LENOVO/Katalon Studio/111rose/FreshV1AutomationBrimo/Screenshot/' + todaysDate) +
	'-') + nowTime) + '.png', FailureHandling.STOP_ON_FAILURE)

Mobile.tap(findTestObject('Object Repository/Top Up/Icon DompetDigital'), 0)

if (contact && history == "no") {
	Mobile.verifyElementVisible(findTestObject('Object Repository/Top Up/Belum Ada Daftar'), 0)
	Mobile.verifyElementVisible(findTestObject('Object Repository/Top Up/Belum Ada Transfer Terakhir'), 0)
}
else if (contact == "yes" && history == "no") {
	Mobile.verifyElementVisible(findTestObject('Object Repository/Top Up/Field Kontak'), 0)
	Mobile.verifyElementVisible(findTestObject('Object Repository/Top Up/Belum Ada Transfer Terakhir'), 0)
}
else if (contact == "no" && history == "yes") {
	Mobile.verifyElementVisible(findTestObject('Object Repository/Top Up/Belum Ada Daftar'), 0)
	Mobile.verifyElementVisible(findTestObject('Object Repository/Top Up/Top Up Terakhir'), 0)
}
else {
	Mobile.verifyElementVisible(findTestObject('Object Repository/Top Up/Field Kontak'), 0)
	Mobile.verifyElementVisible(findTestObject('Object Repository/Top Up/Top Up Terakhir'), 0)
}

if (topUpVia == "history") {
	Mobile.tap(findTestObject('Object Repository/Top Up/Top Up Terakhir'), 0)
}
else if (topUpVia == "kontak") {
//	Mobile.tap(findTestObject('Dompet Digital/Kontak 1'), 0)
//	Mobile.tap(findTestObject('Object Repository/Dompet Digital/newRecord/New - Contact 1'), 0)
	Mobile.tap(findTestObject('Object Repository/Top Up/contact 2'), 0)
}
else if (topUpVia == "topupbaru") {
	Mobile.tap(findTestObject('Object Repository/Top Up/Button Top Up Baru'), 0)
}
else if (topUpVia == "searchkontak") {
	String[] searchArray = [
		"o",
		"ro",
		"roooooooo",
		"zz",
		"aa",
		"roze",
		"rosp",
		"rose sv"
	]
	for (int i =0; i<=7; i++) {
		Mobile.setText(findTestObject('Object Repository/Top Up/Field Cari Daftar'),searchArray[i], 0)
		Mobile.tap(findTestObject('Object Repository/Top Up/Clear Text'), 0)
		if (i == 7) {
			Mobile.hideKeyboard()
			Mobile.delay(5)
			Mobile.tap(findTestObject('Object Repository/Top Up/Layout Kontak'), 0)
		}
	}
}
else {
	throw new Exception("Top Up Method is unknown")
}