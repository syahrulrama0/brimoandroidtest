import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

Mobile.verifyElementVisible(findTestObject('Object Repository/Top Up/Nominal/Dompet Digital'), 0)

Mobile.verifyElementVisible(findTestObject('Object Repository/Top Up/Nominal/Layout Rekening'), 0)

//Mobile.verifyElementVisible(findTestObject('Object Repository/Top Up/Nominal/Minimal Top Up'), 0)

//Mobile.verifyElementVisible(findTestObject('Object Repository/Top Up/Nominal/Nominal Layout'), 0)

//ss
Date today = new Date()

String todaysDate = today.format('MM_dd_yy')

String nowTime = today.format('hh_mm_ss')

Mobile.takeScreenshot(((('C:/Users/LENOVO/Katalon Studio/111rose/FreshV1AutomationBrimo/Screenshot/' + todaysDate) +
	'-') + nowTime) + '.png', FailureHandling.STOP_ON_FAILURE)

if (alias != "null") {
	Mobile.tap(findTestObject('Object Repository/Top Up/Nominal/CheckBox Simpan Untuk Selanjutnya'), 0)
	Mobile.setText(findTestObject('Object Repository/Top Up/Nominal/Field Nama Kontak'),alias.toString(),0)
}

if (status == "success") {
	Mobile.setText(findTestObject('Object Repository/Top Up/Nominal/Nominal Field'),amount.toString(), 0)
}
else {
	String[] amountArray = [
		"0",
		"123456789101112",
		"0",
		"1000"
	]
	for (int i = 0; i<=amountArray.length(); i++) {
		if (i == 0) {
			Mobile.setText(findTestObject('Object Repository/Top Up/Nominal/Nominal Field'),amountArray[i], 0)
			Mobile.verifyElementVisible(findTestObject('Object Repository/Top Up/Nominal/Top Up Disabled'), 0)
		}
		else if (i == 1) {
			Mobile.setText(findTestObject('Object Repository/Top Up/Nominal/Nominal Field'),amountArray[i], 0)
			def bigAmount = Mobile.getText(findTestObject('Object Repository/Top Up/Nominal/Nominal Field'), 0)
			if (bigAmount.length() == 12) {
				Mobile.verifyElementVisible(findTestObject('Object Repository/Top Up/Nominal/Top Up Disabled'), 0)
			}
			else {
				throw new Exception("Nomor Kepanjangan")
			}
		}
		else if (i == 2) {
			for (int j=0; j<=9; j++) {
				Mobile.setText(findTestObject('Object Repository/Top Up/Nominal/Nominal Field'),amountArray[i], 0)
				def bigAmount = Mobile.getText(findTestObject('Object Repository/Top Up/Nominal/Nominal Field'), 0)
				if (bigAmount == 0) {
					Mobile.verifyElementVisible(findTestObject('Object Repository/Top Up/Nominal/Top Up Disabled'), 0)
				}
				else {
					throw new Exception("Angka tidak nol")
				}
			}
		}
		else if (i == 3) {
			Mobile.setText(findTestObject('Object Repository/Top Up/Nominal/Nominal Field'),amountArray[i], 0)
			int pay = Integer.parseInt(amountArray[i])
			if (pay < 10000) {
//				Mobile.verifyElementVisible(findTestObject('Object Repository/Top Up/Nominal/Minimal Top Up'), 0)
				Mobile.verifyElementVisible(findTestObject('Object Repository/Top Up/Nominal/Top Up Disabled'), 0)
			}
			else {
//				Mobile.verifyElementNotVisible(findTestObject('Object Repository/Top Up/Nominal/Minimal Top Up'), pay)
				Mobile.verifyElementVisible(findTestObject('Object Repository/Top Up/Nominal/Top Up Enabled'), 0)
			}
		}
	}
}

Mobile.takeScreenshot(((('C:/Users/LENOVO/Katalon Studio/111rose/FreshV1AutomationBrimo/Screenshot/' + todaysDate) +
	'-') + nowTime) + '.png', FailureHandling.STOP_ON_FAILURE)


Mobile.tap(findTestObject('Object Repository/Top Up/Nominal/Top Up Enabled'), 0)