import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.keyword.excel.ExcelKeywords

import internal.GlobalVariable





Mobile.tap(findTestObject('Catatan Keuangan/buttonbuatcacatanbaru'), 0)

//Mobile.sendKeys(findTestObject('Object Repository/rp'), 10000)
Mobile.tap(findTestObject('Catatan Keuangan/pilihkategori'), 0)

String excelFile = 'datasets\\BRIMO.xlsx'
sheet = ExcelKeywords.getExcelSheetByName("datasets\\BRIMO.xlsx", "Catatan Keuangan")
String kategori = ExcelKeywords.getCellValueByAddress(sheet, "A"+GlobalVariable.giveCases)
String amount = ExcelKeywords.getCellValueByAddress(sheet, "B"+GlobalVariable.giveCases)

switch (kategori.toLowerCase()) {
    case 'makanan':
        Mobile.checkElement(findTestObject('Catatan Keuangan/makananminuman'), 0)

        break
    default:
        break
}

Mobile.tap(findTestObject('Catatan Keuangan/android.widget.EditText - Rp'), 0)

CustomKeywords.'common.other.nominalCatatanKeuangan'(amount)
//Mobile.sendKeys(findTestObject('Catatan Keuangan/android.widget.FrameLayout - rp', [(1) : 1]), Keys.chord(Keys.NUMPAD1))

//Mobile.setText(findTestObject('Object Repository/Catatan Keuangan/Input Nominal'), String.valueOf(nominal), 0)