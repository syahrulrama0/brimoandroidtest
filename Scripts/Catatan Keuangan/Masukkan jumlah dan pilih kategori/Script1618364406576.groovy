import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

Mobile.tap(findTestObject('Object Repository/CatatanKeuangan/buttonbuatcacatanbaru'), 0)

//Mobile.sendKeys(findTestObject('Object Repository/rp'), 10000)
Mobile.tap(findTestObject('Object Repository/CatatanKeuangan/pilihkategori'), 0)

switch (kategori.toString()) {
    case 'Makanan':
        Mobile.checkElement(findTestObject('Object Repository/CatatanKeuangan/makananminuman'), 0)

        break
    default:
        break
}

Mobile.tap(findTestObject('CatatanKeuangan/android.widget.EditText - Rp'), 0)

amount = 19999

//Mobile.sendKeys(findTestObject('CatatanKeuangan/android.widget.FrameLayout - rp', [(1) : 1]), Keys.chord(Keys.NUMPAD1))

Mobile.setText(findTestObject('Object Repository/CatatanKeuangan/android.widget.EditText - Rp'), amount, 0)