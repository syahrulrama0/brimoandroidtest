import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.keyword.excel.ExcelKeywords

import internal.GlobalVariable

Mobile.verifyElementExist(findTestObject('Catatan Keuangan/pilihpembayaran'), 0)

Mobile.tap(findTestObject('Catatan Keuangan/pilihtanggal'), 0)

//CustomKeywords.'screenshot.capture.Screenshot'()

String excelFile = 'datasets\\BRIMO.xlsx'
sheet = ExcelKeywords.getExcelSheetByName("datasets\\BRIMO.xlsx", "Catatan Keuangan")
String date = ExcelKeywords.getCellValueByAddress(sheet, "C"+GlobalVariable.giveCases)
String pembayaran = ExcelKeywords.getCellValueByAddress(sheet, "D"+GlobalVariable.giveCases)

dates = date.toString()

Date today = new Date()

String todaysDate = today.format('dd')


def datePlus = today.clone()
def dateMinus = today.clone()

datePlus = datePlus + 1

dateMinus = dateMinus - 1

Mobile.comment("tanggal hari ini " + todaysDate)
 
		if(todaysDate.charAt(0) == '0') {
			todaysDate = todaysDate.substring(1)
		}
		Mobile.tap(findTestObject('Catatan Keuangan/pilihtanggalpengeluaran', [('date') : "$todaysDate"]), 0)

		Mobile.tap(findTestObject('Catatan Keuangan/Pilihtanggalkalender'), 0)
		
//		indo_date = Mobile.getText(findTestObject('Catatan Keuangan/datadate'), 0)

		
Mobile.tap(findTestObject('Catatan Keuangan/pilihpembayaran'), 0)
switch (pembayaran.toString()) {
	case 'Tunai':
		Mobile.checkElement(findTestObject('Catatan Keuangan/tunai'), 0)

		break
	case 'Debit':
		Mobile.checkElement(findTestObject('Catatan Keuangan/debit'), 0)

		break
	case 'Dompet Digital':
		Mobile.checkElement(findTestObject('Catatan Keuangan/dompetdigital'), 0)

		break
	case 'Kartu Kredit':
		Mobile.checkElement(findTestObject('Catatan Keuangan/kartukredit'), 0)

		break
	default:
		break
}