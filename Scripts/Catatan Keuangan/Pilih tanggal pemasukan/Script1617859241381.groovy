import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

Mobile.tap(findTestObject('Catatan Keuangan/pilihtanggalpemasukan'), 0)

Mobile.tap(findTestObject('Catatan Keuangan/pilihtanggal'), 0)

//CustomKeywords.'screenshot.capture.Screenshot'()

dates = date.toString()

Date today = new Date()

String todaysDate = today.format('dd')


def datePlus = today.clone()
def dateMinus = today.clone()

datePlus = datePlus + 1

dateMinus = dateMinus - 1

Mobile.comment("tanggal hari ini " + todaysDate)
 
		if(todaysDate.charAt(0) == '0') {
			todaysDate = todaysDate.substring(1)
		}
		Mobile.tap(findTestObject('Catatan Keuangan/pilihtanggalpengeluaran', [('date') : "$todaysDate"]), 0)

		Mobile.tap(findTestObject('Catatan Keuangan/Pilihtanggalkalender'), 0)
		