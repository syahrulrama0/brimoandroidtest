import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

Mobile.tap(findTestObject('Catatan Keuangan/catatanbarupemasukan'), 0)

Mobile.tap(findTestObject('Catatan Keuangan/pilihkategoripemasukan'), 0)
switch (kategori.toString()) {
	case 'Gaji':
	Mobile.checkElement(findTestObject('Catatan Keuangan/gaji'), 0)

		break
	case 'Usaha':
	Mobile.checkElement(findTestObject('Catatan Keuangan/usaha'), 0)

		break
	case 'Bonus':
	Mobile.checkElement(findTestObject('Catatan Keuangan/bonus'), 0)

		break
	case 'Bunga':
	Mobile.checkElement(findTestObject('Catatan Keuangan/bunga'), 0)

		break
	case 'Hadiah':
	Mobile.checkElement(findTestObject('Catatan Keuangan/hadiah'), 0)

		break
	case 'Uang Masuk':
	Mobile.checkElement(findTestObject('Catatan Keuangan/uangmasuk'), 0)

		break
	default:
		break
}