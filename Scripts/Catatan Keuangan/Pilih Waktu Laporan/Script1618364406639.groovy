import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

Mobile.tap(findTestObject('Object Repository/CatatanKeuangan/PilihWaktu'), 0)

switch (waktu.toString()) {
	case 'Bulan Ini':
		Mobile.checkElement(findTestObject('Object Repository/CatatanKeuangan/BulanIni'), 0)

		break
	case 'Bulan Lalu':
		Mobile.checkElement(findTestObject('Object Repository/CatatanKeuangan/BulanLalu'), 0)

		break
	case '3 Bulan':
		Mobile.checkElement(findTestObject('Object Repository/CatatanKeuangan/3Bulan'), 0)

		break
	default:
		break
}
