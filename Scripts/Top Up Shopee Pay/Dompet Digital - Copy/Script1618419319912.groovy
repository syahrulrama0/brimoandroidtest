import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

Mobile.callTestCase(findTestCase('Test Cases/forscript/Connection'), [:], FailureHandling.STOP_ON_FAILURE)

CustomKeywords.'database.methods.executeUpdate'(('UPDATE tbl_history_purchase_ewallet SET value = "" WHERE username = "' + GlobalVariable.username) + '"')
CustomKeywords.'database.methods.executeUpdate'(('DELETE FROM tbl_list_purchase WHERE username = "' + GlobalVariable.username) + '" and group_id = "4"')

CustomKeywords.'database.methods.executeUpdate'('''UPDATE tbl_history_purchase_ewallet set value = '[{"number":"08567641510","name":"ShopeePay","customer":"kozo112233","code":"26","type":"null"},{"number":"08008008002","name":"OVO","customer":"OVO TE* D*","code":"25","type":"25"},{"number":"085559547821","name":"LinkAja","customer":"MOHAMMAD IQBAL SUGANDHI","code":"24","type":"null"},{"number":"08008008002","name":"OVO","customer":"OVO TE* D**","code":"25","type":"25"},{"number":"085691335269","name":"LinkAja","customer":"FIKRI ARDIANSYAH","code":"24","type":"null"}]' WHERE username = "'''+GlobalVariable.username+'"')

String queryListPurchase = (((((((((((((((((((((((((((((((((((((('INSERT INTO tbl_list_purchase (username,purchase_number,purchase_nickname,status,purchase_type_id,group_id,favorite,F1) VALUES ' +
	'(\'') + GlobalVariable.username) + '\',\'08567641510\',\'K\',1,24,4,0,null),') + //24 LinkAja
	'(\'') + GlobalVariable.username) + '\',\'081218022786\',\'Customer\',1,24,4,0,null),') + //24 LinkAja
	'(\'') + GlobalVariable.username) + '\',\'088218770335\',\'Aja\',1,24,4,0,null),') + //24 LinkAja
	'(\'') + GlobalVariable.username) + '\',\'081290825284\',\'Julian\',1,24,4,0,null),') + //24 LinkAja
	'(\'') + GlobalVariable.username) + '\',\'082210112982\',\'ADE RISSA HELIZA\',1,24,4,0,null),') + //24 LinkAja
	'(\'') + GlobalVariable.username) + '\',\'082299888040\',\'LingLung\',1,24,4,0,null),') + //24 LinkAja
	'(\'') + GlobalVariable.username) + '\',\'082299888040\',\'Dono\',1,27,4,0,null),') + //27 DANA
	'(\'') + GlobalVariable.username) + '\',\'081288626368\',\'Ojek Pengkolan\',1,15,4,0,301342),') + //15 GoPay
	'(\'') + GlobalVariable.username) + '\',\'082210112982\',\'Gojek\',1,15,4,0,301341),') + //15 GoPay
	'(\'') + GlobalVariable.username) + '\',\'081290825284\',\'sofi\',1,26,4,0,null),') + //26 ShopeePay 08567641510
	'(\'') + GlobalVariable.username) + '\',\'08567641510\',\'kozo\',1,26,4,0,null),') + //26 ShopeePay
	'(\'') + GlobalVariable.username) + '\',\'081218022786\',\'ora opo\',1,25,4,1,null),') + //25 OVO
	'(\'') + GlobalVariable.username) + '\',\'080012345123\',\'0V0\',1,25,4,0,null)' //25 OVO 

String query1 = ((('INSERT INTO tbl_list_purchase (username,purchase_number,purchase_nickname,status,purchase_type_id,group_id,favorite,F1) VALUES ' +
	'(\'') + GlobalVariable.username) + '\',\'08567641510\',\'kozo\',1,26,4,0,null)')

	CustomKeywords.'database.methods.executeUpdate'(('DELETE FROM tbl_list_purchase WHERE username = "' + GlobalVariable.username) + '" and group_id = "4"')
	//CustomKeywords.'database.methods.executeUpdate'(queryListPurchase)
	CustomKeywords.'database.methods.executeUpdate'(query1)

	CustomKeywords.'database.methods.closeDatabaseConnection'()
	
Mobile.tap(findTestObject('ShopeePayTopUp/Laman Dashboard/Icon DompetDigital'), 0)

Mobile.verifyElementExist(findTestObject('ShopeePayTopUp/Dompet Digital/Field Kontak (1)'), 0, FailureHandling.STOP_ON_FAILURE)

Mobile.verifyElementExist(findTestObject('ShopeePayTopUp/Top Up Terakhir'), 0, FailureHandling.STOP_ON_FAILURE)

Mobile.verifyElementExist(findTestObject('ShopeePayTopUp/Dompet Digital/Button Top Up Baru'), 0, FailureHandling.STOP_ON_FAILURE)

Mobile.verifyElementExist(findTestObject('Object Repository/ShopeePayTopUp/topupbarubutton'), 0, FailureHandling.STOP_ON_FAILURE)

Mobile.tap(findTestObject('Object Repository/ShopeePayTopUp/topupbarubutton'), 0)
Date today = new Date()

String todaysDate = today.format('MM_dd_yy')

String nowTime = today.format('hh_mm_ss')

Mobile.takeScreenshot(((('C:/Users/LENOVO/Downloads/Katalon Picture/screenshot_' + todaysDate) + '-') + nowTime) + '.png',
	FailureHandling.STOP_ON_FAILURE)