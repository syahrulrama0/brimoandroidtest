import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.keyword.excel.ExcelKeywords

import internal.GlobalVariable as GlobalVariable

String excelFile = GlobalVariable.excelFile
sheet = ExcelKeywords.getExcelSheetByName(GlobalVariable.excelFile, "Donasi")
String fixAmount = ExcelKeywords.getCellValueByAddress(sheet, "C"+GlobalVariable.giveCases)
String amount = ExcelKeywords.getCellValueByAddress(sheet, "D"+GlobalVariable.giveCases)
def nominal
CustomKeywords.'common.other.donasiAmount'(fixAmount)

if (fixAmount == "manual") {
//	Mobile.verifyElementExist(findTestObject('Object Repository/Donasi/Nominal Sendiri/Field Nominal'),0)
//	Mobile.verifyElementExist(findTestObject('Object Repository/Donasi/Nominal Sendiri/Input Nominal'),0)
//	Mobile.verifyElementExist(findTestObject('Object Repository/Donasi/Nominal Sendiri/Layout Nominal'),0)
	Mobile.verifyElementExist(findTestObject('Object Repository/Donasi/Nominal Sendiri/Minimal Pembayaran'),0)
	
	Mobile.setText(findTestObject('Object Repository/Donasi/Nominal Sendiri/Field Nominal'), amount, 0)
	
	nominal = Mobile.getText(findTestObject('Object Repository/Donasi/Nominal Sendiri/Field Nominal'),0)
	
	nominal = nominal.replaceAll("\\D+","")
	
	GlobalVariable.amount = nominal
}
else {
	def amount2 = Mobile.getText(findTestObject('Object Repository/Donasi/Main Donasi/Field Pilih Nominal'),0)
	amount2 = amount2.replaceAll("\\D+","")
	GlobalVariable.amount = amount2
}

Mobile.tap(findTestObject('Object Repository/Donasi/Main Donasi/Lanjutkan Enable'),0)

//CustomKeywords.'common.other.takeScreenShot'()