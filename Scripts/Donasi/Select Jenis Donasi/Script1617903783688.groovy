import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.keyword.excel.ExcelKeywords

import internal.GlobalVariable as GlobalVariable


String excelFile = GlobalVariable.excelFile
sheet = ExcelKeywords.getExcelSheetByName(GlobalVariable.excelFile, "Donasi")
String donationType = ExcelKeywords.getCellValueByAddress(sheet, "B"+GlobalVariable.giveCases)

if (donationType.toLowerCase() == "zakat") {
	Mobile.tap(findTestObject('Object Repository/Donasi/Jenis Produk/Zakat'),0)
}
else if (donationType.toLowerCase() == "infaq") {
	Mobile.tap(findTestObject('Object Repository/Donasi/Jenis Produk/Infaq Kurban'),0)
}
else {
	throw new Exception("Donation Type is Unknown")
}

def typeDonation = Mobile.getText(findTestObject('Object Repository/Donasi/Main Donasi/Field Pilih Jenis Produk'),0)

GlobalVariable.donationType = typeDonation

//CustomKeywords.'common.other.takeScreenShot'()