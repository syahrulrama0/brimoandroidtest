import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

Mobile.verifyElementExist(findTestObject('Object Repository/Donasi/Konfirmasi/Konfirmasi'), 0)

Mobile.verifyElementExist(findTestObject('Object Repository/Donasi/Konfirmasi/Icon Back'), 0)

Mobile.verifyElementExist(findTestObject('Object Repository/Donasi/Konfirmasi/Sumber Dana'), 0)

Mobile.verifyElementExist(findTestObject('Object Repository/Donasi/Konfirmasi/Nomor Tujuan'), 0)

Mobile.verifyElementExist(findTestObject('Object Repository/Donasi/Konfirmasi/Detail'), 0)

Mobile.verifyElementExist(findTestObject('Object Repository/Donasi/Konfirmasi/Total'), 0)

def userName = Mobile.getText(findTestObject('Object Repository/Donasi/Konfirmasi/Nama Pemilik Rekening'),0)

def norek = Mobile.getText(findTestObject('Object Repository/Donasi/Konfirmasi/Norek'),0)

String provider = Mobile.getText(findTestObject('Object Repository/Donasi/Konfirmasi/Nama Provider'),0)

//def donationType = Mobile.getText(findTestObject('Object Repository/Donasi/Konfirmasi/Jenis Donasi'),0)

def nominal = Mobile.getText(findTestObject('Object Repository/Donasi/Konfirmasi/Nominal'),0)

GlobalVariable.userName = userName

norek = norek.replaceAll("\\D+","")

if (norek != GlobalVariable.getNoRekening) {
	throw new Exception("Nomor Rekening Berbeda")
}

if (provider.toLowerCase() != GlobalVariable.provider) {
	throw new Exception("Provider Berbeda")
}

//if (donationType != GlobalVariable.donationType) {
//	throw new Exception("Provider Berbeda")
//}

nominal = nominal.replaceAll("\\D+","")

if (nominal != GlobalVariable.amount) {
	throw new Exception("Nominal Berbeda")
}

//CustomKeywords.'common.other.takeScreenShot'()

Mobile.tap(findTestObject('Object Repository/Donasi/Konfirmasi/Button Bayar'),0)