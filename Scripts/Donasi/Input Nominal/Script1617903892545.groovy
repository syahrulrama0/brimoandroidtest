import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.keyword.excel.ExcelKeywords

import internal.GlobalVariable as GlobalVariable

String excelFile = GlobalVariable.excelFile
sheet = ExcelKeywords.getExcelSheetByName(GlobalVariable.excelFile, "Donasi")
String amount = ExcelKeywords.getCellValueByAddress(sheet, "D"+GlobalVariable.giveCases)

Mobile.setText(findTestObject('Object Repository/Donasi/Nominal Sendiri/Field Nominal'), amount, 0)

def nominal = Mobile.getText(findTestObject('Object Repository/Donasi/Nominal Sendiri/Field Nominal'),0)

nominal = nominal.replaceAll("\\D+","")

GlobalVariable.amount = nominal

//CustomKeywords.'common.other.takeScreenShot'()
