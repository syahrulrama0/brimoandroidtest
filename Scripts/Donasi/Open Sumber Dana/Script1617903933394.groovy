import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

Mobile.tap(findTestObject('Object Repository/Donasi/Main Donasi/Lanjutkan Enable'),0)

Mobile.verifyElementExist(findTestObject('Object Repository/Donasi/Sumber Dana/Nomor Tujuan'), 0)

Mobile.verifyElementExist(findTestObject('Object Repository/Donasi/Sumber Dana/Sumber Dana'), 0)

Mobile.verifyElementExist(findTestObject('Object Repository/Donasi/Sumber Dana/Donasi'), 0)

Mobile.verifyElementExist(findTestObject('Object Repository/Donasi/Sumber Dana/Back Icon'), 0)

Mobile.tap(findTestObject('Object Repository/Donasi/Sumber Dana/Layout Sumber Dana'), 0)

CustomKeywords.'common.other.takeScreenShot'()