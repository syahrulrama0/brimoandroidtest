import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.keyword.excel.ExcelKeywords

import internal.GlobalVariable as GlobalVariable

String excelFile = GlobalVariable.excelFile
sheet = ExcelKeywords.getExcelSheetByName(GlobalVariable.excelFile, "Donasi")
String chosenProduct = ExcelKeywords.getCellValueByAddress(sheet, "A"+GlobalVariable.giveCases)

if (chosenProduct == "DD") {
	Mobile.tap(findTestObject('Object Repository/Donasi/Produk Donasi/Dompet Dhuafa'),0)
}
else if (chosenProduct == "YBM") {
	Mobile.tap(findTestObject('Object Repository/Donasi/Produk Donasi/YBM BRI'),0)
}
else {
	throw new Exception("Cant Identify The Chosen Product")
}

Mobile.verifyElementExist(findTestObject('Object Repository/Donasi/Main Donasi/Jenis Produk'),0)
String provider = Mobile.getText(findTestObject('Object Repository/Donasi/Main Donasi/Field Pilih Produk Donasi'),0)
GlobalVariable.provider = provider.toLowerCase()

//CustomKeywords.'common.other.takeScreenShot'()