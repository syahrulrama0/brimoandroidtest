import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.keyword.excel.ExcelKeywords

import internal.GlobalVariable

String excelFile = 'datasets\\BRIMO.xlsx'
sheet = ExcelKeywords.getExcelSheetByName("datasets\\BRIMO.xlsx", "Kartu Kredit")
String condition = ExcelKeywords.getCellValueByAddress(sheet, "D"+GlobalVariable.giveCases)

device_Height = Mobile.getDeviceHeight()
device_Width = Mobile.getDeviceWidth()
int startX = device_Width / 2
int endX = startX
int startY = device_Height * 0.30
int endY = device_Height * 0.70
Mobile.swipe(startX, endY, endX, startY)

String getNoRekening
switch(condition) {
	
	case "Saldo_Cukup":
	getNoRekening = Mobile.getText(findTestObject('Object Repository/Kartu Kredit/Nominal Pembayaran dan Sumber Dana/Nomor Rekening'), 0)
	break
	
	case "Saldo_Tidak_Cukup":
	getNoRekening = Mobile.getText(findTestObject('Object Repository/Kartu Kredit/Nominal Pembayaran dan Sumber Dana/Nomor Rekening'), 0)
	break
	
}


if(condition=="Saldo_Cukup") {
	Mobile.comment(getNoRekening)
	Mobile.tap(findTestObject('Object Repository/Kartu Kredit/Form Tagihan Himbara/Button Bayar'), 0)
}
else if(condition=="Saldo_Tidak_Cukup") {
	Mobile.comment(getNoRekening)
	Mobile.verifyElementText(findTestObject('Object Repository/Kartu Kredit/Form Tagihan Himbara/Saldo Anda tidak cukup'), "Saldo Anda tidak cukup")
	Mobile.verifyElementVisible(findTestObject('Object Repository/Kartu Kredit/Form Tagihan Himbara/Button Bayar disable'), 0)
}else {
	throw new Exception ("Kondisi tidak ditemukan")
}

GlobalVariable.getNoRekening = getNoRekening
