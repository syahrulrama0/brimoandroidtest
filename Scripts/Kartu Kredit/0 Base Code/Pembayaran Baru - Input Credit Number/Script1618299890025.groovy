import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.keyword.excel.ExcelKeywords

import internal.GlobalVariable as GlobalVariable

String excelFile = GlobalVariable.excelFile
sheet = ExcelKeywords.getExcelSheetByName(GlobalVariable.excelFile, "Kartu Kredit")
String creditNumber = ExcelKeywords.getCellValueByAddress(sheet, "B"+GlobalVariable.giveCases)

Mobile.setText(findTestObject('Object Repository/Kartu Kredit/Buat baru/Field Nomor Kartu Kredit'), creditNumber.toString(), 0)

Mobile.delay(3)

def getNoTujuan = Mobile.getText(findTestObject('Object Repository/Kartu Kredit/Buat baru/Get Text Nomor Tujuan'), 0)

Mobile.comment(getNoTujuan)

GlobalVariable.getCreditNumber = getNoTujuan

Mobile.tap(findTestObject('Object Repository/Kartu Kredit/Buat baru/Button - Lanjutkan'), 0)
