import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys


Mobile.tap(findTestObject('Object Repository/Kartu Kredit/Konten Struk Pembayaran Prod/Lihat Detail Transaksi'), 0)

Mobile.verifyElementExist(findTestObject('Object Repository/Kartu Kredit/Konten Struk Pembayaran Prod/Tanggal'), 0)
def time = Mobile.getText(findTestObject('Object Repository/Kartu Kredit/Konten Struk Pembayaran Prod/Tanggal'), 0)

Mobile.verifyElementExist(findTestObject('Object Repository/Kartu Kredit/Content payment receipt/Nomor Referensi'), 0)
def refnum = Mobile.getText(findTestObject('Object Repository/Kartu Kredit/Content payment receipt/Nomor Referensi'), 0)


Mobile.verifyElementExist(findTestObject('Object Repository/Kartu Kredit/Content payment receipt/Sumber Dana'), 0)
Mobile.verifyElementExist(findTestObject('Object Repository/Kartu Kredit/Konten Struk Pembayaran Prod/Jenis Transaksi'), 0)
Mobile.verifyElementExist(findTestObject('Object Repository/Kartu Kredit/Konten Struk Pembayaran Prod/Nama Bank'), 0)
Mobile.verifyElementExist(findTestObject('Object Repository/Kartu Kredit/Konten Struk Pembayaran Prod/Nomor kartu'), 0)
Mobile.verifyElementExist(findTestObject('Object Repository/Kartu Kredit/Konten Struk Pembayaran Prod/Nama Pemilik'), 0)

Mobile.verifyElementExist(findTestObject('Object Repository/Kartu Kredit/Konten Struk Pembayaran Prod/Nominal'), 0)
def nominal = Mobile.getText(findTestObject('Object Repository/Kartu Kredit/Konten Struk Pembayaran Prod/Nominal'), 0)
nominal = nominal.replaceAll("\\D+","")
Mobile.comment(nominal)

Mobile.verifyElementExist(findTestObject('Object Repository/Kartu Kredit/Konten Struk Pembayaran Prod/Biaya Admin'), 0)
def biayaAdmin = Mobile.getText(findTestObject('Object Repository/Kartu Kredit/Konten Struk Pembayaran Prod/Biaya Admin'), 0)
biayaAdmin = biayaAdmin.replaceAll("\\D+","")
Mobile.comment(biayaAdmin)

Mobile.verifyElementExist(findTestObject('Object Repository/Kartu Kredit/Konten Struk Pembayaran Prod/Total'), 0)
def total = Mobile.getText(findTestObject('Object Repository/Kartu Kredit/Konten Struk Pembayaran Prod/Total'), 0)

nominal = Integer.parseInt(nominal)
biayaAdmin = Integer.parseInt(biayaAdmin)
def verifyTotalHarga = nominal + biayaAdmin

if(total == verifyTotalHarga) {
	Mobile.comment(total)
	Mobile.comment(verifyTotalHarga)
}

CustomKeywords.'common.other.fileWrite'(time, refnum, total, "Kartu Kredit")

CustomKeywords.'common.screenshot.takeScreenshotAsCheckpoint'()

Mobile.tap(findTestObject('Object Repository/Kartu Kredit/Konten Struk Pembayaran Prod/android.widget.Button - OK'), 0)