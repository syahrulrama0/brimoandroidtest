import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys


def getSumberDana = GlobalVariable.getNoRekening
getSumberDana = getSumberDana.replaceAll("\\D+","")
def SumberDana = Mobile.getText(findTestObject('Kartu Kredit/Konfirmasi prod/konfirmasi sumber dana'), 0)
SumberDana = SumberDana.replaceAll("\\D+","")

if(SumberDana == getSumberDana) {
	Mobile.comment(SumberDana)
	Mobile.comment(getSumberDana)
}else {
	throw new Exception ("Nomor Sumber Dana Berbeda")
}

Mobile.verifyElementExist(findTestObject('Object Repository/Kartu Kredit/Konfirmasi prod/Konfirmasi nomor tujuan'), 0)

Mobile.verifyElementExist(findTestObject('Object Repository/Kartu Kredit/Konfirmasi prod/Konfirmasi Nominal'), 0)
Mobile.verifyElementExist(findTestObject('Object Repository/Kartu Kredit/Konfirmasi prod/Konfirmasi biaya admin'), 0)
Mobile.verifyElementExist(findTestObject('Object Repository/Kartu Kredit/Konfirmasi prod/Konfirmasi total'), 0)


Mobile.tap(findTestObject('Object Repository/Kartu Kredit/Konfirmasi prod/android.widget.Button - Bayar'), 0)


