import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.keyword.excel.ExcelKeywords

import internal.GlobalVariable

Mobile.tap(findTestObject('Object Repository/Kartu Kredit/Buat baru/Dropdown Pilih Bank'), 0)
String pilihBank

String excelFile = GlobalVariable.excelFile
sheet = ExcelKeywords.getExcelSheetByName(GlobalVariable.excelFile, "Kartu Kredit")
String creditCard = ExcelKeywords.getCellValueByAddress(sheet, "A"+GlobalVariable.giveCases)

switch(creditCard) {
	case "Bank_BRI" :
	pilihBank = Mobile.getText(findTestObject('Object Repository/Kartu Kredit/Buat baru/android.widget.TextView - Bank BRI'), 0)
	Mobile.tap(findTestObject('Object Repository/Kartu Kredit/Buat baru/android.widget.TextView - Bank BRI'), 0)
	break
	case "Citibank_Card" :
	pilihBank = Mobile.getText(findTestObject('Object Repository/Kartu Kredit/Buat baru/android.widget.TextView - Citibank Card'), 0)
	Mobile.tap(findTestObject('Object Repository/Kartu Kredit/Buat baru/android.widget.TextView - Citibank Card'), 0)
	break
	case "DBS_Card" :
	pilihBank = Mobile.getText(findTestObject('Object Repository/Kartu Kredit/Buat baru/android.widget.TextView - DBS Card'), 0)
	Mobile.tap(findTestObject('Object Repository/Kartu Kredit/Buat baru/android.widget.TextView - DBS Card'), 0)
	break
	case "HSBC_Card" :
	pilihBank = Mobile.getText(findTestObject('Object Repository/Kartu Kredit/Buat baru/android.widget.TextView - HSBC Card'), 0)
	Mobile.tap(findTestObject('Object Repository/Kartu Kredit/Buat baru/android.widget.TextView - HSBC Card'), 0)
	break
	case "Standard_Chartered_Card" :
	pilihBank = Mobile.getText(findTestObject('Object Repository/Kartu Kredit/Buat baru/android.widget.TextView - Standard Chartered Card'), 0)
	Mobile.tap(findTestObject('Object Repository/Kartu Kredit/Buat baru/android.widget.TextView - Standard Chartered Card'), 0)
	break
}



