import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.keyword.excel.ExcelKeywords

import internal.GlobalVariable

Mobile.verifyElementExist(findTestObject('Object Repository/PIN/Masukkan PIN'), 0)

//CustomKeywords.'common.other.takeScreenShot'()
String excelFile = GlobalVariable.excelFile
sheet = ExcelKeywords.getExcelSheetByName(GlobalVariable.excelFile, "Common")
String pinNumber = ExcelKeywords.getCellValueByAddress(sheet, "C2")

//Mobile.verifyElementExist(findTestObject('Object Repository/PIN/Masukan test'),0)

for (int i=1; i<=6; i++) {
	Mobile.tap(findTestObject('Object Repository/PIN Prod/android.widget.TextView - ' + pinNumber.substring(i-1,i)),0)
}

