import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.keyword.excel.ExcelKeywords 

Mobile.verifyElementExist(findTestObject('Object Repository/PIN/Masukkan PIN'), 0)

sheet1 = ExcelKeywords.getExcelSheet("datasets\\Book1.xlsx")

String pinNumber = ExcelKeywords.getCellByAddress(sheet1, "C2")

for (int i=1; i<=5; i++) {
	Mobile.tap(findTestObject('Object Repository/PIN Prod/android.widget.TextView - ' + pinNumber.substring(i-1,i)),0)
}
