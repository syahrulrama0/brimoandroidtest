import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

if(Mobile.verifyElementText(findTestObject('Object Repository/DPLK/Content Payment Receipt/android.widget.TextView - Transaksi Berhasil'), "Transaksi Berhasil") == false) {
	
	throw new Exception ("Transaksi gagal")
}

def time = Mobile.getText(findTestObject('Object Repository/Transaksi Berhasil/Transaksi Main/Tanggal'),0)
Mobile.comment(time)

def refnum = Mobile.getText(findTestObject('Object Repository/Transaksi Berhasil/Transaksi Main/Refnum'),0)
Mobile.comment(refnum)

def amount = Mobile.getText(findTestObject('Object Repository/Transaksi Berhasil/Transaksi Main/Nominal'),0)
Mobile.comment(amount)

def fee = Mobile.getText(findTestObject('Object Repository/Transaksi Berhasil/Transaksi Main/Fee'),0)
Mobile.comment(fee)

CustomKeywords.'common.other.fileWrite'("Donasi", time, refnum, amount, fee)

def amountLayout = Mobile.getText(findTestObject('Object Repository/Transaksi Berhasil/Transaksi Main/Nominal Layout'),0)
Mobile.comment(amountLayout)
