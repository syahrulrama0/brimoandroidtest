import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

//Mobile.verifyElementExist(findTestObject('Object Repository/Transaksi Berhasil/Transaksi Berhasil'),0)

GlobalVariable.reportStatus = "success"

CustomKeywords.'common.other.takeScreenShot'()

if (feature.toString() == "Listrik") {
	 
}

else {
	def time = Mobile.getText(findTestObject('Object Repository/Transaksi Berhasil/Transaksi Main/Tanggal'),0)
	Mobile.comment(time)
	
	def refnum = Mobile.getText(findTestObject('Object Repository/Transaksi Berhasil/Transaksi Main/Refnum'),0)
	Mobile.comment(refnum)
	
	def amount = Mobile.getText(findTestObject('Object Repository/Transaksi Berhasil/Transaksi Main/Nominal'),0)
	Mobile.comment(amount)
	
	def fee = Mobile.getText(findTestObject('Object Repository/Transaksi Berhasil/Transaksi Main/Fee'),0)
	Mobile.comment(fee)
	
	CustomKeywords.'common.other.fileWrite'(feature, time, refnum, amount, fee)
}

//def totalPayment = Mobile.getText(findTestObject('Object Repository/common/android.widget.TextView - Rp1.001.500 total transaksi'),0)
//
//totalPayment = totalPayment.replaceAll("\\D+", "")
//
//long payment = Long.parseLong(totalPayment)
//
//def balance = GlobalVariable.getBalance
//
//long getBalance = Long.parseLong(balance)
//
//long newBalance = getBalance - payment
//
//GlobalVariable.newBalance = newBalance
//
//System.out.println(getBalance)
//
//System.out.println(payment)
//
//System.out.println(newBalance)

Mobile.tap(findTestObject('Object Repository/Transaksi Berhasil/Button OK'),0)