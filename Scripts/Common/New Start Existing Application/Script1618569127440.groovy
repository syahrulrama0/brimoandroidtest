import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.keyword.excel.ExcelKeywords

import internal.GlobalVariable as GlobalVariable

sheet1 = ExcelKeywords.getExcelSheet("datasets\\Book1.xlsx")

String username = ExcelKeywords.getCellByAddress(sheet1, "A"+rowNumber)

String password = ExcelKeywords.getCellByAddress(sheet1, "B"+rowNumber)

Mobile.startApplication(GlobalVariable.identifierApp, false)

Mobile.delay(5)

Mobile.tap(findTestObject('Laman Login not First Time Login/Button Login'), 0)

Mobile.verifyElementVisible(findTestObject('Laman Login/Image BRImo'), 0)

Mobile.verifyElementVisible(findTestObject('Laman Login/Selamat Datang'), 0)

Mobile.setText(findTestObject('Laman Login/Field Username'), username.toString(), 0)

Mobile.setText(findTestObject('Laman Login/Field Password'), password.toString(), 0)

Mobile.tap(findTestObject('Laman Login/Button Login'), 0)
