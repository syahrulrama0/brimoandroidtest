import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import java.util.*
import java.sql.*
import java.sql.ResultSet as ResultSet
import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory as MobileDriverFactory
import io.appium.java_client.android.AndroidDriver as AndroidDriver
import io.appium.java_client.android.AndroidKeyCode as AndroidKeyCode
import io.appium.java_client.android.nativekey.AndroidKey as AndroidKey
import io.appium.java_client.android.nativekey.KeyEvent as KeyEvent

GlobalVariable.dbName = 'ibank_brimo'

Mobile.callTestCase(findTestCase('Test Cases/Common/Connection'), [:], FailureHandling.STOP_ON_FAILURE)

String username = "brimosv007"

ResultSet response = CustomKeywords.'common.methods.executeQuery'(('SELECT * FROM tbl_brimo_activation WHERE username = "' +
	username.toString()) + '"')

response.next()

String sms_link = response.getString('sms_link')

Mobile.comment(sms_link)

StringBuffer sb = new StringBuffer(sms_link)

sb.delete(4,5)

Mobile.comment(sb.toString())

String newLink = sb.toString()

String target = "brimo.bri.co.id"

String replacement = "172.18.136.91"

int startIndex = newLink.indexOf(target)

int stopIndex = startIndex + target.length()

StringBuilder builder = new StringBuilder(newLink)

String hash = builder.replace(startIndex, stopIndex, replacement);

Mobile.comment(hash)

//assertTrue(builder.toString().contains(replacement));

Mobile.comment(newLink)

Mobile.startExistingApplication('com.google.android.googlequicksearchbox', FailureHandling.STOP_ON_FAILURE)

Mobile.tap(findTestObject('Object Repository/Belum Punya Akun/Search'), 0)

Mobile.setText(findTestObject('Object Repository/Belum Punya Akun/Searchbox'), hash,0)

AndroidDriver<?> driver = MobileDriverFactory.getDriver()

driver.pressKeyCode(AndroidKeyCode.ENTER)

Mobile.tap(findTestObject('Object Repository/Belum Punya Akun/Buka Brimo'),0)

//Mobile.startApplication(GlobalVariable.identifierApp, false)

//Mobile.setText(findTestObject('Object Repository/Test/android.widget.EditText - Search or type web address'), findTestData('Database').getValue(1, 1),0)
//
//Mobile.delay(10)
//
//Mobile.pressHome()
//
//device_Height = Mobile.getDeviceHeight()
//
//device_Width = Mobile.getDeviceWidth()
//
//int startX = device_Width / 2
//
//int endX = startX
//
//int startY = device_Height * 0.30
//
//int endY = device_Height * 0.70
//
////'Swipe Vertical from top to bottom'
////
//Mobile.swipe(startX, endY, endX, startY)
//
////Mobile.swipe(startX, startY, endX, endY)
//'Swipe Vertical from bottom to top'
//Mobile.tap(findTestObject('Object Repository/Test/android.widget.TextView - Chrome'), 0)
//
//String link = 'https://facebook.com'
//
//Mobile.setText(findTestObject('Object Repository/Test/android.widget.EditText - Search or type web address'), link, 0)
//
//AndroidDriver<?> driver = MobileDriverFactory.getDriver()
//
//driver.pressKeyCode(AndroidKeyCode.ENTER)