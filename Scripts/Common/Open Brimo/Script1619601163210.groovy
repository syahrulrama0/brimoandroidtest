import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import java.sql.*

import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.keyword.excel.ExcelKeywords

import internal.GlobalVariable
import io.appium.java_client.android.AndroidDriver
import io.appium.java_client.android.AndroidKeyCode

String excelFile = GlobalVariable.excelFile
sheet = ExcelKeywords.getExcelSheetByName(GlobalVariable.excelFile, "Common")
String username = ExcelKeywords.getCellValueByAddress(sheet, "A2")
String password = ExcelKeywords.getCellValueByAddress(sheet, "B2")
String pinNumber = ExcelKeywords.getCellValueByAddress(sheet, "C2")
String identifierApp = ExcelKeywords.getCellValueByAddress(sheet, "D2")
String condition = ExcelKeywords.getCellValueByAddress(sheet, "E2")
identifierApp = "Resource/"+identifierApp
GlobalVariable.identifierApp = identifierApp

Mobile.comment(GlobalVariable.identifierApp)

if (condition == "false") {
	Mobile.startApplication(GlobalVariable.identifierApp, false)
}
if (condition == "true") {
	Mobile.startApplication(GlobalVariable.identifierApp, true)
}

if (Mobile.verifyElementVisible(findTestObject('Object Repository/Belum Punya Akun/Allowing Brimo'),3, FailureHandling.OPTIONAL) == true) {
	Mobile.tap(findTestObject('Object Repository/Belum Punya Akun/Button Allow First Time Login'),0)
}

//int looping = cases + 1
//ini_untuk_debug
//GlobalVariable.giveCases = looping
String loginStatus = 0

boolean connection = false
boolean inputPin = false

if (Mobile.verifyElementExist(findTestObject('Object Repository/Belum Punya Akun/Belum Punya Akun'), 3, FailureHandling.OPTIONAL) == true) {
	Mobile.tap(findTestObject('Object Repository/Belum Punya Akun/Punya Akun'),0)
	connection = true
}
else {
	Mobile.tap(findTestObject('Laman Login not First Time Login/Button Login'), 0)
}

Mobile.verifyElementVisible(findTestObject('Laman Login/Image BRImo'), 0)

Mobile.verifyElementVisible(findTestObject('Laman Login/Selamat Datang'), 0)

Mobile.setText(findTestObject('Laman Login/Field Username'), username.toString(), 0)

Mobile.setText(findTestObject('Laman Login/Field Password'), password.toString(), 0)

Mobile.tap(findTestObject('Laman Login/Button Login'), 0)

Mobile.delay(7)

if (Mobile.verifyElementExist(findTestObject('Object Repository/Belum Punya Akun/Login'),3,FailureHandling.OPTIONAL)==true) {
	Mobile.callTestCase(findTestCase('Test Cases/Common/Connection'), [:], FailureHandling.STOP_ON_FAILURE)
	String account = "brimosv008"
	CustomKeywords.'common.methods.executeUpdate'(('UPDATE tbl_user SET login_status="'+loginStatus+'" WHERE username = "' +
		account.toString()) + '"')
	Mobile.callTestCase(findTestCase('Test Cases/Common/Closed Connection'), [:], FailureHandling.STOP_ON_FAILURE)
	Mobile.delay(5)
	Mobile.tap(findTestObject('Laman Login/Button Login'), 0)
}

if (Mobile.verifyElementVisible(findTestObject('Object Repository/Belum Punya Akun/Konfirmasi PIN'), 3, FailureHandling.OPTIONAL) == true) {
	connection = true
}

if (Mobile.verifyElementVisible(findTestObject('Object Repository/Belum Punya Akun/Selamat Datang Kembali'),3,FailureHandling.OPTIONAL)==true) {
	Mobile.tap(findTestObject('Object Repository/Belum Punya Akun/Button Ya'),0)
	connection = true
	for (int i=1; i<=6; i++) {
		Mobile.tap(findTestObject('Object Repository/PIN Prod/android.widget.TextView - ' + pinNumber.substring(i-1,i)),0)
		inputPin = true
	}
}

if (connection == true) {
	if (inputPin == false) {
		for (int i=1; i<=6; i++) {
			Mobile.tap(findTestObject('Object Repository/PIN Prod/android.widget.TextView - ' + pinNumber.substring(i-1,i)),0)
		}
	}
	
	GlobalVariable.dbName = 'ibank_brimo'
	
	Mobile.callTestCase(findTestCase('Test Cases/Common/Connection'), [:], FailureHandling.STOP_ON_FAILURE)
	
	String account = "brimosv008"
	
	ResultSet response = CustomKeywords.'common.methods.executeQuery'(('SELECT * FROM tbl_brimo_activation WHERE username = "' +
		account.toString()) + '"')
	
	response.next()
	
	String sms_link = response.getString('sms_link')
	
	StringBuffer sb = new StringBuffer(sms_link)
	
	sb.delete(4,5)
	
	String newLink = sb.toString()
	
	String target = "brimo.bri.co.id"
	
	String replacement = "172.18.136.91"
	
	int startIndex = newLink.indexOf(target)
	
	int stopIndex = startIndex + target.length()
	
	StringBuilder builder = new StringBuilder(newLink)
	
	String hash = builder.replace(startIndex, stopIndex, replacement)
	
	Mobile.comment(hash)
	
	Mobile.startExistingApplication('com.android.chrome', FailureHandling.STOP_ON_FAILURE)
	
	if (Mobile.verifyElementVisible(findTestObject('Object Repository/Belum Punya Akun/Allow mic'), 3,FailureHandling.OPTIONAL) == true) {
		Mobile.tap(findTestObject('Object Repository/Belum Punya Akun/Button Deny'),0)
		Mobile.closeApplication()
		Mobile.startExistingApplication('com.google.android.googlequicksearchbox', FailureHandling.STOP_ON_FAILURE)
		if (Mobile.verifyElementVisible(findTestObject('Object Repository/Belum Punya Akun/Sign To Get Started'), 3,FailureHandling.OPTIONAL) == true) {
			Mobile.tap(findTestObject('Object Repository/Belum Punya Akun/No Thanks'),0)
			Mobile.startExistingApplication('com.google.android.googlequicksearchbox', FailureHandling.STOP_ON_FAILURE)
		}
	}
	
	if (Mobile.verifyElementVisible(findTestObject('Object Repository/Belum Punya Akun/WasUsingChrome'), 3,FailureHandling.OPTIONAL) == true) {
		Mobile.tap(findTestObject('Object Repository/Belum Punya Akun/Search Box'), 0)
		Mobile.tap(findTestObject('Object Repository/Belum Punya Akun/Button Close (1)'), 0)
		Mobile.setText(findTestObject('Object Repository/Belum Punya Akun/Empty SearchBar'), hash,0)
	}
	if (Mobile.verifyElementVisible(findTestObject('Object Repository/Belum Punya Akun/Search box (1)'), 3,FailureHandling.OPTIONAL) == true) {
		Mobile.tap(findTestObject('Object Repository/Belum Punya Akun/Search box (1)'), 0)
		Mobile.setText(findTestObject('Object Repository/Belum Punya Akun/Empty SearchBar'), hash,0)
	}
	
	AndroidDriver<?> driver = MobileDriverFactory.getDriver()
	
	driver.pressKeyCode(AndroidKeyCode.ENTER)
	
	if (Mobile.verifyElementVisible(findTestObject('Object Repository/Belum Punya Akun/Icon close'), 10, FailureHandling.OPTIONAL) == true) {
		Mobile.tap(findTestObject('Object Repository/Belum Punya Akun/Icon close'),0)
	}
	
	Mobile.tap(findTestObject('Object Repository/Belum Punya Akun/Buka Brimo'),0)
	
	if (Mobile.verifyElementVisible(findTestObject('Object Repository/Belum Punya Akun/Button Close'), 10, FailureHandling.OPTIONAL) == true) {
		Mobile.tap(findTestObject('Object Repository/Belum Punya Akun/Button Close'),0)
	}
}