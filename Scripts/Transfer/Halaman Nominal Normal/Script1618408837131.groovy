import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import javax.xml.bind.annotation.XmlElementDecl.GLOBAL

import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.keyword.excel.ExcelKeywords

import internal.GlobalVariable 


String excelFile = GlobalVariable.excelFile
sheet = ExcelKeywords.getExcelSheetByName(GlobalVariable.excelFile, "Transfer")
String amount = ExcelKeywords.getCellValueByAddress(sheet, "C"+GlobalVariable.giveCases)
Mobile.setText(findTestObject('Object Repository/Test Transfer/Input Nominal Transfer - 0'), amount, 0)
Mobile.delay(3)

//def nominal = Mobile.getText(findTestObject('Object Repository/Test Transfer/Input Nominal Transfer - 0'),0)
//nominal = nominal.replaceAll("\\D+","")
//
//GlobalVariable.amount = nominal

CustomKeywords.'common.other.takeScreenShot'()
