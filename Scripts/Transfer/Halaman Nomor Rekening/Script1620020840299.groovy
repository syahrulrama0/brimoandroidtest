import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.keyword.excel.ExcelKeywords

import internal.GlobalVariable


String excelFile = GlobalVariable.excelFile
sheet = ExcelKeywords.getExcelSheetByName(GlobalVariable.excelFile, "Transfer")
String rekening = ExcelKeywords.getCellValueByAddress(sheet, "G"+GlobalVariable.giveCases)

Mobile.comment(rekening)

Mobile.setText(findTestObject('Object Repository/Transfer2/android.widget.EditText - Masukkan Nomor Rekening  Alias'), rekening.toString(),
	0)


CustomKeywords.'common.other.takeScreenShot'()