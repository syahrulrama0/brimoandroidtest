import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

Date today = new Date()

//String todaysDate = today.format('MM_dd_yy')
//
//String nowTime = today.format('hh_mm_ss')
//		
//Mobile.delay(3)
//		
//Mobile.takeScreenshotAsCheckpoint(((('/Users/Dimas PM/Katalon Studio/TransferBRIMO/Screenshot Katalon/' + todaysDate) +
//				'-') + nowTime) + '.png', FailureHandling.STOP_ON_FAILURE)

//CustomKeywords.'common.screenshoot.takeScreenshotAsCheckpoint'()


Mobile.delay(3)
setting = setting.toString()

		switch (setting) {
			case 'Edit':

				Mobile.tap(findTestObject('Object Repository/Test Transfer/Edit Kontak - Setting'), 0)

				Mobile.tap(findTestObject('Object Repository/Test Transfer/Edit Kontak - Edit daftar'), 0)

				Mobile.setText(findTestObject('Object Repository/Test Transfer/Edit Daftar Transfer - Simpan Sebagai'),'Naufal jordan', 0)

				Mobile.tap(findTestObject('Object Repository/Test Transfer/Edit Kontak - Button Simpan'), 0)

				Mobile.verifyElementExist(findTestObject('Object Repository/Test Transfer/Edit Kontak Verify - Daftar berhasil diedit'), 0)

				break

			case 'Favorite':


				Mobile.tap(findTestObject('Object Repository/Test Transfer/Edit Kontak - Setting'), 0)

				Mobile.tap(findTestObject('Object Repository/Test Transfer/Edit Kontak - Jadikan favorit'), 0)

				Mobile.verifyElementVisible(findTestObject('Object Repository/Test Transfer/Edit Kontak Verify  - Berhasil menambahkan Favorit'), 0)

				Mobile.tap(findTestObject('Object Repository/Transfer2/Transfer 2-1/android.widget.ImageButton1'), 0)

				break

			case 'Hapus Favorite':

				Mobile.tap(findTestObject('Object Repository/Test Transfer/Edit Kontak - Setting'), 0)

				Mobile.tap(findTestObject('Object Repository/Test Transfer/Edit Kontak - Hapus dari favorit'), 0)

				Mobile.verifyElementVisible(findTestObject('Object Repository/Test Transfer/Edit Kontak Verify - Berhasil dihapus dari Favorit'), 0)

				Mobile.tap(findTestObject('Object Repository/Transfer2/Transfer 2-1/android.widget.ImageButton1'), 0)
				break

			case 'Hapus Daftar':

				Mobile.tap(findTestObject('Object Repository/Test Transfer/Edit Kontak - Setting'), 0)

				Mobile.tap(findTestObject('Object Repository/Test Transfer/Edit Kontak  - Hapus dari daftar'), 0)

			//Mobile.verifyElementVisible(findTestObject(''), 0)

				break
		}
