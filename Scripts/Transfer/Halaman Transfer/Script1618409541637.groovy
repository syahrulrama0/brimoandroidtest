import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

//Mobile.callTestCase(findTestCase('General/login sukses'), [:], FailureHandling.STOP_ON_FAILURE)
Mobile.callTestCase(findTestCase('General/Database Connect'), [:], FailureHandling.STOP_ON_FAILURE)
Mobile.tap(findTestObject('Object Repository/Transfer/android.widget.ImageView'), 0)


if (contact =="no" && history == "no") {
	CustomKeywords.'database.methods.executeUpdate'(('DELETE FROM tbl_history_transfer WHERE username = "brimosv008') + '"')
	//REKENING_BRI
	CustomKeywords.'database.methods.executeUpdate'(('DELETE FROM tbl_list_account_bri  WHERE username = "brimosv008' ) + '" and id = 9011')
	
	
	//REKENING_NON_BRI
	CustomKeywords.'database.methods.executeUpdate'(('DELETE FROM tbl_list_account_nonbri  WHERE username = "brimosv008' ) + '" and id = ')
	
	Mobile.verifyElementVisible(findTestObject('Object Repository/Test Transfer/History item'), 0)
	Mobile.verifyElementVisible(findTestObject('Object Repository/Test Transfer/Cari Daftar item'), 0)
	Mobile.verifyElementVisible(findTestObject('Object Repository/Test Transfer/Daftar Kontak Item'), 0)

}
else if (contact == "yes" && history == "no") {
	
	//REKENING_BRI
	CustomKeywords.'database.methods.executeUpdate'(("INSERT INTO tbl_list_account_bri (id,username,account,account_name,account_nickname,status,favorite) VALUES (9212,'brimosv008','020601000064301','Dimas Prayogo M','Dimas',1,0)"))
	//CustomKeywords.'database.methods.executeUpdate'(("INSERT INTO tbl_trx_transfer_brimo (reference_num,username,transfer_type,status,account,account_destination,account_name_destination,amount,admin_bank,bank_destination,trx_schedule_type,account_type) VALUES ('302317268511','brimoqa001','PaymentTransferLuar',0,'020601000136506','01456325877','DUMMY NAME PRM',10000.00,6500.00,'BANK BCA','SGRA','BritAma / IDR')"))
	CustomKeywords.'database.methods.executeUpdate'(('DELETE FROM tbl_history_transfer WHERE username = "brimosv008') + '"')
	
	//REKENING_NON_BRI
	CustomKeywords.'database.methods.executeUpdate'(("INSERT INTO tbl_list_account_nonbri (id,username,account,bank_code,account_name,account_nickname,status,favorite) VALUES (9212,'brimosv008','020601000064301','009','Dimas Prayogo M','Dimas',1,0)"))
	
	
	//Mobile.verifyElementVisible(findTestObject('Object Repository/Test Transfer/History item'), 0)
	Mobile.verifyElementVisible(findTestObject('Object Repository/Test Transfer/Cari Daftar item'), 0)
	Mobile.verifyElementVisible(findTestObject('Object Repository/Test Transfer/Daftar Kontak Item'), 0)
}
else if (contact == "no" && history == "yes") {
	String history2 = '[{"number":"020601010998508","name":"BANK BRI","customer":"TINA PRIATINA","code":"002"},{"number":"020601000051308","name":"BANK BRI","customer":"MENSOS RI","code":"002"},{"number":"020601122201504","name":"BANK BRI","customer":"FITRA MEFA PRATAMA","code":"002"},{"number":"020601000136506","name":"BANK BRI","customer":"ROBERT FRANS","code":"002"}]'
	String history3 = '[{"number":"00520000001500","name":"BANK BCA","customer":"01-01-CHIP-GOOD","code":"014"},{"number":"4564564560","name":"BANK BRIAGRO","customer":"DUMMY NAME BSM TITIKDUA","code":"494"},{"number":"020601010998508",{"number":"2580147802","name":"BANK DKI","customer":"DUMMY NAME BSM TITIKDUA","code":"111"},{"number":"5234567890","name":"BANK BNI","customer":"DUMMY NAME PRM","code":"009"},{"number":"2580645678","name":"BANK BCA","customer":"DUMMY NAME PRM","code":"014"}]'
	
	CustomKeywords.'database.methods.executeUpdate'(("INSERT INTO tbl_history_transfer (username,value) VALUES ('brimosv008','"+ history2 +"')"))
	CustomKeywords.'database.methods.executeUpdate'(("INSERT INTO tbl_history_transfer (username,value) VALUES ('brimosv008','"+ history3 +"')"))
	
	
	//REKENING_BRI
	CustomKeywords.'database.methods.executeUpdate'(('DELETE FROM tbl_list_account_bri WHERE username = "brimosv008' ) + '" and id = 9011')
	
	//CustomKeywords.'database.methods.executeUpdate'(('DELETE FROM tbl_trx_transfer_brimo WHERE username = "brimosv008' ) + '" and reference_num = "444"')

	//REKENING_NON_BRI
	CustomKeywords.'database.methods.executeUpdate'(('DELETE FROM tbl_list_account_nonbri  WHERE username = "brimosv008' ) + '" and id = ')
	
	Mobile.verifyElementVisible(findTestObject('Object Repository/Test Transfer/History item'), 0)
	//Mobile.verifyElementVisible(findTestObject('Object Repository/Test Transfer/Cari Daftar item'), 0)
	//Mobile.verifyElementVisible(findTestObject('Object Repository/Test Transfer/Daftar Kontak Item'), 0)
	
}
else if (contact == "yes" && history == "yes"){
	String history2 = '[{"number":"020601010998508","name":"BANK BRI","customer":"TINA PRIATINA","code":"002"},{"number":"020601000051308","name":"BANK BRI","customer":"MENSOS RI","code":"002"},{"number":"020601122201504","name":"BANK BRI","customer":"FITRA MEFA PRATAMA","code":"002"},{"number":"020601000136506","name":"BANK BRI","customer":"ROBERT FRANS","code":"002"}]'
	String history3 = '[{"number":"00520000001500","name":"BANK BCA","customer":"01-01-CHIP-GOOD","code":"014"},{"number":"4564564560","name":"BANK BRIAGRO","customer":"DUMMY NAME BSM TITIKDUA","code":"494"},{"number":"020601010998508",{"number":"2580147802","name":"BANK DKI","customer":"DUMMY NAME BSM TITIKDUA","code":"111"},{"number":"5234567890","name":"BANK BNI","customer":"DUMMY NAME PRM","code":"009"},{"number":"2580645678","name":"BANK BCA","customer":"DUMMY NAME PRM","code":"014"}]'
	
	
	//REKENING_BANK_BRI
	CustomKeywords.'database.methods.executeUpdate'(("INSERT INTO tbl_history_transfer (username, value) VALUES ('brimosv008','"+ history2 +"')"))
	CustomKeywords.'database.methods.executeUpdate'(("INSERT INTO tbl_history_transfer (username,value) VALUES ('brimosv008','"+ history3 +"')"))
	CustomKeywords.'database.methods.executeUpdate'(("INSERT INTO tbl_list_account_bri (id,username,account,account_name,account_nickname,status,favorite) VALUES (9212,'brimosv008','020601000064301','Dimas Prayogo M','Dimas',1,0)"))
	
	//CustomKeywords.'database.methods.executeUpdate'(("INSERT INTO tbl_trx_transfer_brimo (reference_num,username,transfer_type,status,account,account_destination,account_name_destination,amount,admin_bank,bank_destination,trx_schedule_type,account_type) VALUES ('302317268511','brimoqa001','PaymentTransferLuar',0,'020601000136506','01456325877','DUMMY NAME PRM',10000.00,6500.00,'BANK BCA','SGRA','BritAma / IDR')"))

	//REKENING_NON_BRI
	CustomKeywords.'database.methods.executeUpdate'(("INSERT INTO tbl_list_account_nonbri (id,username,account,bank_code,account_name,account_nickname,status,favorite) VALUES (9212,'brimosv008','020601000064301','009','Dimas Prayogo M','Dimas',1,0)"))
	
	
	
	Mobile.verifyElementVisible(findTestObject('Object Repository/Test Transfer/History item'), 0)
	Mobile.verifyElementVisible(findTestObject('Object Repository/Test Transfer/Cari Daftar item'), 0)
	Mobile.verifyElementVisible(findTestObject('Object Repository/Test Transfer/Daftar Kontak Item'), 0)
}

