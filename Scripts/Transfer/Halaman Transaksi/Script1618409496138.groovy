import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile

import internal.GlobalVariable as GlobalVariable

		
Mobile.delay(3)
		


		Mobile.verifyElementExist(findTestObject('Object Repository/Test Transfer/Transaksi Berhasil - Tanggal'),0)
		Mobile.verifyElementExist(findTestObject('Object Repository/Test Transfer/Transaksi Berhasil - Nomor Referensi'),0)
		Mobile.verifyElementExist(findTestObject('Object Repository/Test Transfer/Transaksi Berhasil - Sumber Dana'),0)
		Mobile.verifyElementExist(findTestObject('Object Repository/Test Transfer/Transaksi Berhasil - Jenis Transaksi'),0)
		Mobile.verifyElementExist(findTestObject('Object Repository/Test Transfer/Transaksi Berhasil - Bank Tujuan'),0)
		Mobile.verifyElementExist(findTestObject('Object Repository/Test Transfer/Transaksi Berhasil - Nomor Tujuan'),0)
		Mobile.verifyElementExist(findTestObject('Object Repository/Test Transfer/Transaksi Berhasil -- Nama Tujuan'),0)
		Mobile.verifyElementExist(findTestObject('Object Repository/Test Transfer/Transaksi Berhasil - Nominal'),0)
		Mobile.verifyElementExist(findTestObject('Object Repository/Test Transfer/Transaksi Berhasil - Biaya Admin'), 0)
		
		
		Mobile.tap(findTestObject('Object Repository/Daftar Transfer/Transaksi Berhasil- Lihat Detail Transaksi'), 0)

		Mobile.tap(findTestObject('Object Repository/Transfer/android.widget.TextView - Lihat Lebih Sedikit'), 0)

		
		
		def time = Mobile.getText(findTestObject('Object Repository/Test Transfer/Transaksi Berhasil - Tanggal'), 0)
		Mobile.comment(time)
		
		def refnum = Mobile.getText(findTestObject('Object Repository/Test Transfer/Transaksi Berhasil - Nomor Referensi'), 0)
		Mobile.comment(refnum)
		
//		def getSumberDana = Mobile.getText(findTestObject('Object Repository/Test Transfer/Transaksi Berhasil - Sumber Dana'), 0)
//		Mobile.comment(getSumberDana)
		
//		def getJenisTransaki = Mobile.getText(findTestObject('Object Repository/Test Transfer/Transaksi Berhasil - Jenis Transaksi'), 0)
//		Mobile.comment(getJenisTransaki)
		
//		def getBankTujuan = Mobile.getText(findTestObject('Object Repository/Test Transfer/Transaksi Berhasil - Bank Tujuan'), 0)
//		Mobile.comment(getBankTujuan)
		
//		def getNoTujuan = Mobile.getText(findTestObject('Object Repository/Test Transfer/Transaksi Berhasil - Nomor Tujuan'), 0)
//		Mobile.comment(getNoTujuan)
		
//		def getNamaTujuan = Mobile.getText(findTestObject('Object Repository/Test Transfer/Transaksi Berhasil -- Nama Tujuan'), 0)
//		Mobile.comment(getNoTujuan)
		
		def nominal = Mobile.getText(findTestObject('Object Repository/Test Transfer/Transaksi Berhasil - Nominal'), 0)
		Mobile.comment(nominal)
		nominal = nominal.replaceAll("\\D+","")		
		GlobalVariable.amount = nominal
		
		
		def fee = Mobile.getText(findTestObject('Object Repository/Test Transfer/Transaksi Berhasil - Biaya Admin'), 0)
		Mobile.comment(fee)
		
		CustomKeywords.'common.other.fileWrite'("Transfer", time, refnum, nominal, fee)
		//CustomKeywords.'common.ConvertReport.fileWrite'(getTime, getNoRef,"Menu Transfer")
		

		
		
		Mobile.tap(findTestObject('Object Repository/Transfer/android.widget.Button - OK'), 0)
		
		Mobile.startApplication(GlobalVariable.identifierApp, false)
	