import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile

import internal.GlobalVariable




//Mobile.verifyElementExist(findTestObject('Object Repository/Transfer/Nomor Rekening Transfer'), 0, FailureHandling.STOP_ON_FAILURE)
//Mobile.verifyElementExist(findTestObject('Object Repository/Transfer/Nomor Rekening Bank Tujuan Transfer'), 0, FailureHandling.STOP_ON_FAILURE)
//Mobile.verifyElementExist(findTestObject('Object Repository/Transfer/Halaman Konfirmasi Nominal'), 0, FailureHandling.STOP_ON_FAILURE)
//Mobile.verifyElementExist(findTestObject('Object Repository/Transfer/Halaman Konfirmasi Biaya Admin'), 0, FailureHandling.STOP_ON_FAILURE)
//Mobile.verifyElementExist(findTestObject('Object Repository/Transfer/Halaman Konfirmasi Total'), 0, FailureHandling.STOP_ON_FAILURE)

Mobile.verifyElementVisible(findTestObject('Object Repository/Transfer/Nomor Rekening Transfer'), 0)
Mobile.verifyElementVisible(findTestObject('Object Repository/Transfer/Nomor Rekening Bank Tujuan Transfer'), 0)
Mobile.verifyElementVisible(findTestObject('Object Repository/Transfer/Halaman Konfirmasi Nominal'), 0)
Mobile.verifyElementVisible(findTestObject('Object Repository/Transfer/Halaman Konfirmasi Biaya Admin'), 0)
Mobile.verifyElementVisible(findTestObject('Object Repository/Transfer/Halaman Konfirmasi Total'), 0)

def norek = Mobile.getText(findTestObject('Object Repository/Transfer/Nomor Rekening Transfer'),0)

norek = norek.replaceAll("\\D+","")

GlobalVariable.getNomorRekening= norek

//CustomKeywords.'common.other.takeScreenShot'()
