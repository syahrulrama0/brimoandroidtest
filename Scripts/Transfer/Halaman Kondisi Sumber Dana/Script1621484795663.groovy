import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint

import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

import com.kms.katalon.keyword.excel.ExcelKeywords

String excelFile = 'datasets\\BRIMO.xlsx'
sheet = ExcelKeywords.getExcelSheetByName("datasets\\BRIMO.xlsx", "Transfer")
String condition = ExcelKeywords.getCellValueByAddress(sheet, "D"+rowNumber)

Mobile.tap(findTestObject('Object Repository/Transfer2/Transfer 2-1/TAP - SUMBER DANA'), 0)
Mobile.delay(4)
String pilihRekening

switch(condition) {
	
	case "Saldo_Cukup":
	pilihRekening= Mobile.getText(findTestObject('Object Repository/Sumber Dana/SAVING - 1194 0100 7867 504'), 0)
	break
	
	case "Saldo_Tidak_Cukup":
	pilihRekening= Mobile.getText(findTestObject('Object Repository/Test Transfer/Verify Nominal - Saldo Anda tidak cukup'), 0)
	break
	
}

CustomKeywords.'common.screenshot.takeScreenshotAsCheckpoint'()

if(condition=="Saldo_Cukup") {
	Mobile.comment(pilihRekening)
}
else if(condition=="Saldo_Tidak_Cukup") {
	Mobile.comment(pilihRekening)
	Mobile.verifyElementText(findTestObject('Object Repository/Test Transfer/Verify Nominal - Saldo Anda tidak cukup'), "Saldo Anda tidak cukup")
	Mobile.verifyElementVisible(findTestObject('Object Repository/Transfer2/Transfer 2-1/Button Disable- Transfer'), 0)
}else {
	throw new Exception ("Kondisi tidak ditemukan")
}

GlobalVariable.getNoRekening = pilihRekening
