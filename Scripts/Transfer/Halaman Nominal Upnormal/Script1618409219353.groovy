import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

String[] amountArray = [
	"0",
	"123456789101112",
	"0",
	"1000"
]
for (int i = 0; i<=amountArray.length; i++) {
	if (i == 0) {
		Mobile.setText(findTestObject('Object Repository/Test Transfer/Input Nominal Transfer - 0'),amountArray[i], 0)
		Mobile.verifyElementVisible(findTestObject('Object Repository/Transfer2/Transfer 2-1/Button Disable- Transfer'), 0)
	}
	else if (i == 1) {
		Mobile.setText(findTestObject('Object Repository/Test Transfer/Input Nominal Transfer - 0'),amountArray[i], 0)
		def bigAmount = Mobile.getText(findTestObject('Object Repository/Test Transfer/Input Nominal Transfer - 0'), 0)
		bigAmount = bigAmount.replaceAll("\\D+","")
		if (bigAmount.length() == 12) {
			//Mobile.verifyElementVisible(findTestObject('Object Repository/Test Transfer/Sumber Dana - Button Transfer'), 0)
			Mobile.verifyElementVisible(findTestObject('Object Repository/Transfer2/Transfer 2-1/Button Disable- Transfer'), 0)
		}
		else {
			throw new Exception("Nominal Kepanjangan")
		}
		Mobile.clearText(findTestObject('Object Repository/Test Transfer/Input Nominal Transfer - 0'), 0)
	}
	else if (i == 2) {
		for (int j=0; j<=9; j++) {
			Mobile.setText(findTestObject('Object Repository/Test Transfer/Input Nominal Transfer - 0'),amountArray[i], 0)
			def bigAmount = Mobile.getText(findTestObject('Object Repository/Test Transfer/Input Nominal Transfer - 0'), 0)
			Mobile.comment(bigAmount)
			if (bigAmount == "0") {
				Mobile.verifyElementVisible(findTestObject('Object Repository/Transfer2/Transfer 2-1/Button Disable- Transfer'), 0)
			}
			else {
				throw new Exception("Angka tidak nol")
			}
		}
	}
	else if (i == 3) {
		Mobile.setText(findTestObject('Object Repository/Test Transfer/Input Nominal Transfer - 0'),amountArray[i], 0)
		int pay = Integer.parseInt(amountArray[i])
		if (pay < 10000) {
			Mobile.verifyElementVisible(findTestObject('Object Repository/Test Transfer/Verify Nominal Transfer- Minimal Transfer Rp10.000'), 0)
			Mobile.verifyElementVisible(findTestObject('Object Repository/Transfer2/Transfer 2-1/Button Disable- Transfer'), 0)
			}
		else {
			throw new Exception("Minimal Transfer Harus Rp.10.000")
			}
	}
}