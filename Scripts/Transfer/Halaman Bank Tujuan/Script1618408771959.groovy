import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.entity.global.GlobalVariableEntity
import com.kms.katalon.keyword.excel.ExcelKeywords
import internal.GlobalVariable as GlobalVariable
import internal.GlobalVariable

String excelFile = GlobalVariable.excelFile
sheet = ExcelKeywords.getExcelSheetByName(GlobalVariable.excelFile, "Transfer")
String bank = ExcelKeywords.getCellValueByAddress(sheet, "F"+GlobalVariable.giveCases)

Mobile.comment(bank)


Mobile.tap(findTestObject('Object Repository/object transfer/field Bank Tujuan- BANK BRI'), 0)
Mobile.setText(findTestObject('Object Repository/Test Transfer/Cari Bank - 1'), bank, 0)
Mobile.delay(3)
//device_Height = Mobile.getDeviceHeight()
//device_Width = Mobile.getDeviceWidth()
//int startY = device_Height / 2
//int endY = startY
//int startX = device_Width * 0.30
//int endX = device_Width * 0.70
String pilihBank
//SString bank = bank.toString()
switch(bank) {
	case "bank bri":
	pilihBank = Mobile.getText(findTestObject('Object Repository/object transfer/Layout Item Bank'), 0)
	Mobile.tap(findTestObject('Object Repository/object transfer/Layout Item Bank'), 0)
	break
	case "bank bca":
	//Mobile.swipe(startX, endY, endX, startY)
	pilihBank = Mobile.getText(findTestObject('Object Repository/object transfer/android.widget.LinearLayout-BCA'), 0)
	Mobile.tap(findTestObject('Object Repository/object transfer/android.widget.LinearLayout-BCA'), 0)
	break
	case "bank mandiri":
	//Mobile.swipe(startX, endY, endX, startY)
	pilihBank = Mobile.getText(findTestObject('Object Repository/Test Transfer/Cari Bank - 1 - BANK MANDIRI'), 0)
	Mobile.tap(findTestObject('Object Repository/Test Transfer/Cari Bank - 1 - BANK MANDIRI'), 0)
	break
	case "citibank":
	//Mobile.swipe(startX, endY, endX, startY)
	pilihBank = Mobile.getText(findTestObject('Object Repository/Test Transfer/Cari Bank - 1 - CITIBANK INDONESIA'), 0)
	Mobile.tap(findTestObject('Object Repository/Test Transfer/Cari Bank - 1 - CITIBANK INDONESIA'), 0)
	Mobile.comment(bank)
	break
	case "bank bni":
	//Mobile.swipe(startX, endY, endX, startY)
	pilihBank = Mobile.getText(findTestObject('Object Repository/Test Transfer/Cari Bank - 1 - BANK BNI'), 0)
	Mobile.tap(findTestObject('Object Repository/Test Transfer/Cari Bank - 1 - BANK BNI'), 0)
	break
	case "bank citibank":
	//Mobile.swipe(startX, endY, endX, startY)
	pilihBank = Mobile.getText(findTestObject('Object Repository/Test Transfer/Cari Bank - 1 - BANK BJB SYARIAH'), 0)
	Mobile.tap(findTestObject('Object Repository/Test Transfer/Cari Bank - 1 - BANK BJB SYARIAH'), 0)
	Mobile.comment(bank)
	break
	default:
	break
}




// if(bank=="bank bni" || bank=="bank mandiri" ||bank=="bank bca"||bank=="bank bjb") {
// Mobile.comment(pilihBank)
//}
// else if(bank=="bank bri") {
//Mobile.comment(pilihBank)
// Mobile.verifyElementVisible(findTestObject(''), 0)
//}else {
//	throw new Exception ("Bank tidak ditemukan")
//}

//GlobalVariable.getNoRekening = pilihRekening
//Mobile.comment(GlobalVariable.getNoRekening)
