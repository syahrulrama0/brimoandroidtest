import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import java.util.concurrent.TimeUnit




TimeUnit.SECONDS.sleep(1);
	
if (condition == "correct") {
	for (int i=1; i<=6; i++) {
			Mobile.tap(findTestObject('Object Repository/Transfer/android.widget.TextView - ' + number.substring(i-1,i)), 0)
	}
}
else if (condition == "incorrect") {
	String[] pinArray = ["111111", "111999", "999999"]
	for (int i=1; i<=2; i++) {
		for (int j=1; j<=6; j++) {
			Mobile.tap(findTestObject('Object Repository/Transfer/android.widget.TextView - '+ pinArray[i].substring(j-1,j)), 0)
			Mobile.verifyElementVisible(findTestObject('Object Repository/Transfer2/Transfer 2-1/Verify  - PIN Anda salah'), 0)
		}
	}}
	
else if (condition == "forgetPIN")
{
	Mobile.tap(findTestObject('Object Repository/Test Transfer/Halaman PIN - Lupa PIN'), 0)
	
	//Mobile.tap(findTestObject('Object Repository/Test Transfer/Halaman PIN- Lupa PIN - Input Password'), 0)
	Mobile.setText(findTestObject('Object Repository/Test Transfer/Halaman Lupa Pin - Input Password'), pinPassword, 0)
	def password = Mobile.getText(findTestObject('Object Repository/Test Transfer/Halaman Lupa Pin - Input Password'), 0)
	if (password.length()>=7)
	{
		Mobile.verifyElementVisible(findTestObject('Object Repository/Test Transfer/Halaman PIN-  Button - OK'), 0)
	}
	else {
	Mobile.verifyElementVisible(findTestObject('Object Repository/Test Transfer/Halaman PIN- Button OK - Disable'), 0)
	}
}
else {
	throw new Exception("condition unknown")
}

//CustomKeywords.'common.screenshoot.takeScreenshotAsCheckpoint'()

