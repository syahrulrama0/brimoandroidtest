import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import java.util.concurrent.TimeUnit

import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.keyword.excel.ExcelKeywords

import internal.GlobalVariable


String excelFile = 'datasets\\BRIMO.xlsx'
sheet = ExcelKeywords.getExcelSheetByName("datasets\\BRIMO.xlsx", "Kode QR")
String pinNumber = ExcelKeywords.getCellValueByAddress(sheet, "D"+GlobalVariable.giveCases)
Mobile.comment(pinNumber)
CustomKeywords.'common.other.takeScreenShot'()

TimeUnit.SECONDS.sleep(1);
	

	for (int i=1; i<=6; i++) {
			Mobile.tap(findTestObject('Object Repository/Transfer/android.widget.TextView - ' + pinNumber.substring(i-1,i)), 0)
	}
	