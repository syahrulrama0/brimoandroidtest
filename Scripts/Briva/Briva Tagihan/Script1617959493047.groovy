import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.keyword.excel.ExcelKeywords

import internal.GlobalVariable as GlobalVariable

Mobile.verifyElementExist(findTestObject('Object Repository/BRIVA/Tagihan/Tagihan'), 0)

Mobile.verifyElementExist(findTestObject('Object Repository/BRIVA/Tagihan/Total Pembayaran'), 0)

Mobile.verifyElementExist(findTestObject('Object Repository/BRIVA/Tagihan/Sumber Dana'), 0)

def brivaNumber = Mobile.getText(findTestObject('Object Repository/BRIVA/Tagihan/Nomor Briva'),0)
brivaNumber = brivaNumber.replaceAll("\\D+","")

Mobile.comment(brivaNumber)
Mobile.comment(GlobalVariable.destinationAccount)

if (brivaNumber != GlobalVariable.destinationAccount) {
	throw new Exception("Destination Number Berbeda")
}

String excelFile = 'datasets\\BRIMO.xlsx'
sheet = ExcelKeywords.getExcelSheetByName("datasets\\BRIMO.xlsx", "Briva")
String saveName = ExcelKeywords.getCellValueByAddress(sheet, "B"+GlobalVariable.giveCases)

if (saveName != "null") {
	Mobile.tap(findTestObject('Object Repository/BRIVA/Tagihan/Checklist Simpan untuk Selanjutnya'),0)
	Mobile.setText(findTestObject('Object Repository/BRIVA/Tagihan/Field Nama'),saveName,0)
}

def norek = Mobile.getText(findTestObject('Object Repository/BRIVA/Tagihan/Norek'),0)
norek = norek.replaceAll("\\D+","")

GlobalVariable.accountNumber = norek

def amount = Mobile.getText(findTestObject('Object Repository/BRIVA/Tagihan/Besaran Pembayaran'),0)
amount = amount.replaceAll("\\D+","")

GlobalVariable.amount = amount

//CustomKeywords.'common.other.takeScreenShot'()

//Mobile.tap(findTestObject('Object Repository/BRIVA/Konfirmasi Briva Tagihan/Sumber Dana Layout'),0)`q