import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.keyword.excel.ExcelKeywords

import internal.GlobalVariable as GlobalVariable

String excelFile = GlobalVariable.excelFile
sheet = ExcelKeywords.getExcelSheetByName(GlobalVariable.excelFile, "Briva")
String brivaNumber = ExcelKeywords.getCellValueByAddress(sheet, "A"+GlobalVariable.giveCases)

Mobile.setText(findTestObject('Object Repository/BRIVA/Pembayaran Baru/Field Nomor Briva'), brivaNumber,0)

Mobile.verifyElementExist(findTestObject('Object Repository/BRIVA/Pembayaran Baru/Lanjutakan Enable'),0)

def accountBriva = Mobile.getText(findTestObject('Object Repository/BRIVA/Pembayaran Baru/Field Nomor Briva'),0)

GlobalVariable.destinationAccount = accountBriva

//CustomKeywords.'common.other.takeScreenShot'()

Mobile.tap(findTestObject('Object Repository/BRIVA/Pembayaran Baru/Lanjutakan Enable'),0)