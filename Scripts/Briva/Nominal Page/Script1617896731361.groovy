import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

Mobile.verifyElementExist(findTestObject('Object Repository/BRIVA/Nominal Dan Sumber Dana Briva/Briva'),0)

Mobile.verifyElementExist(findTestObject('Object Repository/BRIVA/Nominal Dan Sumber Dana Briva/Nomor Tujuan'),0)

Mobile.verifyElementExist(findTestObject('Object Repository/BRIVA/Nominal Dan Sumber Dana Briva/Nominal Pembayaran'),0)

Mobile.verifyElementExist(findTestObject('Object Repository/BRIVA/Nominal Dan Sumber Dana Briva/Sumber Dana'),0)

Mobile.verifyElementExist(findTestObject('Object Repository/BRIVA/Nominal Dan Sumber Dana Briva/Icon Back'),0)

Mobile.verifyElementExist(findTestObject('Object Repository/BRIVA/Nominal Dan Sumber Dana Briva/Button Bayar Disable'),0)

def accountNumber = Mobile.getText(findTestObject('Object Repository/BRIVA/Nominal Dan Sumber Dana Briva/Nomor Briva'),0)

accountNumber = accountNumber.replaceAll("\\D+","")

Mobile.comment(accountNumber)
Mobile.comment(GlobalVariable.destinationAccount)
StringBuffer sb = new StringBuffer(accountNumber)
if (accountNumber != GlobalVariable.destinationAccount) {
	sb.delete(0,1)
	Mobile.comment(sb.toString())
	def newString = sb.toString()
	if (newString != GlobalVariable.destinationAccount) {
		throw new Exception("Nomor BRIVA Berbeda")
	}
}

if (saveName != "null") {
	Mobile.tap(findTestObject('Object Repository/BRIVA/Nominal Dan Sumber Dana Briva/Simpan Untuk Selanjutnya'),0)
	Mobile.setText(findTestObject('Object Repository/BRIVA/Nominal Dan Sumber Dana Briva/Field Nama'), saveName,0)
}

Mobile.setText(findTestObject('Object Repository/BRIVA/Nominal Dan Sumber Dana Briva/Field Saldo'), amount,0)

def bigAmount = Mobile.getText(findTestObject('Object Repository/BRIVA/Nominal Dan Sumber Dana Briva/Field Saldo'),0)

bigAmount = bigAmount.replaceAll("\\D+","")

GlobalVariable.amount = bigAmount

CustomKeywords.'common.other.takeScreenShot'()