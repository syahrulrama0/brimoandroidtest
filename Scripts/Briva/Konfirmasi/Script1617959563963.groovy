import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

def norek = Mobile.getText(findTestObject('Object Repository/BRIVA/Nominal Dan Sumber Dana Briva/Norek'),0)

norek = norek.replaceAll("\\D+","")

GlobalVariable.accountNumber = norek

Mobile.tap(findTestObject('Object Repository/BRIVA/Nominal Dan Sumber Dana Briva/Button Bayar Enable'),0)

Mobile.verifyElementExist(findTestObject('Object Repository/BRIVA/Konfirmasi Briva/Konfirmasi'),0)

Mobile.verifyElementExist(findTestObject('Object Repository/BRIVA/Konfirmasi Briva/Icon Back'),0)

Mobile.verifyElementExist(findTestObject('Object Repository/BRIVA/Konfirmasi Briva/Sumber Dana'),0)

Mobile.verifyElementExist(findTestObject('Object Repository/BRIVA/Konfirmasi Briva/Nomor Tujuan'),0)

Mobile.verifyElementExist(findTestObject('Object Repository/BRIVA/Konfirmasi Briva/Catatan'),0)

Mobile.verifyElementExist(findTestObject('Object Repository/BRIVA/Konfirmasi Briva/Detail'),0)

def accountNumber = Mobile.getText(findTestObject('Object Repository/BRIVA/Konfirmasi Briva/Norek'),0)
accountNumber = accountNumber.replaceAll("\\D+","")

def brivaAccount = Mobile.getText(findTestObject('Object Repository/BRIVA/Konfirmasi Briva/Nomor Briva'),0)
brivaAccount = brivaAccount.replaceAll("\\D+","")

if (accountNumber != GlobalVariable.accountNumber) {
	throw new Exception("Norek Berbeda")
}
StringBuffer sb = new StringBuffer(brivaAccount)
if (brivaAccount != GlobalVariable.destinationAccount) {
	sb.delete(0,1)
	Mobile.comment(sb.toString())
	def newString = sb.toString()
	if (newString != GlobalVariable.destinationAccount) {
		throw new Exception("Nomor BRIVA Berbeda")
	}
}

CustomKeywords.'common.other.takeScreenShot'()

Mobile.tap(findTestObject('Object Repository/BRIVA/Konfirmasi Briva/Button Bayar'),0)