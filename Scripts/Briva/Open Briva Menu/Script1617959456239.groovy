import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import com.kms.katalon.keyword.excel.ExcelKeywords

Mobile.tap(findTestObject('Object Repository/Dashboard/Briva Image'),0)

Mobile.verifyElementExist(findTestObject('BRIVA/Briva Menu/Briva'), 0)

Mobile.verifyElementExist(findTestObject('BRIVA/Briva Menu/Pembayaran Terakhir'), 0)

Mobile.verifyElementExist(findTestObject('BRIVA/Briva Menu/Daftar Briva'), 0)

Mobile.verifyElementExist(findTestObject('BRIVA/Briva Menu/Button Pembayaran Baru'), 0)

Mobile.verifyElementExist(findTestObject('BRIVA/Briva Menu/Field Search'), 0)

//CustomKeywords.'common.other.takeScreenShot'()