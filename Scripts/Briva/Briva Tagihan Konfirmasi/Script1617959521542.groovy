import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

Mobile.tap(findTestObject('Object Repository/BRIVA/Tagihan/Button Bayar'),0)

Mobile.verifyElementExist(findTestObject('Object Repository/BRIVA/Konfirmasi Briva Tagihan/Sumber Dana'), 0)

Mobile.verifyElementExist(findTestObject('Object Repository/BRIVA/Konfirmasi Briva Tagihan/Nomor Tujuan'), 0)

Mobile.verifyElementExist(findTestObject('Object Repository/BRIVA/Konfirmasi Briva Tagihan/Catatan'), 0)

Mobile.verifyElementExist(findTestObject('Object Repository/BRIVA/Konfirmasi Briva Tagihan/Detail'), 0)

Mobile.verifyElementExist(findTestObject('Object Repository/BRIVA/Konfirmasi Briva Tagihan/Button Bayar'), 0)

def norek = Mobile.getText(findTestObject('Object Repository/BRIVA/Konfirmasi Briva Tagihan/Norek'),0)
norek = norek.replaceAll("\\D+","")

def brivaAccount = Mobile.getText(findTestObject('Object Repository/BRIVA/Konfirmasi Briva Tagihan/Nomor Briva'),0)
brivaAccount = brivaAccount.replaceAll("\\D+","")

def amount = Mobile.getText(findTestObject('Object Repository/BRIVA/Konfirmasi Briva Tagihan/Total Pembayaran'),0)
amount = amount.replaceAll("\\D+","")

if (brivaAccount != GlobalVariable.destinationAccount) {
	throw new Exception("Destination Number Berbeda")
}

if (amount != GlobalVariable.amount) {
	throw new Exception("Nominal Berbeda")
}

if (norek != GlobalVariable.accountNumber) {
	throw new Exception("Nomor Rekening Berbeda")
}

//CustomKeywords.'common.other.takeScreenShot'()

Mobile.tap(findTestObject('Object Repository/BRIVA/Konfirmasi Briva Tagihan/Button Bayar'),0)