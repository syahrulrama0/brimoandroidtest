import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

Mobile.tap(findTestObject('Asuransi Prudential/PilihJenisAsuransi'), 0)
Mobile.tap(findTestObject('Object Repository/Asuransi Prudential/AsuransiPrudentialProd'), 0)
Mobile.tap(findTestObject('Object Repository/Asuransi Prudential/PilihTipePembayaran'), 0)
switch (tipe.toString()) {
	case 'Top Up Premi':
	Mobile.checkElement(findTestObject('Object Repository/Asuransi Prudential/TopUpPremi'), 0)

	Mobile.setText(findTestObject('Object Repository/Asuransi Prudential/Masukkannopolisprod'), polis.toString(),
		0)
	
break
	case 'Premi Pertama':
	Mobile.checkElement(findTestObject('Object Repository/Asuransi Prudential/PremiPertama'), 0)

	Mobile.setText(findTestObject('Object Repository/Asuransi Prudential/Masukkannopolisprod'), polis.toString(),
		0)
	
break
	case 'Premi Lanjutan':
	Mobile.checkElement(findTestObject('Object Repository/Asuransi Prudential/PremiLanjutan'), 0)
	
	Mobile.setText(findTestObject('Object Repository/Asuransi Prudential/Masukkannopolisprod'), polis.toString(),
		0)
	
break
	case 'Biaya Cetak Ulang Polis':
	Mobile.checkElement(findTestObject('Object Repository/Asuransi Prudential/BiayaCetakUlangPolis'), 0)

	Mobile.setText(findTestObject('Object Repository/Asuransi Prudential/Masukkannopolisprod'), polis.toString(),
		0)

break
	case 'Biaya Perubahan Polis':
	Mobile.checkElement(findTestObject('Object Repository/Asuransi Prudential/BiayaPerubahanPolis'), 0)

	Mobile.setText(findTestObject('Object Repository/Asuransi Prudential/Masukkannopolisprod'), polis.toString(),
		0)
	
break
	case 'Biaya Cetak Kartu':
	Mobile.checkElement(findTestObject('Object Repository/Asuransi Prudential/BiayaCetakKartu'), 0)

	Mobile.setText(findTestObject('Object Repository/Asuransi Prudential/Masukkannopolisprod'), polis.toString(),
		0)
	default:
	break
	
}

Mobile.tap(findTestObject('Object Repository/Asuransi Prudential/ButtonLanjutkan'), 0)

Date today = new Date()

String todaysDate = today.format('MM_dd_yy')

String nowTime = today.format('hh_mm_ss')

Mobile.takeScreenshot('C:/Users/Dillajp/Katalon Studio/capture/Prudential'+ todaysDate + '-' + nowTime + '.png', FailureHandling.STOP_ON_FAILURE)

