import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable


//AppiumDriver<?> driver = MobileDriverFactory.getDriver()
//
//driver.terminateApp('id.co.bri.brimo')

//Mobile.startExistingApplication('id.co.bri.brimo')
Mobile.startApplication(GlobalVariable.identifierApp,  false)

Mobile.tap(findTestObject('Object Repository/Login/LoginAwal'), 0)

//not_run: Mobile.tap(findTestObject('Laman First Time Login/Button Punya Akun'), 0)

//Mobile.verifyElementVisible(findTestObject('Object Repository/Login/Image BRImo'), 0)

Mobile.verifyElementVisible(findTestObject('Object Repository/Login/Selamat Datang'), 0)

Mobile.setText(findTestObject('Object Repository/Login/Username'), username.toString(), 0)

Mobile.setText(findTestObject('Object Repository/Login/Password'), password.toString(), 0)

Mobile.tap(findTestObject('Object Repository/Login/LoginButton'), 0)

not_run: Mobile.verifyElementVisible(findTestObject('Object Repository/Login/Brimo Image'), 0)

Mobile.verifyElementVisible(findTestObject('Object Repository/Login/Field Rekening Lain'), 0)

Mobile.verifyElementVisible(findTestObject('Object Repository/Login/Icon Home'), 0)

Date today = new Date()

String todaysDate = today.format('MM_dd_yy')

String nowTime = today.format('hh_mm_ss')

Mobile.takeScreenshot(((('/Users/tsi-psd/Katalon Studio/Brimo Native Android/Screenshot/Android/screenshot_' + todaysDate) +
	'-') + nowTime) + '.png', FailureHandling.STOP_ON_FAILURE)
