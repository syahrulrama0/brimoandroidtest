import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.keyword.excel.ExcelKeywords

import internal.GlobalVariable

String excelFile = GlobalVariable.excelFile
sheet = ExcelKeywords.getExcelSheetByName(GlobalVariable.excelFile, "Listrik")
String nominal = ExcelKeywords.getCellValueByAddress(sheet, "C"+GlobalVariable.giveCases)

Mobile.delay(3)
switch (nominal.toString()) {
	case '20000':
	Mobile.tap(findTestObject('Object Repository/Listrik/Token/20'), 0)

break
	case '50000':
	Mobile.tap(findTestObject('Object Repository/Listrik/Token/50'), 0)

break
default:
break
}

Mobile.tap(findTestObject('Object Repository/Listrik/Token/ButtonKonfirmasiBeli'), 0)

Date today = new Date()

String todaysDate = today.format('MM_dd_yy')

String nowTime = today.format('hh_mm_ss')

//Mobile.takeScreenshot('C:/Users/Dillajp/Katalon Studio/capture/Prudential'+ todaysDate + '-' + nowTime + '.png', FailureHandling.STOP_ON_FAILURE)
