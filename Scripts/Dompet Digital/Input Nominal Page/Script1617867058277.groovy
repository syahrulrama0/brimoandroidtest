import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.keyword.excel.ExcelKeywords

import internal.GlobalVariable as GlobalVariable

String excelFile = GlobalVariable.excelFile
sheet = ExcelKeywords.getExcelSheetByName(GlobalVariable.excelFile, "Dompet Digital")
String amounts = ExcelKeywords.getCellValueByAddress(sheet, "D"+GlobalVariable.giveCases)
String saveName = ExcelKeywords.getCellValueByAddress(sheet, "E"+GlobalVariable.giveCases)

Mobile.tap(findTestObject('Object Repository/Dompet Digitals/Top Up Baru/Lanjutkan Enable'),0)

Mobile.verifyElementExist(findTestObject('Object Repository/Dompet Digitals/Nominal/Dompet Digital'), 0, FailureHandling.STOP_ON_FAILURE)

Mobile.verifyElementExist(findTestObject('Object Repository/Dompet Digitals/Nominal/Icon Back'), 0, FailureHandling.STOP_ON_FAILURE)

Mobile.verifyElementExist(findTestObject('Object Repository/Dompet Digitals/Nominal/Sumber Dana'), 0, FailureHandling.STOP_ON_FAILURE)

Mobile.verifyElementExist(findTestObject('Object Repository/Dompet Digitals/Nominal/Top up Disable'), 0, FailureHandling.STOP_ON_FAILURE)

Mobile.verifyElementExist(findTestObject('Object Repository/Dompet Digitals/Nominal/NomorTujuan'), 0, FailureHandling.STOP_ON_FAILURE)

if (saveName != "null") {
	Mobile.tap(findTestObject('Object Repository/Dompet Digitals/Nominal/Checklist Simpan Untuk Selanjutnya'),0)
	Mobile.setText(findTestObject('Object Repository/Dompet Digitals/Nominal/Field Nama'),saveName,0)
}

Mobile.setText(findTestObject('Object Repository/Dompet Digitals/Nominal/Field nominal'),amounts,0)

def amount = Mobile.getText(findTestObject('Object Repository/Dompet Digitals/Nominal/Field nominal'),0)

amount = amount.replaceAll("\\D+","")

GlobalVariable.amount = amount

def norek = Mobile.getText(findTestObject('Object Repository/Dompet Digitals/Nominal/Norek'),0)

norek = norek.replaceAll("\\D+","")

GlobalVariable.getNoRekening = norek

CustomKeywords.'common.other.takeScreenShot'()

Mobile.tap(findTestObject('Object Repository/Dompet Digitals/Konfirmasi/Button Top Up'),0)