import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.keyword.excel.ExcelKeywords

import internal.GlobalVariable

String excelFile = GlobalVariable.excelFile
sheet = ExcelKeywords.getExcelSheetByName(GlobalVariable.excelFile, "Dompet Digital")
String phoneNumbers = ExcelKeywords.getCellValueByAddress(sheet, "C"+GlobalVariable.giveCases)
String dompetDigital = ExcelKeywords.getCellValueByAddress(sheet, "A"+GlobalVariable.giveCases)
String gopayType = ExcelKeywords.getCellValueByAddress(sheet, "B"+GlobalVariable.giveCases)

Mobile.tap(findTestObject('Object Repository/Dompet Digitals/Top Up Baru/Pilihan Dompet Digital'),0)

if (dompetDigital.toLowerCase() == "dana") {
	Mobile.tap(findTestObject('Object Repository/Dompet Digital/android.widget.TextView - DANA'),0)
}
else if (dompetDigital.toLowerCase() == "gopay") {
	Mobile.tap(findTestObject("Object Repository/Dompet Digital/android.widget.TextView - GoPay"), 0)
	Mobile.tap(findTestObject("Object Repository/v2/Wallet New Form/EditText - Pilih Jenis GoPay"), 0)
	if (gopayType.toLowerCase() == "customer") {
		Mobile.tap(findTestObject("Object Repository/v2/Wallet New Form/Gopay - TextView - Customer"), 0)
	}
	else if (gopayType.toLowerCase() == "driver") {
		Mobile.tap(findTestObject("Object Repository/v2/Wallet New Form/Gopay - TextView - Driver"), 0)
	}
}
else if (dompetDigital.toLowerCase() == "ovo") {
	Mobile.tap(findTestObject("Object Repository/Dompet Digital/android.widget.TextView - OVO"), 0)
}
else if (dompetDigital.toLowerCase() == "shopeepay") {
	Mobile.tap(findTestObject("Object Repository/Dompet Digital/android.widget.TextView - ShopeePay"), 0)
}
else if (dompetDigital.toLowerCase() == "linkaja") {
	Mobile.tap(findTestObject("Object Repository/Dompet Digital/android.widget.TextView - LinkAja"), 0)
}

Mobile.setText(findTestObject('Object Repository/Dompet Digitals/Top Up Baru/Field Nomor Tujuan'), phoneNumbers, 0)

def phoneNumber = Mobile.getText(findTestObject('Object Repository/Dompet Digitals/Top Up Baru/Field Nomor Tujuan'),0)

GlobalVariable.phoneNumber = phoneNumber

CustomKeywords.'common.other.takeScreenShot'()