import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

String provider = Mobile.getText(findTestObject('Object Repository/Donasi/Sumber Dana/Nama Provider'),0)

def donationType = Mobile.getText(findTestObject('Object Repository/Donasi/Sumber Dana/Jenis Donasi'),0)

def norek = Mobile.getText(findTestObject('Object Repository/Donasi/Sumber Dana/Norek'), 0) 

norek = norek.replaceAll("\\D+","")

GlobalVariable.accountNumber = norek

if (provider.toLowerCase() != GlobalVariable.provider) {
	throw new Exception("Provider Berbeda")
}
if (donationType != GlobalVariable.donationType) {
	throw new Exception("Jenis Donasi Berbeda")
}

CustomKeywords.'common.other.takeScreenShot'()

Mobile.tap(findTestObject('Object Repository/Donasi/Sumber Dana/Button Bayar Enable'),0)