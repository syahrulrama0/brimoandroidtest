import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

//def norek = Mobile.getText(findTestObject('Object Repository/Dompet Digitals/Nominal/Norek'),0)
//
//norek = norek.replaceAll("\\D+","")
//
//GlobalVariable.accountNumber = norek

//Mobile.tap(findTestObject('Object Repository/Dompet Digitals/Nominal/Top Up Enable'),0)

//CustomKeywords.'common.other.takeScreenShot'()

Mobile.verifyElementExist(findTestObject('Object Repository/Dompet Digitals/Konfirmasi/Konfirmasi'), 0, FailureHandling.STOP_ON_FAILURE)

Mobile.verifyElementExist(findTestObject('Object Repository/Dompet Digitals/Konfirmasi/Sumber Dana'), 0, FailureHandling.STOP_ON_FAILURE)

Mobile.verifyElementExist(findTestObject('Object Repository/Dompet Digitals/Konfirmasi/Nomor Tujuan'), 0, FailureHandling.STOP_ON_FAILURE)

Mobile.verifyElementExist(findTestObject('Object Repository/Dompet Digitals/Konfirmasi/Detail'), 0, FailureHandling.STOP_ON_FAILURE)

Mobile.verifyElementExist(findTestObject('Object Repository/Dompet Digitals/Konfirmasi/Button Top Up'), 0, FailureHandling.STOP_ON_FAILURE)

def phoneNumber = Mobile.getText(findTestObject('Object Repository/Dompet Digitals/Konfirmasi/Notelp'),0)

phoneNumber = phoneNumber.replaceAll("\\D+", "")

if (phoneNumber != GlobalVariable.phoneNumber) {
	throw new Exception("Nomor Telepon Berbeda")
}

def accountNumber = Mobile.getText(findTestObject('Object Repository/Dompet Digitals/Konfirmasi/Norek'),0)

accountNumber = accountNumber.replaceAll("\\D+", "")

if (accountNumber != GlobalVariable.getNoRekening) {
	throw new Exception("Norek Berbeda")
}

def amount = Mobile.getText(findTestObject('Object Repository/Dompet Digitals/Konfirmasi/Amount'),0)

amount = amount.replaceAll("\\D+","")

if (amount != GlobalVariable.amount) {
	throw new Exception("Amount Berbeda")
}

Mobile.tap(findTestObject('Object Repository/Dompet Digitals/Konfirmasi/Button Top Up'),0)