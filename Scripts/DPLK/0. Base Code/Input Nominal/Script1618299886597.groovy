import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.keyword.excel.ExcelKeywords

import internal.GlobalVariable as GlobalVariable

def getNomorTujuan = GlobalVariable.akunDPLK

def nomorTujuan = Mobile.getText(findTestObject('Object Repository/DPLK/Nominal dan Sumber Dana/Nomor Tujuan'), 0)

nomorTujuan = nomorTujuan.replaceAll("\\D+","")


String excelFile = GlobalVariable.excelFile
sheet = ExcelKeywords.getExcelSheetByName(GlobalVariable.excelFile, "DPLK")
String nominalTopUp = ExcelKeywords.getCellValueByAddress(sheet, "B"+GlobalVariable.giveCases)


if(nomorTujuan==getNomorTujuan) {
	Mobile.comment(getNomorTujuan)
	Mobile.comment(nomorTujuan)
}
else {
	throw new Exception ("Nomor akun DPLK Tidak Sama dengan yang di pilih/input Sebelumnya")

}

Mobile.setText(findTestObject('Object Repository/DPLK/Nominal dan Sumber Dana/Input Nominal'), nominalTopUp, 0)