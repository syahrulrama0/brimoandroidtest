import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys


Mobile.verifyElementExist(findTestObject('Object Repository/DPLK/Content Payment Receipt/Tanggal'), 0)
def time = Mobile.getText(findTestObject('Object Repository/DPLK/Content Payment Receipt/Tanggal'), 0)

Mobile.verifyElementExist(findTestObject('Object Repository/DPLK/Content Payment Receipt/No Referensi'), 0)
def refnum = Mobile.getText(findTestObject('Object Repository/DPLK/Content Payment Receipt/No Referensi'), 0)

Mobile.verifyElementExist(findTestObject('Object Repository/DPLK/Content Payment Receipt/Sumber Dana'), 0)
Mobile.verifyElementExist(findTestObject('Object Repository/DPLK/Content Payment Receipt/Jenis Transaksi'), 0)
Mobile.verifyElementExist(findTestObject('Object Repository/DPLK/Content Payment Receipt/No Akun DPLK'), 0)
Mobile.verifyElementExist(findTestObject('Object Repository/DPLK/Content Payment Receipt/Nama Pelanggan'), 0)

Mobile.verifyElementExist(findTestObject('Object Repository/DPLK/Content Payment Receipt/Nominal'), 0)
def nominal = Mobile.getText(findTestObject('Object Repository/DPLK/Content Payment Receipt/Nominal'), 0)
nominal = nominal.replaceAll("\\D+","")
Mobile.comment(nominal)

Mobile.verifyElementExist(findTestObject('Object Repository/DPLK/Content Payment Receipt/Biaya Admin'), 0)
def biayaAdmin = Mobile.getText(findTestObject('Object Repository/DPLK/Content Payment Receipt/Biaya Admin'), 0)
biayaAdmin = biayaAdmin.replaceAll("\\D+","")
Mobile.comment(biayaAdmin)

Mobile.verifyElementExist(findTestObject('Object Repository/DPLK/Content Payment Receipt/Total'), 0)
def total = Mobile.getText(findTestObject('Object Repository/DPLK/Content Payment Receipt/Total'), 0)

nominal = Integer.parseInt(nominal)
biayaAdmin = Integer.parseInt(biayaAdmin)
def verifyTotalHarga = nominal + biayaAdmin

if(total == verifyTotalHarga) {
	Mobile.comment(total)
	Mobile.comment(verifyTotalHarga)
}

CustomKeywords.'common.other.fileWrite'(time, refnum, total, "DPLK")

CustomKeywords.'common.screenshot.takeScreenshotAsCheckpoint'()

Mobile.tap(findTestObject('Object Repository/DPLK/Content Payment Receipt/android.widget.Button - OK'), biayaAdmin)