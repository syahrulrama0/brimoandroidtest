import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.keyword.excel.ExcelKeywords

import internal.GlobalVariable as GlobalVariable

String excelFile = 'datasets\\BRIMO.xlsx'
sheet = ExcelKeywords.getExcelSheetByName("datasets\\BRIMO.xlsx", "DPLK")
String noAkunDPLK = ExcelKeywords.getCellValueByAddress(sheet, "A"+GlobalVariable.giveCases)

Mobile.tap(findTestObject('Object Repository/DPLK/Menu DPLK/android.widget.Button - Top Up DPLK'), 0)

Mobile.delay(3)

Mobile.tap(findTestObject('Object Repository/DPLK/Menu DPLK/android.widget.Button - Top Up Baru'), 0)

Mobile.setText(findTestObject('Object Repository/DPLK/Top Up Baru/EditText - Masukkan Nomor Akun'), noAkunDPLK, 0)

def akunDPLK = Mobile.getText(findTestObject('Object Repository/DPLK/Top Up Baru/Verify No Akun DPLK'), 0) 

CustomKeywords.'common.screenshot.takeScreenshotAsCheckpoint'()

Mobile.tap(findTestObject('Object Repository/DPLK/Menu DPLK/android.widget.Button - Lanjutkan'), 0)

GlobalVariable.akunDPLK = akunDPLK


