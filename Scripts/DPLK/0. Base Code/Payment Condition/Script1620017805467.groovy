import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint

import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

import com.kms.katalon.keyword.excel.ExcelKeywords

String excelFile = GlobalVariable.excelFile
sheet = ExcelKeywords.getExcelSheetByName(GlobalVariable.excelFile, "DPLK")
String condition = ExcelKeywords.getCellValueByAddress(sheet, "C"+GlobalVariable.giveCases)


String getNoRekening
switch(condition) {
	
	case "Saldo_Cukup":
	getNoRekening = Mobile.getText(findTestObject('Object Repository/DPLK/Nominal dan Sumber Dana/Nomor rekening'), 0)
	break
	
	case "Saldo_Tidak_Cukup":
	getNoRekening = Mobile.getText(findTestObject('Object Repository/DPLK/Nominal dan Sumber Dana/Nomor rekening'), 0)
	break
	
}

//CustomKeywords.'common.screenshot.sc'()

if(condition=="Saldo_Cukup") {
		Mobile.tap(findTestObject('Object Repository/DPLK/Nominal dan Sumber Dana/Button - Top Up'), 0)
}
else if(condition=="Saldo_Tidak_Cukup") {
	Mobile.comment(getNoRekening)
	Mobile.verifyElementText(findTestObject('Object Repository/DPLK/Nominal dan Sumber Dana/alert - Saldo Anda tidak cukup'), "Saldo Anda tidak cukup")
	Mobile.verifyElementVisible(findTestObject('Object Repository/DPLK/Nominal dan Sumber Dana/Disable Button - Top Up'), 0)
}else {
	throw new Exception ("Jenis rekening tidak ditemukan")
}

GlobalVariable.getNoRekening = getNoRekening



