import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.keyword.excel.ExcelKeywords

import internal.GlobalVariable as GlobalVariable

String excelFile = GlobalVariable.excelFile
sheet = ExcelKeywords.getExcelSheetByName(GlobalVariable.excelFile, "Brizzi")
String numberBrizzi = ExcelKeywords.getCellValueByAddress(sheet, "A"+GlobalVariable.giveCases)
int amount = ExcelKeywords.getCellValueByAddress(sheet, "B"+GlobalVariable.giveCases)

Mobile.setText(findTestObject('Brizzi/Brizzi Main/Field No Kartu BRIZZI'), numberBrizzi,0)

String brizziNumber = Mobile.getText(findTestObject('Brizzi/Brizzi Main/Field No Kartu BRIZZI'),0)

brizziNumber = brizziNumber.replaceAll("\\D+","")

GlobalVariable.provider = brizziNumber

Mobile.verifyElementVisible(findTestObject('Brizzi/Brizzi Main/Nominal'), 0)

Mobile.verifyElementVisible(findTestObject('Object Repository/Brizzi/Besaran Nominal/android.widget.TextView - Rp20.000'),0)

Mobile.verifyElementVisible(findTestObject('Object Repository/Brizzi/Besaran Nominal/android.widget.TextView - Rp50.000'),0)

Mobile.verifyElementVisible(findTestObject('Object Repository/Brizzi/Besaran Nominal/android.widget.TextView - Rp100.000'),0)

Mobile.verifyElementVisible(findTestObject('Object Repository/Brizzi/Besaran Nominal/android.widget.TextView - Rp150.000'),0)

Mobile.verifyElementVisible(findTestObject('Object Repository/Brizzi/Besaran Nominal/android.widget.TextView - Rp200.000'),0)

Mobile.verifyElementVisible(findTestObject('Object Repository/Brizzi/Besaran Nominal/android.widget.TextView - Rp300.000'),0)

Mobile.verifyElementVisible(findTestObject('Object Repository/Brizzi/Besaran Nominal/android.widget.TextView - Rp400.000'),0)

Mobile.verifyElementVisible(findTestObject('Object Repository/Brizzi/Besaran Nominal/android.widget.TextView - Rp500.000'),0)

Mobile.verifyElementVisible(findTestObject('Object Repository/Brizzi/Besaran Nominal/android.widget.TextView - Rp1.000.000'),0)

CustomKeywords.'common.other.brizziAmount'(amount)

def selectedAmount = Mobile.getText(findTestObject('Brizzi/Brizzi Main/Besaran Total Pembelian'), 2)

selectedAmount = selectedAmount.replaceAll("\\D+","")

GlobalVariable.amount = selectedAmount

//CustomKeywords.'common.other.takeScreenShot'()

Mobile.tap(findTestObject('Object Repository/Brizzi/Brizzi Main/Button Beli'),0)