import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

Mobile.verifyElementExist(findTestObject('Object Repository/Brizzi/Konfirmasi/Sumber Dana'), 0)

Mobile.verifyElementExist(findTestObject('Object Repository/Brizzi/Konfirmasi/Nomor Tujuan'), 0)

Mobile.verifyElementExist(findTestObject('Object Repository/Brizzi/Konfirmasi/Detail'), 0)

def brizziNumber = Mobile.getText(findTestObject('Object Repository/Brizzi/Konfirmasi/Nomor Brizzi'),0)

brizziNumber = brizziNumber.replaceAll("\\D+","")

if (brizziNumber != GlobalVariable.provider) {
	throw new Exception("Nomor kartu berbeda")
}

def norek = Mobile.getText(findTestObject('Object Repository/Brizzi/Konfirmasi/Norek'),0)

norek = norek.replaceAll("\\D+","")

Mobile.comment(norek)
Mobile.comment(GlobalVariable.accountNumber)

if (norek != GlobalVariable.getNoRekening) {
	throw new Exception("Nomor rekening berbeda")
}

def amount = Mobile.getText(findTestObject('Object Repository/Brizzi/Konfirmasi/Besaran Nominal'),0)

amount = amount.replaceAll("\\D+","")

if (amount != GlobalVariable.amount) {
	throw new Exception("Besaran Berbeda")
}

int amounts = Integer.parseInt(amount) 

def adminFee = Mobile.getText(findTestObject('Object Repository/Brizzi/Konfirmasi/Biaya Admin'),0)

adminFee = adminFee.replaceAll("\\D+","")

int fee = Integer.parseInt(adminFee)

def totalAmount = Mobile.getText(findTestObject('Object Repository/Brizzi/Konfirmasi/Total Dibayarkan'),0)

totalAmount = totalAmount.replaceAll("\\D+","")

int bigAmount = Integer.parseInt(totalAmount)

if ((amounts + fee) != bigAmount) {
	throw new Exception("Kalkulasi Salah")
}

//CustomKeywords.'common.other.takeScreenShot'()

Mobile.tap(findTestObject('Object Repository/Brizzi/Konfirmasi/Button Beli'),0)