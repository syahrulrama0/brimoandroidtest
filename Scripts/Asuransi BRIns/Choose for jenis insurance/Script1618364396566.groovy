import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.keyword.excel.ExcelKeywords

import internal.GlobalVariable

String excelFile = GlobalVariable.excelFile
sheet = ExcelKeywords.getExcelSheetByName(GlobalVariable.excelFile, "Asuransi")
String noPelanggan = ExcelKeywords.getCellValueByAddress(sheet, "A"+GlobalVariable.giveCases)
String jenisAsuransi = ExcelKeywords.getCellValueByAddress(sheet, "B"+GlobalVariable.giveCases)
String jenisPrudential = ExcelKeywords.getCellValueByAddress(sheet, "C"+GlobalVariable.giveCases)

Mobile.tap(findTestObject('Object Repository/Asuransi/android.widget.EditText - Pilih Jenis Asuransi'), 0)


Mobile.comment(jenisAsuransi)

Mobile.delay(3)

if(jenisAsuransi.toLowerCase() == "brins") {
	
	Mobile.comment(jenisAsuransi)
	
	Mobile.tap(findTestObject('Object Repository/Asuransi/android.widget.TextView - BRI Insurance'),0)
	
	Mobile.setText(findTestObject('Object Repository/Asuransi/android.widget.EditText - Masukkan Nomor BRIVA'), noPelanggan, 0)
	
} else if (jenisAsuransi.toLowerCase() == "prudential"){
	
	Mobile.tap(findTestObject('Object Repository/Asuransi/android.widget.TextView - Prudential'), 0)
	
	Mobile.tap(findTestObject('Object Repository/Asuransi/android.widget.EditText - Pilih Tipe Pembayaran'), 0)
	
	if(jenisPrudential.toLowerCase() == "premipertama") {
		
		Mobile.tap(findTestObject('Object Repository/Asuransi/android.widget.TextView - Premi Pertama  SPAJ'), 0)
		
	}else if(jenisPrudential.toLowerCase() == "premilanjutan") {
		
		Mobile.tap(findTestObject('Object Repository/Asuransi/android.widget.TextView - Premi Lanjutan'), 0)
		
	}else if(jenisPrudential.toLowerCase() == "topup") {
		
		Mobile.tap(findTestObject('Object Repository/Asuransi/android.widget.TextView - Top Up Premi'), 0)
		
	}
	else if(jenisPrudential.toLowerCase() == "biayacetakulang") {
		
		Mobile.tap(findTestObject('Object Repository/Asuransi/android.widget.TextView - Biaya Cetak Ulang Polis'), 0)
		
	}
	else if(jenisPrudential.toLowerCase() == "biayaperubahan") {
		
		Mobile.tap(findTestObject('Object Repository/Asuransi/android.widget.TextView - Biaya Perubahan Polis'), 0)
		
	}else if(jenisPrudential.toLowerCase() == "biayacetakkartu") {
		
		Mobile.tap(findTestObject('Object Repository/Asuransi/android.widget.TextView - Biaya Cetak Kartu'), 0)
		
	}
	
	
	Mobile.setText(findTestObject('Object Repository/Asuransi/android.widget.EditText - Masukkan Nomor Polis'),noPelanggan ,0)
}



Mobile.tap(findTestObject('Object Repository/Asuransi/android.widget.Button - Lanjutkan'), 0)