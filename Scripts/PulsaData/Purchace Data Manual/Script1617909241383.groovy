import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.keyword.excel.ExcelKeywords

import internal.GlobalVariable as GlobalVariable

String excelFile = GlobalVariable.excelFile
sheet = ExcelKeywords.getExcelSheetByName(GlobalVariable.excelFile, "Pulsa Data")
String phoneNumber = ExcelKeywords.getCellValueByAddress(sheet, "A"+GlobalVariable.giveCases)

Mobile.setText(findTestObject('Pulsa Data/Main/Field Nomor Tujuan'), phoneNumber, 0)

def inputNumber = Mobile.getText(findTestObject('Pulsa Data/Main/Field Nomor Tujuan'), 0)

GlobalVariable.phoneNumber = inputNumber

phoneCode = inputNumber.substring(0,4)

GlobalVariable.phoneCode = phoneCode

CustomKeywords.'common.other.verifyProvider'(phoneCode)

Mobile.tap(findTestObject('Pulsa Data/Main/Widged Paket Data'),0)

Mobile.verifyElementVisible(findTestObject('Pulsa Data/Main/androidx.recyclerview.widget.RecyclerView'),0)

def bottomPrice = Mobile.getText(findTestObject('Object Repository/Pulsa Data/Data/Paket/Harga Besar'),0)

bottomPrice = bottomPrice.replaceAll("\\D+","")

bottomPrice = bottomPrice.substring(0, bottomPrice.length() - 2)

Mobile.tap(findTestObject('Object Repository/Pulsa Data/Data/Paket/Layout Paket 1'),0)

def totalAmount = Mobile.getText(findTestObject('Object Repository/Pulsa Data/Data/Paket/Total Harga'),0)

totalAmount = totalAmount.replaceAll("\\D+","")

if (bottomPrice != totalAmount) {
	throw new Exception("Nominal Berbeda")
}

CustomKeywords.'common.other.takeScreenShot'()

Mobile.tap(findTestObject('Object Repository/Pulsa Data/Main/Button Beli'),0)

//Mobile.tap(findTestObject('Pulsa Data/Main/Button Beli'),0)
//if (GlobalVariable.provider == "Simpati") {
//	Mobile.verifyElementExist(findTestObject('Pulsa Data/Data/Nominal/android.widget.TextView - Rp1.000.000'), 0)
//	Mobile.verifyElementExist(findTestObject('Pulsa Data/Data/Nominal/android.widget.TextView - Rp100.000'), 0)
//	Mobile.verifyElementExist(findTestObject('Pulsa Data/Data/Nominal/android.widget.TextView - Rp15.000'), 0)
//	Mobile.verifyElementExist(findTestObject('Pulsa Data/Data/Nominal/android.widget.TextView - Rp150.000'), 0)
//	Mobile.verifyElementExist(findTestObject('Pulsa Data/Data/Nominal/android.widget.TextView - Rp20.000'), 0)
//	Mobile.verifyElementExist(findTestObject('Pulsa Data/Data/Nominal/android.widget.TextView - Rp200.000'), 0)
//	Mobile.verifyElementExist(findTestObject('Pulsa Data/Data/Nominal/android.widget.TextView - Rp25.000'), 0)
//	Mobile.verifyElementExist(findTestObject('Pulsa Data/Data/Nominal/android.widget.TextView - Rp30.000'), 0)
//	Mobile.verifyElementExist(findTestObject('Pulsa Data/Data/Nominal/android.widget.TextView - Rp300.000'), 0)
//	Mobile.verifyElementExist(findTestObject('Pulsa Data/Data/Nominal/android.widget.TextView - Rp40.000'), 0)
//	Mobile.verifyElementExist(findTestObject('Pulsa Data/Data/Nominal/android.widget.TextView - Rp50.000'), 0)
//	Mobile.verifyElementExist(findTestObject('Pulsa Data/Data/Nominal/android.widget.TextView - Rp500.000'), 0)
//	Mobile.verifyElementExist(findTestObject('Pulsa Data/Data/Nominal/android.widget.TextView - Rp75.000'), 0)
//}
//else if (GlobalVariable.provider == "As") {
//	Mobile.verifyElementExist(findTestObject('Pulsa Data/Data/Nominal/android.widget.TextView - Rp1.000.000'), 0)
//	Mobile.verifyElementExist(findTestObject('Pulsa Data/Data/Nominal/android.widget.TextView - Rp100.000'), 0)
//	Mobile.verifyElementExist(findTestObject('Pulsa Data/Data/Nominal/android.widget.TextView - Rp15.000'), 0)
//	Mobile.verifyElementExist(findTestObject('Pulsa Data/Data/Nominal/android.widget.TextView - Rp20.000'), 0)
//	Mobile.verifyElementExist(findTestObject('Pulsa Data/Data/Nominal/android.widget.TextView - Rp200.000'), 0)
//	Mobile.verifyElementExist(findTestObject('Pulsa Data/Data/Nominal/android.widget.TextView - Rp25.000'), 0)
//	Mobile.verifyElementExist(findTestObject('Pulsa Data/Data/Nominal/android.widget.TextView - Rp30.000'), 0)
//	Mobile.verifyElementExist(findTestObject('Pulsa Data/Data/Nominal/android.widget.TextView - Rp300.000'), 0)
//	Mobile.verifyElementExist(findTestObject('Pulsa Data/Data/Nominal/android.widget.TextView - Rp40.000'), 0)
//	Mobile.verifyElementExist(findTestObject('Pulsa Data/Data/Nominal/android.widget.TextView - Rp50.000'), 0)
//	Mobile.verifyElementExist(findTestObject('Pulsa Data/Data/Nominal/android.widget.TextView - Rp500.000'), 0)
//	Mobile.verifyElementExist(findTestObject('Pulsa Data/Data/Nominal/android.widget.TextView - Rp75.000'), 0)
//} 
//else if (GlobalVariable.provider == "Indosat") {
//	Mobile.verifyElementExist(findTestObject('Pulsa Data/Data/Nominal/android.widget.TextView - Rp25.000'), 0)
//	Mobile.verifyElementExist(findTestObject('Pulsa Data/Data/Nominal/android.widget.TextView - Rp50.000'), 0)
//	Mobile.verifyElementExist(findTestObject('Pulsa Data/Data/Nominal/android.widget.TextView - Rp100.000'), 0)
//	Mobile.verifyElementExist(findTestObject('Pulsa Data/Data/Nominal/android.widget.TextView - Rp150.000'), 0)
//	
//}
//else {
//	throw new Exception(GlobalVariable.provider)
//}
//
//switch (amount) {
//	case 15000 :
//	Mobile.tap(findTestObject('Pulsa Data/Data/Nominal/android.widget.TextView - Rp15.000'), 0)
//	case 20000 :
//	Mobile.tap(findTestObject('Pulsa Data/Data/Nominal/android.widget.TextView - Rp20.000'), 0)
//	case 25000 :
//	Mobile.tap(findTestObject('Pulsa Data/Data/Nominal/android.widget.TextView - Rp25.000'), 0)
//	case 30000 : 
//	Mobile.tap(findTestObject('Pulsa Data/Data/Nominal/android.widget.TextView - Rp30.000'), 0)
//	case 40000 :
//	Mobile.tap(findTestObject('Pulsa Data/Data/Nominal/android.widget.TextView - Rp40.000'), 0)
//	case 50000 :
//	Mobile.tap(findTestObject('Pulsa Data/Data/Nominal/android.widget.TextView - Rp50.000'), 0)
//	case 75000 :
//	Mobile.tap(findTestObject('Pulsa Data/Data/Nominal/android.widget.TextView - Rp75.000'), 0)
//	case 100000 :
//	Mobile.tap(findTestObject('Pulsa Data/Data/Nominal/android.widget.TextView - Rp100.000'), 0)
//	case 150000 :
//	Mobile.tap(findTestObject('Pulsa Data/Data/Nominal/android.widget.TextView - Rp150.000'), 0)
//	case 200000 :
//	Mobile.tap(findTestObject('Pulsa Data/Data/Nominal/android.widget.TextView - Rp200.000'), 0)
//	case 300000 :
//	Mobile.tap(findTestObject('Pulsa Data/Data/Nominal/android.widget.TextView - Rp300.000'), 0)
//	case 500000 :
//	Mobile.tap(findTestObject('Pulsa Data/Data/Nominal/android.widget.TextView - Rp500.000'), 0)
//	case 1000000 :
//	Mobile.tap(findTestObject('Pulsa Data/Data/Nominal/android.widget.TextView - Rp1.000.000'), 0)
//	default :
//	throw new exception("Nominal tidak ada")
//}