import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

Mobile.tap(findTestObject('Pulsa Data/Main/Icon Kontak'),0)

Mobile.verifyElementVisible(findTestObject('Kontak/Cari'), 0)

Mobile.verifyElementVisible(findTestObject('Kontak/Pilih Kontak'), 0)

if (provider.toLowerCase() == "finnet") {
	Mobile.setText(findTestObject('Pulsa Data/Main/Field Cari'),"telkomsel",0)
}
else if (provider.toLowerCase() == "mitrakom") {
	Mobile.setText(findTestObject('Pulsa Data/Main/Field Cari'),"indosat",0)
}
else if (provider.toLowerCase() == "artarajasa") {
	Mobile.setText(findTestObject('Pulsa Data/Main/Field Cari'),"XL",0)
}

def contactNumber = Mobile.getText(findTestObject('Object Repository/Kontak/Pilih Kontak HP/Phone Number'), 0)

Mobile.tap(findTestObject('Object Repository/Kontak/Pilih Kontak HP/Kontak 1'), 0)

if (contactNumber.substring(0,3) == "+62") {
	def nomorTujuan = Mobile.getText(findTestObject('Pulsa Data/Main/Field Nomor Tujuan'), 0)
	
	StringBuffer sb = new StringBuffer(nomorTujuan)
	
	sb.delete(0,1)

	String newString = "+62" + sb.toString()
	if (newString != contactNumber) {
		throw new Exception("Nomor Berbeda")
	}
}

else {
	def nomorTujuan = Mobile.getText(findTestObject('Pulsa Data/Main/Field Nomor Tujuan'), 0)
	if (nomorTujuan != contactNumber) {
		throw new Exception("Nomor Berbeda")
	}
}

def inputNumber = Mobile.getText(findTestObject('Pulsa Data/Main/Field Nomor Tujuan'),0)

GlobalVariable.phoneNumber = inputNumber

phoneCode = inputNumber.substring(0,4)

GlobalVariable.phoneCode = phoneCode

CustomKeywords.'common.other.verifyProvider'(phoneCode)

Mobile.tap(findTestObject('Pulsa Data/Main/Widged Paket Data'),0)

Mobile.verifyElementVisible(findTestObject('Pulsa Data/Main/androidx.recyclerview.widget.RecyclerView'),0)

def bottomPrice = Mobile.getText(findTestObject('Object Repository/Pulsa Data/Data/Paket/Harga Besar'),0)

bottomPrice = bottomPrice.replaceAll("\\D+","")

bottomPrice = bottomPrice.substring(0, bottomPrice.length() - 2)

Mobile.tap(findTestObject('Object Repository/Pulsa Data/Data/Paket/Layout Paket 1'),0)

def totalAmount = Mobile.getText(findTestObject('Object Repository/Pulsa Data/Data/Paket/Total Harga'),0)

totalAmount = totalAmount.replaceAll("\\D+","")

if (bottomPrice != totalAmount) {
	throw new Exception("Nominal Berbeda")
}

CustomKeywords.'common.other.takeScreenShot'()

Mobile.tap(findTestObject('Object Repository/Pulsa Data/Main/Button Beli'),0)