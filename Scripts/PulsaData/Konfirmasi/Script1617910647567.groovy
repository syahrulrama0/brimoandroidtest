import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

Mobile.verifyElementExist(findTestObject('Object Repository/Pulsa Data/Data/Konfirmasi/Konfirmasi'), 0)

def norek = Mobile.getText(findTestObject('Object Repository/Pulsa Data/Data/Konfirmasi/Norek'),0)

norek = norek.replaceAll("\\D+","")

if (GlobalVariable.accountNumber != norek) {
	throw new Exception("Norek Berbeda")
}

def notelp = Mobile.getText(findTestObject('Object Repository/Pulsa Data/Data/Konfirmasi/Notelp'), 0)

notelp = notelp.replaceAll("\\D+","")

if (GlobalVariable.phoneNumber != notelp) {
	throw new Exception("Notelp Berbeda")
}

//CustomKeywords.'common.other.takeScreenShot'()

Mobile.tap(findTestObject('Object Repository/Pulsa Data/Data/Konfirmasi/Button Beli'),0)