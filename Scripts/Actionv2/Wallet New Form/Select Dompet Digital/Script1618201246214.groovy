import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

//verify title dompet digital
Mobile.verifyElementVisible(findTestObject("Object Repository/Top Up/Top Up Baru - Dompet Digital/TextView - Dompet Digital"), 0)

//tap arrow
Mobile.tap(findTestObject("Object Repository/Top Up/Top Up Baru - Dompet Digital/Arrow - Pilih Dompet Digital"), 0)

Mobile.delay(2)

CustomKeywords.'common.screenshot.takeScreenshotAsCheckpoint'()

wallets = dompetDigital.toString()

switch(wallets) {
	case'LinkAja':
		Mobile.tap(findTestObject("Object Repository/v2/Wallet New Form/TextView - LinkAja"), 0)
		break;

	case'OVO':
		Mobile.tap(findTestObject("Object Repository/Top Up/Top Up Baru - Dompet Digital/Dropdown - OVO"), 0)
		break;
		
	case'ShopeePay':
		Mobile.tap(findTestObject("Object Repository/v2/Wallet New Form/TextView - ShopeePay"), 0)
		break;
		
	case'GoPay':
		Mobile.tap(findTestObject("Object Repository/v2/Wallet New Form/TextView - GoPay"), 0)
		break;
		
}

gopayOpt = gopayType.toString()

if(wallets == 'GoPay') {
	
	//tap Jenis gopay
		Mobile.tap(findTestObject("Object Repository/v2/Wallet New Form/EditText - Pilih Jenis GoPay"), 0)
	
	switch(gopayOpt) {
		case'customer':
		//ini tap customer
		Mobile.tap(findTestObject("Object Repository/v2/Wallet New Form/Gopay - TextView - Customer"), 0)
		break;
		
		case'driver':
		//ini tap driver
		Mobile.tap(findTestObject("Object Repository/v2/Wallet New Form/Gopay - TextView - Driver"), 0)
		
		break;
	}
	
}

//tap LinkAja
//Mobile.tap(findTestObject("Object Repository/v2/Wallet New Form/TextView - LinkAja"), 0)

//tap ovo sementara
//Mobile.tap(findTestObject("Object Repository/Top Up/Top Up Baru - Dompet Digital/Dropdown - OVO"), 0)

CustomKeywords.'common.screenshot.takeScreenshotAsCheckpoint'()