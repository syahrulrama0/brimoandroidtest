import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.keyword.excel.ExcelKeywords

import internal.GlobalVariable

String excelFile = GlobalVariable.excelFile
sheet = ExcelKeywords.getExcelSheetByName(GlobalVariable.excelFile, "KAI")
String nomorKAI = ExcelKeywords.getCellValueByAddress(sheet, "A"+GlobalVariable.giveCases)

//verify title KAI
Mobile.verifyElementVisible(findTestObject('Object Repository/v2/KAI/Form/TextView - KAI'), 0)

//verify text Bayar Tiket KAI
Mobile.verifyElementVisible(findTestObject('Object Repository/v2/KAI/Form/TextView - Bayar Tiket KAI'), 0)

//get
//def nomorTujuan = Mobile.getText(findTestObject('Object Repository/v2/Wallet Nominal Form/android.widget.TextView - OVO - 08008008001'), 0)
//nomorTujuan = nomorTujuan.replaceAll("\\D+","")
//
//GlobalVariable.getNomorTujuan = nomorTujuan

//input Nomor KAI
Mobile.setText(findTestObject('Object Repository/v2/KAI/Form/Nomor KAI'), nomorKAI.toString(), 0)

//Screenshot
//CustomKeywords.'common.screenshot.takeScreenshotAsCheckpoint'()

//tap button Lanjutkan
Mobile.tap(findTestObject('Object Repository/v2/KAI/Form/KAI - Button - Lanjutkan'), 0)