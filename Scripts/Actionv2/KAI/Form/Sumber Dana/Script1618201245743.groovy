import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import com.kms.katalon.keyword.excel.ExcelKeywords

////Verify text Tagihan
//Mobile.tap(findTestObject('Object Repository/v2/KAI/Form/TextView - Tagihan'), 0)
//
////Lihat Detail Tagihan
//Mobile.tap(findTestObject('Object Repository/v2/KAI/Form/Detail Tagihan'), 0)
//
////Tap Sumber Dana Detail
//Mobile.tap(findTestObject('Object Repository/v2/KAI/Form/Icon Detail Sumber Dana'), 0)
//
////Sumber Dana
////add delay
//Mobile.delay(5)
//
////Screenshot
//CustomKeywords.'common.screenshot.takeScreenshotAsCheckpoint'()
//
////Select Sumber Dana
//device_Height = Mobile.getDeviceHeight()
//device_Width = Mobile.getDeviceWidth()
//int startX = device_Width / 2
//int endX = startX
//int startY = device_Height * 0.30
//int endY = device_Height * 0.90
//
//String pilihRekening
//
//switch(sumberDana) {
//	case "rekeningUtama":
//	pilihRekening = Mobile.getText(findTestObject("Object Repository/v2/Wallet Nominal Form/Sumber Dana/norek 1 - rekening utama - TextView - 0206 0100 0051 308"), 0)
//	Mobile.tap(findTestObject("Object Repository/v2/Wallet Nominal Form/Sumber Dana/norek 1 - rekening utama - TextView - 0206 0100 0051 308"), 0)
//	break
//	
//	case "saldoTidakCukup":
//	pilihRekening = Mobile.getText(findTestObject("Object Repository/v2/Wallet Nominal Form/Sumber Dana/Norek 2  - GIRO SALDO TIDAK CUKUP"), 0)
//	Mobile.tap(findTestObject("Object Repository/v2/Wallet Nominal Form/Sumber Dana/Norek 2  - GIRO SALDO TIDAK CUKUP"), 0)
//	break
//	
//	case "britamax":
//	Mobile.swipe(startX, endY, endX, startY)
//	pilihRekening = Mobile.getText(findTestObject("Object Repository/v2/Wallet Nominal Form/Sumber Dana/Norek 3 - Britama x - 0206 2900 0008 507"), 0)
//	Mobile.tap(findTestObject("Object Repository/v2/Wallet Nominal Form/Sumber Dana/Norek 3 - Britama x - 0206 2900 0008 507"), 0)
//	break
//	
//	case "freeze":
//	Mobile.swipe(startX, endY, endX, startY)
//	pilihRekening = Mobile.getText(findTestObject("Object Repository/v2/Wallet Nominal Form/Sumber Dana/Freeze"), 0)
//	Mobile.tap(findTestObject("Object Repository/v2/Wallet Nominal Form/Sumber Dana/Freeze"), 0)
//	break
//	
//	case "dormant":
//	Mobile.swipe(startX, endY, endX, startY)
//	pilihRekening = Mobile.getText(findTestObject("Object Repository/v2/Wallet Nominal Form/Sumber Dana/Norek 4 - Dormant"), 0)
//	Mobile.tap(findTestObject("Object Repository/v2/Wallet Nominal Form/Sumber Dana/Norek 4 - Dormant"), 0)
//	break
//	
//	case "closed":
//	Mobile.swipe(startX, endY, endX, startY)
//	pilihRekening = Mobile.getText(findTestObject("Object Repository/v2/Wallet Nominal Form/Sumber Dana/Closed"), 0)
//	Mobile.tap(findTestObject("Object Repository/v2/Wallet Nominal Form/Sumber Dana/Closed"), 0)
//	break
//}
//
//if (sumberDana == "rekeningUtama" || sumberDana == "britamax" ) {
//	//tap button Top Up
//	Mobile.tap(findTestObject('Object Repository/v2/KAI/Form/Button - Bayar'), 0)
//}
//else if (sumberDana == "freeze" || sumberDana == "dormant" || sumberDana == "closed"  || SumberDana == "saldoTidakCukup") {
//	Mobile.comment(pilihRekening)
//} else {
//	throw new Exception ("Jenis Rekening tidak ditemukan")
//}
//
//GlobalVariable.getNomorRekening = pilihRekening
//Mobile.comment(GlobalVariable.getNomorRekening)
//
////close Sumber Dana
////Mobile.tap(findTestObject('Object Repository/v2/Wallet Nominal Form/Close Dompet Digital'), 0)
//
///////////////////////////////////

//Verify text Tagihan

Mobile.verifyElementVisible(findTestObject('Object Repository/v2/KAI/Form/TextView - Tagihan'), 0)
//Screenshot
//CustomKeywords.'common.screenshot.takeScreenshotAsCheckpoint'()

String excelFile = 'datasets\\BRIMO.xlsx'
sheet = ExcelKeywords.getExcelSheetByName("datasets\\BRIMO.xlsx", "KAI")
String condition = ExcelKeywords.getCellValueByAddress(sheet, "B"+GlobalVariable.giveCases)

//String getNoRekening
//switch(condition) {
//	
//	case "Saldo_Cukup":
//	getNoRekening = Mobile.getText(findTestObject('Object Repository/v2/KAI/Form/Nomor Rekening KAI'), 0)
//	break
//	
//	case "Saldo_Tidak_Cukup":
//	getNoRekening = Mobile.getText(findTestObject('Object Repository/v2/KAI/Form/Nomor Rekening KAI'), 0)
//	break
//	
//}

//CustomKeywords.'common.screenshot.takeScreenshotAsCheckpoint'()

//if(condition=="Saldo_Cukup") {
//	Mobile.comment(getNoRekening)
	Mobile.tap(findTestObject('Object Repository/v2/KAI/Form/Button - Bayar'), 0)
//}
//else if(condition=="Saldo_Tidak_Cukup") {
//	Mobile.comment(getNoRekening)
////	Mobile.verifyElementVisible(findTestObject(''), 0)
//}else {
//	throw new Exception ("Kondisi tidak ditemukan")
//}

//GlobalVariable.getSumberDana = getNoRekening