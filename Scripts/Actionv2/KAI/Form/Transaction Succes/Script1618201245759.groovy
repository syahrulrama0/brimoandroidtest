import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.mobile.keyword.internal.MobileAbstractKeyword
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

Mobile.delay(3)

//Title trx berhasil
Mobile.verifyElementVisible(findTestObject('Object Repository/v2/KAI/Form/Success - Transaksi Berhasil'), 0)

//screenshot
CustomKeywords.'common.screenshot.takeScreenshotAsCheckpoint'()

//tap Detail trx
//Mobile.tap(findTestObject('Object Repository/v2/KAI/Struk Pembayaran/Lihat Detail Transaksi'), 0)

//get nama feature
//Mobile.verifyElementExist(findTestObject('Object Repository/v2/KAI/Struk Pembayaran/Jenis Transaksi'), 0)
//def featKAI = Mobile.getText(findTestObject('Object Repository/v2/KAI/Struk Pembayaran/Jenis Transaksi'), 0)

//get nomor rekening
//Mobile.verifyElementExist(findTestObject('Object Repository/v2/KAI/Struk Pembayaran/Sumber Dana'), 0)
//def sumberDana = Mobile.getText(findTestObject('Object Repository/v2/KAI/Struk Pembayaran/Sumber Dana'), 0)

//get tanggal
Mobile.verifyElementExist(findTestObject('v2/KAI/Struk Pembayaran/Tanggal Trx'), 0)
def time = Mobile.getText(findTestObject('v2/KAI/Struk Pembayaran/Tanggal Trx'), 0)

//get nomor referensi
Mobile.verifyElementExist(findTestObject('Object Repository/v2/KAI/Struk Pembayaran/NoRef'), 0)
def refnum = Mobile.getText(findTestObject('Object Repository/v2/KAI/Struk Pembayaran/NoRef'), 0)

//get nominal
Mobile.verifyElementExist(findTestObject('Object Repository/v2/KAI/Struk Pembayaran/Nominal'), 0)
def nominal = Mobile.getText(findTestObject('Object Repository/v2/KAI/Struk Pembayaran/Nominal'), 0)
nominal = nominal.replaceAll("\\D+", "")
Mobile.comment(nominal)

//get biayaAdmin
Mobile.verifyElementExist(findTestObject('Object Repository/v2/KAI/Struk Pembayaran/Biaya Admin'), 0)
def biayaAdmin = Mobile.getText(findTestObject('Object Repository/v2/KAI/Struk Pembayaran/Biaya Admin'), 0)
biayaAdmin = biayaAdmin.replaceAll("\\D+", "")
Mobile.comment(biayaAdmin)

//total
Mobile.verifyElementExist(findTestObject('Object Repository/v2/KAI/Struk Pembayaran/Total'), 0)
def total = Mobile.getText(findTestObject('Object Repository/v2/KAI/Struk Pembayaran/Total'), 0)

nominal = Integer.parseInt(nominal)
biayaAdmin = Integer.parseInt(biayaAdmin)
def verifyTotalHarga = nominal + biayaAdmin 

if(total == verifyTotalHarga) {
	Mobile.comment(total)
	Mobile.comment(verifyTotalHarga)
}

CustomKeywords.'common.other.fileWrite'(time, refnum, total, "KAI")

CustomKeywords.'common.screenshot.takeScreenshotAsCheckpoint'()

//tap lihat lebih sedikit
//Mobile.tap(findTestObject('Object Repository/v2/KAI/Struk Pembayaran/Lihat Lebih Sedikit'), 0)

//tap button OK
Mobile.tap(findTestObject('Object Repository/v2/KAI/Form/Success Button - OK'), 0)

