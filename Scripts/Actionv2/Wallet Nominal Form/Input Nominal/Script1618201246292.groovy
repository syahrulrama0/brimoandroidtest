import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

//verify title Dompet Digital
Mobile.verifyElementVisible(findTestObject('Object Repository/v2/Wallet Nominal Form/TextView - Dompet Digital'), 0)

//verify text Nominal
Mobile.verifyElementVisible(findTestObject('Object Repository/v2/Wallet Nominal Form/TextView - Nominal'), 0)

//get
def nomorTujuan = Mobile.getText(findTestObject('Object Repository/v2/Wallet Nominal Form/android.widget.TextView - OVO - 08008008001'), 0)
nomorTujuan = nomorTujuan.replaceAll("\\D+","")

GlobalVariable.getNomorTujuan = nomorTujuan

//input Nominal
Mobile.setText(findTestObject('Object Repository/v2/Wallet Nominal Form/Input Nominal'), nominal.toString(), 0)

//Screenshot
CustomKeywords.'common.screenshot.takeScreenshotAsCheckpoint'()

Mobile.tap(findTestObject('Object Repository/Dompet Digitals/Konfirmasi/Button Top Up'),0)

Mobile.tap(findTestObject('Object Repository/Dompet Digitals/Konfirmasi/Button Top Up'),0)