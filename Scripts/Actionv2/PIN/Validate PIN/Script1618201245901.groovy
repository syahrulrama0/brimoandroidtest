import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import java.util.concurrent.TimeUnit


Mobile.verifyElementExist(findTestObject('Object Repository/v2/PIN/TextView - Masukkan PIN'), 0)

CustomKeywords.'common.screenshot.takeScreenshotAsCheckpoint'()

TimeUnit.SECONDS.sleep(3);

if (condition == "correct") {
for (int i=1; i<=5; i++) {
		Mobile.tap(findTestObject('Object Repository/v2/PIN/Button ' + number.substring(i-1,i)), 0)
}
}
else if (condition == "incorrect") {
String[] pinArray = ["111111", "111999", "999999"]
for (int i=1; i<=2; i++) {
	for (int j=1; j<=6; j++) {
		Mobile.tap(findTestObject('Object Repository/v2/PIN/Button '+ pinArray[i].substring(j-1,j)), 0)
	}
}
Mobile.delay(3)
CustomKeywords.'common.screenshot.takeScreenshotAsCheckpoint'()
}

Mobile.delay(3)
Mobile.closeApplication()

