import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.keyword.excel.ExcelKeywords

import internal.GlobalVariable

String excelFile = GlobalVariable.excelFile
sheet = ExcelKeywords.getExcelSheetByName(GlobalVariable.excelFile, "Cicilan")
String jenisCicilan = ExcelKeywords.getCellValueByAddress(sheet, "B"+GlobalVariable.giveCases)

//Verify title Cicilan Finance
Mobile.verifyElementVisible(findTestObject('Object Repository/v2/Cicilan/New Form/Title TextView - Cicilan Finance'), 0)

//Tap Dropdown Jenis Cicilan
Mobile.tap(findTestObject('Object Repository/v2/Cicilan/New Form/EditText - Pilih Jenis Cicilan'), 0)

//
Mobile.delay(3)

CustomKeywords.'common.screenshot.takeScreenshotAsCheckpoint'()

cicilan = jenisCicilan.toString()

switch (cicilan) {
	case 'BAF':
		Mobile.tap(findTestObject('Object Repository/v2/Cicilan/New Form/TextView - Cicilan BAF'), 0)

		break
	case 'FIF':
		Mobile.tap(findTestObject('Object Repository/v2/Cicilan/New Form/TextView - Cicilan FIF'), 0)

		break
	case 'FINANSIA':
		Mobile.tap(findTestObject('Object Repository/v2/Cicilan/New Form/TextView - Cicilan FINANSIA'), 0)

		break
	case 'OTO':
		Mobile.tap(findTestObject('Object Repository/v2/Cicilan/New Form/TextView - Cicilan OTO'), 0)

		break
	case 'VERENA':
		Mobile.tap(findTestObject('Object Repository/v2/Cicilan/New Form/TextView - Cicilan VERENA'), 0)

		break
	case 'WOM':
		Mobile.tap(findTestObject('Object Repository/v2/Cicilan/New Form/TextView - Cicilan WOM'), 0)

		break
	default:
		break
}