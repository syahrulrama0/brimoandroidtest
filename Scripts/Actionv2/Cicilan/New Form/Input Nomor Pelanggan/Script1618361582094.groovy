import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.keyword.excel.ExcelKeywords

import internal.GlobalVariable

String excelFile = GlobalVariable.excelFile
sheet = ExcelKeywords.getExcelSheetByName(GlobalVariable.excelFile, "Cicilan")
String noPelanggan = ExcelKeywords.getCellValueByAddress(sheet, "A"+GlobalVariable.giveCases)

//Verify text Nomor Pelanggan
Mobile.verifyElementVisible(findTestObject("Object Repository/v2/Cicilan/New Form/TextView - Nomor Pelanggan"), 0)

//CustomKeywords.'common.screenshot.takeScreenshotAsCheckpoint'()

//Input nomor Pelanggan
Mobile.setText(findTestObject("Object Repository/v2/Cicilan/New Form/EditText - Nomor Pelanggan"), noPelanggan.toString(), 0)

CustomKeywords.'common.screenshot.takeScreenshotAsCheckpoint'()

