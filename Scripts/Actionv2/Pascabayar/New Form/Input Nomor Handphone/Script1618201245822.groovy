import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.keyword.excel.ExcelKeywords

import internal.GlobalVariable

String excelFile = GlobalVariable.excelFile
sheet = ExcelKeywords.getExcelSheetByName(GlobalVariable.excelFile, "Pascabayar")
String nomorPascabayar = ExcelKeywords.getCellValueByAddress(sheet, "A"+GlobalVariable.giveCases)

//Verify title Pascabayar
Mobile.verifyElementVisible(findTestObject('Object Repository/v2/Pascabayar/New Form/TextView - Pascabayar'), 0)

//CustomKeywords.'common.screenshot.takeScreenshotAsCheckpoint'()

//Input Nomor Handphone
Mobile.setText(findTestObject('Object Repository/v2/Pascabayar/New Form/EditText - Nomor Tujuan'), nomorPascabayar, 0)

CustomKeywords.'common.screenshot.takeScreenshotAsCheckpoint'()

