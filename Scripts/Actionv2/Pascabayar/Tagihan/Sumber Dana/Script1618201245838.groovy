import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import com.kms.katalon.keyword.excel.ExcelKeywords

//Verify text Tagihan
Mobile.verifyElementVisible(findTestObject('Object Repository/v2/Pascabayar/Tagihan/TextView - Tagihan'), 0)

//Screenshot
CustomKeywords.'common.screenshot.takeScreenshotAsCheckpoint'()

String excelFile = GlobalVariable.excelFile
sheet = ExcelKeywords.getExcelSheetByName(GlobalVariable.excelFile, "Pascabayar")
String condition = ExcelKeywords.getCellValueByAddress(sheet, "B"+GlobalVariable.giveCases)

String getNoRekening
switch(condition) {
	
	case "Saldo_Cukup":
	getNoRekening = Mobile.getText(findTestObject('Object Repository/v2/Pascabayar/Tagihan/Nomor Rekening'), 0)
	break
	
	case "Saldo_Tidak_Cukup":
	getNoRekening = Mobile.getText(findTestObject('Object Repository/v2/Pascabayar/Tagihan/Nomor Rekening'), 0)
	break
	
}

CustomKeywords.'common.screenshot.takeScreenshotAsCheckpoint'()

if(condition=="Saldo_Cukup") {
	Mobile.comment(getNoRekening)
	Mobile.tap(findTestObject('Object Repository/v2/Pascabayar/Tagihan/Button - Bayar'), 0)
}
else if(condition=="Saldo_Tidak_Cukup") {
	Mobile.comment(getNoRekening)
//	Mobile.verifyElementVisible(findTestObject(''), 0)
}else {
	throw new Exception ("Kondisi tidak ditemukan")
}

GlobalVariable.getSumberDana = getNoRekening