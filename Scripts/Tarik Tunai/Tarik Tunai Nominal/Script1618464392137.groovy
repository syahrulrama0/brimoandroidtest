import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.keyword.excel.ExcelKeywords
import internal.GlobalVariable as GlobalVariable


String excelFile = GlobalVariable.excelFile
sheet = ExcelKeywords.getExcelSheetByName(GlobalVariable.excelFile, "Tarik Tunai")
String amount = ExcelKeywords.getCellValueByAddress(sheet, "C"+GlobalVariable.giveCases)

Mobile.delay(3)


String pilihNominal
switch(amount) {
	case "100000" :
	Mobile.tap(findTestObject('Object Repository/Tarik Tunai Object/Nominal - Rp100.000'), 0)
	pilihNominal = Mobile.getText(findTestObject('Object Repository/Tarik Tunai Object/Nominal - Rp100.000'), 0)
	break
	case "200000" :
	Mobile.tap(findTestObject('Object Repository/Tarik Tunai Object/Nominal - Rp200.000'), 0)
	pilihNominal = Mobile.getText(findTestObject('Object Repository/Tarik Tunai Object/Nominal - Rp200.000'), 0)
	break
	case "300000" :
	Mobile.tap(findTestObject('Object Repository/Tarik Tunai Object/Nominal - Rp300.000'), 0)
	pilihNominal = Mobile.getText(findTestObject('Object Repository/Tarik Tunai Object/Nominal - Rp300.000'), 0)
	break
	case "400000" :
	Mobile.tap(findTestObject('Object Repository/Tarik Tunai Object/Nominal - Rp400.000'), 0)
	pilihNominal = Mobile.getText(findTestObject('Object Repository/Tarik Tunai Object/Nominal - Rp400.000'), 0)
	break
	case "500000" :
	Mobile.tap(findTestObject('Object Repository/Tarik Tunai Object/Nominal - Rp500.000'), 0)
	pilihNominal = Mobile.getText(findTestObject('Object Repository/Tarik Tunai Object/Nominal - Rp500.000'), 0)
	Mobile.comment(pilihNominal)
	break
	case "600000" :
	Mobile.tap(findTestObject('Object Repository/Tarik Tunai Object/Nominal - Rp600.000'), 0)
	pilihNominal = Mobile.getText(findTestObject('Object Repository/Tarik Tunai Object/Nominal - Rp600.000'), 0)
	Mobile.comment(pilihNominal)
	break
	case "700000" :
	Mobile.tap(findTestObject('Object Repository/Tarik Tunai Object/Nominal - Rp700.000'), 0)
	pilihNominal = Mobile.getText(findTestObject('Object Repository/Tarik Tunai Object/Nominal - Rp700.000'), 0)
	case "800000" :
	Mobile.tap(findTestObject('Object Repository/Tarik Tunai Object/Nominal - Rp800.000'), 0)
	pilihNominal = Mobile.getText(findTestObject('Object Repository/Tarik Tunai Object/Nominal - Rp800.000'), 0)
	break
	case "900000" :
	Mobile.tap(findTestObject('Object Repository/Tarik Tunai Object/Nominal - Rp900.000'), 0)
	pilihNominal = Mobile.getText(findTestObject('Object Repository/Tarik Tunai Object/Nominal - Rp900.000'), 0)
	break
	case "1000000" :
	Mobile.tap(findTestObject('Object Repository/Tarik Tunai Object/Nominal - Rp1.000.000'), 0)
	pilihNominal = Mobile.getText(findTestObject('Object Repository/Tarik Tunai Object/Nominal - Rp1.000.000'), 0)
	break
	case "1000000" :
	Mobile.scrollToText('1.000.000')
	Mobile.tap(findTestObject('Object Repository/Tarik Tunai Object/Nominal - Rp1.000.000'), 0)
	pilihNominal = Mobile.getText(findTestObject('Object Repository/Tarik Tunai Object/Nominal - Rp1.000.000'), 0)
	break
	default:
	break
}
CustomKeywords.'common.screenshoot.takeScreenshotAsCheckpoint'()

//def pilihNominal = Mobile.getText(findTestObject('Object Repository/Tarik Tunai Object/Nominal - Rp100.000'), 0)
//pilihNominal = pilihNominal.replaceAll("\\D+","")
GlobalVariable.getNominal = pilihNominal
Mobile.comment(GlobalVariable.getNominal)