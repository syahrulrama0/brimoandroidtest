import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile

import internal.GlobalVariable




//Mobile.verifyElementExist(findTestObject('Object Repository/Telkom Object/Halaman Konfirmasi - No Rekening'), 0, FailureHandling.STOP_ON_FAILURE)
//Mobile.verifyElementExist(findTestObject('Object Repository/Telkom Object/Halaman Konfirmasi - Nomor Tujuan'), 0, FailureHandling.STOP_ON_FAILURE)
//Mobile.verifyElementExist(findTestObject('Object Repository/Telkom Object/Halaman Konfirmasi - Nominal'), 0, FailureHandling.STOP_ON_FAILURE)
//Mobile.verifyElementExist(findTestObject('Object Repository/Telkom Object/Halaman Konfirmasi - Biaya Admin'), 0, FailureHandling.STOP_ON_FAILURE)
//Mobile.verifyElementExist(findTestObject('Object Repository/Telkom Object/Halaman Konfirmasi - Total'), 0, FailureHandling.STOP_ON_FAILURE)

Mobile.verifyElementVisible(findTestObject('Object Repository/Telkom Object/Halaman Konfirmasi - No Rekening'), 0)
Mobile.verifyElementVisible(findTestObject('Object Repository/Telkom Object/Halaman Konfirmasi - Nomor Tujuan'), 0)
Mobile.verifyElementVisible(findTestObject('Object Repository/Telkom Object/Halaman Konfirmasi - Nominal'), 0)
Mobile.verifyElementVisible(findTestObject('Object Repository/Telkom Object/Halaman Konfirmasi - Biaya Admin'), 0)
Mobile.verifyElementVisible(findTestObject('Object Repository/Telkom Object/Halaman Konfirmasi - Total'), 0)

def norek = Mobile.getText(findTestObject('Object Repository/Telkom Object/Halaman Konfirmasi - No Rekening'),0)

norek = norek.replaceAll("\\D+","")

GlobalVariable.getNomorRekening= norek

//CustomKeywords.'common.other.takeScreenShot'()
