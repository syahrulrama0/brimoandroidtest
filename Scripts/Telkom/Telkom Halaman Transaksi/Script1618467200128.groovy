import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile

import internal.GlobalVariable as GlobalVariable
device_Height = Mobile.getDeviceHeight()
device_Width = Mobile.getDeviceWidth()
int startX = device_Width / 2
int endX = startX
int startY = device_Height * 0.30
int endY = device_Height * 0.85

Mobile.tap(findTestObject('Object Repository/Telkom Object/tap - Lihat Detail Transaksi'), 0)
Mobile.delay(3)
Mobile.verifyElementVisible(findTestObject('Object Repository/Telkom Object/Telkom Transaksi Verify  - Tanggal'), 0)
Mobile.verifyElementVisible(findTestObject('Object Repository/Telkom Object/Telkom Transaksi Verify - Nomor Referensi'), 0)
Mobile.verifyElementVisible(findTestObject('Object Repository/Telkom Object/Verify Transaksi - Jenis Transaksi'), 0)
Mobile.verifyElementVisible(findTestObject('Object Repository/Telkom Object/Verify Transaksi - Sumber Dana'), 0)
Mobile.verifyElementVisible(findTestObject('Object Repository/Telkom Object/Telkom Transaksi Verify - Nama'), 0)
Mobile.verifyElementVisible(findTestObject('Object Repository/Telkom Object/Telkom Transaksi Verify - Nomor Pelanggan'), 0)
Mobile.verifyElementVisible(findTestObject('Object Repository/Telkom Object/Telkom Transaksi Verify - Kode Regional'), 0)
Mobile.verifyElementVisible(findTestObject('Object Repository/Telkom Object/Telkom Transaksi Verify - Jumlah Tagihan'), 0)
Mobile.verifyElementVisible(findTestObject('Object Repository/Telkom Object/Telkom Transaksi Verify - Nomor Referensi'), 0)

Mobile.swipe(startX, endY, endX, startY)

Mobile.verifyElementVisible(findTestObject('Object Repository/Telkom Object/Telkom Transaksi Verify - Nominal'), 0)
Mobile.verifyElementVisible(findTestObject('Object Repository/Telkom Object/Telkom Transaksi Verify - Biaya Admin'), 0)
Mobile.verifyElementVisible(findTestObject('Object Repository/Telkom Object/Telkom Transaksi Verify - Total'), 0)

Mobile.tap(findTestObject('Object Repository/Telkom Object/Telkom Button - OK'), 0)



def time = Mobile.getText(findTestObject('Object Repository/Telkom Object/Telkom Transaksi Verify  - Tanggal'),0)
def refnum = Mobile.getText(findTestObject('Object Repository/Telkom Object/Telkom Transaksi Verify - Nomor Referensi'),0)
def nominal = Mobile.getText(findTestObject('Object Repository/Telkom Object/Telkom Transaksi Verify - Nominal'),0)

def fee = Mobile.getText(findTestObject('Object Repository/Telkom Object/Telkom Transaksi Verify - Biaya Admin'),0)


Mobile.comment(time)
time = time.replaceAll("\\D+","")
GlobalVariable.time = time

CustomKeywords.'common.other.fileWrite'("Telkom", time, refnum, nominal, fee)

