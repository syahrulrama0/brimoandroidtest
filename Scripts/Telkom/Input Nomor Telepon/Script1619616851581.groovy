import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.keyword.excel.ExcelKeywords

import internal.GlobalVariable as GlobalVariable

String excelFile = GlobalVariable.excelFile
sheet = ExcelKeywords.getExcelSheetByName(GlobalVariable.excelFile, "Telkom")
String nomor = ExcelKeywords.getCellValueByAddress(sheet, "A"+GlobalVariable.giveCases)

Mobile.verifyElementExist(findTestObject('Object Repository/Telkom Object/Telkom Input Nomor Tujuan/Telkom'),0)

Mobile.verifyElementExist(findTestObject('Object Repository/Telkom Object/Telkom Input Nomor Tujuan/Icon Back'), 0)

Mobile.delay(4)

Mobile.setText(findTestObject('Object Repository/Telkom Object/Telkom- Nomor Telepon'), nomor, 0)

CustomKeywords.'common.other.takeScreenShot'()
