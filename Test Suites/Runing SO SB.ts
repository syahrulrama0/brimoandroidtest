<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Runing SO SB</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>42e84b45-787f-4a61-87cf-9f694fab846f</testSuiteGuid>
   <testCaseLink>
      <guid>3a62dad8-36fe-475d-9360-25f1f60a63b1</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Info Saldo/Runing Info Saldo</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>1f4238f4-7177-463d-a1d4-3e7507fcea1a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Transfer Sesama Bank BRI/Transfer_dari_Daftar_Tambah_Baru</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>1e8ec8a7-78d3-4607-8f2a-713215cc362c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Transfer Antar Bank/Transfer_dari_Daftar_Tambah_Baru_Bank_Lain_BNI</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>0d0b1cd4-578e-48a3-9f54-de337e02f285</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Briva/Runing Briva</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>5fd6783d-e6c8-4aa4-8c10-aab76a2d82b3</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Kartu Kredit/Runing Kartu Kredit BRI</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a430d74c-39b5-4a55-a076-eccc701ff093</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Kartu Kredit/Runing Kartu Kredit Citibank</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>6a64f306-0873-4973-845e-fc53c7f00ff7</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Listrik/Runing PLN Tagihan</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>7c5b445c-5c82-43d1-bd2d-48ad0102da20</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Listrik/Runing PLN Token</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>261aa3fa-5bdd-4966-9f74-a0e3ab206823</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Pulsa/Runing Pulsa Telkomsel</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a77fc55a-81a5-4958-8311-0952921de5f8</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Dompet Digital/Runing LinkAja</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>fc7549b2-9d16-4074-84e7-20147b71bc78</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Dompet Digital/Runing Ovo</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f129b9bc-68ed-4ccd-8423-5b21668ed573</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Dompet Digital/Runing Gopay</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d858523f-7693-4b75-9b07-9498a67f00d1</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Brizzi/Runing Brizzi</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
