<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Running All feature</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>80c0b095-3d57-43cb-a73b-35bfa284578a</testSuiteGuid>
   <testCaseLink>
      <guid>6f167a63-a576-408f-a465-8529549ea05a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Info Saldo/Runing Info Saldo</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f516bcc8-c711-4898-9d26-7160252db4f7</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Transfer Sesama Bank BRI/Transfer_dari_Daftar_Tambah_Baru</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e56c4bdc-269c-4b75-95c1-4f0d60cd1d73</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Transfer Antar Bank/Transfer_dari_Daftar_Tambah_Baru_Bank_Lain_BCA</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>83fbaf94-4edf-44b8-b4b8-f1b262032c28</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Transfer Antar Bank/Transfer_dari_Daftar_Tambah_Baru_Bank_Lain_BNI</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c55c8b49-599d-45fa-99a3-539ee406c563</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Transfer Antar Bank/Transfer_dari_Daftar_Tambah_Baru_Bank_Lain_Mandiri</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>24999fd7-56e7-42b2-a182-9d671ed33838</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Transfer Antar Bank/Transfer_dari_Daftar_Tambah_Baru_Bank_Lain_Citibank</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>414712f8-a631-4595-9f2e-292763404592</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Briva/Runing Briva</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ca7e6a0e-7b62-483a-8434-e8ce7e068139</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Brizzi/Runing Brizzi</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>483726bb-1b2e-43eb-949e-922c83db8592</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Pulsa/Runing Pulsa Telkomsel</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>80add82a-e675-4aac-9e1d-0b2f16c62106</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Pulsa/Runing Pulsa M3</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>62f45305-3455-4a02-850c-f4f157167bb7</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Pulsa/Runing Pulsa XL</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>544c6cc0-e936-492f-8bdc-ea5fd3cfa08d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Pulsa/Running Pulsa 3</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>8be278e8-fb8c-4c4a-a1bd-88f6f3178930</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Pulsa/Running Pulsa Axis</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>40918772-1701-4d0e-a8e3-f1dd29e463dc</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Pulsa/Running Pulsa Smartfren</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>97e7f850-d5bb-4fbe-b66e-6a5be70e25ff</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Dompet Digital/Runing Gopay</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>84101d0c-07b8-46e3-9833-3eede8d1ff54</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Dompet Digital/Runing Dana</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>2bf8af33-9402-4055-82f2-42ba682d3040</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Dompet Digital/Runing LinkAja</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>97d1e0fc-4429-4797-88f3-8ae2894179e4</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Dompet Digital/Runing Ovo</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>cb0f9648-67e6-4bc6-8232-d642fc2ace91</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Dompet Digital/Runing ShopeePay</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>03887f6f-c545-4b24-8f68-d9897ff8dc23</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Telkom/Running Telkom</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>59edbfcd-2b9a-4c23-a257-bb5495a12269</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Cicilan Finance/Runing OTO</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9870d8a7-fa4f-40c2-9a0d-90733ad02553</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Cicilan Finance/Runing WOM</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a2546851-0115-4e5a-858b-ec9324c8dc63</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Cicilan Finance/Runing FIF</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>205f77a9-10f2-4dd4-8b50-ded4a9e5088a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/DPLK/Runing DPLK</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>3f07f427-0fde-4124-b04e-5724750b28eb</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/KAI/Running KAI</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>45cc3e22-8d03-4926-ae3b-ddc000aa8ea6</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Donasi/Runing Dompet Dhuafa Infaq</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>bc9ac282-de2f-4297-ab60-aa025b0bcd6b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Donasi/Runing Dompet Dhuafa Zakat</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>22965882-add7-43ad-83c7-5e5a952dd115</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Donasi/Runing YBM Infaq</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>2580ad2e-a4c6-458a-b1fd-97af7b695387</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Donasi/Runing YBM Zakat</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>7f38792b-cbb4-40c9-a974-1268442e9f42</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Kartu Kredit/Runing Kartu Kredit BRI</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>8cd8ff96-3c78-4a14-9ff0-11f21bc7a4db</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Kartu Kredit/Runing Kartu Kredit Citibank</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>bf5434b5-6a7f-471e-b061-8bc141e4b51b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Kartu Kredit/Runing Kartu Kredit DBS Card</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>58bc6aca-3cf9-4347-82c3-84027ae1445d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Kartu Kredit/Runing Kartu Kredit HSBC Card</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>bf539b4a-b27a-4979-b2d0-20cc3ae1e308</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Kartu Kredit/Runing Kartu Kredit Standard Chartered Card</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b7d1d9fc-0088-48d0-a049-87436d8f3ab3</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Pascabayar/Runing Pascabayar</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>8113357c-8a66-45b3-8077-0df48fe7b4bf</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Listrik/Runing PLN Tagihan</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>8ed55f00-4390-4588-8d12-9fa508339fcf</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Listrik/Runing PLN Token</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>0130f2e7-afc4-4cac-a274-fb499a8dfac8</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Asuransi BRIns/Runing Brins</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d8ce1141-5de1-404e-8957-ca8d68d9a731</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Asuransi BRIns/Runing Prudential Premi Pertama</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>5304ce4d-61cc-48ed-8b5d-a902e91fbe7c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Asuransi BRIns/Runing Prudential Premi Lanjutan</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>eb5fa55d-c2cb-4e72-ab83-4f58d4643aa6</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Asuransi BRIns/Runing Prudential TopUp</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>de24a4e6-fa3a-4ac9-9f50-cb01f059d4e2</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Asuransi BRIns/Runing Prudential Biaya Cetak Ulang</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c18196d5-b186-4382-9d3e-708cc4fe0db5</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Asuransi BRIns/Runing Prudential Biaya Perubahan</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>6fff032d-c0c7-43b1-a02b-6176ab19c9f3</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Asuransi BRIns/Runing Prudential Biaya Cetak Kartu</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>38f842c9-45dd-474c-881c-3de10ecd29be</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TV/Runing TV Transvision</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
