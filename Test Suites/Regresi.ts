<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Regresi</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>39c49d94-ae46-4f0f-8c48-4b971758b18e</testSuiteGuid>
   <testCaseLink>
      <guid>dcfcd52e-67a9-4152-9837-55134e857d01</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Pulsa/Runing Pulsa M3</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f784b67c-d1db-4b9f-b2a3-d7a5c5bdf55a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Pulsa/Runing Pulsa XL</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>15ca4d2b-6037-4b79-b46c-ae9d5299e506</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Pulsa/Running Pulsa 3</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>8f485b62-2805-4de8-9568-027c9bddbcda</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Pulsa/Running Pulsa Axis</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>72a4efe6-bfce-4949-b4b6-8aa25186505d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Pulsa/Running Pulsa Smartfren</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9dc933d8-648f-4bdf-92ca-1170d71a0f06</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Pulsa/Runing Pulsa Telkomsel</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>64257ebf-5b91-4b59-855a-9d55e7304a45</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/PulsaData/Runing Data Telkomsel</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>422e6265-3cb9-47a0-8182-80dc7db6c72c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/PulsaData/Running Data Indosat</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>976c4776-fa0f-4b28-8ccb-f991947d4f0d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Transfer Antar Bank/Transfer_dari_Daftar_Tambah_Baru_Bank_Lain_BCA</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b7f54dc2-b99b-4418-a79e-75a0122ca03e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Transfer Antar Bank/Transfer_dari_Daftar_Tambah_Baru_Bank_Lain_BNI</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>03289ae5-8b82-4f6d-9d79-a6e552f5bbfb</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Transfer Antar Bank/Transfer_dari_Daftar_Tambah_Baru_Bank_Lain_Citibank</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f756a189-7281-4ee6-b14b-5708350f6b4b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Transfer Antar Bank/Transfer_dari_Daftar_Tambah_Baru_Bank_Lain_Mandiri</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ecdb0d62-395c-4c66-83c8-fc9733d509b0</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Transfer Sesama Bank BRI/Transfer_dari_Daftar_Tambah_Baru</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a9cc3e0e-30a1-42fd-b184-f7616e322eed</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Mutasi/Runing Mutasi</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b1f5eb73-9d30-4728-bdca-c0fe88175fb9</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Dompet Digital/Runing Dana</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b9d37bb0-026c-4ecb-8abb-889c653261a1</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Dompet Digital/Runing Gopay</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>503aa017-bc8c-4628-80cf-f70dee83da52</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Dompet Digital/Runing LinkAja</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>bc98bc13-303d-4ab7-9f68-c77c1132af64</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Dompet Digital/Runing Ovo</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>aa493a14-d92c-4ee2-8610-ca848f660a7b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Dompet Digital/Runing ShopeePay</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ce5b00ea-74b8-43c9-a6ac-6efde0842ba9</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Brizzi/Runing Brizzi</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>409f92d4-7ea0-40f8-85e7-f9b5025d80ea</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Briva/Runing Briva</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>1d2cf489-bb9a-4fe4-969d-eb3b2a3b8f72</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Listrik/Runing PLN Tagihan</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>5fc78f0e-533e-4cbd-b830-02b3abe28d07</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Listrik/Runing PLN Token</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>1a67cab6-9153-4b8d-9738-fbe302a7e05f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Listrik/Running Non Taglis</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>7fd8a9e4-9f1c-485b-b827-6cb91da8547c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Asuransi BRIns/Runing Prudential Biaya Cetak Kartu</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>36ad8d00-c40a-457f-822f-161ce89ef676</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Asuransi BRIns/Runing Prudential Biaya Cetak Ulang</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>2ae7c5fc-b29c-4e46-8231-7ee2cbf51cbd</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Asuransi BRIns/Runing Prudential Biaya Perubahan</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>2fb93704-4fe8-45f8-9747-1e1892296572</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Asuransi BRIns/Runing Prudential Premi Lanjutan</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>992e9d8d-ebcb-4f38-ac96-9c450b17d993</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Asuransi BRIns/Runing Prudential Premi Pertama</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>2727955f-7e71-4fce-99da-a3886785e764</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Asuransi BRIns/Runing Brins</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d52998cd-7123-4a38-a80b-29f28dfa3189</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Cicilan Finance/Runing FIF</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f2d73ca0-44de-4a84-a7c6-75a00a62c753</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Cicilan Finance/Runing OTO</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d484abc8-7188-466d-bff2-5b19d22db466</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Cicilan Finance/Runing WOM</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>3d3e8b90-2d94-4740-be41-dc02235c1edb</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Donasi/Runing Dompet Dhuafa Infaq</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>3d19182c-07c2-478c-a31c-cff7f33907f1</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Donasi/Runing Dompet Dhuafa Zakat</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>81665efa-43c2-47d5-ad1f-78d71186618d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Donasi/Runing YBM Infaq</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>35520216-7842-4353-b649-2991aad65596</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Donasi/Runing YBM Zakat</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>4bfe7c49-1dc5-4d71-a5e0-7d98270dc943</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/KAI/Running KAI</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>6947bd91-84fa-4660-a80a-4c31d44ef79d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Asuransi BRIns/Runing Prudential TopUp</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d6f463c9-8818-48b1-9a3d-996309d7afe3</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Kartu Kredit/Runing Kartu Kredit Citibank</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f22ca1e7-0dde-4ad3-9550-e6943e87d2f9</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Kartu Kredit/Runing Kartu Kredit DBS Card</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>fa779e3c-991b-4776-b265-2e43fcf46fba</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Kartu Kredit/Runing Kartu Kredit HSBC Card</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>10f8f635-35df-46fe-a018-15d14b7a5262</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Kartu Kredit/Runing Kartu Kredit Standard Chartered Card</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>35b8278a-59e5-46e7-bd6c-d751b5967831</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Kartu Kredit/Runing Kartu Kredit BRI</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>658f9be7-dc87-44f3-bb3b-c4bda2183519</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Telkom/Running Telkom</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ff5a7f1e-5f39-4740-86fa-e931e7382163</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/TV/Runing TV Transvision</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>6c9202d5-73cf-43da-b05e-3283eb4b4782</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/DPLK/Runing DPLK</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>6db2938e-9a70-42a3-9f39-f14fb79fce64</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Info Saldo/Runing Info Saldo</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>79485fb7-544f-4939-982a-5a9aeca3b238</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Pascabayar/Running Pascabayar Kartu Halo</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>dd429343-3a4b-4f85-a6f4-71b247c84cf8</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Pascabayar/Running Pascabayar Kartu Indosat</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>68cb79d4-2cb5-4825-8bdb-3f9dfc4ffa3e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Pascabayar/Running Pascabayar Kartu XL</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
