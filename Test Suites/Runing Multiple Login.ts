<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Runing Multiple Login</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>da457871-5adb-4ac0-b17a-2a4968e47305</testSuiteGuid>
   <testCaseLink>
      <guid>a160ab8e-6c53-4e2c-9d72-6201d6478eeb</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/End To End Multiple Login/Login dan Info Saldo</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>1d6a1e48-5193-4745-b8bf-3973603f6970</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/End To End Multiple Login/Runing Briva</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e2897762-5254-487f-8f15-6c45eb65a13d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/End To End Multiple Login/Runing Brizzi</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>0e5eea41-0b8e-421f-8c2c-2fae079609a4</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/End To End Multiple Login/Runing Cicilan FIF</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d3028375-711a-4290-b740-c42937c8d87b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/End To End Multiple Login/Runing Cicilan OTO</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>815994b1-7e1f-4e6c-ab0d-c2cb8bc172ef</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/End To End Multiple Login/Runing Cicilan WOM</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>efaad550-da62-4649-9e2e-90b95df052f6</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/End To End Multiple Login/Runing Dompet Digital Dana</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>5d230344-668e-405b-943b-f33c7421d291</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/End To End Multiple Login/Runing Dompet Digital Gopay</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f9f43667-b56d-4ef0-8127-d85ecb4c31e0</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/End To End Multiple Login/Runing Dompet Digital LinkAja</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ed7920ef-6577-49fa-8987-82cafa2436b8</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/End To End Multiple Login/Runing Dompet Digital OVO</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>5cbaec33-2764-47b4-9a9b-80076a877508</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/End To End Multiple Login/Runing Dompet Digital ShopeePay</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>67981d5a-d091-4ba5-9a47-8bc53817bae9</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/End To End Multiple Login/Runing Donasi Dompet Dhuafa Infaq</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>2f057289-8fd5-4420-9bb4-26c395b9438f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/End To End Multiple Login/Runing Donasi Dompet Dhuafa Zakat</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>8134a421-783e-4c0b-8f5e-cd22283adeda</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/End To End Multiple Login/Runing Donasi YBM Infaq</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>42eff17b-7a5f-4487-ae5a-b1fdc62d9e98</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/End To End Multiple Login/Runing Donasi YBM Zakat</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>cabfc23d-56c8-4ec6-88d6-21f7851b33b4</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/End To End Multiple Login/Runing DPLK</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>8848fbb7-c8af-4c6d-b097-ef0cd9c03f08</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/End To End Multiple Login/Runing KAI</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>2102fcd9-c8b7-4368-b65b-15e6d130ff06</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/End To End Multiple Login/Runing Kartu Kredit BRI</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>4925cb8e-aa1f-41b5-b747-52745535de8e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/End To End Multiple Login/Runing Kartu Kredit Citibank</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>79cad376-2ae8-4926-85ed-a3cb85ae4326</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/End To End Multiple Login/Runing Kartu Kredit DBS</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>944c0c46-ec19-40b4-8bc4-60ecdec12703</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/End To End Multiple Login/Runing Kartu Kredit HSBC</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>6fcc70c8-f2f4-491d-a106-81218a274e8a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/End To End Multiple Login/Runing Kartu Kredit Standard Chartedred</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e77781b1-de43-4bf3-a9dc-0396a2a7b0aa</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/End To End Multiple Login/Runing Listrik Tagihan</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>aa0fcfc9-c8fe-4116-aa85-4d7f8e9d56a8</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/End To End Multiple Login/Runing Listrik Token</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>4633722f-643f-418f-9157-efd6aea3aa42</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/End To End Multiple Login/Runing Pascabayar</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>aae4f8a7-ad92-4170-9b3b-eef12e84ae9b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/End To End Multiple Login/Runing Pulsa 3</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>118f77b5-9e89-44dd-9a9b-9d1c9076ecb1</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/End To End Multiple Login/Runing Pulsa Axis</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>36c41f85-027c-4f0a-936b-e1827a2d9b7d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/End To End Multiple Login/Runing Pulsa M3</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>11454f38-ae67-45d7-b927-4cac72714484</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/End To End Multiple Login/Runing Pulsa Smartfren</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ac40f2ee-e0b5-4ff4-848b-86254816f7ac</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/End To End Multiple Login/Runing Pulsa Telkomsel</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>68d1fd7e-54be-4ad8-9dad-d219e0fd6a37</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/End To End Multiple Login/Runing Pulsa XL</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>843d9322-361d-4dd9-9e90-901bd0de8d4e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/End To End Multiple Login/Runing Telkom</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>29464817-5e22-4321-accd-2f48d38a985c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/End To End Multiple Login/Runing Transfer Bank Citibank</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>934c09e8-7721-4f6b-8ced-b6316a413b29</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/End To End Multiple Login/Runing Transfer Bank Lain BCA</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9f7b4eff-5e75-45c8-8d7b-06691ea5f5e6</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/End To End Multiple Login/Runing Transfer Bank Lain BNI</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>4788c7e7-f2af-475d-b3d4-573f682a757b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/End To End Multiple Login/Runing Transfer Bank Mandiri</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>83165976-876e-4ac5-a8bd-74737ebbc1ea</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/End To End Multiple Login/Runing Transfer Sesama BRI</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>3d3807ed-376a-482f-bfed-1bba2bd36902</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/End To End Multiple Login/Runing TV</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
