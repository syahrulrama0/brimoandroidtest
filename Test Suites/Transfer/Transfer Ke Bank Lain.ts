<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Transfer Ke Bank Lain</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>628314c3-f5ea-47ea-8180-9d2f14750ce0</testSuiteGuid>
   <testCaseLink>
      <guid>d5ed95bc-a41f-47b3-9a02-a24111a6c9eb</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Transfer Antar Bank/Transfer_dari_Daftar_Tambah_Baru_Bank_Lain_BCA</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a3d124d8-76ee-4304-af0b-641b0c35e153</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Transfer Antar Bank/Transfer_dari_Daftar_Tambah_Baru_Bank_Lain_Mandiri</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>fe0ffb72-0e5c-40b9-b211-1c480327255a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Transfer Antar Bank/Transfer_dari_Daftar_Tambah_Baru_Bank_Lain_BNI</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>aee50abf-a957-43ed-ae56-e3ea28e2a47f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Transfer Antar Bank/Transfer_dari_Daftar_Tambah_Baru_Bank_Lain_Citibank</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
