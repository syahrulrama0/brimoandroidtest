<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Dompet Digital</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>7aa04c9b-f7d3-4805-9184-01255fe6e370</testSuiteGuid>
   <testCaseLink>
      <guid>959f85e1-9432-4521-95d3-cf20139b3e5c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/LinkAja/Runing LinkAja</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>cc5b06af-4c1d-468a-ab39-84ddcbd169e7</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/OVO/Runing OVO</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>27601fa0-73a2-4e16-9a17-2af0247db8eb</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Top Up Shopee Pay/Runing Shopee Pay</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>50a85ec4-af35-4af9-a9fb-d9a434e15a87</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Gopay/Runing Gopay</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
