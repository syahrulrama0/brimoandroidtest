package internal

import com.kms.katalon.core.configuration.RunConfiguration
import com.kms.katalon.core.main.TestCaseMain


/**
 * This class is generated automatically by Katalon Studio and should not be modified or deleted.
 */
public class GlobalVariable {
     
    /**
     * <p></p>
     */
    public static Object identifierApp
     
    /**
     * <p></p>
     */
    public static Object excelFile
     
    /**
     * <p></p>
     */
    public static Object username
     
    /**
     * <p></p>
     */
    public static Object password
     
    /**
     * <p></p>
     */
    public static Object dbUrl
     
    /**
     * <p></p>
     */
    public static Object dbName
     
    /**
     * <p></p>
     */
    public static Object dbPort
     
    /**
     * <p></p>
     */
    public static Object dbUsername
     
    /**
     * <p></p>
     */
    public static Object dbPassword
     
    /**
     * <p></p>
     */
    public static Object timeoutShort
     
    /**
     * <p></p>
     */
    public static Object timeoutMiddle
     
    /**
     * <p></p>
     */
    public static Object nominalPulsa
     
    /**
     * <p></p>
     */
    public static Object nominalTransfer
     
    /**
     * <p></p>
     */
    public static Object destinationAccount
     
    /**
     * <p></p>
     */
    public static Object destinationBank
     
    /**
     * <p></p>
     */
    public static Object destinationPhone
     
    /**
     * <p></p>
     */
    public static Object detailTransfer
     
    /**
     * <p></p>
     */
    public static Object destinationAccountBRI
     
    /**
     * <p></p>
     */
    public static Object goingOn
     
    /**
     * <p></p>
     */
    public static Object typeGopay
     
    /**
     * <p></p>
     */
    public static Object phoneNumber
     
    /**
     * <p></p>
     */
    public static Object accountNumber
     
    /**
     * <p></p>
     */
    public static Object amount
     
    /**
     * <p></p>
     */
    public static Object provider
     
    /**
     * <p></p>
     */
    public static Object phoneCode
     
    /**
     * <p></p>
     */
    public static Object userName
     
    /**
     * <p></p>
     */
    public static Object donationType
     
    /**
     * <p></p>
     */
    public static Object getNomorTujuan
     
    /**
     * <p></p>
     */
    public static Object getNomorRekening
     
    /**
     * <p></p>
     */
    public static Object getCreditNumber
     
    /**
     * <p></p>
     */
    public static Object getSumberDana
     
    /**
     * <p></p>
     */
    public static Object akunDPLK
     
    /**
     * <p></p>
     */
    public static Object getPhoneCode
     
    /**
     * <p></p>
     */
    public static Object getNumber
     
    /**
     * <p></p>
     */
    public static Object getNominal
     
    /**
     * <p></p>
     */
    public static Object getNoRekening
     
    /**
     * <p></p>
     */
    public static Object giveCases
     
    /**
     * <p></p>
     */
    public static Object reportStatus
     
    /**
     * <p></p>
     */
    public static Object backCounter
     
    /**
     * <p></p>
     */
    public static Object currentTestCaseId
     
    /**
     * <p></p>
     */
    public static Object getBalance
     
    /**
     * <p></p>
     */
    public static Object newBalance
     

    static {
        try {
            def selectedVariables = TestCaseMain.getGlobalVariables("default")
			selectedVariables += TestCaseMain.getGlobalVariables(RunConfiguration.getExecutionProfile())
            selectedVariables += TestCaseMain.getParsedValues(RunConfiguration.getOverridingParameters())
    
            identifierApp = selectedVariables['identifierApp']
            excelFile = selectedVariables['excelFile']
            username = selectedVariables['username']
            password = selectedVariables['password']
            dbUrl = selectedVariables['dbUrl']
            dbName = selectedVariables['dbName']
            dbPort = selectedVariables['dbPort']
            dbUsername = selectedVariables['dbUsername']
            dbPassword = selectedVariables['dbPassword']
            timeoutShort = selectedVariables['timeoutShort']
            timeoutMiddle = selectedVariables['timeoutMiddle']
            nominalPulsa = selectedVariables['nominalPulsa']
            nominalTransfer = selectedVariables['nominalTransfer']
            destinationAccount = selectedVariables['destinationAccount']
            destinationBank = selectedVariables['destinationBank']
            destinationPhone = selectedVariables['destinationPhone']
            detailTransfer = selectedVariables['detailTransfer']
            destinationAccountBRI = selectedVariables['destinationAccountBRI']
            goingOn = selectedVariables['goingOn']
            typeGopay = selectedVariables['typeGopay']
            phoneNumber = selectedVariables['phoneNumber']
            accountNumber = selectedVariables['accountNumber']
            amount = selectedVariables['amount']
            provider = selectedVariables['provider']
            phoneCode = selectedVariables['phoneCode']
            userName = selectedVariables['userName']
            donationType = selectedVariables['donationType']
            getNomorTujuan = selectedVariables['getNomorTujuan']
            getNomorRekening = selectedVariables['getNomorRekening']
            getCreditNumber = selectedVariables['getCreditNumber']
            getSumberDana = selectedVariables['getSumberDana']
            akunDPLK = selectedVariables['akunDPLK']
            getPhoneCode = selectedVariables['getPhoneCode']
            getNumber = selectedVariables['getNumber']
            getNominal = selectedVariables['getNominal']
            getNoRekening = selectedVariables['getNoRekening']
            giveCases = selectedVariables['giveCases']
            reportStatus = selectedVariables['reportStatus']
            backCounter = selectedVariables['backCounter']
            currentTestCaseId = selectedVariables['currentTestCaseId']
            getBalance = selectedVariables['getBalance']
            newBalance = selectedVariables['newBalance']
            
        } catch (Exception e) {
            TestCaseMain.logGlobalVariableError(e)
        }
    }
}
