package common

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows

import internal.GlobalVariable

import java.util.Scanner;
import java.io.File;
import java.io.FileWriter
import java.io.IOException;
import io.appium.java_client.android.AndroidDriver
import io.appium.java_client.android.nativekey.AndroidKey
import io.appium.java_client.android.nativekey.KeyEvent


public class other {

	@Keyword
	def verifyProvider(String phoneCode) {
		switch (phoneCode) {
			case "0812": case "0822":
				GlobalVariable.provider = "simPATI"
				break;
			case "0823": case"0852":
				GlobalVariable.provider = "As"
			case "0814": case "0815": case "0816": case "0855": case "0856": case "0857": case "0858":
				GlobalVariable.provider = "Indosat"
				break;
			case "0817": case "0818": case "0819": case "0859": case "0877": case "0878":
				GlobalVariable.provider = "XL"
				break;
			case "0838": case "0831": case "0832": case "0833":
				GlobalVariable.provider = "AXIS"
			default :
				GlobalVariable.provider = "Belum Terdaftar"
		}
	}

	@Keyword
	def brizziAmount(int amount) {
		switch (amount) {
			case 20 : Mobile.tap(findTestObject('Object Repository/Brizzi/Besaran Nominal/android.widget.TextView - Rp20.000'),0);
				break
			case 50 : Mobile.tap(findTestObject('Object Repository/Brizzi/Besaran Nominal/android.widget.TextView - Rp50.000'),0);
				break
			case 100 : Mobile.tap(findTestObject('Object Repository/Brizzi/Besaran Nominal/android.widget.TextView - Rp100.000'),0);
				break
			case 150 : Mobile.tap(findTestObject('Object Repository/Brizzi/Besaran Nominal/android.widget.TextView - Rp150.000'),0);
				break
			case 200 : Mobile.tap(findTestObject('Object Repository/Brizzi/Besaran Nominal/android.widget.TextView - Rp200.000'),0);
				break
			case 300 : Mobile.tap(findTestObject('Object Repository/Brizzi/Besaran Nominal/android.widget.TextView - Rp300.000'),0);
				break
			case 400 : Mobile.tap(findTestObject('Object Repository/Brizzi/Besaran Nominal/android.widget.TextView - Rp400.000'),0);
				break
			case 500 : Mobile.tap(findTestObject('Object Repository/Brizzi/Besaran Nominal/android.widget.TextView - Rp500.000'),0);
				break
			case 1000 : Mobile.tap(findTestObject('Object Repository/Brizzi/Besaran Nominal/android.widget.TextView - Rp1.000.000'),0);
				break
		}
	}

	@Keyword
	def fileWrite(String fitur, String time, String refnum, String nominal, String fee) {
		Scanner sc = new Scanner(new File("customer.csv"))
		String strNumber
		while(sc.hasNext()) {
			String filePerLine = sc.nextLine()
			String[] data = filePerLine.split(",")
			strNumber = data[0].trim ();
		}
		int number = 1
		FileWriter writer = new FileWriter("customer.csv",true);
		if (strNumber != null) {
			number = Integer.parseInt(strNumber)
			number = number + 1
		}
		writer.write(number + "," +fitur +","+GlobalVariable.getNoRekening+","+time+","+refnum+","+nominal+","+fee+","+"OK"+","+GlobalVariable.currentTestCaseId);
		writer.write("\n")
		sc.close()
		writer.close();
	}

	@Keyword
	def takeScreenShot() {
		Date today = new Date()
		String todaysDate = today.format('MM_dd_yy')
		String nowTime = today.format('hh_mm_ss')
		Mobile.delay(1)
		Mobile.takeScreenshotAsCheckpoint("/Users/LENOVO/Downloads/Katalon Picture/screenshot_"+todaysDate+"_"+nowTime+".png", FailureHandling.STOP_ON_FAILURE)
	}

	@Keyword
	def takeScreenShotMac() {
		Date today = new Date()
		String todaysDate = today.format('MM_dd_yy')
		String nowTime = today.format('hh_mm_ss')
		Mobile.delay(1)
		Mobile.takeScreenshotAsCheckpoint("/Users/indranofiyan/Documents/Katalon Picture/screenshot_"+todaysDate+"_"+nowTime+".png", FailureHandling.STOP_ON_FAILURE)
	}

	@Keyword
	def donasiAmount(String amount) {
		switch (amount) {
			case "10000" : Mobile.tap(findTestObject('Object Repository/Donasi/Nominal Donasi/android.widget.TextView - Rp10.000'),0)
				break
			case "20000" : Mobile.tap(findTestObject('Object Repository/Donasi/Nominal Donasi/android.widget.TextView - Rp20.000'),0)
				break
			case "50000" : Mobile.tap(findTestObject('Object Repository/Donasi/Nominal Donasi/android.widget.TextView - Rp50.000'),0)
				break
			case "70000" : Mobile.tap(findTestObject('Object Repository/Donasi/Nominal Donasi/android.widget.TextView - Rp70.000'),0)
				break
			case "100000" : Mobile.tap(findTestObject('Object Repository/Donasi/Nominal Donasi/android.widget.TextView - Rp100.000'),0)
				break
			case "150000" : Mobile.tap(findTestObject('Object Repository/Donasi/Nominal Donasi/android.widget.TextView - Rp150.000'),0)
				break
			case "200000" : Mobile.tap(findTestObject('Object Repository/Donasi/Nominal Donasi/android.widget.TextView - Rp200.000'),0)
				break
			case "300000" : Mobile.tap(findTestObject('Object Repository/Donasi/Nominal Donasi/android.widget.TextView - Rp300.000'),0)
				break
			case "500000" : Mobile.tap(findTestObject('Object Repository/Donasi/Nominal Donasi/android.widget.TextView - Rp500.000'),0)
				break
			case "1000000" : Mobile.tap(findTestObject('Object Repository/Donasi/Nominal Donasi/android.widget.TextView - Rp1.000.000'),0)
				break
			case "manual" : Mobile.tap(findTestObject('Object Repository/Donasi/Nominal Donasi/android.widget.TextView - Masukkan Nominal Sendiri'),0)
				break
			default :  throw new Exception("Nominal Tidak Terdefinisi")
				break
		}
	}

	@Keyword
	def verifyAvailiablePulsa(String provider) {
		switch (provider.toLowerCase()) {
			case "xl" :
				Mobile.verifyElementExist(findTestObject('Object Repository/Pulsa/XL/android.widget.TextView - Rp25.000'),0)
				Mobile.verifyElementExist(findTestObject('Object Repository/Pulsa/XL/android.widget.TextView - Rp50.000'),0)
				Mobile.verifyElementExist(findTestObject('Object Repository/Pulsa/XL/android.widget.TextView - Rp100.000'),0)
				Mobile.verifyElementExist(findTestObject('Object Repository/Pulsa/XL/android.widget.TextView - Rp200.000'),0)
				break
			case "simpati" :
				Mobile.verifyElementExist(findTestObject('Object Repository/Pulsa/Simpati/android.widget.TextView - Rp15.000'),0)
				Mobile.verifyElementExist(findTestObject('Object Repository/Pulsa/Simpati/android.widget.TextView - Rp20.000'),0)
				Mobile.verifyElementExist(findTestObject('Object Repository/Pulsa/Simpati/android.widget.TextView - Rp25.000'),0)
				Mobile.verifyElementExist(findTestObject('Object Repository/Pulsa/Simpati/android.widget.TextView - Rp30.000'),0)
				Mobile.verifyElementExist(findTestObject('Object Repository/Pulsa/Simpati/android.widget.TextView - Rp40.000'),0)
				Mobile.verifyElementExist(findTestObject('Object Repository/Pulsa/Simpati/android.widget.TextView - Rp50.000'),0)
				Mobile.verifyElementExist(findTestObject('Object Repository/Pulsa/Simpati/android.widget.TextView - Rp75.000'),0)
				Mobile.verifyElementExist(findTestObject('Object Repository/Pulsa/Simpati/android.widget.TextView - Rp100.000'),0)
				Mobile.verifyElementExist(findTestObject('Object Repository/Pulsa/Simpati/android.widget.TextView - Rp150.000'),0)
				Mobile.verifyElementExist(findTestObject('Object Repository/Pulsa/Simpati/android.widget.TextView - Rp200.000'),0)
				Mobile.verifyElementExist(findTestObject('Object Repository/Pulsa/Simpati/android.widget.TextView - Rp300.000'),0)
				Mobile.verifyElementExist(findTestObject('Object Repository/Pulsa/Simpati/android.widget.TextView - Rp500.000'),0)
				Mobile.verifyElementExist(findTestObject('Object Repository/Pulsa/Simpati/android.widget.TextView - Rp1.000.000'),0)
				break
			case "indosat" :
				Mobile.verifyElementExist(findTestObject('Object Repository/Pulsa/Indosat/android.widget.TextView - Rp25.000'),0)
				Mobile.verifyElementExist(findTestObject('Object Repository/Pulsa/Indosat/android.widget.TextView - Rp50.000'),0)
				Mobile.verifyElementExist(findTestObject('Object Repository/Pulsa/Indosat/android.widget.TextView - Rp100.000'),0)
				Mobile.verifyElementExist(findTestObject('Object Repository/Pulsa/Indosat/android.widget.TextView - Rp150.000'),0)
				break
			default :  throw new Exception("provider belum tersedia")
				break
		}
	}

	@Keyword
	def nominalCatatanKeuangan(String amount) {
		AndroidDriver<?> driver = ((MobileDriverFactory.getDriver()) as AndroidDriver<?>)
		for (int i=1; i<=nominal.length(); i++) {
			String keypad = nominal.substring(i-1,i)
			switch (keypad.toString()) {
				case '0' :
					driver.pressKey(new KeyEvent(AndroidKey.NUMPAD_0))
					break
				case '1' :
					driver.pressKey(new KeyEvent(AndroidKey.NUMPAD_1))
					break
				case '2' :
					driver.pressKey(new KeyEvent(AndroidKey.NUMPAD_2))
					break
				case '3' :
					driver.pressKey(new KeyEvent(AndroidKey.NUMPAD_3))
					break
				case '4' :
					driver.pressKey(new KeyEvent(AndroidKey.NUMPAD_4))
					break
				case '5' :
					driver.pressKey(new KeyEvent(AndroidKey.NUMPAD_5))
					break
				case '6' :
					driver.pressKey(new KeyEvent(AndroidKey.NUMPAD_6))
					break
				case '7' :
					driver.pressKey(new KeyEvent(AndroidKey.NUMPAD_7))
					break
				case '8' :
					driver.pressKey(new KeyEvent(AndroidKey.NUMPAD_8))
					break
				case '9' :
					driver.pressKey(new KeyEvent(AndroidKey.NUMPAD_9))
					break
			}
		}
	}

	@Keyword
	def selectedAmountPulsa(String amount) {
		if (GlobalVariable.provider.toLowerCase() == "xl") {
			switch (amount) {
				case "25" :
					Mobile.tap(findTestObject('Object Repository/Pulsa/XL/android.widget.TextView - Rp25.000'),0)
					break
				case "50" :
					Mobile.tap(findTestObject('Object Repository/Pulsa/XL/android.widget.TextView - Rp50.000'),0)
					break
				case "100" :
					Mobile.tap(findTestObject('Object Repository/Pulsa/XL/android.widget.TextView - Rp100.000'),0)
					break
				case "200" :
					Mobile.tap(findTestObject('Object Repository/Pulsa/XL/android.widget.TextView - Rp200.000'),0)
				default : throw new Exception("Nominal tidak tersedia")
					break
			}
		}
		else if (GlobalVariable.provider.toLowerCase() == "indosat") {
			switch (amount) {
				case "25" :
					Mobile.tap(findTestObject('Object Repository/Pulsa/Indosat/android.widget.TextView - Rp25.000'),0)
					break
				case "50" :
					Mobile.tap(findTestObject('Object Repository/Pulsa/Indosat/android.widget.TextView - Rp50.000'),0)
					break
				case "100" :
					Mobile.tap(findTestObject('Object Repository/Pulsa/Indosat/android.widget.TextView - Rp100.000'),0)
					break
				case "150" :
					Mobile.tap(findTestObject('Object Repository/Pulsa/Indosat/android.widget.TextView - Rp150.000'),0)
				default : throw new Exception("Nominal tidak tersedia")
					break
			}
		}
		else if (GlobalVariable.provider.toLowerCase() == "simpati") {
			switch (amount) {
				case "15" :
					Mobile.tap(findTestObject('Object Repository/Pulsa/Simpati/android.widget.TextView - Rp15.000'),0)
					break
				case "20" :
					Mobile.tap(findTestObject('Object Repository/Pulsa/Simpati/android.widget.TextView - Rp20.000'),0)
					break
				case "25" :
					Mobile.tap(findTestObject('Object Repository/Pulsa/Simpati/android.widget.TextView - Rp25.000'),0)
					break
				case "30" :
					Mobile.tap(findTestObject('Object Repository/Pulsa/Simpati/android.widget.TextView - Rp30.000'),0)
					break
				case "40" :
					Mobile.tap(findTestObject('Object Repository/Pulsa/Simpati/android.widget.TextView - Rp40.000'),0)
					break
				case "50" :
					Mobile.tap(findTestObject('Object Repository/Pulsa/Simpati/android.widget.TextView - Rp50.000'),0)
					break
				case "75" :
					Mobile.tap(findTestObject('Object Repository/Pulsa/Simpati/android.widget.TextView - Rp75.000'),0)
					break
				case "100" :
					Mobile.tap(findTestObject('Object Repository/Pulsa/Simpati/android.widget.TextView - Rp100.000'),0)
					break
				case "150" :
					Mobile.tap(findTestObject('Object Repository/Pulsa/Simpati/android.widget.TextView - Rp150.000'),0)
					break
				case "200" :
					Mobile.tap(findTestObject('Object Repository/Pulsa/Simpati/android.widget.TextView - Rp200.000'),0)
					break
				case "300" :
					Mobile.tap(findTestObject('Object Repository/Pulsa/Simpati/android.widget.TextView - Rp300.000'),0)
					break
				case "500" :
					Mobile.tap(findTestObject('Object Repository/Pulsa/Simpati/android.widget.TextView - Rp500.000'),0)
					break
				case "1000" :
					Mobile.tap(findTestObject('Object Repository/Pulsa/Simpati/android.widget.TextView - Rp1.000.000'),0)
					break
				default : throw new Exception("Nominal tidak tersedia")
					break
			}
		}
		else {
			throw new Exception("Provider Belum Didaftarakan")
		}
	}
}
