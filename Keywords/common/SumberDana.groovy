package common

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows


import internal.GlobalVariable


public class SumberDana {
	@Keyword
	def Sumber_Dana(String sumberdana) {
		Mobile.tap(findTestObject('Object Repository/Transfer2/Transfer 2-1/TAP - SUMBER DANA'), 0)
		Mobile.delay(3)
		//		deviceHeight = Mobile.getDeviceHeight()
		//		deviceWidth = Mobile.getDeviceWidth()
		//		int startX = device_Width / 2
		//		int endX = startX
		//		int startY = device_Height * 0.30
		//		int endY = device_Height * 0.70
		String pilihRekening
		switch(sumberdana) {
			case "Tabungan_Utama" :
			//Mobile.swipe(startX, endY, endX, startY)
				pilihRekening = Mobile.getText(findTestObject('Object Repository/Sumber Dana/android.widget.TextView - Tabungan Utama  0206 0100 0051 308'), 0)
				Mobile.tap(findTestObject('Object Repository/Transfer2/Transfer 2-1/android.widget.TextView - 0206 0100 0051 308 (1)'), 0)
				break
			case "Giro_saldo_cukup" :
			//Mobile.swipe(startX, endY, endX, startY)
				pilihRekening = Mobile.getText(findTestObject('Object Repository/Transfer2/Transfer 2-1/GIRO SALDO CUKUP - 0019 0100 1364 305'), 0)
				Mobile.tap(findTestObject('Object Repository/Transfer2/Transfer 2-1/GIRO SALDO CUKUP - 0019 0100 1364 305'), 0)
				break
			case "Deposito" :
			//Mobile.swipe(startX, endY, endX, startY)
				pilihRekening = Mobile.getText(findTestObject('Object Repository/Sumber Dana/android.widget.TextView - Deposito'), 0)
				Mobile.tap(findTestObject('Object Repository/Sumber Dana/android.widget.TextView - 0206 0100 0074 445'), 0)
				break
			case "Freeze" :
				Mobile.swipe(startX, endY, endX, startY)
				pilihRekening = Mobile.getText(findTestObject('Object Repository/Sumber Dana/android.widget.TextView - Freeze'), 0)
				Mobile.tap(findTestObject('Object Repository/Sumber Dana/android.widget.TextView - 0019 0101 5545 503'), 0)
				break
			case "Dormant" :
				Mobile.swipe(startX, endY, endX, startY)
				pilihRekening = Mobile.getText(findTestObject('Object Repository/Sumber Dana/android.widget.TextView - Dormant'), 0)
				Mobile.tap(findTestObject('Object Repository/Sumber Dana/android.widget.TextView - 0206 0100 0002 529'), 0)
				break
			case "Giro_saldo_tidak_cukup" :
				Mobile.swipe(startX, endY, endX, startY)
				pilihRekening = Mobile.getText(findTestObject('Object Repository/Sumber Dana/android.widget.TextView - GIRO SALDO TIDAK CUKUP'), 0)
				Mobile.tap(findTestObject('Object Repository/Sumber Dana/android.widget.TextView - 0206 0100 0260 313'), 0)
				break
			case "Britama_X" :
				Mobile.swipe(startX, endY, endX, startY)
				pilihRekening = Mobile.getText(findTestObject('Object Repository/Transfer2/Transfer 2-1/BRITAMA X - 0206 0112 2201 504'), 0)
				Mobile.tap(findTestObject('Object Repository/Transfer2/Transfer 2-1/BRITAMA X - 0206 0112 2201 504'), 0)
				break
			case "Closed" :
				Mobile.swipe(startX, endY, endX, startY)
				pilihRekening = Mobile.getText(findTestObject('Object Repository/Sumber Dana/android.widget.TextView - Closed'), 0)
				Mobile.tap(findTestObject('Object Repository/Sumber Dana/android.widget.TextView - Closed'), 0)
				break
			case "Saving" :
			//Mobile.swipe(startX, endY, endX, startY)
				pilihRekening = Mobile.getText(findTestObject('Object Repository/Sumber Dana/SAVING - 1194 0100 7867 504'), 0)
				Mobile.tap(findTestObject('Object Repository/Sumber Dana/SAVING - 1194 0100 7867 504'), 0)
				break
				break
			default:
				break
		}



		//if(sumberdana=="Tabungan_Utama" || sumberdana=="Brigita_Giro" || sumberdana=="Britama_X") {
		//		Mobile.tap(''), 0)
		//
		//}
		if(sumberdana=="Closed" || sumberdana=="Freeze" ||sumberdana=="Dormant"||sumberdana=="Deposito") {
			Mobile.comment(pilihRekening)
		}
		else if(sumberdana=="Giro_saldo_tidak_cukup") {
			Mobile.verifyElementVisible(findTestObject('Object Repository/Test Transfer/Verify Nominal - Saldo Anda tidak cukup'), 0)
			Mobile.verifyElementVisible(findTestObject('Object Repository/Transfer2/Transfer 2-1/Button Disable- Transfer'), 0)
			Mobile.comment(pilihRekening)
		}

		//else {
		//	throw new Exception ("Jenis rekening tidak ditemukan")
		//}

		GlobalVariable.getSumberDana = pilihRekening
		Mobile.comment(GlobalVariable.getSumberDana)

	}}
